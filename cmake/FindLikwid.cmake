# looks for likwid
include(CheckFortranSourceCompiles)

  find_library(LIKWID_LIB
               NAMES likwid
               HINTS ${LIKWID_ROOT}/lib
               PATHS ${CMAKE_PREFIX_PATH} ${PKG_LIKWID_LIBRARY_DIRS}
                     ${LIB_INSTALL_DIR} ENV LIKWID_LIBRARY_DIRS)

  find_path(LIKWID_INCLUDES
            NAMES likwid.h
            HINTS ${LIKWID_ROOT}/include
            PATHS ${CMAKE_PREFIX_PATH} ${PKG_LIKWID_INCLUDE_DIRS}
                  ${INCLUDE_INSTALL_DIR} ENV LIKWID_INCLUDE_DIRS)

set(LIKWID_LIBRARIES ${LIKWID_LIB})
set(LIKWID_INCLUDE_DIRS ${LIKWID_INCLUDES})

set(CMAKE_REQUIRED_INCLUDES ${LIKWID_INCLUDES})
set(CMAKE_REQUIRED_LIBRARIES ${LIKWID_LIBRARIES})
check_Fortran_source_compiles("program test_likwid\nuse likwid\nend program" LIKWID_WORKS SRC_EXT ".F90")
if(LIKWID_WORKS)
	message(STATUS "Likwid found and working")
	if(CMAKE_BUILD_TYPE MATCHES Likwid)
		message(STATUS "Will build with likwid support")
	   add_definitions(-DUSE_LIKWID)
	endif()
else(LIKWID_WORKS)
	message(STATUS "Likwid not found or not working")
endif(LIKWID_WORKS)

mark_as_advanced(LIKWID_INCLUDE_DIRS LIKWID_LIBRARIES)

