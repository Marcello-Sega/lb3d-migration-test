find_package(MPI COMPONENTS Fortran C REQUIRED)
find_package(Slurm QUIET)

if(NOT MPI_Fortran_HAVE_F08_MODULE)
   message(FATAL_ERROR "The mpi_f08 module is strictly necessary")
endif()

find_package(ZLIB)
find_package(Likwid)
if(NSKG_ELEC)
  find_package(FFTW)
endif(NSKG_ELEC)

set(HDF5_PREFER_PARALLEL ON)
set(HDF5_USE_STATIC_LIBRARIES ON)

if(NSKG_INSTALL_HDF5)
   message(STATUS "HDF5: Setting up HDF5 to be build locally")
   set(HDF5_EXTERNALLY_CONFIGURED 1)

   include(ExternalProject)

   ExternalProject_Add(local_hdf5
      URL  https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.10/hdf5-1.10.6/src/hdf5-1.10.6.tar.gz
      URL_HASH  MD5=37f3089e7487daf0890baf3d3328e54a
      CMAKE_ARGS -DHDF5_BUILD_FORTRAN=ON -DBUILD_SHARED_LIBS=OFF
            -DHDF5_BUILD_CPP_LIB=OFF -DHDF5_ENABLE_PARALLEL=ON
            -DHDF5_BUILD_HL_LIB=OFF -HDF5_ENABLE_Z_LIB_SUPPORT=ON
            -DHDF5_BUILD_EXAMPLES=OFF -HDF5_BUILD_TOOLS=OFF
            -DHDF5_ENABLE_Z_LIB_SUPPORT=ON
            -DBUILD_TESTING=OFF -DHDF5_DISABLE_COMPILER_WARNINGS=ON
            -DMPI_Fortran_COMPILER=${MPI_Fortran_COMPILER}
            -DCMAKE_Fortran_COMPILER=${MPI_Fortran_COMPILER}
            -DCMAKE_C_COMPILER=${MPI_C_COMPILER}
            -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}
      PATCH_COMMAND sed -i "s/project (HDF5.*/project (HDF5 C Fortran)/" CMakeLists.txt
   )

   set(HDF5_ROOT "${CMAKE_BINARY_DIR}/" CACHE STRING "" FORCE)
   set(CMAKE_PREFIX_PATH "${HDF5_ROOT};${CMAKE_PREFIX_PATH}/" CACHE STRING "" FORCE)

   #these are the relevant variables find_package would set
   set(HDF5_INCLUDE_DIRS ${CMAKE_BINARY_DIR}/include/static/)
   set(HDF5_Fortran_LIBRARIES
               ${CMAKE_BINARY_DIR}/lib/libhdf5_tools.a
               ${CMAKE_BINARY_DIR}/lib/libhdf5_fortran.a
               ${CMAKE_BINARY_DIR}/lib/libhdf5_f90cstub.a
               ${CMAKE_BINARY_DIR}/lib/libhdf5.a
               ${ZLIB_LIBRARIES}
   )
elseif(HDF5_PATH_BY_HAND)
   # We need another logical branch here: We don't want to build HDF5, but at the same time we also know where our HDF5 lies (= we don't need to search for ${HDF5_ROOT}).
   message(STATUS "HDF5: Using pre-existing HDF5 library from ${HDF5_ROOT} without searching.")
else()
   find_package(HDF5 COMPONENTS C Fortran REQUIRED)
   if(NOT HDF5_IS_PARALLEL)
     message(FATAL_ERROR
       "HDF5: HDF5 is not parallel!\n"
       "Specify path to parallel HDF5 library by setting environment\n"
       "variable HDF5_ROOT, or run cmake with -DNSKG_INSTALL_HDF5=ON\n"
       "to download&compile your own local copy of parallel HDF5.\n\n")
   endif()
endif()


if(NOT CMAKE_VERSION VERSION_LESS 3.11)
    find_package (Python COMPONENTS Interpreter REQUIRED)
else()
    set(Python_ADDITIONAL_VERSIONS 3)
    find_package (PythonInterp REQUIRED)

    set(Python_EXECUTABLE ${PYTHON_EXECUTABLE})
    set(Python_VERSION ${PYTHON_VERSION_STRING})

    set(Python_FOUND TRUE) 
endif()

if(Python_VERSION VERSION_LESS 3)
    message(FATAL_ERROR "Only Python 3 is supported. Found version ${Python_VERSION} instead. (If you have python3 in a non-standard location, provide the executable with -DPYTHON_EXECUTABLE=<full-path>)")
endif()

if(CMAKE_Fortran_COMPILER_ID MATCHES GNU)
   if(NSKG_USE_BLAS)
     find_package(BLAS)
     if(BLAS_FOUND)
       message(STATUS "BLAS found: ${BLAS_LIBRARIES}")
       add_definitions(-DBLAS)
     endif(BLAS_FOUND)
   endif(NSKG_USE_BLAS)
endif()
