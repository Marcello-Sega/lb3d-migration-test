message(STATUS "C compiler: ${CMAKE_C_COMPILER}")
message(STATUS "Fortran compiler: ${CMAKE_Fortran_COMPILER}")

if(CMAKE_C_COMPILER_ID MATCHES "GNU|AppleClang")
    string(APPEND CMAKE_C_FLAGS " -fPIC -Ofast -march=native -ftree-vectorize -g")
endif()

if(APPLE AND UNIX)
  add_definitions(-DNSKG_OSX)
endif (APPLE AND UNIX)

AutodetectHostArchitecture()
message(STATUS "Target Architecture: ${TARGET_ARCHITECTURE}")
message(STATUS "CPU Family         : ${_cpu_family}")
message(STATUS "CPU Model          : ${_cpu_model}")
message(STATUS "COMPILER VERSION   : ${CMAKE_Fortran_COMPILER_VERSION}")
message(STATUS "COMPILER ID        : ${CMAKE_Fortran_COMPILER_ID}")

#User defined appended flags
string(APPEND CMAKE_Fortran_FLAGS         "${APPEND_FFLAGS}" )
string(APPEND CMAKE_Fortran_FLAGS_PROFILE "${APPEND_FFLAGS}" )
string(APPEND CMAKE_Fortran_FLAGS_TRACE   "${APPEND_FFLAGS}" )

if(CMAKE_Fortran_COMPILER_ID MATCHES GNU)


    # see the discussion around https://github.com/scipy/scipy/issues/11611
    # for the flag  -fallow-argument-mismatch (pops out when calling xdrfvector functions)

    if(NOT NSKG_SINGLE_PRECISION)
        string(APPEND CMAKE_Fortran_FLAGS " -fdefault-real-8 -fdefault-double-8")
    endif()

    if(BLAS_FOUND)
        string(APPEND CMAKE_Fortran_FLAGS " -fexternal-blas")
        string(APPEND CMAKE_Fortran_FLAGS " -fblas-matmul-limit=1024")
    endif(BLAS_FOUND)

    string(APPEND CMAKE_Fortran_FLAGS " -ffree-line-length-none")
    string(APPEND CMAKE_Fortran_FLAGS " -g -fbacktrace -fcheck=all -ftree-loop-distribution -fopenmp-simd")
    string(APPEND CMAKE_Fortran_FLAGS " -fvect-cost-model=unlimited")
    ####string(APPEND CMAKE_Fortran_FLAGS " -ffpe-trap=zero")   # TODO Make code comply so that we can add back the "invalid,zero,overflow" into this safeguard
    string(APPEND CMAKE_Fortran_FLAGS " -march=native -mtune=native -Ofast ")

    string(APPEND CMAKE_Fortran_FLAGS_DEBUG " -Og ")
    #COVERAGE
    string(APPEND CMAKE_Fortran_FLAGS_COVERAGE " --coverage")
    string(APPEND CMAKE_EXE_LINKER_FLAGS_COVERAGE " --coverage")
    string(APPEND CMAKE_C_FLAGS_COVERAGE " --coverage")
    # Every exception to Werror must  be justified, or the purpose of 
    # blocking compilation on warning is lost.

    # -Wall -Wextra: we start by including all possible warnings. Don't remove!
    string(APPEND CMAKE_Fortran_FLAGS " -Wall -Wextra ")

    # -Wno-character-truncation to allow assigning strings in class constructors
    #  which are shorter than the strings themselves. This is well-behaved in gfortran
    #  as the remaining characters are whitespaces. This is not necessarily true, but
    #  doing it properly is probably too much hassle. TODO fix this at some stage.
    string(APPEND CMAKE_Fortran_FLAGS " -Wno-character-truncation ")


    # -Wno-function-elimination is to allow using impure function in logical evaluations
    #  This might end up in an underfined behavior. The way out would be to assign first
    #  a temporary logical variable, then set it using the function, then finally use the
    #  temporary variable within the logical evaluation. A bit cumbersome, so we leave it out
    #  for the time being.
    string(APPEND CMAKE_Fortran_FLAGS " -Wno-function-elimination ")


    # -Wno-c-binding-type avoid what seems to be a bug in gfortran that complains about 
    # an mpi-related variable at the beginning of submodules. I never found out what the real
    # issue was. TODO MWE to be submitted to gcc developers.
    string(APPEND CMAKE_Fortran_FLAGS " -Wno-c-binding-type ") 


    # -Wno-do-subscript to avoid the compiler (wrongly, I asssume, unless branch prediction
    # is the problem in triggering "Array reference at (1) out of bounds (0 < 1) in loop" for
    # cases like this (array lower bound being 1)
    # do x=1,N ; if(x > 1) then ; n_x = array(x - 1) ; else ; n_x = 0 ; end if
    string(APPEND CMAKE_Fortran_FLAGS " -Wno-do-subscript ") 
      
    # We avoid checking C at the moment because of XDRF many issues. To be fixed or reintroduced once 
    # XDRF is removed.
    #    string(APPEND CMAKE_C_FLAGS       "-Wall  -Wextra -Werror")

    #TRACING
    string(APPEND CMAKE_C_FLAGS_TRACE " -p -pg -finstrument-functions")
    string(APPEND CMAKE_Fortran_FLAGS_TRACE " -p -pg -finstrument-functions")
    string(APPEND CMAKE_EXE_LINKER_FLAGS_TRACE " -p -pg -finstrument-functions")

    #PROFILING
    string(APPEND CMAKE_Fortran_FLAGS_PROFILE " -pg -O0")
    string(APPEND CMAKE_Fortran_FLAGS_PROFILE " -Wno-maybe-uninitialized")


elseif(CMAKE_Fortran_COMPILER_ID MATCHES Intel)
    string(APPEND CMAKE_Fortran_FLAGS " -m64 -g") # -i8 is equivalent to gfortran -fdefault-integer-8
    if(TARGET_ARCHITECTURE STREQUAL "skylake-avx512") #  -Xhost seems not to switch on the same features as -xCORE-AVX512 on skylake-avx512. We do it by hand
         string(APPEND CMAKE_Fortran_FLAGS " -xCORE-AVX512 -mtune=skylake-avx512")
    else()
         string(APPEND CMAKE_Fortran_FLAGS " -xHost")
    endif()
    string(APPEND CMAKE_Fortran_FLAGS " -fp-model=fast=2 -Ofast  -qopt-zmm-usage=high -vec-threshold0")
    if(CMAKE_Fortran_COMPILER_ID MATCHES IntelLLVM)
         # ifx ("IntelLLVM") does not know about the -qopt-prefetch flag any more.
    else()
         string(APPEND CMAKE_Fortran_FLAGS " -qopt-prefetch=5")
    endif()
    if(NOT NSKG_SINGLE_PRECISION)
        string(APPEND CMAKE_Fortran_FLAGS " -real-size 64 -double-size 64")
    endif()
    string(APPEND CMAKE_Fortran_FLAGS " -diag-disable 5425" ) # warning produced by h5pfc flag
    #string(APPEND CMAKE_Fortran_FLAGS " -heap-arrays 32")

    #enable the following to get vectorization reports from the compiler
    string(APPEND CMAKE_Fortran_FLAGS_DEBUG " -qopt-report=1 -qopt-report-phase=vec")

    string(APPEND CMAKE_Fortran_FLAGS_DEBUG " -traceback -check=all -g -fpe0" )
    string(APPEND CMAKE_Fortran_FLAGS_DEBUG " -debug=all -error_limit=1" )

    if(CMAKE_BUILD_TYPE MATCHES Coverage)
        message(FATAL_ERROR "Coverage build type not yet implemented for Intel compilers")
    endif()

elseif(CMAKE_Fortran_COMPILER_ID MATCHES Cray)
    string(APPEND CMAKE_Fortran_FLAGS "-Wl,--no-relax -fPIC  -dynamic -O 3 -h fp4")
endif()

#User defined appended flags
string(APPEND CMAKE_Fortran_FLAGS         "${APPEND_FFLAGS}" )
string(APPEND CMAKE_Fortran_FLAGS_PROFILE "${APPEND_FFLAGS}" )
string(APPEND CMAKE_Fortran_FLAGS_TRACE   "${APPEND_FFLAGS}" )
