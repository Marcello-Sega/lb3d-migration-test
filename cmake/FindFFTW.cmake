# https://github.com/wjakob/layerlab/blob/master/CMakeLists.txt SEE LICENCE
# (BSD?) or replace ... - Find the FFTW library
#
# Usage: find_package(FFTW [REQUIRED] [QUIET] )
#
# It sets the following variables: FFTW_FOUND               ... true if fftw is
# found on the system FFTW_LIBRARIES           ... full path to fftw library
# FFTW_INCLUDE_DIRS        ... fftw include directory

# If environment variable FFTWDIR is specified, it has same effect as FFTW_ROOT
if(NOT FFTW_ROOT AND ENV{FFTWDIR})
  set(FFTW_ROOT $ENV{FFTWDIR})
endif()

# Check if we can use PkgConfig
find_package(PkgConfig)

# Determine from PKG
if(PKG_CONFIG_FOUND AND NOT FFTW_ROOT)
  pkg_check_modules(PKG_FFTW QUIET "fftw")
  pkg_check_modules(PKG_FFTW QUIET "fftw")
endif()

# Check whether to search static or dynamic libs
set(CMAKE_FIND_LIBRARY_SUFFIXES_SAV ${CMAKE_FIND_LIBRARY_SUFFIXES})

if(${FFTW_USE_SHARED_LIBS})
  set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_SHARED_LIBRARY_SUFFIX})
else()
  set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_STATIC_LIBRARY_SUFFIX})
endif()

if(FFTW_ROOT)
  find_library(FFTW_LIB
               NAMES fftw dfftw fftw_threads dfftw_threads
               PATHS ${FFTW_ROOT}
               PATH_SUFFIXES "lib" "lib64"
               NO_DEFAULT_PATH)
  find_path(FFTW_INCLUDES
            NAMES fftw.h dfftw.h
            PATHS ${FFTW_ROOT}
            PATH_SUFFIXES "include"
            NO_DEFAULT_PATH)

else()

  find_library(FFTW_LIB
               NAMES dfftw dfftw_threads fftw fftw_threads
               PATHS ${CMAKE_PREFIX_PATH} ${PKG_FFTW_LIBRARY_DIRS}
                     ${LIB_INSTALL_DIR} ENV FFTW_LIBRARY_DIRS)

  find_path(FFTW_INCLUDES
            NAMES dfftw.h fftw.h
            PATHS ${CMAKE_PREFIX_PATH} ${PKG_FFTW_INCLUDE_DIRS}
                  ${INCLUDE_INSTALL_DIR} ENV FFTW_INCLUDE_DIRS)

endif(FFTW_ROOT)

set(FFTW_LIBRARIES ${FFTW_LIB})
set(FFTW_INCLUDE_DIRS ${FFTW_INCLUDES})

set(CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES_SAV})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(FFTW DEFAULT_MSG FFTW_INCLUDE_DIRS
                                  FFTW_LIBRARIES)
mark_as_advanced(FFTW_INCLUDE_DIRS FFTW_LIBRARIES FFTW_LIB)
