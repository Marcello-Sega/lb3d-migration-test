set(M4ARCH "LINUX" CACHE STRING "Force architecture choice for m4")

# Manual
set(NSKG_FORD_VERSION "6.2.5" CACHE STRING "Required ford version")
option(NSKG_MANUAL_SEARCH_INDEX "Enable the online search index for the manual" OFF)

set(Defines "")	# Will hold a list of NSKG_XYZ options to be available as -DXYZ cmake-time options, and `XYZ` defines in the sources
# External connectivity
option         (NSKG_INSTALL_HDF5 "Download and install HDF5 locally" OFF)
list(APPEND Defines INSTALL_HDF5)
option         (NSKG_USE_BLAS "Use BLAS (if found) instead of Fortran intrinsics" ON)
option         (NSKG_TESTS "Activate generation of the test suite" ON)
# Configuration of behaviour
option         (NSKG_SINGLE_PRECISION "Use single (rather than double) precision floats" OFF)
list(APPEND Defines SINGLE_PRECISION)
# Configuration of compile-time modules
option         (NSKG_ELEC "Enable ELEC (out of tree as of 2022)" OFF)	# final removal of sources in 3cfc0ecdc89fcc3a7600d668097abaf77c943e18
list(APPEND Defines ELEC)
# Quirks
# Testing & Debugging
option         (NSKG_DEBUG "Enable debug mode of some functions" OFF)	# libnskg/src/old/nskg_io.F90
list(APPEND Defines DEBUG)
option         (NSKG_DEBUG_LE "Enable some debug output for Lees-Edwards" OFF)	# libnskg/src/old/nskg_io.F90 libnskg/src/bc/lb/nskg_leesedwards.F90
list(APPEND Defines DEBUG_LE)
option         (NSKG_DEBUG_MPI "Enable some debug output for MPI internals" OFF)	# libnskg/src/old/nskg_io.F90 libnskg/include/nskg.h
list(APPEND Defines DEBUG_MPI)
option         (NSKG_DEBUG_REPORTMDCOMM "Lees-Edwards communication debugging" OFF)	# libnskg/src/old/nskg_io.F90 libnskg/src/bc/md/nskg_md_bc_leesedwards.F90
list(APPEND Defines DEBUG_REPORTMDCOMM)
option         (NSKG_ESMALL_ZERO "Set esmall to zero (for certain numerical stability tests)" OFF)
list(APPEND Defines ESMALL_ZERO)

set(NSKG_FLAGLIST "")	# List of compile configuration flags

foreach(Define ${Defines})
  if(${NSKG_${Define}})
    add_definitions(-D${Define})
    list(APPEND NSKG_FLAGLIST ${Define})
  endif()
endforeach()

string(REPLACE ";" " -D" NSKG_FLAGS "${NSKG_FLAGLIST}")

# vim: ts=85 nowrap
