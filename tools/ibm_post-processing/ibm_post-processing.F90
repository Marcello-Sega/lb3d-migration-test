#include "nskg.h"

program ibm_postprocessing
  !> Post-processing and analysis for mesh particles
  use nskg_lb_class
  use nskg_ibm_class
  use nskg_mesh_point_class, only:nskg_mesh_point
  use nskg_mesh_class, only:nskg_mesh,compute_geometry_and_inflate_forces,nskg_viscoelastic_coefficients
  use nskg_mesh_set_class, only:nskg_mesh_set,nskg_parms_mesh_set,read_vtk
  use nskg_lagrangian_object_class, only:nskg_lagrangian_object
  use nskg_halo_class, only: nskg_mpi
  use nskg_parms_module
  use nskg_geometry_class
  use nskg_globals_module
  use nskg_parallel_globals_module
  use nskg_io_hdf5_module
  use nskg_io_interface
  use nskg_system_class
  use nskg_linked_list_class
  use nskg_interactions_class
  use nskg_triangle_class, only: nskg_triangle
  use nskg_potential_coulomblike_class, only: nskg_potential_coulomblike, nskg_parms_potential_coulomblike 
  use nskg_features_class
  implicit none

  type :: analyse_parms
    character(len=FILENAME) :: name_input
      !> Name of the input file 
    character(len=FILENAME) :: name_vtk
      !> Name of the vtk file to be read
    integer                 :: nx,ny,nz
    logical                 :: print_vtk    
    integer                 :: meshnumbers(IBM_MAX_MESHES)
    character(len=FILENAME) :: meshfiles(IBM_MAX_MESHES)
  end type

  type(analyse_parms) :: parm

  type,extends(nskg_parms_mesh_set) :: nskg_parms_langevin_mesh_set
  end type        

  type,extends(nskg_mesh_set)       :: nskg_langevin_mesh_set
  end type        
  
  type(nskg_parms_langevin_mesh_set),target :: plm = nskg_parms_langevin_mesh_set(&
    shortname='LM',longname='Langevin Mesh'            ,&
    interpolation_range = 2                            ,&
    time_step_dump_vtk = 0                             ,&    
    halo = 1.0                                         ,&    
    number_random_lagrangian_object =0                 ,&    
    meshnumbers = 0                                    ,&
    meshfiles = ''                                     ,&
    geometry_file= ''                                  ,&
    fix_position= .false.                              ,&
    compute_inertia_ellipsoid = .false.                ,&
    time_step_dump_mesh_properties= 0                  ,&
    restore_file = ''                                  ,&
    steady_step         = 0                            ,&
    soft_warning        = .false.                       &
    )

  class(nskg_langevin_mesh_set), pointer :: LM
  integer                               :: nargs,i
  character(len=CLIARG)                 :: arg
  character(len=FILENAME)               :: tmp_name

  call features%init         
  nargs = command_argument_count()
  if(nargs.lt.1) then
    call get_command_argument(0, arg)
    if(myrankc.eq.0)write(*,*) 'Input file missing'
    call MPI_Finalize()
    stop
  else
    call get_command_argument(1, arg) ; read(arg,*)  parm%name_input
    call read_namelist(parm)
    plm%meshnumbers = parm%meshnumbers
    plm%meshfiles = parm%meshfiles
    plm%time_step_dump_mesh_properties = 1
    write(tmp_name,'(A)') parm%name_vtk(:len(trim(parm%name_vtk))-4)//'_post-proc'
    print*, trim(tmp_name)
  endif
  !
  !Init system  
  allocate(SYS)
  features%lattice_boltzmann%active = .true.
  SYST%box=[parm%nx,parm%ny,parm%nz]
  LB%ncomponents = 1
  call SYS%init(.false.,SYST)
  allocate(LM)
  call plm%bcast_parms

  write(plm%geometry_file,'(A)') 'geometry.dat'
  call LM%setup(plm,SYS%lattice%geometry,SYS%lattice%mpi,lb_lattice=SYS%lattice)
  print*, parm%name_vtk
  call read_vtk(LM,trim(parm%name_vtk))
  if(myrankc.eq.0)print*, 'Setup meshes: DONE. Total mesh numbers',size(LM%meshes)

  do i=1,size(LM%meshes)
    call LM%meshes(i)%p%compute_center_geometric()
    call antirebox(LM%meshes(i)%p)
  end do

  call LM%write_vtk(trim(tmp_name),rebox=.false.)
  if(myrankc.eq.0)print*,"Writing init vtk: DONE"
  call MPI_Finalize()

contains
      
  subroutine antirebox(this)
    implicit none
    class(nskg_mesh),target :: this
    real(kind=FP), dimension(3) :: box
    integer                     :: dir,i
    box = this%geometry%size
    select type(node=>this%vertices%elements%array)
    class is (nskg_mesh_point)
      do i=1,this%vertices%elements%size
        do dir = 1,3
          !if((node(i)%pos(dir).lt.(box(dir)/2)).and.(abs(node(i)%pos(dir)-this%center(dir)).gt.(box(dir)/2)))then
          if(abs(node(i)%pos(dir)-this%center(dir)).gt.(box(dir)/2))then
            if(node(i)%pos(dir)-this%center(dir).gt.0)then
              node(i)%pos(dir) = node(i)%pos(dir)-box(dir)
            else
              node(i)%pos(dir) = node(i)%pos(dir)+box(dir)
            endif
          endif
        end do
      end do
    end select
  end subroutine

    subroutine read_namelist(parm)
      type(analyse_parms),intent(inout) :: parm
      !internal
      character(len=FILENAME)  :: name_vtk
      integer              :: nx,ny,nz
      integer              :: meshnumbers(IBM_MAX_MESHES)
      logical              :: print_vtk
      character(len=FILENAME)  :: meshfiles(IBM_MAX_MESHES)
      !
      namelist /analyse_properties/nx, ny, nz, meshnumbers,meshfiles, name_vtk, print_vtk
      !init
      name_vtk    = 'output'
      nx          = 0
      ny          = 0 
      nz          = 0 
      meshnumbers = 0
      meshfiles   = 'sph_ico_5120.msh'
      print_vtk   = .false.
      !
      call nskg_open(unit=42, file=parm%name_input, status='old')
      read_catch_namelist_error_macro(42,analyse_properties,'analyse_properties')
      close(42)
      !
      parm%name_vtk        = name_vtk
      parm%nx              = nx
      parm%ny              = ny
      parm%nz              = nz
      parm%print_vtk       = print_vtk
      parm%meshnumbers     = meshnumbers
      parm%meshfiles       = meshfiles
    end subroutine
  end program
