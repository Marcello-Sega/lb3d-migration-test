#!/usr/bin/env python3
# Generates Xdmf files from NSKG .h5 (scalar fields) files.
#
# Standard use:
# ./h5toxmf.py <data.h5>
#      creates the file <data.xmf> alongside <data.h5>, that can be easily opened in ParaView

import glob, h5py

def rename_h5(name,rename=False):
    """
    Generate the name for the .xmf file from the .h5 one

    :param name:   A string with the .h5 filename
    :param rename: If true, and the .h5 filename follows the NSKG output
                   file convention, generate the .xmf filename using a
                   paraview-friendly convention. Otherwise, just replace
                   .h5 with .xmf
    """

    if rename:
        import re
        try:
            base, rid = name[:-3].split('-')
            if ( re.match('\d{10}$',rid) is not None) and (re.match('_t\d{8}$',base[-10:]) is not None):
                return base[:-10]+'_'+rid+'.'+base[-8:]+'.xmf'
        except:
            pass

    return name[:-3]+'.xmf'


def generate_xmf(arg,rename=False):
    """
    Generates Xdmf files from NSKG .h5 files

    :param arg:    A string or a list of strings with .h5 filenames
    :param rename: If True and the input file is in the old NSKG style
                   (e.g., od_out_t00000399-1814836018.h5), name the
                   .xmf file to fit paraview convention on time series
                   (here, od_out_1814836018.00000399.xmf)
    :returns:      Creates .xmf files with the same basename as the
                   input .h5 ones

    """

    filelist = glob.glob(arg)

    if len(filelist) == 0:
        raise ValueError('No file found')

    for h5f in filelist:
        xdmf_filename = rename_h5(h5f,rename)
        try:
            h5file = h5py.File(h5f,'r')
        except OSError as e :
            if __name__ == "__main__":
                exception = SystemExit
            else:
                exception = OSError
            raise( exception('{}'.format(e)+' Problem with hdf5 input file '+h5f)) from None

        shape = h5file['OutArray'].shape

        with open(xdmf_filename, 'w') as f:
            f.write(
"""<?xml version="1.0" encoding="utf-8"?>
<Xdmf xmlns:xi="http://www.w3.org/2001/XInclude" Version="3.0">
 <Domain>
  <Grid Name="Grid">
   <Geometry Origin="" Type="ORIGIN_DXDYDZ">
    <DataItem DataType="Float" Dimensions="3" Format="XML" Precision="8">0 0 0</DataItem>
    <DataItem DataType="Float" Dimensions="3" Format="XML" Precision="8">1 1 1</DataItem>
   </Geometry>
   <Topology Dimensions="{} {} {}" Type="3DCoRectMesh"/>
   <Attribute Center="Node" Name="Data" Type="Scalar">
    <DataItem DataType="Float" Precision="8" Dimensions="{} {} {}" Format="HDF">{}:/OutArray</DataItem>
   </Attribute>
  </Grid>
 </Domain>
</Xdmf>
""".format(*shape,*shape,h5f))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Generate Xdmf files from NSKG\'s Hdf5')
    parser.add_argument('filenames', metavar='filename', nargs='+', help='HDF5 files in NSKG format')
    parser.add_argument('--rename', action='store_true')

    args = parser.parse_args()

    for arg in args.filenames:
        generate_xmf(arg,args.rename)

