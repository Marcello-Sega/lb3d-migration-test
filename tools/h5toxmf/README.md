# h5toxmf

Generates Xdmf files from NSKG .h5 (scalar fields) files. Xdmf is an
xml format that can be used to load directly the .h5 files in paraview
(or visit) without converting them to vtk.
https://www.xdmf.org/index.php/XDMF_Model_and_Format

The python script extracts the information on the dimensions of the grid
without loading the whole data, and generate one (small) .xmf file for
each of the .h5 files passed. The .xmf file can be then opened within
paraview, allowing it to interprete the .h5 file and load the data
stored therein.

By using the '--rename' switch, if the input hdf5 file follows the old (pre-2023) NSKG
output filename convention, the output Xdmf file will be named in a
paraview-friendly way that allows for time-series to be recognized.

Example:

```
h5toxmf.py --rename d_out_t00000000-1428568305.h5
ls *xmf
d_out_1428568305.00000000.xmf
```
