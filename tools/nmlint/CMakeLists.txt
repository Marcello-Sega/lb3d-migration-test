set(script "nmlint.py")
configure_file(${script} ${CMAKE_CURRENT_BINARY_DIR})
install(PROGRAMS ${script} DESTINATION bin)
