#!/usr/bin/env python3

# ## A sanity-checker for input files and their namelists.
# Usage:
#	./nmlint.py <input>
# If no <input> file is given, we will try to use "./input" instead.
#
# ## Test scope
# If NSKG has produced the file "nskg.input" with the list of processed namelists[^1], we
# will read it, and compare the names of namelists between it and the actual input.
#
# (We don't need to check for the actual variables in the namelists, since typos and outdated
# entries at that place, will trigger a Fortran runtime error).
#
# @TODO This currently only checks the main input file, but not the auxiliary files (like the
# MD particle, potential, or wall setup).
#
# [^1]: "nskg.input" will not contain all namelists, but only those that have been present in
# the actual input file. See <https://git.iek.fz-juelich.de/compflu/nskg/-/issues/421>.
# However, while that is an issue in the general case, this test is insensitive by design,
# since either a namelist is present in the input input file (then it's present in the output
# too), and when it's not present in the output, then we know that something has gone wrong.
# Still, this works only for the main input file; for auxiliary input files, we do not
# unconditionally have access to the equivalent of "nskg.input" (see
# <https://git.iek.fz-juelich.de/compflu/nskg/-/issues/531>), so we can't do the same tests
# here.

import sys, os.path, string
import numpy as np
import f90nml as nml

# CLI parser
s_input = "./input"
if (len(sys.argv)>1):
    s_input = sys.argv[1]

s_autoinput="nskg.input"
b_autoinput=False

if not(os.path.isfile(s_input)):
    raise FileNotFoundError('Input file "'+s_input+'" not found.')

if s_autoinput in s_input:
    print('Warning. It seems like you try to check automated input file "'+s_autoinput+'". Usefulness will be limited.')

if os.path.isfile(s_autoinput):
    b_autoinput = True
else:
    print('Warning. Automated input file "'+s_autoinput+'" not found. Usefulness will be limited.')

Failure = False

o_input = nml.read(s_input)
l_input_namelists = list(o_input)

if b_autoinput:
    o_autoinput = nml.read(s_autoinput)
    l_autoinput_namelists = list(o_autoinput)
    for s_nml in l_input_namelists:
        if (s_nml in l_autoinput_namelists):
            print('🙂 Namelist "'+s_nml+'" good: present in both "'+s_input+'" and "'+s_autoinput+'"') 
        else:
            Failure = True
            print('🤦 Namelist "'+s_nml+'" bad: in "'+s_input+'" but not in "'+s_autoinput+'"')

if Failure:
    print("Failure 🤦")
    sys.exit(1)

print("Success 🙂")
