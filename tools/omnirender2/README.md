# OmniRender 2

3D visualisation of NSKG simulation results (density fields, surfaces, MD particles) is a repetitive task. This
tool automates the process to combine field (from files, converted to VTK) and particle position data (from MD
output) in a visually appealing format, without reinventing the wheel every time.

OmniRender is located and maintained in the separate nskg-utils repository, found at <https://git.iek.fz-juelich.de/compflu/nskg-utils/-/blob/master/src/vistools/omnirender>.
