#include "nskg.h"
program nskg

!> NSKG, the source file of the application that makes use of libnskg

  use nskg_common_headers_macro
  use nskg_system_class
  use nskg_features_class
  use nskg_lb_class
  use nskg_cg_class
  use nskg_relax_class, only: set_relax
  use nskg_bc_module, only: nskg_bc_advection_post&
       &, nskg_bc_complete_halo_exchange, nskg_bc_init&
       &, nskg_bc_init_step, nskg_bc_shutdown
  use nskg_advection_module
  use nskg_globals_module
  use nskg_helper_module
  use nskg_io_module
  use nskg_io_hdf5_module, only: nskg_init_metadata_hdf5
  use nskg_io_checkpoint_module
  use nskg_io_stress_module, only: compute_fluid_stress_xz_profile
  use nskg_log_module
  use nskg_parms_module
  use nskg_parallel_module
  use nskg_timer_module, only: start_timer, stop_timer, sum_timer
  use nskg_parms_system_class
  use nskg_ad_class
  use nskg_io_hdf5_module, only: nskg_io_init_hdf5, nskg_io_shutdown_hdf5, nskg_add_inputfile_metadata_hdf5
  use nskg_md_class
  implicit none

  integer :: i
  integer :: tstart
  logical :: insanity

  insanity = .false.
#ifdef USE_LIKWID
  call likwid_markerInit()
  call likwid_markerThreadInit()
  call likwid_markerRegisterRegion("Compute")
#endif

  !!! General system initialisation !!!
  call features%init      ! Bring all features into place. From ../../libnskg/src/core/features/nskg_features.F90
  allocate(SYS)
  call SYS%init(.true.)   ! Does all the hard work of setting up features (including setting myrankc). From ../../libnskg/src/system/nskg_system_class.F90
  ! Logging (attaching the input file(s) to the HDF5 output)
  call prepare_metadata

  !!! LB and halo setup, restore/checkpoint handling !!!
  call set_restore_parameters() ! From ../../libnskg/src/chkp/nskg_io_checkpoint.F90
  if ( SYST%restore ) then      ! @TODO decouple core, system, and chkp...
     call log_msg("Restoring system from checkpoint.")
     call restore_checkpoint()
  end if
  call SYS%setup()
  ! after initializing the populations the halo is tainted an needs to be exchanged
  SYS%Lattice%halo_is_tainted = .true.
  call SYS%Lattice%sendrecv_halo()
  ! Start from timestep zero, unless starting from checkpoint
  if ( SYST%restore ) then
    tstart = SYST%n_restore
  else
    tstart = 0
  endif
  nt = tstart
  call nskg_bc_init()

  call initialize_forces_lb
  call initialize_forces_meshes

  do i=1,Lattice%parms%ncomponents
     call Lattice%fields%c(i)%density%check_presence_of_rocks() ! sets the has_walls, has_colloids flags
  enddo
  call Lattice%fields%compute()

  ! TODO refactor the stress calculation code.
  if(check_interval(Lattice%parms%dump_n_stress_xz_profile)) call compute_fluid_stress_xz_profile()
  call dump_data_timed
  call dump_meshes_and_properties

  if ( SYST%n_sanity_check /= 0 ) call nskg_sanity_check(insanity)

  call timestep_checkpoint(insanity)

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!                     The main loop                         !!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  nt = tstart + 1
  start_time_t = mpi_wtime()
  call start_timer(ti_total)    ! Initialisation and checkpoint restoration is excluded from total run
                                ! time, but included into I/O times, so that the sum of timers might end
                                ! up beyond 100% (in case timers are running before `ti_total` has been
                                ! started).
  call log_msg_hdr("Starting time loop")
  call SYS%Lattice%sendrecv_halo()
  do while (nt.le.SYST%n_iteration)
    call log_timestep
    if (features%nofluiddynamics%active .eqv. .false.) then
        call nskg_bc_init_step()
        call nskg_bc_complete_halo_exchange()
    end if

    if(features%relaxation%active) call Lattice_relaxation%apply_protocols(nt)
    TIMEIT(ti_md_compute_pair_forces, call compute_forces_md)

    if(nt.gt.1) then
      !> From time step 2 on: advection, halo exchange
      DEBUG_MSG("nskg_advect")
      TIMEIT(ti_adv, call nskg_advect; call timestep_bounceback)
      SYS%Lattice%halo_is_tainted=.true.
      TIMEIT(ti_halo, call SYS%Lattice%sendrecv_halo)
    end if
    call timestep_advection_post
    TIMEIT(ti_fields, call SYS%Lattice%fields%compute)

    call timestep_meshes

    call compute_forces_meshes
    call timestep_fields
    TIMEIT(ti_md_integration_step, call timestep_md)
    if (features%elec%active .eqv. .true.) then
       call SYS%elec%update_fields()
    end if
    TIMEIT(ti_md_io, call dump_md)

    if(check_interval(Lattice%parms%dump_n_stress_xz_profile)) call compute_fluid_stress_xz_profile()

    TIMEIT(ti_halo, call SYS%Lattice%sendrecv_force_halo)

    !!! Collision !!!
    DEBUG_MSG("nskg_collide")
    if(.not.(features%nofluiddynamics%active .or. features%nocollision%active)) then
       TIMEIT(ti_intc, call SYS%Lattice%do_collisions)
    endif

    !!! Output of science data, if desired !!!
    TIMEIT(ti_dump, call dump_data_timed; call dump_meshes_and_properties)

    !!! Sanity check. !!!
    if ( check_interval(SYST%n_sanity_check) ) then
      DEBUG_MSG("nskg_sanity_check")
      call nskg_sanity_check(insanity)
    end if

    call timestep_checkpoint(insanity)

    ! Abort simulation if it is not sane.
    if (insanity) call Abend()

    ! `nt` measures the time coordinate from simulation start, including
    ! the offset from restoring a checkpoint.
    ! `timesteps_count` always counts the no of timesteps performed in
    ! *this* run of the simulation.
    nt = nt + 1
    timesteps_count = timesteps_count + 1
  end do

  ! CALL set_endtime()
  end_time_t = mpi_wtime()
  call stop_timer(ti_total)

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!! Finished - now tidy up.                                   !!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  call sum_timer() ! dump timer evaluation

  !TODO: reintroduce/reimplement elec_report_timers
  !call elec_report_timers()

  call nskg_bc_shutdown()

  call nskg_io_shutdown_hdf5()
  call dump_performance_info(calculate_performance(end_time_t - start_time_t,timesteps_count))

  call acc_limit_report

  call SYS%destruct()
  call FinalizeMPIcomms()

  call log_msg_ws("Exiting ...")
#ifdef USE_LIKWID
  call likwid_markerClose()
#endif








contains
  subroutine prepare_metadata
    !> Ensure that the HDF5 comment string is filled with metadata
    if (myrankc .eq. 0) then
      ! If we use HDF5, we add the contents of input files to the metadata.
      call nskg_init_metadata_hdf5 ! This is defined in ../../libnskg/src/core/io/nskg_io_hdf5.F90 but nowhere else called (except for checkpoint restore at [[restore_lattice_MPhdf5]])
    end if
  end subroutine prepare_metadata

  subroutine initialize_forces_lb
    if (features%nofluiddynamics%active .eqv. .false.) then
      call nskg_bc_complete_halo_exchange()
    end if

    call SYS%Lattice%reset_force     ! The array nskg_force is allocated and initialized to 0 everywhere. After this call, nskg_force is ready to use.
  end subroutine initialize_forces_lb


  subroutine initialize_forces_meshes
    !> Mesh forcing
    if (SYST%restore) then
      if (features%ibm%active) then

        TIMEIT(ti_mesh_reset_forces, call SYS%IBM%reset_forces())
        TIMEIT(ti_mesh_geometry_internal_forces, call SYS%IBM%compute_geometry_and_internal_forces())
        if(features%interactions%active) call SYS%interactions%compute_pair_forces_mesh(SYS%IBM, SYS%Walls)
        !This 'spread_forces' was here before, but it should not be needed when restoring the simulation
        !because the force has been already spread to the lattice before the checkpoint
        !call SYS%IBM%spread_forces()
      end if
    end if
  end subroutine initialize_forces_meshes

  subroutine timestep_meshes
    if(features%ibm%active) then
      if(nt.ge.SYS%IBM%parms%steady_step) then
        call SYS%IBM%integration_step(use_fluid=.not.features%nofluiddynamics%active)
      endif
    endif

    if(features%ebf%active) then
      TIMEIT(ti_mesh_reset_forces,    call SYS%EBF%reset_forces())
      TIMEIT(ti_ebf_integration_step, call SYS%EBF%integration_step())
    end if
  end subroutine timestep_meshes

  subroutine dump_meshes_and_properties
    !> Write IBM and Aidun/EBF meshes to VTK

    ! TODO Clarify if IBM/EBF should start at 0 or dump_n_start
    if(features%ibm%active) then
      if (nt.ge.SYS%IBM%parms%steady_step) then ! dump time zero
        ! TODO Wrap these into TIMEIT(ti_mesh_dump, call ...)
        if (check_interval(SYS%IBM%parms%time_step_dump_vtk)) &
          call SYS%IBM%write_vtk("ibm_particles",timestamp=nt)
        if (check_interval(SYS%IBM%parms%time_step_dump_mesh_properties)) &
          call SYS%IBM%dump_mesh_properties(nt)
      endif
    end if

    if(features%ebf%active) then ! dump time zero
      if (check_interval(SYS%EBF%parms%time_step_dump_vtk)) &
        call SYS%EBF%write_vtk("ebf_particles",timestamp=nt)
      if (check_interval(SYS%EBF%parms%time_step_dump_mesh_properties)) &
        call SYS%EBF%dump_mesh_properties(nt)
    end if
  end subroutine

  subroutine dump_data_timed
    !> The context for calling dump_data (HDF5 output) from ../../libnskg/src/old/nskg_io.F90
    if ( nt >= Lattice%parms%dump_n_start ) then
      call dump_data()
      if (features%thermal%active)             call SYS%thermal%dump_temperature()
      if (features%elec%active)                call SYS%elec%dump()
      if (features%advection_diffusion%active) call SYS%advection_diffusion%dump()
    end if
  end subroutine dump_data_timed

  subroutine compute_forces_meshes
    call SYS%Lattice%reset_force    ! All forces added to nskg_force before this call will be overwritten.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Add forces to nskg_force only after this comment. !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!    call nskg_force_apply() TODO OTHMANE CHECK THIS

    if(features%ibm%active) then
      TIMEIT(ti_mesh_geometry_internal_forces, call SYS%IBM%compute_geometry_and_internal_forces())
      if(nt.lt.SYS%IBM%parms%steady_step)return
      if(features%interactions%active)then
        TIMEIT(ti_ibm_compute_pair_forces, call SYS%interactions%compute_pair_forces_mesh(SYS%IBM, SYS%Walls))
      endif
      TIMEIT(ti_ibm_compute_and_spread_forces, call SYS%IBM%spread_forces)
    endif

    if(features%ebf%active) then
      if(features%interactions%active) call SYS%interactions%compute_pair_forces_mesh(SYS%EBF, SYS%Walls)
      call SYS%EBF%spread_forces()
    endif

    ! Add constant and/or Kolmogorov forces to nskg_force.


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Add forces to nskg_force only before this comment. !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  end subroutine compute_forces_meshes


  subroutine timestep_fields
    if (features%advection_diffusion%active .eqv. .true.) call SYS%advection_diffusion%update_fields()
    if (features%thermal%active .eqv. .true.)             call SYS%thermal%update_temperature()
  end subroutine timestep_fields

  subroutine timestep_md
    if(features%md%active) then
        call SYS%md%ladd_particles%mass_correction%update_ratios
        call SYS%md%ladd_particles%step
        call SYS%md%point_particles%step
    endif
  end subroutine timestep_md

  subroutine dump_md
    if (features%md%active) then
      if (check_interval(md%n_dump, Lattice%parms%dump_n_start)) then
        call SYS%md%ladd_particles%dump_configuration
        call SYS%md%point_particles%dump_configuration
      endif
    endif
  end subroutine dump_md

  subroutine acc_limit_report
    if (features%interactions%active) call SYS%interactions%report_acc_limit_violations
  end subroutine acc_limit_report

  subroutine compute_forces_md
    if(features%md%active) then
      call SYS%md%ladd_particles%reset_forces_and_torques()
      call SYS%md%point_particles%reset_forces_and_torques()
      call SYS%md%moving_plane_force()
      if(features%interactions%active) then ! TODO implement cross-interactions
        call SYS%interactions%compute_pair_forces_md(SYS%md%ladd_particles,SYS%Lattice%rocks)
        call SYS%interactions%compute_pair_forces_md(SYS%md%point_particles,SYS%Lattice%rocks)
      endif
    endif
  end subroutine compute_forces_md

  subroutine timestep_bounceback
    !> Carry out bounce-back if needed
    !>
    !> @TODO Assess (profiling?) if these repeated queries can/should be out-sourced to a run-time status variable
    logical :: need_bounceback
    need_bounceback = (SYS%Lattice%fields%c(1)%density%has_walls .or. SYS%Lattice%fields%c(1)%density%has_colloids ) &
            & .and. .not. features%nofluiddynamics%active
    if (need_bounceback) then
      call SYS%Lattice%bounce_back()
      if(features%md%active) call SYS%md%ladd_particles%bounce_back
    endif
  end subroutine timestep_bounceback

  subroutine timestep_advection_post
    if (features%nofluiddynamics%active .eqv. .false.) then
        DEBUG_MSG("nskg_bc_advection_post")
        ! makes a halo exchange for certain invasive fluids
        call nskg_bc_advection_post()
    end if
  end subroutine timestep_advection_post

  subroutine timestep_checkpoint(insanity)
    !> Dump a checkpoint if its time for it, or if something has gone wrong (`insanity`)
    logical :: insanity
    if ( insanity .or. check_interval(SYST%n_checkpoint) ) then
      call start_timer(ti_dump)
      DEBUG_MSG("process_checkpoints")
      call process_checkpoints(insanity)
      call stop_timer(ti_dump)
      ! TODO Consistent calls (and timing) of all offlattice object dumps
      if(features%ibm%active) call SYS%IBM%dump_mesh("ibm_dump_meshes",nt)
      if(features%ebf%active) call SYS%EBF%dump_mesh("ebf_dump_meshes",nt)
    end if
  end subroutine timestep_checkpoint

end program nskg
