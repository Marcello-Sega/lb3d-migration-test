# NSKG: The Application Sources

## nskg
This is the source file of the NSKG simulation application. While all the actual logic is out-sourced to `libnskg`, the set-up, initialisation, and time loop happens here.

Build instructions: see `README.md` at the root of this repository.


## nskgtrace
This is a python wrapper of nskg compiled with the instrumentation
options. Together with a database matching function names with their
addresses, it dumps a full trace of the calls with references to source
file and line number

To enable it, configure using -DCMAKE_BUILD_TYPE=Trace

This option will create the executable script `'nskgtrace'` in the same
directory as `'nskg'`. It can be invoked in the same way as nskg (also in
parallel) and produces an output like this:

```
entering:main:/home/m.sega/nskg/nskg/src/nskg.F90:12
entering :MAIN__:/home/m.sega/nskg/nskg/src/nskg.F90:8
entering   :0x41e870
entering     :__nskg_system_class_MOD_default_init:/home/m.sega/nskg/libnskg/src/system/nskg_system_class.F90:271
entering       :0x580ab0
entering         :__nskg_log_module_MOD_log_msg_ws:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:125
entering           :__nskg_log_module_MOD_log_msg:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:35
08:11:28 -
 exiting           :__nskg_log_module_MOD_log_msg:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:35
entering           :__nskg_log_module_MOD_log_msg:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:35
08:11:28 - Initialized MPI_COMM_WORLD, starting output.
 exiting           :__nskg_log_module_MOD_log_msg:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:35
entering           :__nskg_log_module_MOD_log_msg:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:35
08:11:28 -
 exiting           :__nskg_log_module_MOD_log_msg:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:35
 exiting         :__nskg_log_module_MOD_log_msg_ws:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:125
entering         :__nskg_log_module_MOD_log_msg:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:35
08:11:28 - Requested 1 processors.
 exiting         :__nskg_log_module_MOD_log_msg:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:35
 exiting       :0x580ab0
entering       :0x586bd0
entering         :__nskg_log_module_MOD_log_msg_ws:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:125
entering           :__nskg_log_module_MOD_log_msg:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:35
08:11:28 -
 exiting           :__nskg_log_module_MOD_log_msg:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:35
entering           :__nskg_log_module_MOD_log_msg:/home/m.sega/nskg/libnskg/src/core/helper/nskg_log.F90:35
...
```
