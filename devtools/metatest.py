#!/usr/bin/env python3
# A meta-test for our test suite
# Performs sanity checks on the configuration of the test suite (both the CTest setup, and the integration into Gitlab CI)

import os
import re
import sys
import numpy as np
from glob import glob
import argparse
import f90nml


parser = argparse.ArgumentParser(
                    prog='Metatest',
                    description='Performs sanity checks on the configuration of the test suite (both the CTest setup, and the integration into Gitlab CI)')

parser.add_argument('-d', '--debug', action='store_true', help='Print debugging information')
parser.add_argument('-r', '--root', default='../tests/', help='Root directory used to search for tests')
args = parser.parse_args()


# format: ('trigger directory', 'error message', 'trigger file name', 'fatal')
inconsistencies = []

# List of NSKG input files
# (This does not rely on file names, but on the existence of the `&system/` namelist. As of 00955ca3, there is only one file left ("./tools/inflate/input_inflate") that matches the `find . -type f -name "*input*"` wildcard but does not have the `&system/` namelist present)
# Sub-directory whitelist (expecting no new directories to be created until !671 gets merged) {
tests_dirs =  [ s.split('/')[-2] for s in glob(args.root+'/**/CMakeLists.txt')]
# Excluded "bb/" (but keep !631 in mind)
# Excluded "unit/" (take care manually)
# Excluded "msh/" (does not contain tests)
tests_whitelist = ["1cpu"]   # This does NOT ignore these tests, but just not trigger the failure state if the test fails. Every exception has to be justified here {{
# * "1cpu": `perf/lb/1cpu` exists (as an optional CI stage), but for whatever reason, we are too lazy to add `perf/{sc,cg}/1cpu` as well.
# }}
# }
os.chdir(args.root)

def fatal_or_whitelist(testname):
    for S in tests_whitelist:
        if (S in testname):
            return False
    return True

l_inputs = []
l_ciyml = []
l_cmakelists = []
for tests_wd in tests_dirs:
    for args.root, dirs, files in os.walk(tests_wd, topdown=False):
        for name in files:
            fname = os.path.join(args.root, name)
            try:
                O = f90nml.read(fname)
                O["system"]
                if not ((name == "nskg.performance.log") or (name == "nskg.input")):
                    l_inputs += [(args.root, name)]
                if args.debug:
                    print("# NML", fname)
            except:
                if args.debug:
                    print("# (?)", fname)
            if (name == "ci.yml"):
                l_ciyml += [fname]
            elif (name == "CMakeLists.txt"):
                l_cmakelists += [fname]
        for fdir in dirs:
            fpath = os.path.join(args.root, fdir)
            if not os.path.isfile(os.path.join(fpath, "CMakeLists.txt")):
                # This is an error in any case, because directories missing a CMakeLists.txt will be ignored by CMake, hence we should throw them out of the test suite.
                inconsistencies += [(fpath, "(Type1) CMake file in directory "+fdir+" missing", fpath, fatal_or_whitelist(fpath))]

# Test for typical error modes...
for T in l_inputs:
    thisdir = T[0]
    # ... Local CMakeLists.txt missing
    s_cmake = os.path.join(thisdir,"CMakeLists.txt")
    if not os.path.isfile(s_cmake):
        inconsistencies += [(thisdir, "(Type2) CMake file "+s_cmake+" missing", T[1], True)]

    # ... Parent CMakeLists.txt unavailable, or subdirectory not referred, or possibly commented out
    s_up = os.path.split(thisdir)
    childdir = s_up[-1]
    s_cmake_up = os.path.join(s_up[0],"CMakeLists.txt")
    if os.path.isfile(s_cmake_up):
        with open(s_cmake_up) as O:
            s_found = ""
            lastline = 0
            for I, L in enumerate(O):
                if args.debug:
                    print(I,':',L)
                if childdir in L:
                    s_found = L
                    lastline = I+1
        if len(s_found) < 1:
            inconsistencies += [(thisdir, "(Type3) Missing subdirectory referral to "+childdir+" in "+s_cmake_up, T[1], True)]
        elif ('#' in s_found):
            inconsistencies += [(thisdir, "(Type4) Comment in "+s_cmake_up+(":%d"%lastline)+" Please check manually", T[1], True)]
    else:
        inconsistencies += [(thisdir, "(Type5) CMake file in parent directory "+s_cmake_up+" missing", T[1], True)]

    # ... Parent ci.yml unavailable, or subdirectory not referred, or possibly commented out
    s_ciyml_up = os.path.join(s_up[0],"ci.yml")
    if os.path.isfile(s_ciyml_up):
        with open(s_ciyml_up) as O:
            s_ctest_name = thisdir.replace("/", "-")+":"    # Adding ":" at the end ensures that suffixes are ignored
            s_found = ""
            lastline = 0
            for I, L in enumerate(O):
                if s_ctest_name in L:
                    s_found = L
                    lastline = I+1
        if len(s_found) < 1:
            inconsistencies += [(thisdir, "(Type6) Missing subdirectory referral to "+s_ctest_name+" in "+s_ciyml_up, T[1], fatal_or_whitelist(s_ctest_name))]
        elif ('#' in s_found):
            inconsistencies += [(thisdir, "(Type7) Comment in "+s_ciyml_up+(":%d"%lastline)+" Please check manually", T[1], fatal_or_whitelist(s_ctest_name))]
    else:
        inconsistencies += [(thisdir, "(Type8) CI YAML in parent directory "+s_ciyml_up+" missing", T[1], True)]
# Check all ci.yml and CMakeLists.txt for comments
danger_regex = re.compile("#.*(- |:|add_subdirectory)")
for S in l_ciyml+l_cmakelists:
    with open(S) as O:
        for I, L in enumerate(O):
            if danger_regex.search(L):
                inconsistencies += [('CommentChecker', "(Type9) Comment in "+S+(":%d"%(I+1))+" Please check manually", 'CommentChecker', fatal_or_whitelist(L))]
# Check ci.yml for duplicate 'include:' statements
for S in l_ciyml:
    i_include_count = 0
    i_last_line = 0
    with open(S) as O:
        for I, L in enumerate(O):
            if 'include' in L:
                i_include_count += 1
                i_last_line = I
    if (i_include_count>1):
        inconsistencies += [('CommentChecker', "(TypeA) Multiple 'include:' statements in "+S+(" (last in line:%d)"%(i_last_line+1))+" Please check manually", 'CommentChecker', fatal_or_whitelist(L))]

# Print list of found issues
fatal_inconsistencies = len(inconsistencies)
for T in inconsistencies:
    print(T[1], "\t\t", T[0])
    if not T[3]:
        fatal_inconsistencies -= 1

print("\n")
if len(inconsistencies)>0:
    print("Found %d inconsistencies (%d fatal) in the test suite (multiply counting duplicates)"%(len(inconsistencies), fatal_inconsistencies))

if fatal_inconsistencies>0:
    print("Failure 🤦")
    sys.exit(1)
else:
    print('Success 🙂')
