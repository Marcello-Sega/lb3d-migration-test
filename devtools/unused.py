#!/usr/bin/env python3
from glob import glob
import re     
called = set()
defined= {}
files = glob('./**/*.F90',recursive=True)
for f in files[:]:
   for line in open(f).readlines():
       if re.match('^ *subroutine',line[:-1],re.IGNORECASE):
           sub = re.sub('\(.*','',re.sub('^ *subroutine *','',line,re.IGNORECASE)).strip().lower()
           defined[sub]=f
       if re.match(r'.*\bcall\b',line[:-1],re.IGNORECASE):
           called.add(re.sub('.*%','',re.sub('\(.*','',re.sub('.*call *','',line[:-1],flags=re.IGNORECASE))).strip().lower())
       if re.match(r'.*\b(procedure|generic)\b.*::.*=>\s*',line[:-1],re.IGNORECASE):
           S=re.sub( \
                '.*=>\s*', \
                '', \
                line[:-1], \
                flags=re.IGNORECASE).strip().lower()
           called.add(S)

keys = set(defined.keys())
sort = []
for unused in keys.difference(called):
    sort.append(defined[unused]+': '+unused)

for s in sorted(sort):
    print(s)
