#!/usr/bin/env gnuplot
set term png size 600, 300
set output "ibm_stencils.png"

# Lagrange Stencils (discretised Dirac δ) after the function interpolation_kernel() in ../../../libnskg/src/offlattice_objects/lagrangian_object/nskg_lagrangian_set_class.F90
ir2(r)=(r<1)?1-r:0
ir4(r)=(r<1)? (3 - 2 * r + sqrt(1. + 4. * r - 4. * r*r)) / 8.0 : (r<2)?  (5 - 2 * r - sqrt(-7. + 12. * r - 4. * r*r)) / 8.0 : 0

set samples 6543
set xlabel 'Distance (lu)'
set ylabel 'Relative weight'

pl [0:2.5] \
"+" u 1:(ir2($1)) w l lw 3 title 'Interpolation range 2', \
"+" u 1:(ir4($1)) w l lw 3 title 'Interpolation range 4'
