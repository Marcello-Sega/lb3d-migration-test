#!/bin/bash
#SBATCH -o job.%j.%N.out
#SBATCH -e error.%j.out
#SBATCH -J JOBNAME
#SBATCH --get-user-env
#SBATCH --partition=long
#SBATCH -n 8

#YOUR_ENVIRONMENT_VARIABLE="Value of your environment variable"
srun /__SOURCE/PATH__/build/nskg/src/nskg -f input -T $SLURM_JOB_ID
