#!/bin/bash
# Leave the directory in a clean state (delete data, queue status files etc.)
rm -f chkp/* data/* error* job* *.npz nskg.input nskg.performance.log
mkdir -p data chkp
