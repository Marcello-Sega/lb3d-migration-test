title: Literature lists

Scientific and programming work on NSKG is extensively backed by literature.
A major part of the references is present as [BibTeX-style bibliography entries](|gitlabfile|/doc/nskg_literature.bib).

This file contains some literature references that have been found in a shape that does not follow this semantics.



[[nskg_invasion_inv.F90]]
```
  !>  In and outflux with boundary conditions according to Zou and He, 
  !>  [@bib:zou-he:1997]
  !>  , or, applied and formulated for D3Q19
  !>  in Kutay et al. Computers and Geotechnics, 33, 381 (2006)
  !>  Laboratory validation of lattice Boltzmann method for modeling pore-scale flow in granular materials.
  !>  https://doi.org/10.1016/j.compgeo.2006.08.002

```

[[nskg_invasion_slip.F90]]
```
!> Partial slip boundary Khalid Ahmed and Martin Hecht
  !> [@bib:]
  !> See M. Hecht and J. Harting 2012 (arXiv:0811.4593v6)
  !> N. K. Ahmed and M. Hecht, doi:10.1088/1742-5468/2009/09/P09017
  !>
```

[[nskg_invasion_vel.F90]]
```
    !> Zou & He B.C. for a shear flow.
    !> [@bib:hecht-harting:2010]
    !> See: M. Hecht and J. Harting J. Stat. Mech. (2010) P01018
```

[[nskg_leesedwards.F90]]
```
           ! If this processor is on minimum x-plane, find the
           ! two processors to write to.
           ! leprocs changed order, one added, as suggested by Jens Oct 2003 [@bib:]

           ! and/or if processor is on max x-plane...
           !leproc(2,3) added as suggested by Jens, Oct 2003 [@bib:]
```

[[nskg_cg_class.F90]]

subroutine `do_collisions_cg(this)`
```
         !> [@bib:]
         !> @note: beta = 0.99 here (beta_kl  in 10.1016/j.jcp.2013.03.039)
         !> is used to avoid having a  too stiff interface which generates
         !> negative populations (e.g. already  for  beta=0.999) or a too
         !> diffuse interface (already with beta=0.9)
```

**nskg_elec_fluxes.F90**
```
subroutine calc_fluxes_diffusion(N, flux_site_plus, flux_site_minus)
  !> Handler function. Two options are:
  !>
  !> [@bib:]
  !>  * "pagonabarraga": fluxes computed as in DOI:10.1039/b901553a

  !>  * "rempfer": fluxes computed as in arXiv:1604.02054v1

  !> Difference is the discretization of the Nernst-Planck equations.
```

***
**nskg_elec_helper.F90**
```
function get_solvation_potential_m(N, i, j, k)
  !> Get the specific form of the solvation potential for negative ions.
  !> 0. Coupled to the concentration field, Capuani et al (DOI: 10.1063/1.1760739) [@bib:]
  !> 1. Coupled to the water volume fraction, Onuki (DOI: https://doi.org/10.1103/PhysRevLett.119.118001) [@bib:]
```

```
function get_solvation_potential_p(N, i, j, k)
  !> Get the specific form of the solvation potential for positive ions.
  !> 0. Coupled to the concentration field, Capuani et al (DOI: 10.1063/1.1760739) [@bib:]
  !> 1. Coupled to the water volume fraction, Onuki (DOI: https://doi.org/10.1103/PhysRevLett.119.118001) [@bib:]
```
```
function get_solvation_field(N, i, j, k)
  !> Solvation might be coupled to the concentration field, such as in Capuani
  !> et al (DOI: 10.1063/1.1760739), or to the density of water, such as in the
  !> later works of Onuki (DOI: https://doi.org/10.1103/PhysRevLett.119.118001) [@bib:]
```

***
