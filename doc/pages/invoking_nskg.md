title: Invoking NSKG


# Invocation of NSKG
## Command-line arguments

@todo As of 34eef08ad4a2eb88e2515299d3264639c256c9c9, the command line arguments are processed in [[nskg_parse_arguments]]. Wherever they will land in the end, leave a link to that place here.

`-f input-file-name` The input file (main configuration & setup)

: Pass the name of the input file (containing the simulation set-up).
If `-f` is not given, NSKG will read `.input-file` in the current directory. If this file contains
the name of another input file, this will be loaded. If it contains the string `INTERACTIVE`, then
NSKG will expect the contents of an input file to be passed from stdin.

The format and semantics of the input file are explained on the [**File I/O page**](../|page|/parts/fileio.html)).
<!--
reference to paragraph
## Namelists, General Input file
-->

`-r <restore-string>` Checkpoint format

:   Name of the checkpoint to restore.

@todo See documentation of `SYST%restore_string`

`-T <file-tag>` Alternative file tag
: Every simulation has a **file tag** (identifier part in file names). It is `default` by default, but you can set it via this CLI argument. (If the input file defines a `SYST%file_tag`, this will take precedence).
Setting the file tag can be handy to name simulation runs, to pass external information known no earlier than run time.

Some suggestions:

* `nskg -T $(date +%s)` will name files by a quasi-random unique run ID (system time). (This caters those who are still used to unpredictable file names of the `chk-uid` kind, from ancient times.)
* On a Slurm-managed queue, you can use `nskg -T $SLURM_JOB_ID` to include the job ID number in all your output file names.
* On a Slurm-managed queue, you can use `nskg -T $SLURM_JOB_NAME` to include the job name (the one you have passed to the squeue script with `# SBATCH -J $SLURM_JOB_NAME`) into output file names.
* `nskg -T test4_mod0.001`, to keep previous states of a setup while fine-tuning parameters.



## Simulation Workflow

 For test runs and very small simulations, the binary can be executed by hand (using the
 `mpirun` command for local parallelism):
```shell
cd src
mpirun -np 2 /__SOURCE/PATH__/build/nskg/src/nskg -f input
```

This will run the solver on two local CPUs, set up with the contents of your local file `input`.
You can vary the `-np` argument of `mpirun` to adjust the number of CPUs used. Be aware that the allowed number
of CPUs will depend on the shape and size of the lattice.
The lazy way to comply (not make the partitioner life unnecessarily
difficult) is to use powers of two for both the lattice size
`SYST%box = NX NY NZ` and the number of CPUs `np`.

The sweet-spot of parallelisation on 2020ish CPUs (trade-off between CPU cache size, and communication on-die/between sockets/between machines) is around
@TODO lattice nodes per CPU core. We don't have to guess here, we can (and should) show off scaling graphs somewhere (`performance/benchmark` looks like a good candidate to start from; we also have [some context-free graphs in the GitLab Wiki](|gitlab|/-/wikis/rewrite/performances)).

Hence, as a rule of thumb, use
\begin{equation}
\mathtt{np} \approx \frac{\mathtt{NX}\times\mathtt{NY}\times\mathtt{NZ}}{?}
\end{equation}
for production simulations.

### Submit to a cluster queue

For full-scale jobs, it is likely that the code will be launched from a
batch queue system. It is highly recommended that the documentation for
the specific system used is consulted with great care, since batch queue
systems may vary widely in their behaviour, and batch queue system
operators may vary widely in their persistence on rules of domestic
authority.

This is an example job script for **Slurm**, one of the most widespread
queueing systems on HPC clusters:
```shell
{!../media/scripts/slurm_submit.sh!}
```
If you copy/paste, or [pull it from here](|gitlabfile|/doc/media/scripts/slurm_submit.sh), replace `/__SOURCE/PATH__/` with the absolute path to your NSKG source code.

The `#SBATCH ` comments are the Slurm way to configure your queue submission.
Tune the argument of `-n` for the number of requested CPU cores, and `--partition` to select the appropriate cluster partition (queue).
Pass some descriptive job name to the `-J` option, to tell apart your simulations.

Submission is done by calling `sbatch slurm_submit.sh`.
Slurm will (unlike, say, plain SSH) respect and pass your current shell environment to the runner jobs. In case you have an environment set-up (like loaded `module`s), this will be available at run time on the nodes.


### Clean the working directory
NSKG has the habit to produce a lot of files, some of which are not necessary/in the way when re-running a simulation during early design stages.
It has proven handy to have a script around, that removes checkpoints, log files etc., and ensures the existence of important subdirectories:
```shell
{!../media/scripts/clean.sh!}
```
Copy (or [download](|gitlabfile|/doc/media/scripts/clean.sh)) this, place it in your simulation directory next to your `input` file, and make it executable with `chmod +x`.
