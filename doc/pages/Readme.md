# NSKG: Ford Static Pages

(This page itself is ignored by Ford due to the missing `title: ` attribute at the beginning)

This is the root/initial directory for the Ford-based documentation of NSKG.

Content here fulfills the following structural functions:
* General static content, meta-information (project documentation that is not strictly code documentation, hence makes no sense to squeeze into some source file)
* Usage notes, workflows, references to external tools, evaluation scripts etc.
* everyone's favourite way to access the structure of the documentation. 
* Site-specific material. The source files should be generically documented. Information about usage at HI-ERN, the sun cluster, or similar, is best placed here.
* Collecting complementary data in one place, outside the Fortran source tree: literature, figures, external documents
* Contribution guidelines, best practice
* A notoriously incomplete back-log of projects within the group of users, who has experience in using and/or developing which parts of the code.
* We'll see how much of the fundamental documentation (general methods, formulae etc.) we will include in the source code, and or in dedicated pages around here.
* Documentation of Ford itself (meta-documentation)

On the long run, we want this directory to ease approach NSKG for different types of users (scientists/end users, thesis supervisors, code developers, glue code developers, etc.).
