title: Introduction to the Lattice-Boltzmann Method

Introduction
============

This document describes a parallel-processing implementation of our
Lattice-Boltzmann (LB) model for amphiphilic fluid dynamics
[@bib:chen-boghosian-coveney-nekovee:2000; @bib:nekovee-coveney-chen-boghosian:2000].
The main features of the model, which distinguish it from prior
lattice-Boltzmann schemes are

-   The model conserves mass separately for each chemical species
    present (water, oil, amphiphile) and maintains a vector-valued
    orientational degree of freedom for the amphiphilic species.

-   It offers control of the viscosities of each component separately
    (by adjusting the corresponding relaxation time parameter) and
    molecular masses of the various species present.

-   Interactions between fluid components are incorporated from
    bottom-up by introducing self-consistently generated mean-field
    forces between the fluid particles, rather than by a-priori positing
    a macroscopic free energy functional.

With the amphiphilic interactions extinguished the current version of
the model reduces to the Shan-Chen Lattice-Boltzmann model
[@bib:shan-chen:1993; @bib:shan-chen:1994] for binary immiscible fluids.
For a detailed description of the model and its lattice-Boltzmann and
lattice-gas automata (LGA) predecessor we refer the reader to
[@bib:boghosian-coveney-emerton:1996; @bib:shan-chen:1993; @bib:shan-chen:1994; @bib:chen-boghosian-coveney-nekovee:2000; @bib:nekovee-coveney-chen-boghosian:2000].

This document gives a brief overview of the algorithms involved,
and refers to instructions for compilation and running of the NSKG code on different parallel
platforms, and a detailed description of input and output.

<!---
[contrib]


To be fully involved in the current developent of NSKG, please subscribe
to the <http://listserver.tue.nl/mailman/listinfo/nskg-announce> mailing
list and then keep others informed about your updates by sending an
email to
[nskg-announce\@listserver.tue.nl](mail:nskg-announce@listserver.tue.nl),
in addition to pushing your git commits to
`gitolite@bert.phys.tue.nl:/mesosim/soft/nskg.git` at the TU/e.

-->

@TODO Advertisement for HI ERN GitLab

<!--
[/contrib]
-->





The NSKG Algorithm {#sec:nskg_algorithm}
=================

The simulation takes place on a 3-dimensional lattice, consisting of
\(n_x\) sites in the \(x\)-direction, \(n_y\) nodes in the y-direction, and
\(n_z\) sites in the \(z\)-direction. Each node is joined to its neighbours
by a set of lattice vectors \(c_i\). In the current implementation of the
above algorithm in 3D we use a projected face-centred hypercubic (PFCHC)
lattice [@bib:frisch-hasslacher-pomeau:1986]. The motivation for using
this lattice is that it is known to yield isotropic Navier-Stokes
behaviour for a single-phase fluid [@bib:frisch-hasslacher-pomeau:1986].
There are in total 25 lattice vectors joining each site to 19 neighbours
- the vectors pointing in the X, Y, and Z directions have twofold
degeneracy.

We use the notation that the vectors \(c_i\) are the \(i^{th}\) column
vector of the matrix $$\left(\,
  \begin{array}{ccccccccccccccccccc}
    1 & -1 & 0 &  0 & 0 &  0 & 1 &  1 & 1 &  1 & -1 & -1 & -1 & -1 & 0 &  0 &  0 &  0 & 0 \\
    0 &  0 & 1 & -1 & 0 &  0 & 1 & -1 & 0 &  0 &  1 & -1 &  0 &  0 & 1 &  1 & -1 & -1 & 0 \\
    0 &  0 & 0 &  0 & 1 & -1 & 0 &  0 & 1 & -1 &  0 &  0 &  1 & -1 & 1 & -1 &  1 & -1 & 0 \\
  \end{array}\,
  \right)$$ The vectors of the D3Q19 grid are illustrated in
Fig. [2](#fig_d3q19){reference-type="ref" reference="fig_d3q19"}.

![The geomety of our D3Q19 lattice with the lattice vectors \(\mathbf{c}_i\).](../|media|/pictures/d3q19.png)

The code accounts for three fluid species: red (oil), green
(surfactant), and blue (water)- the green particles behave like dipoles,
and therefore possess orientational degrees of freedom. A given velocity
vector at a given site will have a certain density \(f^r_i\), \(f^b_i\),
\(f^s_i\) of red, blue and green particles, respectively, there will be
also a net average dipole moment \(\mathbf{d}\), for surfactant particles
at each site. During each time step of the
simulation, the following processes take place:

-   **Advection** The particles are propagated along their velocity
    vectors, to adjacent sites. More specifically, for each site at
    position , the particles with velocity \(\hat{\mathbf{c}}_i\) are
    moved to the site at position \(\mathbf{r} + \mathbf{c}_i\).

-   **Collision**

    At each site, all the particles at that site interact: they collide
    and are redistributed. This takes place over several steps:

    -   Calculation of Hamiltonian and dipole moments

        A vector \(\mathbf{h}\) is used in calculation of the surfactant
        interactions - the reader is referred to the papers describing
        the algorithm in depth for more detail
        [@bib:chen-boghosian-coveney-nekovee:2000] . The new equilibrium
        surfactant dipole \(\mathbf{d}^{eq}\) is calculated from
        \(\mathbf{h}\).

    -   Calculation of forces

        At each site, the forces on each particle species are
        calculated: \(\mathbf{F}^{cc}\), \(\mathbf{F}^{cs}\),
        \(\mathbf{F}^{sc}\), and \(\mathbf{F}^{ss}\) are the forces on
        coloured particles due to other coloured particles, on coloured
        particles due to surfactants, on surfactants due to coloured
        particles, and on surfactants due to other surfactants,
        respectively.

        To use single component multi phase (SCMP) or multi component
        multi phase (MCMP) approaches, an attractive force
        \(\mathbf{F}^{cc}\) among each component is introduced. To include
        attractive interactions the logical values SCMP or MCMP can be
        set in the input-file.

        These forces are then summed to find the net force on particles
        of a given species at that site.

    -   Redistribution

        At each site:

        -   The density \(\rho^{\sigma}\) and fluid velocity
            \({\bf u}^{\sigma}\) of each fluid component is computed:

            $$\rho^{\sigma}= m^{\sigma}\sum_i \; n_i^{\sigma},$$

            $$\rho^{\sigma} {\bf u}^\sigma = m^{\sigma}\sum_i \; {\bf c}_i n_i^{\sigma}$$
            where \(m^{\sigma}\) is the molecular mass of species
            \(\sigma\).

        -   The average velocity \(\tilde{\mathbf{u}}\) which enters the
            equilibrium distribution functions \(n_i^{\sigma,eq}\) is
            calculated: $${\tilde {\bf u}}
                    =
                    \sum_\sigma \frac {\rho^\sigma {\bf
                    u}^\sigma}{\tau_\sigma}
                    /
                    \sum_\sigma \frac {\rho^\sigma} {\tau_\sigma}.$$

        -   The force term is calculated.

        -   The equilibrium densities for each species for each velocity
            vector, \(n^{\sigma\mathrm{(eq)}}_i\), are found.

        -   The collision itself takes place: the particle densities are
            adjusted through the fundamental equation of the NSKG model:

            $$n^{\sigma}_i \leftarrow
                                n^{\sigma}_i -
                                \frac 1 {\tau^{\sigma}}
                                \left(
                                    n^{\sigma}_i -
                                    n^{\sigma\mathrm{(eq)}}_i
                                \right)$$



 These parameters control aspects specific to the Lattice-Boltzmann
 method, such as relaxation times. For example, the kinematic viscosity
 \(\nu^{\sigma}\) of each fluid component \(\sigma\) is related to the
 relaxation time parameter \(\tau^{\sigma}\) through the relation
 [@bib:chen-boghosian-coveney-nekovee:2000] \(\nu^\sigma
 = (\tau^{\sigma}- 1/2)/\beta_0\), with \(\beta_0=3\) for the 19 velocity
 vector model implemented in the present version of the code. In case of
 binary applications these parameters also control the immiscibility of
 the system. For example, in a binary system with equal densities of oil
 and water, equal relaxation times \(\tau_b=\tau_r=\tau\) and equal masses
 ` amass_b=amass_r=m`, the theoretical critical value of water-oil
 coupling \(g_{br}^c\) beyond which the system phase separates is given by
 [@bib:shan-chen:1993; @bib:shan-chen:1994]
 \(g_{br}^{c}= m(\tau-1/2)/(\tau b \bar{n})\), where \(b\) is the number of
 non-zero velocity vectors and \(\bar{n}\) is the average density of the
 fluid.
