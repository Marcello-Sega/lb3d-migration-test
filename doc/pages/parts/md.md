title: Molecular dynamics

Molecular Dynamics
==================

Molecular Dynamics (MD) introduces discrete particles, that are tracked
outside the discrete Lattice Boltzmann lattice. MD particles have
translational and rotational degrees of freedom. Their trajectories
are forward-integrated, following Newton's equation of motion.

Using MD in NSKG consists of four steps:

1. The MD module is configured in the `/md_input/` namelist.
2. Particles (location, radius, coupling, interaction class) are defined in an additional file, given by `MD%geometry_file`.
3. Particle-particle and particle-wall interactions (pair potentials) are defined in an additional file, given by `MD%potential_file`.
4. The simulation outputs particle properties (location, velocity, force) for further use, post-processing, and visualisation.



## Concepts
MD particles are biaxial ellipsoids

:  In NSKG, general particles are implemented as ellipsoids of revolution, i. e. with an equatorial radius `R_orth`, a polar radius `R_para`, and one axis that defines the spatial orientation of the particle. The radii are properties of single particles, so you can create **polydispersity** by initialising particles with different radii.


Spheres

:  As a very common special case of ellipsoids, `R_orth=R_para` means spherical particles, for which the orientation has no impact on the interactions. For convenience, when you leave `R_para` undefined or set it to a negative value, NSKG automatically sets it to `R_orth`, creating spherical particles.


Orientation

:  We have two ways of defining the orientation of MD particles:
   `orientation` is the vector pointing in the axis direction (this is not a complete definition of orientation, and leaves one of the Euler angles undefined);
   alternatively, the code lets you specify the whole `quaternion`, that defines exact spatial orientation unambiguously and without gimbal lock.
   Enable orientation tracking with the `MD%dump_orientation` or `MD%dump_quaternion` properties of the `&md_input/` namelist.


   @TODO space-fixed vs. body-fixed frame of reference


Centroid-based vs. Surface-based interactions

:  Interaction distances are either defined from the centre of the particle, or from its surface. 


Static obstacles

:  One special type of MD particles is *static obstacles*, that interact with the fluid and other
   particles with regular MD, but are fixed (ignored by the integrator), and (more importantly)
   are allowed to overlap. Choose the parameter `obstacle=T` in the particle namelist to enable
   this feature for a particle.



### Coupling
@TODO Ladd coupling in a nutshell




Close contact correction:
When there is no fluid node left between the surfaces of two Ladd particles, a correction is implemented during the `bounceback` procedure. During the standard bounceback procedure if a surrounding node is found to belong to a neighbouring particle (and not fluid), then a dummy Boltzmann distribution is initialized at that surrounding node with the velocity of the surface node of the neighbouring particle. For the correction term, the geometric mean of average fluid densities at the intial and surrounding node is used to make the contributions symmetric.  No bounceback correction is done to the populations (as the surrounding nodes are not real fluid nodes).

Without this correction, a strong attractive force is observed between two Ladd particles in close contact, similar to depletion/entropic forces, with a pronounced step-like shape as a function of the distance (because it depends on the number of contacts)



@TODO Friction/point coupling in a nutshell (see also [in-depth point particle page](../../|page|/parts/md_friction.html))







## NSKG MD User interface

### Input: MD particle specification

```fortran
{!../media/md_particles.nml!}
```

#### Density

@TODO Density limitation from numerical stability [@bib:ladd-verberg:2001]

@TODO Namelists, references to interaction matrix etc.

@TODO external file for initial positions, specify size, mass/density, inertia, orientation, linear & angular velocity, how to hold certain components constant


#### Interactions

MD particles interact with each other, and with static objects, via [interactions (pair potentials)](../../|page|/parts/interactions.html).

@TODO Refer to the [interaction matrix matrix](|gitlab|/-/wikis/rewrite/interaction-matrix-matrix), once ported somewhere here.


### Output: Get MD information from NSKG simulations

@TODO trajectory file




----------------------

## Historical notes, tombstones, etc.

There is a (likely incomplete) [MD history page](../|page|/md_history.html).

The interface for lubrication corrections used to be located within the Ladd coupling part of MD,
but nowadays it is implemented within the generic interaction framework, at
[[nskg_potential_lubrication]].






A list of tests regarding MD
----------------------------

- [`md/follow-velocityfield/ladd-2-comp`](|gitlabfile|/tests/md/follow-velocityfield/ladd-2-comp)
- [`md/follow-velocityfield/fix-particle-ladd`](|gitlabfile|/tests/md/follow-velocityfield/fix-particle-ladd)
- [`md/follow-velocityfield/friction`](|gitlabfile|/tests/md/follow-velocityfield/friction)
- [`md/follow-velocityfield/ladd`](|gitlabfile|/tests/md/follow-velocityfield/ladd)
- [`md/momentum-conservation/momentum-kick-fluid`](|gitlabfile|/tests/md/momentum-conservation/momentum-kick-fluid)
- [`md/momentum-conservation/momentum-kick-particle-2comp`](|gitlabfile|/tests/md/momentum-conservation/momentum-kick-particle-2comp)
- [`md/momentum-conservation/momentum-kick-point-particle`](|gitlabfile|/tests/md/momentum-conservation/momentum-kick-point-particle)
- [`md/momentum-conservation/momentum-kick-particle`](|gitlabfile|/tests/md/momentum-conservation/momentum-kick-particle)
- [`md/momentum-conservation/momentum_and_mass`](|gitlabfile|/tests/md/momentum-conservation/momentum_and_mass)
- [`md/lees-edwards-bc/single_particle_transverse_vel`](|gitlabfile|/tests/md/lees-edwards-bc/single_particle_transverse_vel)
- [`md/md-substrate-friction/friction_between_particles_and_substrate`](|gitlabfile|/tests/md/md-substrate-friction/friction_between_particles_and_substrate)
- [`md/friction-coefficient/ladd-relative-velocity`](|gitlabfile|/tests/md/friction-coefficient/ladd-relative-velocity)
- [`md/friction-coefficient/ladd`](|gitlabfile|/tests/md/friction-coefficient/ladd)
- [`md/moving-plane/repulsive_plane`](|gitlabfile|/tests/md/moving-plane/repulsive_plane)
- [`md/potentials/interpolated`](|gitlabfile|/tests/md/potentials/interpolated)
- [`md/potentials/lj`](|gitlabfile|/tests/md/potentials/lj)
- [`md/potentials/lj-ladd-rock`](|gitlabfile|/tests/md/potentials/lj-ladd-rock)
- [`md/potentials/lj-rocks`](|gitlabfile|/tests/md/potentials/lj-rocks)
- [`md/potentials/magnetic`](|gitlabfile|/tests/md/potentials/magnetic)
- [`md/potentials/lj-ladd`](|gitlabfile|/tests/md/potentials/lj-ladd)
- [`basic/md_rock_input/md_input`](|gitlabfile|/tests/basic/md_rock_input/md_input)
- [`basic/md_rock_input/rock_input`](|gitlabfile|/tests/basic/md_rock_input/rock_input)
- [`basic/md_rock_input/md_rock_input`](|gitlabfile|/tests/basic/md_rock_input/md_rock_input)
