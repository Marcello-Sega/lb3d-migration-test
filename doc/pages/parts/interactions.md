title: Interactions

# Interactions

## Scope
Offlattice objects (MD particles, IBM and EBF meshes) can be configured to experience forces (momentum
transfer) from other actors within the simulation (interaction between different instances of the same
type, but also between different types, as well as on-lattice structures (rocks)).


@TODO Port the [interaction matrix matrix](|gitlab|/-/wikis/rewrite/interaction-matrix-matrix) here, including context (in particular [[compute_pair_forces_mesh_rock]] and its mesh-Ladd-broken-by-design note).


The interaction is set up on two layers:

1. Definition of interaction partners: all objects which are able to interact, have a `types` property, that is populated by the entries of the `inter%potential_file` file in the `/interactions/` namelist. See [[nskg_interactions_class]] for more details.
2. For each pair, the desired type of interaction (force as a function of distance) can be chosen.

## The pair potentials available in NSKG

### Smooth pair potentials (extended interactions)
* [[nskg_potential_coulomblike_class]]
* [[nskg_potential_lj_class]]
* [[nskg_potential_vdw_class]]

### Pair potentials with finite range (contact interactions)
* [[nskg_potential_linear_class]]
* [[nskg_potential_hertz_class]]
* [[nskg_potential_wca_class]]

### Specialized hydrodynamic interactions
* [[nskg_potential_capillary_class]]
* [[nskg_potential_lubrication_class]]

### Historically present interactions
This section is a *tombstone* for a number of interactions, that were in use in previous versions of the sources.

* **BPrH, BPrHaG – Berne-Pechukas Repulsive Hooke ± attractive Gaussian:** Historically used to model ellipsoids in MD, superseded by IBM particles
* **DLVO – Derjaguin-Landau-Verwey-Overbeek**
* **Gay-Berne**: LJ modification for ellipsoids
* **Fullerene**
* **Interpolation** from potential data supplied from an external data file
* **Magnetic**

We provide this list to remind that historic versions of NSKG have made use of these, hence there is a code base to start with, in case a re-introduction of one of these is desired. Keep in mind, though, that some of these forces have been subject to ad-hoc modifications, and restorations should start with a clean-room design.

## Special notes for Molecular Dynamics
### Forcing points: Surface vs. centroid
For MD particles with finite radius (Ladd), you can choose if the distance between interaction partners should be measured between surfaces, or the centres of mass.

Surface interactions (`distances="surfaces"`)

:  The **gap distance** between the surfaces determines the interaction distance. Forces formulated for surfaces (van der Waals, hydrodynamic interactions like capillary force and lubrication correction) are formulated to act from the surface. It is also useful for ad-hoc short-ranged repulsive forces, that avoid too close contact between particles.

Surface interactions with hydrodynamic correction (`distances="hydrodynamic_surfaces"`)

:  This also uses the gap distance, but **extends effective particle radii by 0.5 LU**. For flat walls, any bounce-back rule fullway or halfway (as implemented here, see @TODO add reference to [@KrugerPhD] section 5.3.3.2 (or 5.3.2 ???), and [@Ladd:1994]) places the hydrodynamic boundary condition about 0.5 LU away from the solid boundary node. For non-flat boundaries, this gets more complicated; the hydrodynamic boundary for spheres is at a distance from 0 up to 0.5 LU further away from the center than the radius used to define the particle @TODO add reference to A.J.C. Ladd, J. Fluid Mech., 271, 311 (1994) doi:10.1017/S0022112094001783.  As a practical note, simulations profit from a gap that leaves fluid nodes in between Ladd particles. This option increases numerical stability for cases like dense suspensions in the absence of strong repulsive forces.

Note, however, that this is effectively a modification of particle sizes, which modifies the range of forces, and slightly changes the aspect ratio of ellipsoids.

Central forces (`distances="centers"`)

:  The interaction is calculated from **centroid-to-centroid distance**. For MD, this makes most sense for long-range interactions like Coulomb forces, where this is necessary to restore a proper potential well, and identical far field between point and Ladd particles.


## Acceleration limiter

A well-known annoyance in MD and mesh-force settings are force spikes, where acceleration on
particles or mesh vertices exceed the magnitudes that the integrators can deal with before
running into stability issues.
This is in particular an issue whenever forces are formulated with fast diverging terms (e. g.
VdW or the lubrication correction).

### Choice and parameters
A force/acceleration is considered excessive when its magnitude exceeds `inter%acc_limit` in
the `/interactions/` namelist.
NSKG will by default terminate (with an insightful error message) should this situation be reached.

You can, however, circumvent this safeguard, by setting the switch `inter%acc_limit_rule` to
`"rescale"`. Then the acceleration limiter will scale down the force vector in a way that the
magnitude of the acceleration equals `inter%acc_limit`. Whenever an interaction partner is
accelerated beyond `inter%acc_limit`, a warning will be printed on the commandline (or into the
log if configured so). In the end, a report will be printed, that summarises the number of
necessary scale-down-force events relative to the number of time steps.

Additionally, you could set `inter%acc_limit_rule = 'none'`, which disables all checks for
acceleration limits (but not the hard limit of the integrator!) and NaNs (for testing purposes,
or in case it becomes relevant for performance).

### Purpose, scope, limitations
The acceleration limiter is more flexible than ad-hoc force limitations (that used to be built
into each interaction separately). It can properly handle multiple forces (it works on the sum,
not on the individual force contributions). Its effect is limited to a temporary invalidation of
interaction forces (deviations from the analytical formula), and it does not affect hydrodynamic
forces.

This means that, in rare cases, when interaction forces and hydrodynamic forces are acting
into the same direction, even the force limiter cannot prevent the (still present) hard limit of
the integrator to terminate the simulation.

At that point, at the very latest, you should reconsider design changes to your system (reduce
density, reduce interaction strength, exclude pathological initial conditions, improve mesh
resolution). Apparently, your setup creates forces that exceed the limits of what the stability
of the integrator can handle, and NSKG is not an appropriate tool for this purpose.


## For programmers: Style guide for interactions
An interaction, in the context of NSKG, is an abstraction layer over the concept of pair potentials.
It is the force that is acting at some distance \(r\), and as a such, does in general not need any extra information about the interaction partners.

The relation between the interaction distance \(r\) and the actual geometry of the simulation is not determined by the interaction, but by the code using it (MD, IBM, …).


### Fortran module, Inheritance, Documentation
@TODO Common properties: cutoff, types

* The interaction type `X` is coded as the Fortran module `nskg_potential_X_class`, in its own file `libnskg/src/core/potentials/nskg_potential_X.F90`. The file starts with a module comment, that provides a terse, but substantial **scientific context** for the potential (at least an original publication, notes on the physical idea, applicability, etc.).
* The potential is called `nskg_potential_X`, inheriting from `nskg_potential`.
* Additional parameters of the interaction are introduced as member variables of the `nskg_parms_potential_X` type, that is inherited from `nskg_parms_potential`. **Place descriptions of the parameters here, as post-fix comments to the variables.**
* We have one special instance of `nskg_parms_potential_X`, that is `default_parms`, which contains the fallback values whenever a value is not given in the interaction input file.
* The member subroutine `read_nml_X` creates the parameter variables and populates them with their value.


This policy may change in the future (with tightening bonds to the browseable manual), but it is a common standard to start with in a coherent manner.
