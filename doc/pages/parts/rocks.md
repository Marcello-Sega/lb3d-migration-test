title: Static obstacles

### Obstacle files
 
 An obstacle matrix may be specified in the `obs_file` variable. This
 contains the name of an ASCII file,
 
 @TODO Aren't rock files HDF5 these days?
 
 in turn containing a number of
 lines. Each line contains three integer coordinates, followed by the
 value `1` or `0`, corresponding to an obstacle at that site, or no
 obstacle. Alternatively, an XDR or HDF5 file can be supplied. If XDR
 file is supplied, compiler `-DUSEXDRF` should be used, otherwise it will
 not read `rock_colour`. Note that by default the program will read the
 obstacle file from the `nskg/rocks` directory.
 
 <!---
 
 begin of former user-wetting.tex
 
 000370a0-fc2a-4e75-b8df-eaaf530d4ffc
 
 -->
 
 Moreover, we can specify the wettability of the obstacle. In our code,
 Shan-Chen model is used for multicomponent simulations, which provides a
 convenient environment for tuning fluid-wall interaction. There are
 various ways to implement fluid-wall interactions, in our code four ways
 are implemented.
 
 1.  Virtual fluid density based interaction (by Jens and Sebastian,
     Tested):
     $$F_w =  - G_f \Psi(\mathbf{x}) \sum_{\alpha} \Psi(\rho_w) s(\mathbf{x} + e_{\alpha}) e_{\alpha}\label{eq:user-wetting-method-SchHar}$$
     where \(s(\mathbf{x} + e_{\alpha})\) is a switch function, which is
     equal to \(1\) or \(0\) for solid or a fluid phase, respectively.
     \(\rho_w\) is the virtual fluid density in wall, \(G_f\) is the
     interaction strength for fluid-fluid interaction. Disadvantage: the
     interaction force is copuled with fluid-fluid interaction, because
     it uses \(G_f\) parameter.
 
     Parameters required to use this model:
 
     -   `zeroforceoffset` = `.false.` (!Attention: by default it is
         true.)
 
     -   `obs_file` = '...', in this file \(\rho_w\) is set, which is
         `rock_colour` in the code. There is offset -\(5\) by reading
         `rock_colour` from rock file, `rock_colour` = \(5.0\) is
         neutral/equally wetting, no force interaction between fluid and
         wall, `rock_colour` \(> 5.0\) is red fluid wetting, `rock_colour`
         \(< 5.0\) is blue fluid wetting.
 
     -   `g_br` will be automatically read from Shan-Chen model
         parameters.
 
 2.  Density based interaction (by Qingguang, Tested)
     $$F_w =  - G_w \Psi(\mathbf{x}) \sum_{\alpha}  s(\mathbf{x} + e_{\alpha}) e_{\alpha}
     \label{eq:density-xie}$$ where \(G_w\) is the interaction strength for
     fluid-wall interaction, independent from \(G_f\). This model is
     similar with that in Huang's paper. Parameters required to use this
     model:
 
     -   `g_wr`, `g_wb`
 
     -   `g_value_double = .true.`, the code reads two rock files,
 
         -   `obs_file_r ` for `g_wr`
 
         -   `obs_file_b ` for `g_wb`
 
         -   Attention: no offset -\(5\).
 
     -   `gw_wet_global = .true.`, `g_wr`, `g_wb` from inputfile are used
         (No offset). The rock files are only used to generate the rock
         geometry.
 
     -   `gw_wet_global = .false.`, `g_wr`, `g_wb` from rock files are
         used (Offset \(-5\)).

     - `gw_wet_global` has a new name of `PSI%wetting_scheme = 'global'`

 
     Benckmark: \(g_{br} = 0.1\), \(\rho_r^{major}=\rho_b^{major} = 0.7\),
     \(\rho_r^{minor}=\rho_b^{minor} = 0.042\), \(g_{wb}=-g_{wr}\) We can
     predict the contact angle with followed formula,
     $$\cos (\theta) = \frac{g_{wb}-g_{wr}}{g_{br}\left(\Psi(\rho_r^{major})-\Psi(\rho_b^{minor})\right)/2}$$
     The general behaviour of the wall parameter vs. the resulting
     contact angle is shown in figure
     [3](#fig:wetting-den-xie){reference-type="ref" reference="fig:wetting-den-xie"}.
 
     ![Contact angle vs. `g_wr-g_wb` for the method described by the above equation.](../|media|/plots/wetting-den-xie.png)

     @warning 1. Determine if the figure is needed any more. If yes: 2. Redo data (and include it), 3. Convince ford to include figures inside lists.
 
 
     `rock_colour_init` (`logical`, `.false.`)
 
     :   If set to `.true.`use the parameter `rock_colour` to specify the
         colour for the rock sites for both components. If the aboved two
         ways are used to control contact angle, this parameter should be
         set to `.false.`
 
     `rock_colour` (`real`, `0.0`)
 
     :   When using `rock_colour_init = .true.`: Specify the colour for
         the rock sites for both components.
 
 <!---
 
 end of former user-wetting.tex
 
 -->
