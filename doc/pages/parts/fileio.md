title: File Input and Output


# Input: Instruct NSKG to do things
## Namelists, General Input file
The project makes extensive use of Fortran's **namelist** feature: at runtime, we can read values from text files (Fortran syntax) into variables.

The NSKG configuration takes place by an **input file**, that is, a collection of namelists within a file, that is commonly (but not necessarily) called `input`. This file is passed to the command line of the binary (see [**Invocation**](|page|/../invoking_nskg.html)).
The input file contains the entire system set-up, except for information supplied by extra files (see below).

With the many situations that NSKG covers, the configuration options necessary for a given simulation set-up are very diverse; the complete **list of input parameters** is very long.

@TODO Refer to the automated namelist parameter overview in the manual, once operational


## Extra files (namelists) for special purposes
Apart from the main input file, NSKG makes use of the same namelist format to set up the geometry of off-lattice objects.

Particle positions/properties in Molecular Dynamics
: The `/md_input/` parameter in the main input file awaits the parameter `MD%geometry_file`, that defines a file name. The MD particle configuration takes place there.
Reading of the particle configuration is done in **set_md_particle_properties_from_namelist**, writing in **write_nml**.
<!-- TODO Find a fix for missing FORD links -->
<!-- TODO Reference to MD documentation here -->

Mesh geometry (IBM)
: The `/immersed_boundary/` namelist parameter `IB%geometry_file` specifies a file, that contains the mesh geometry as `mesh_properties` namelists.
<!-- TODO Reference to IBM documentation here -->

Interaction potentials
: The `/interactions/` namelist calls for a file `inter%potential_file`, that defines a file to read the pair potential configuration from. Used by both mesh (IBM) and MD.
<!-- TODO pair potential link -->

Walls
: `WL%walls_file` from the `/walls/` namelist, reads a file and gathers the wall configuration via the `/wall/` namelists there.
<!-- TODO I only found this in IBM context. Is this something IBM specific? -->

## Geometry input
Special field initialisations need HDF5 files at the size of the LB lattice as input.
These include the **obstacle files**, and flow initialisation from **velocity input**.



# Output: Gather data from simulations
Some namelist parameters modify the names/directories of output files:

* The **file tag** (`default` by default, tunable via the namelist parameter `SYST%file_tag` in the `/system/` namelist). It defines the unique name of your simulation campaign, hence controls (part of) the name of output files.
* `SYST%folder` is the directory to write scientific output files and logs into (default: current directory)
* `SYST%chkp_dir` is a subdirectory of `SYST%folder` where to store checkpoints (default: `.`, i. e. the current directory as well)


## Status, Logging, Diagnostics
* The primary response of NSKG happens over its command line output.
* An explicit log file can be specified via `SYST%logfile` (in the `/system/` namelist of the input file): then verbose output is written there instead of the terminal.
* A complete list of configuration parameters (all defaults, everything explicitly set via input file, and calculated during system initialisation) is written into the file `nskg.input`. This file name deliberately excludes the file tag; only use it for ad-hoc debugging, and expect it to be overwritten by subsequent simulation runs.


## Scientific output
For each observable, the `dump_n_*` tunables define the rate at which observables are dumped as files, for further analysis, post-processing, and visualisation.

Lattice fields (density, velocities, etc.) use HDF5 as an output format.
By convention, the HDF5 container files hold the data in a single dataset called `OutArray`, in Fortran (column-major) order z-y-x.
<!--Where input file alongside?-->

IBM meshes are dumped as VTK mesh files.

For Molecular Dynamics, the file format for trajectory output can be chosen by the parameter `MD%dump_format`.
In the easiest `'asc'` case, this is just a text file, that contains the x-y-z particle coordinates line-by-line.


<!-- Do we want/need to mention others? (XDRF, vtk, ASCII, binary, …) -->


## Checkpoints
NSKG supports checkpointing. The latest state of the system is (over)written into the subdirectory `SYST%chkp_dir` for easy restoring after program termination.
