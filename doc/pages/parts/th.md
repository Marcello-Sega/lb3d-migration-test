title: Thermal Lattice Boltzmann


# Thermal fluctuations
  --------------------

@TODO Check for overlap with [!626](|gitlab|/-/merge_requests/626).

 Thermal fluctuations can be re-introduced into Lattice Boltzmann. Using
 the MRT collision model (section
 [3.1](#subsec:LB-MRT){reference-type="ref" reference="subsec:LB-MRT"}),
 normal distributed forces are added to the modes
 [@bib:duenweg-schiller-ladd:2007]. These forces are rescaled by the
 \(\sqrt{3\rho k_\mathrm{B}T}\) and the root of the weight factors of the
 mode \(\sqrt{\mathbf{w}_i \mathbf{M}_{ki}^2}\). Here \(\mathbf{w}_i\) is the
 weight of the lattice vectors \(\mathbf{c}_i\). The forces on the
 hydrodynamic modes are also rescaled by
 \(\sqrt{1-\tau_{\text{bulk}}^{-2}}\) or
 \(\sqrt{1-\tau_{\text{shear}}^{-2}}\) respectively. This way the
 fluctuations happen on the lattice time scale and respect the Péclet
 number. With \(\tau_{\text{bulk}}\) and \(\tau_{\text{shear}}=1\), the
 variance of the \(x,y,z\) velocities is \(k_\mathrm{B}T\). A negative
 \(k_\mathrm{B}T\) disables the calculation of thermalisation.
 
 Thermal fluctuations will only be active if the MRT model with a
 positive \(k_\mathrm{B}T\) is chosen.



A list of tests regarding Thermal
---------------------------------

- [`lb/thermal-fluctuations/thermal_fluctuations`](|gitlabfile|/tests/lb/thermal-fluctuations/thermal_fluctuations)
- [`th/contact_angle_sukop`](|gitlabfile|/tests/th/contact_angle_sukop)
- [`th/heat_diffusion`](|gitlabfile|/tests/th/heat_diffusion)
- [`th/contact_angle`](|gitlabfile|/tests/th/contact_angle)
- [`th/laplace/laplace`](|gitlabfile|/tests/th/laplace/laplace)
- [`th/reactive`](|gitlabfile|/tests/th/reactive)
- [`th/laplace_multicomp/laplace_multicomp`](|gitlabfile|/tests/th/laplace_multicomp/laplace_multicomp)
- [`th/contact_angleEDM/contact_angleEDM`](|gitlabfile|/tests/th/contact_angleEDM/contact_angleEDM)
- [`th/laplaceEDM/laplaceEDM`](|gitlabfile|/tests/th/laplaceEDM/laplaceEDM)
- [`perf/th/1core`](|gitlabfile|/tests/perf/th/1core)
