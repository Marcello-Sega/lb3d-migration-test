title: MD: Friction particles

@TODO Leftover from the dev manual. rewrite-regression?

`dynamics` (`character(32)`, `'langevin'`)

:   Only for interaction='friction' (point particles).

    `'langevin'`

    :   “Langevin dynamics” or, for \(k_BT=0\) classical mechanics. See
        also 'thermal\_teleportation'.

    `'brownian'`

    :   “Brownian dynamics”, the particle inertia is neglected and the
        particle's velocity is adapting instantaneously to its terminal
        velocity, which depends on the fluids velocity and the friction
        force. For \(k_BT>0\), thermal fluctuations are included. Should
        not be used for two-way coupling to the fluid.

`thermal_teleportation` (`logical`, `.true.`)

:   Only for dynamics='langevin'. If `.true.`, for \(k_BT>0\), the thermal
    fluctuations are still included according to Brownian dynamics, i.e.
    instantaneous displacement of the particles, while interaction
    forces with the fluid and potentials includes inertia. This helps to
    decouple the thermal fluctuations from the fluid, as they are
    introduced externally and not as a result from fluctuations in the
    fluid.

    Disabling (`.false.`) leads to “true” Langevin dynamics. This is
    only tested with nofluiddynamics active or
    stokes\_on\_fluid=.false., and will most likely produce unphysical
    results if coupled to the fluid.

`randompartforce` (`logical`, `.false.`)

:   If `.true.`, a random term is added to the forces acting on the
    particles. It is not a Langevin-integrator! This serves only as a
    makeshift solution until thermal fluctuations on the fluid level are
    included. Ask Konrad Schwenke for details.
