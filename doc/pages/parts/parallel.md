title: Parallel

# Parallel performance
  --------------------

 We have implemented two parallel versions of the LB algorithm, one for
 binary immiscible systems and one for ternary amphiphilic systems.
 Parallel performance of both binary and amphiphilic version of the
 algorithm was benchmarked on two parallel platforms with totally
 different architectures: the CRAY T3E 1200E at CSAR and the SGI O2000 at
 Boston University. The CRAY T3E system is a distributed memory Massively
 Parallel Processor (MPP) machines (Dec Alpha EV6 600 MHZ processors with
 256 MBytes of memory each), whereas the SGI O2000 machines have
 cache-coherent Non-Uniform Memory Architecture (ccNUMA). All benchmark
 simulations were performed on \(64^3\) systems using the PFCHC lattice
 velocity vectors for each component and \(500\) time steps. Fluid
 densities at each lattice site and surfactant dipole moments were
 written out periodically at every \(50\) time step of the simulations. The
 performance (number of time steps per minute) of the binary and the
 ternary codes on both platforms are summarised in the table below.
 
   time steps/minute       |  2 CPU  |  4 CPU  |  8 CPU  |  16 CPU  |  32 CPU  |  64 CPU
   ----------------------- | ------- | ------- | ------- | -------- | -------- | --------
   LB/CRAY T3E (binary)    |   4.1   |  9.70   |  19.0   |   32.6   |   54.2   |  110.4
   LB/CRAY T3E (ternary)   |   2.1   |   4.3   |   7.7   |   16.7   |  34.30   |   59.8
   LB/O200 (binary)        |   6.7   |  12.5   |  23.0   |   44.3   |   80.5   |  133.3
   LB/O200 (ternary)       |   2.6   |   5.1   |   9.8   |   19.3   |   36.0   |   58.1


 
# Postprocessing with parallel HDF5 {#sec:hdf5}
  ---------------------------------

 The Hierarchical Data Format (HDF) [@bib:hdf5] provides a tool to
 generate highly portable, platform-independent data. Most postprocessing
 can be done with parallel HDF5 now (exceptions are small ascii dumps),
 as well as reading of rock files. No other part of the IO can be done
 through parallel HDF5, i.e. no checkpointing (currently exists in
 experimental form only) or input. All previous options for the
 postprocessed data output format can still be used as before. Note that
 using HDF for the output might speed up the IO considerably (depending
 on the grid size, number of processors used and number of IO timesteps)
 compared to the other options.
 
 NSKG version 6 has been tested with HDF5-1.8.4, HDF5-1.8.5, HDF5-1.8.9
 and HDF5-1.8.15-patch1 so far.
 
### Enabling postprocessing with HDF5
 
 HDF5 IO is the standard exchange format for scientific data. The floating
 point precision equals the one with which the application has been compiled
 (double by default); if desired, it can be downgraded to single-precision
 by setting the `dump_single_precision` flag present in LB, AD and their
 respective subsystems (SC, CG, TH).
 
### Including meta data
 
 HDF5 provides the possibility to add meta data to the raw data files.
 Each output file gets the meta data added to it. The metadata which are
 added to all postprocessed output files include:
 
 -   version of NSKG (including revision number),
 
 -   version of the HDF5 library,
 
 -   the username,
 
 -   the filename of the input file,
 
 -   the compiler flags used,
 
 -   the hostname,
 
 -   the start time and date of the simulation,
 
 -   the total number of processors,
 
 -   the way the grid is divided up by the number of processors,
 
 -   the decomposition,
 
 -   a copy of all relevant input files.



# Postprocessing and visualization
  --------------------------------
 
 If the internal postprocessor is not used, the output files will require
 postprocessing, both to convert them to a platform-independent format,
 and, in the case of fully parallel runs, to tie the outputs from all the
 processors together. Once this is done the resulting data files can be
 used to calculate various quantities, such as structure factor and
 pressure and/or perform visualization.
 
## Postprocessing fully-parallel output

 
 Move into the `code` directory, and type `../utils/post/postprocess`;
 the postprocessor will automatically determine the number of CPUs which
 were used, and generate output files. Once the postprocessing has been
 completed, the files ending in `.bin` may be deleted.
 
     % cd nskg/code
     % ../utils/post/postprocess
 
 A C program to process fully-parallel output is also available. This may
 not function under all platfroms, but is very fast:
 
     % cd nskg/code
     % ../utils/cpost/post
