title: Lattice-Boltzmann


## LB-MRT {#subsec:LB-MRT}
   ------

### Overview
 
 In the multi-relaxation-time
 scheme [@bib:pan-luo-miller:2006; @bib:lallemand-luo:2000] the collision
 operator in the lattice-Boltzmann method writes
 @TODO There [wa]s (?) a MAPLE file in the directory `nskg/doc/notes/MRT`, that explains step by step the contruction of the MRT algorithm.
 $$-\mathbf{S}\cdot\left(\mathbf{n}-{\mathbf{n}}^{\mathrm{\tiny{eq}}}\right)$$
 where \(\mathbf{S}\) is called collision matrix, whose eigenvalues are in
 the interval \([0,2]\). For the BGK operator, this matrix simplifies to
 \(\mathbf{S}=\frac{1}{\tau}\mathbb{I}\).
 
 It is possible to write the collision operator as a function of the
 stochastical moment \(\mathbf{m}\)
 $$-{\mathbf{M}}^{-1}\cdot\mathbf{S}\cdot\left(\mathbf{m}-{\mathbf{m}}^{\mathrm{\tiny{eq}}}\right)$$
 where the matrix \(\mathbf{M}\) represents the transformation between
 velocity space \(\mathbb(V)\) and the space of stochastical moments
 \(\mathbb{M}\) (It has an orthogonal basis, but is not normalized). The
 moment vector \(\mathbf{m}\) is defined by:
 $$\mathbf{m}=\mathbf{M}\cdot\mathbf{n}, \qquad {\mathbf{m}}^{\mathrm{\tiny{eq}}}=f(\rho,\mathbf{u})$$
 
 The transformed (3rd order expansion of the) equilibrium distribution
 \({\mathbf{m}}^{\mathrm{\tiny{eq}}}\) has for its components
 $$\rho^{\mathrm{\tiny{eq}}} =\rho, \qquad
 e^{\mathrm{\tiny{eq}}} = -11\rho+\frac{19}{\rho}(\mathbf{u}\cdot\mathbf{u}),
 \qquad \varepsilon^{\mathrm{eq}}=w_{\varepsilon}\rho+\frac{w_{\varepsilon j}}{\rho}(\mathbf{u}\cdot\mathbf{u}),
 \qquad w_{\varepsilon} = 3,\qquad w_{\varepsilon j} = - \frac{11}{2}$$
 $${j_{x}}^{\mathrm{\tiny{eq}}} = \mathrm{u}_{x}, \qquad
 {j_{y}}^{\mathrm{\tiny{eq}}} = \mathrm{u}_{y}, \qquad
 {j_{z}}^{\mathrm{\tiny{eq}}} = \mathrm{u}_{z}$$
 $${q_{x}}^{\mathrm{\tiny{eq}}}=-\frac{2}{3}+\frac{5}{2\rho^{2}}(\mathrm{u}_{y}^{2}+\mathrm{u}_{z}^{2}) \mathrm{u}_{x}, \qquad
 {q_{y}}^{\mathrm{\tiny{eq}}}=-\frac{2}{3}+\frac{5}{2\rho^{2}}(\mathrm{u}_{z}^{2}+\mathrm{u}_{x}^{2}) \mathrm{u}_{y}, \qquad
 {q_{z}}^{\mathrm{\tiny{eq}}}=-\frac{2}{3}+\frac{5}{2\rho^{2}}(\mathrm{u}_{x}^{2}+\mathrm{u}_{y}^{2}) \mathrm{u}_{z}$$
 $${p_{xx}}^{\mathrm{\tiny{eq}}} =\frac{1}{\rho} ( 2 {\mathrm{u}_{x}}^{2}-{\mathrm{u}_{y}}^{2}-{\mathrm{u}_{z}}^{2}), \qquad
 {p_{ww}}^{\mathrm{\tiny{eq}}}=\frac{1}{\rho}({\mathrm{u}_{y}}^{2}-{\mathrm{u}_{z}}^{2})$$
 $${p_{xy}}^{\mathrm{\tiny{eq}}}=\frac{1}{\rho} \mathrm{u}_{x} \mathrm{u}_{y}, \qquad
 {p_{xz}}^{\mathrm{\tiny{eq}}}=\dfrac{1}{\rho} \mathrm{u}_{x} \mathrm{u}_{z}, \qquad
 {p_{yz}}^{\mathrm{\tiny{eq}}}=\frac{1}{\rho} \mathrm{u}_{y} \mathrm{u}{z}$$
 $${\pi_{xx}}^{\mathrm{\tiny{eq}}} = w_{xx} p_{xx},\qquad
 {\pi_{ww}}^{\mathrm{\tiny{eq}}} = w_{xx} p_{ww},\qquad w_{xx} = -\frac{1}{2}$$
 $${m_{x}}^{\mathrm{\tiny{eq}}} = \frac{3}{2\rho^{2}}( {\mathrm{u}_{x}}^{2} ({\mathrm{u}_{y}}^{2}-{\mathrm{u}_{z}}^{2})), \qquad
 {m_{y}}^{\mathrm{\tiny{eq}}} = \frac{3}{2\rho^{2}}( {\mathrm{u}_{y}}^{2} ({\mathrm{u}_{z}}^{2}-{\mathrm{u}_{x}}^{2})), \qquad
 {m_{z}}^{\mathrm{\tiny{eq}}} = \frac{3}{2\rho^{2}}( {\mathrm{u}_{z}}^{2} ({\mathrm{u}_{x}}^{2}-{\mathrm{u}_{y}}^{2}))$$
 
 With the Eigenvalues of the transformation matrix
 \(w_{\varepsilon}, w_{\varepsilon j} \text{ and } w_{xx}\) as further free
 parameters. Together with the BGK collision matrix above, the given
 values recover the BGK method. A stability optimized 3-dimensional model
 uses \(w_{\varepsilon}=0, w_{\varepsilon j}=475/63 \text{ and } w_{xx}=0\)
 along with the collision
 matrix [@bib:dhumieres-ginzburg-krafczyk-lallemand-luo:2002]
 $$\begin{aligned}
 \nonumber
 \mathbf{S}&=&\mathrm{diag}(0,1/\tau_{\mathrm{bulk}},1.4,0,1.2,0,1.2,0,1.2,1/\tau,\\
 \nonumber
 && 1.4,1/\tau,1.4,1/\tau,1/\tau,1/\tau,1.98,1.98,1.98)\end{aligned}$$
 The kinematic viscosity \(\nu\) and the bulk viscosity \(\eta\) are:
 $$\nu=\frac{1}{6}(2\tau-1)\qquad\eta=\frac{1}{9}(2\tau_{\mathrm{bulk}}-1)$$
 The matrix \(\mathbf{M}\) writes $$M = \left(
     \begin{array}{rrrrrrrrrrrrrrrrrrr}
       1&  1&  1&  1&  1&  1& 1& 1& 1& 1& 1& 1& 1& 1& 1& 1& 1& 1&  1\\
       -11&-11&-11&-11&-11&-11& 8& 8& 8& 8& 8& 8& 8& 8& 8& 8& 8& 8&-30\\
       -4& -4& -4& -4& -4& -4& 1& 1& 1& 1& 1& 1& 1& 1& 1& 1& 1& 1& 12\\
       1& -1&  0&  0&  0&  0& 1& 1& 1& 1&-1&-1&-1&-1& 0& 0& 0& 0&  0\\
       -4&  4&  0&  0&  0&  0& 1& 1& 1& 1&-1&-1&-1&-1& 0& 0& 0& 0&  0\\
       0&  0&  1& -1&  0&  0& 1&-1& 0& 0& 1&-1& 0& 0& 1& 1&-1&-1&  0\\
       0&  0& -4&  4&  0&  0& 1&-1& 0& 0& 1&-1& 0& 0& 1& 1&-1&-1&  0\\
       0&  0&  0&  0&  1& -1& 0& 0& 1&-1& 0& 0& 1&-1& 1&-1& 1&-1&  0\\
       0&  0&  0&  0& -4&  4& 0& 0& 1&-1& 0& 0& 1&-1& 1&-1& 1&-1&  0\\
       2&  2& -1& -1& -1& -1& 1& 1& 1& 1& 1& 1& 1& 1&-2&-2&-2&-2&  0\\
       -4& -4&  2&  2&  2&  2& 1& 1& 1& 1& 1& 1& 1& 1&-2&-2&-2&-2&  0\\
       0&  0&  1&  1& -1& -1& 1& 1&-1&-1& 1& 1&-1&-1& 0& 0& 0& 0&  0\\
       0&  0& -2& -2&  2&  2& 1& 1&-1&-1& 1& 1&-1&-1& 0& 0& 0& 0&  0\\
       0&  0&  0&  0&  0&  0& 1&-1& 0& 0&-1& 1& 0& 0& 0& 0& 0& 0&  0\\
       0&  0&  0&  0&  0&  0& 0& 0& 0& 0& 0& 0& 0& 0& 1&-1&-1& 1&  0\\
       0&  0&  0&  0&  0&  0& 0& 0& 1&-1& 0& 0&-1& 1& 0& 0& 0& 0&  0\\
       0&  0&  0&  0&  0&  0& 1& 1&-1&-1&-1&-1& 1& 1& 0& 0& 0& 0&  0\\
       0&  0&  0&  0&  0&  0&-1& 1& 0& 0&-1& 1& 0& 0& 1& 1&-1&-1&  0\\
       0&  0&  0&  0&  0&  0& 0& 0& 1&-1& 0& 0& 1&-1&-1& 1&-1& 1&  0\\
     \end{array}
   \right)$$
 
### Usage
 
 The usage of the MRT collision step is set by the logical value of the
 `MRT`-entry. If no relaxation frequencies are given in the input-file
 the MRT algorithm recovers the BGK method, useful for testing. There are
 seven independent relaxation rates for each component *y* to be set by
 input-file options `taubulk_`*y*=1/s02\_*y*, `s03_`*y*, `s05_`*y*,
 `tau_`*y*=1/s10\_*y*, `s11_`*y*, `s14_`*y*, `s17_`*y*. If using a
 ternary system the relaxation of the dipole momentum is further
 controlled by the BGK-like relaxation time `tau_`d.
 
## Local Relaxation times
   ----------------------
 
 To capture phenomena in the Knudsen regime local relaxation times as
 discribed in [@bib:zhang-gu-barber-emerson:2006] can be enabled. The
 local relaxation time is then calculated as
 $$\tau = \sqrt{\frac{3 \pi}{8}} \frac{Kn N_{channel}}{1+0.7e^{\frac{y}{l}}}+ 0.5,$$
 where \(Kn\) is the Knudsen number, \(N_{channel}\) is the hight of the
 channel in lattice units, \(y\) is the distance of the current lattice
 node to the next wall and \(l\) is the mean free path of the molecules in
 the bulk flow. The calculation of the local relaxation time has to be
 done seperatly from the simulation. A tool can be found in
 `/nskg/utils/local_tau/`. This tool also calculates the distance for each
 lattice node to the nearest rock site. Files for a geometry file with
 name `geom.xdr` will be named `rockdistgeom.xdr` for the distance file
 and `tauposgeomcomp1` for the local relaxation times for component 1 and
 so on.
 


 @TODO Move the contents of this whole namelist documentation paragraph [[nskg_parms.F90]], find a sensible way to display them (keep in mind that this is some of the most important and most often needed parts of the whole documentation)! Move away the parts of this list that are not defined in this file (e. g. boundary condition definitions) into the respective subroutines/files.



## Relaxation time namelists {#relaxation-time}
   -------------------------
 
 We have implemented single and multi relaxation time schemes, as well as
 non homogeneous relaxation times. To use the different options, one
 needs to add `/relax_<type>/` with additional parameters as shown below.
 
### Homogeneous single relaxation time
 
 For the classical single relaxation time (SRT), use the following
 namelist `/relax_homogeneous/` and the parameter(s) below:
 
 `tau` (`real`, `1.0`)
 
 :   homogeneous single relaxation time
 
### Multi-relaxation time
 
 The multi-relaxation time (MRT) lattice Boltzmann is known for its
 superior numerical stability over the BGK model, which can allow us to
 reach smaller values of viscosity in comparison to the BGK as discussed
 in the end of this subsection. To switch to the MRT scheme, use the
 following namelist `/relax_mrt_homogeneous/` and the parameter(s) below:
 
 `omega` (`real`, `1.0`)
 
 :   MRT relaxation frequency
 
 `omegabulk` (`real`, `1.0`)
 
 :   MRT bulk relaxation frequency
 
 `s03` (`real`, `1.0`)
 
 :   element from the collision matrix (see section
     [3.1](#subsec:LB-MRT){reference-type="ref" reference="subsec:LB-MRT"} for details.)
 
 `s05` (`real`, `1.0`)
 
 :   element from the collision matrix (see section
     [3.1](#subsec:LB-MRT){reference-type="ref" reference="subsec:LB-MRT"} for details.)
 
 `s11` (`real`, `1.0`)
 
 :   element from the collision matrix (see section
     [3.1](#subsec:LB-MRT){reference-type="ref" reference="subsec:LB-MRT"} for details.)
 
 `s17` (`real`, `1.0`)
 
 :   element from the collision matrix (see section
     [3.1](#subsec:LB-MRT){reference-type="ref" reference="subsec:LB-MRT"} for details.)
 
     To recover the BGK model, set `omega=1`, `omegabulk=1.19`,
     `s03=1.4`, `s05=1.2`, `s11=1.4` and `s17=1.98`. With those
     parameters, we can reach a maximum velocity of \(0.19\), which
     corresponds to a Mach number of approximately \(0.33\), and values of
     kinematic viscosity larger than \(2.54 \cdot 10^{-3}\) as explained in
     [@bib:dhumieres-ginzburg-krafczyk-lallemand-luo:2002].
 
### Liquid-vapor phase
 
 For a liquid-vapor phase, use the following namelist
 `/relax_liquid_vapor/` and the parameter(s) below:
 
 `tau_liquid` (`real`, `1.0`)
 
 :   relaxation time for the liquid phase
 
 `tau_vapor` (`real`, `1.0`)
 
 :   relaxation time for the vapor phase
 
 `rho_liquid` (`real`, `1.0`)
 
 :   density for the liquid phase
 
 `rho_vapor` (`real`, `0.01`)
 
 :   density for the vapor phase
 
### Shear dependent non-homogeneous relaxation time (power law fluid)
 
 Here we consider a power law fluid where the local relaxation time (or
 viscosity) depends on the local shear rate such that
 $$\tau_{t} = 0.5 + (\tau_{0}-0.5)|\dot{\gamma}|^{n-1},
     \label{eq:powerlaw_relax}$$ with
 \(\dot{\gamma} = 2\sqrt{\sum_{\alpha,\beta=1}^3 S_{\alpha,\beta} S_{\alpha,\beta}}\)
 and \(S_{\alpha,\beta}\) being the local deviatoric stress. Locally
 varying \(\tau\), particularly at higher driving forces, compromises the
 initial stability of the simulations [@sullivan2006simulation]. For this
 reason we apply a feedback on the relaxation time such that
 $$\tau_{t}^{*} = \tau_{t-1} - \lambda(\tau_{t} - \tau_{t-1})$$ In the
 case of a biperiodic system confined by two parallel walls placed at
 \(x=\pm W/2\), and for a flow driven by a constant pressure gradient (or
 body force) \(\partial_z p \equiv f_z\) along the *z-*direction, the
 velocity profile for a power law fluid is
 $$u_z(x) = \frac{n}{n+1}\left (-\frac{f_z}{k}\right )^{1/n}\left (\frac{W}{2} \right )^{1+1/n} \left [1- \left (\frac{|x|}{\frac{W}{2}}\right )^{1+1/n} \right ],$$
 with \(k\) being the consistency index. The maximum velocity reads as
 $$u_{max} = \frac{n}{n+1}\left (-\frac{f_z}{k}\right )^{1/n}\left (\frac{W}{2}\right )^{1+1/n}$$
 For the non-homogeneous shear dependent single relaxation time for a
 power law fluid, use the following namelist `/relax_power_law/` and the
 parameter(s) below:
 
 `tau` (`real`, `1.0`)
 
 :   Initial relaxation time corresponding to \(\tau_0\) in equation
     [\[eq:powerlaw\_relax\]](#eq:powerlaw_relax){reference-type="eqref" reference="eq:powerlaw_relax"}.
 
 `flow_index` (`real`, `1.0`)
 
 :   flow index parameter corresponding to \(n\) in equation
     [\[eq:powerlaw\_relax\]](#eq:powerlaw_relax){reference-type="eqref" reference="eq:powerlaw_relax"} which defines the nature of the
     fluid. For Newtonian fluid, \(n=1\). For a shear-thinning fluid,
     \(n<1\). For a shear-thickening fluid, \(n>1\).
 
 `lambda` (`real`, `0.1`)
 
 :   a factor used to adjust the initial stability of the simulations.
 
 **N.B.** For \(n=1\), \(k=\rho\nu\) where \(\rho\nu\) is the dynamical
 viscosity. The parameter \(\tau_0\) in equation
 [\[eq:powerlaw\_relax\]](#eq:powerlaw_relax){reference-type="eqref" reference="eq:powerlaw_relax"} can be related to \(k\) such that
 \(\tau_0 = \alpha k/(\rho c_s^2) + 0.5\), with \(c_s\) being the lattice
 speed of sound and \(\alpha\) a scaling factor that needs to be adjusted
 such that the maximum velocity from the simulation and analytic matches.
 So far, I did not find a better solution where the scaling factor can be
 omitted. The literature discussing power law fluids with the lattice
 Boltzmann method are relatively scarce and not that thorough (typical
 from that time). Examples on the choice of parameters to simulate
 shear-thickening, thinning and Newtonian fluids respectively are
 provided in the test folders under the subdirectory lb/relax.



## Body force modules {#fixed-input:force}
   ------------------

@TODO Nowadays handled by the `/extforce_*/` namelists from [[nskg_external_force_class]].
 
 The `nskg_force` array offers a more flexible and better readable way to
 apply body forces than the earlier implemented acceleration options
 `g_accn` etc. . While `nskg_force` can be employed indirectly by other
 modules such as `nskg_md_fluid_friction`, implementations that provide
 forcing functionality directly to the user should be selectable by
 setting `force=<forcing-type>` in `/fixed_input/` and provide possibly
 further options in a separate namelist `/force_<forcing-type>/`.
 
 `force=='constant'`
 
 :   This module implements constant forcing in a predefined, rectangular
     chunk of the system. This is similar to what can be done with
     `g_accn` etc. but note that here we have a force instead of an
     acceleration. Implementation for more than one component is still
     missing. Also, this module supports automatic steering towards a
     desired rate of mass flow in the \(z\)-direction. Only the
     \(z\)-component of the force is steered. The idea is to update
     $$\mathtt{force(3)}
         =
         \mathtt{previous\_force(3)}\times
         \frac{\mathtt{auto\_massfluxz}}{\mathtt{actual\_massflow}}$$
     each `n_auto` time steps. The user is responsible to set `n_auto` to
     a reasonable value or else this will not work at all. The new value
     for `force(3)` is printed to stdout, in case of restoring from a
     checkpoint, it needs to be copied manually into the input file
     replacing the initial value for `force(3)`. See section
     [5.6.1](#force-constant){reference-type="ref" reference="force-constant"} for the full parameter list.

To be moved to [[nskg_external_force_class.F90]] subroutine `init_external_force(this, geometry, rocks, fields)`
 
 `force=='kolmogorov'`
 
 :   This module implements a force $$\mathtt{amplitude}\times
         \sin
         \frac
         {2\pi(x-0.5)}
         {\mathtt{nx}}$$ in \(z\)-direction that is sinusoidally modulated
     in \(x\)-direction, producing Kolmogorov flow. Implementation for more
     than one component is still missing. See section
     [5.6.2](#force-kolmogorov){reference-type="ref" reference="force-kolmogorov"} for the full parameter list.

To be moved to [[nskg_external_force_class.F90]] subroutine `init_external_force(this, geometry, rocks, fields)`
 
 `force=='pulsation'`
 
 :   This module implements a pulsatile force
     $$\mathtt{force\_puls} \times [1 + \mathtt{amplitude}\times
         \cos(\mathtt{frequency} \times \mathtt{time})]$$ in time. The
     implementation is based on section
     [5.6.1](#force-constant){reference-type="ref" reference="force-constant"}. See section
     [5.6.3](#force-pulsation){reference-type="ref" reference="force-pulsation"} for the full parameter list.


To be moved to [[nskg_external_force_class.F90]] subroutine `init_external_force(this, geometry, rocks, fields)`
 
### The `force_constant` namelist {#force-constant}
 
 The namelist `force_constant` contains the following parameters:
 
 `auto_massfluxz` (`real`, `0.0`)
 
 :   Targeted total mass flow in whole plane `z==1`.
 
 `force` (`real(3)`, `(/0.0,0.0,0.0/)`)
 
 :   Force to be applied per LB site.
 
 `maxx` (`integer(3)`, `(/-1,-1,-1/)`)
 
 :   Maximum lattice position to apply force, negative values are
     automatically replaced by the corresponding maximum coordinates.
 
 `minx` (`integer(3)`, `(/-1,-1,-1/)`)
 
 :   Minimum lattice position to apply force.
 
 `n_auto` (`integer`, `0`)
 
 :   Time step interval after which force is updated (0 turns steering
     off).
 
 `constant_f_y_min` (`real`, `0`)
 
 :   Y-coordinate beyond which body force acts.
 
 This namelist contains also the parameters to set a gaussian potential
 attracting particles towards the x-z-plane
 \(U(y) = E_0 \cdot \exp[-(y-y_0)^2/2w^2]\)
 
 `gaussian_plane_active` (`logical`, `.false.`)
 
 :   Use this potential.
 
 `gaussian_plane_pos` (`real`, `0`)
 
 :   Position \(y_0\) of the attractive plane.
 
 `gaussian_plane_E` (`real`, `0`)
 
 :   Energy constant \(E_0\).
 
 `gaussian_plane_c` (`real`, `0`)
 
 :   Cut-off distance of plane potential.
 
 `gaussian_plane_sqwidth` (`real`, `0`)
 
 :   Width of the gaussian \(w^2\)
 
### The `force_kolmogorov` namelist {#force-kolmogorov}
 
 The namelist `force_kolmogorov` contains the following parameters:
 
 `amplitude` (`real`, `0.0`)
 
 :   Maximum absolute value of force.

To be moved to [[nskg_external_force_class.F90]] subroutine `read_namelist_external_force_kolmogorov(this)`
 
### The `force_pulsation` namelist {#force-pulsation}
 
 The namelist `force_pulsation` contains the following parameters:
 
 `force_puls` (`real(3)`, `(/0.0,0.0,0.0/)`)
 
 :   Force to be applied per LB site.
 
 `amplitude` (`real`, `0.0`)
 
 :   Amplitude of the pulsation.
 
 `frequency` (`real`, `0.0`)
 
 :   Frequency of the pulsation.

To be moved to [[nskg_external_force_class.F90]] subroutine `read_namelist_external_force_kolmogorov(this)`


# Numerical instabilities
  =======================

 Numerical instabilities were found to occur in the simulations of both
 ternary and binary systems. These instabilities happen when the force
 coupling constant \(g_{br}\), \(g_{bs}\) and \(g_{ss}\), or the mean
 densities, were increased beyond certain threshold values. They are a
 consequence of the forcing term which can become large enough to make
 the RHS side of the lattice-Boltzmann equations (see e.g. Eq. 14 in
 [@bib:nekovee-coveney-chen-boghosian:2000]) negative. Instabilities can
 also occur for a single component system, provided an external force
 (such as gravity) is preset which is large enough to make the RHS of the
 lattice-Boltzmann equations negative. We think that these instabilities
 are inherent to *any* lattice-Boltzmann scheme which contains a body
 force term. Underlying the lattice-Boltzmann scheme is the assumption
 that the single-particle distribution functions are close to a local
 Maxwellian and can therefore be expanded around such a a distribution in
 powers of \(U/c_s\), with \(U\) the fluid velocity and \(c_s\) the speed of
 sound. This assumption is violated when the acceleration of the
 particles by the body force, be it external or mean-field, is large
 enough such that a far-from equilibrium distribution is reached. In
 exploring the parameter space of the model, therefore, the possibility
 of these instabilities should be taken into account.
 
 In the case of the binary water-surfactant system in \(3D\) and for water
 and surfactant densities which are \(\sim 1\) the region of stability
 corresponds to \(|g_{bs}|< 0.01\), \(-0.001\leq g_{ss}<0\) (Note that
 \(g_{ss}>0\) results in anti-alignment of surfactant directors).
 Performing several simulations for \(16^3\) binary system indicates that,
 for fixed \(g_{bs}\) the stability region can be extended to larger values
 of \(g_{ss}\) by reducing the average surfactant concentration in the
 system. For example, the binary water-surfactant system is numerically
 stable for \(f_b=f_g=1\), \(g_{bs}=-0.01\), \(-0.001 \leq g_{ss}\) and remains
 stable if \(g_{ss}\) is increased (in absolute value) to \(-0.03\) while at
 the same time \(f_g\) is decreased to \(0.125\). The inverse temperature was
 held fixed at \(\beta=10\) in these tests but does not seem to play a
 crucial role in numerical instabilities. It would be useful if the
 program could detect the onset of these instabilities, e.g. by checking
 the sign of the RHS of the lattice-Boltzmann equations after the
 collision step. Currently a clear sign that the system will be unstable
 are large velocities in comparison to the speed of sound \(1/\sqrt{3}\).
 
 The time over which particles are accelerated is controlled by the
 dimensionless relaxation times and so one expect that decreasing the
 relaxation time would improve the stability of the code. However, note
 that for \(\tau<1\) the RHS of the lattice-Boltzmann equations may become
 negative even in the absence of forces. Moreover an absolute lower limit
 of \(\tau>1/2\) is required to have positive viscosity.

 The stability of the model is heavily dependant on the choice of
 equilibrium distribution, `bdist`. The choice implementing the original
 Shan-Chen force, `bdist`=2, is generally more stable than `bdist`=3,
 which only contains linear forcing terms in the equilibrium
 distribution. However, the expansion used by `bdist`=3 makes similar
 assumptions to the Chapman-Enskog analysis of the lattice Boltzmann
 equation. Therefore regions of parameter space outside its stability
 must be treated with care.

 @TODO Add a note about the history and current state (both scientifically
 and from the wiring in the code) of the ability to tune the equilibrium
 distribution.


# A list of tests regarding lattice-boltzmann relaxation time and body forces
  ---------------------------------------------------------------------------

- [`ibm/basics/body-force-on-particle`](|gitlabfile|/tests/ibm/basics/body-force-on-particle)
- [`lb/ternary/ternary`](|gitlabfile|/tests/lb/ternary/ternary)
- [`lb/poiseuille-cyl`](|gitlabfile|/tests/lb/poiseuille-cyl)
- [`lb/thermal-fluctuations/thermal_fluctuations`](|gitlabfile|/tests/lb/thermal-fluctuations/thermal_fluctuations)
- [`lb/lees-edwards-bc/lees-edwards-bc_ncomponents_2`](|gitlabfile|/tests/lb/lees-edwards-bc/lees-edwards-bc_ncomponents_2)
- [`lb/lees-edwards-bc/lees-edwards-bc-1proc`](|gitlabfile|/tests/lb/lees-edwards-bc/lees-edwards-bc-1proc)
- [`lb/lees-edwards-bc/lees-edwards-bc`](|gitlabfile|/tests/lb/lees-edwards-bc/lees-edwards-bc)
- [`lb/lees-edwards-bc/lees-edwards-bc-inv`](|gitlabfile|/tests/lb/lees-edwards-bc/lees-edwards-bc-inv)
- [`lb/pulsatile-flow/pulsatile-flow`](|gitlabfile|/tests/lb/pulsatile-flow/pulsatile-flow)
- [`lb/sliding-wall/sliding_wall`](|gitlabfile|/tests/lb/sliding-wall/sliding_wall)
- [`lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_1`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_1)
- [`lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_0`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_0)
- [`lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_2`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_2)
- [`lb/poiseuille-2D-bdist-bc/rocks-from-file`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/rocks-from-file)
- [`lb/poiseuille-2D-bdist-bc/bdist_8_bcsel_0_simple`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/bdist_8_bcsel_0_simple)
- [`lb/poiseuille-2D-bdist-bc/twofluids`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/twofluids)
- [`lb/poiseuille-2D-bdist-bc/vartau`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/vartau)
- [`lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_0_simple`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_0_simple)
- [`lb/poiseuille-2D-bdist-bc/bdist_8_bcsel_0_edm`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/bdist_8_bcsel_0_edm)
- [`lb/relax/power_law_shear_thickening`](|gitlabfile|/tests/lb/relax/power_law_shear_thickening)
- [`lb/relax/liquidvapor`](|gitlabfile|/tests/lb/relax/liquidvapor)
- [`lb/relax/power_law_shear_thinning`](|gitlabfile|/tests/lb/relax/power_law_shear_thinning)
- [`lb/relax/power_law_newtonian`](|gitlabfile|/tests/lb/relax/power_law_newtonian)
- [`lb/relax/mrt_poiseuille`](|gitlabfile|/tests/lb/relax/mrt_poiseuille)
- [`lb/invasive-flow/inv_fluid_12_velocity_bc_ncomponents_3`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_12_velocity_bc_ncomponents_3)
- [`lb/invasive-flow/inv_fluid_11_pressure_bc_itype_0`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_11_pressure_bc_itype_0)
- [`lb/invasive-flow/inv_fluid_8_binary_invasion`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_8_binary_invasion)
- [`lb/invasive-flow/inv_fluid_7_binary_invasion`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_7_binary_invasion)
- [`lb/invasive-flow/inv_fluid_15_velocity_bc_itype_0`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_15_velocity_bc_itype_0)
- [`lb/invasive-flow/inv_fluid_12_velocity_bc_itype_-1`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_12_velocity_bc_itype_-1)
- [`lb/invasive-flow/inv_fluid_12_velocity_bc_itype_1`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_12_velocity_bc_itype_1)
- [`lb/invasive-flow/inv_fluid_2_water_invasion`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_2_water_invasion)
- [`lb/invasive-flow/inv_fluid_11_pressure_bc_itype_1`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_11_pressure_bc_itype_1)
- [`lb/invasive-flow/inv_fluid_13_velocity_bc_itype_6`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_13_velocity_bc_itype_6)
- [`lb/invasive-flow/inv_fluid_15_velocity_bc_itype_2`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_15_velocity_bc_itype_2)
- [`lb/invasive-flow/inv_fluid_1_oil_invasion`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_1_oil_invasion)
- [`lb/invasive-flow/inv_fluid_11_pressure_bc_ncomponents_2`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_11_pressure_bc_ncomponents_2)
- [`lb/invasive-flow/inv_fluid_15_velocity_bc_itype_1`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_15_velocity_bc_itype_1)
- [`lb/invasive-flow/inv_fluid_12_velocity_bc_itype_0`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_12_velocity_bc_itype_0)
- [`lb/invasive-flow/inv_fluid_3_surfactant_invasion`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_3_surfactant_invasion)
- [`lb/invasive-flow/inv_fluid_4_mixed_invasion`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_4_mixed_invasion)
- [`lb/simple-forcing/singlefluid_guo`](|gitlabfile|/tests/lb/simple-forcing/singlefluid_guo)
- [`lb/simple-forcing/binary_guo`](|gitlabfile|/tests/lb/simple-forcing/binary_guo)
- [`lb/simple-forcing/binary`](|gitlabfile|/tests/lb/simple-forcing/binary)
- [`lb/simple-forcing/singlefluid_shan_chen`](|gitlabfile|/tests/lb/simple-forcing/singlefluid_shan_chen)
- [`lb/simple-forcing/binary_shan_chen`](|gitlabfile|/tests/lb/simple-forcing/binary_shan_chen)
- [`basic/io/checkpointing/lb`](|gitlabfile|/tests/basic/io/checkpointing/lb)
- [`basic/io/checkpointing/lb-1comp`](|gitlabfile|/tests/basic/io/checkpointing/lb-1comp)
- [`perf/lb/1core`](|gitlabfile|/tests/perf/lb/1core)
- [`memcheck/lb`](|gitlabfile|/tests/memcheck/lb)
