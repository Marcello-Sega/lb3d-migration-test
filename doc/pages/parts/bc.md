title: Boundary conditions

As part of the general system geometry and setup, the namelists `/system/`, `/lattice_boltzmann/`, `/rock_boundaries/`, `/lb_initialization/` contain the different boundary conditions.

<!--
Maybe give an overview of the possiblities, like the (broken) table before:
   `boundary_cond_name` 'SLIT_X'            Rock channel in z-direction with cleared sites at the ends of the domain.
   `boundary_width`   1                     Width of the walls (1 is sufficient).
@TODO JH7r translated these numbers into the (current meaning of the) string constants in use today, but this seems inconsistent/doing other things than noted here.
   `init_cond_name`   'INIT_CONST'          This will set up an approximate gradient at the start.
   `inv_fluid_name`   'INV_PRESSURE'        On-site density boundary conditions.
   `inv_type`         1                     Zou & He
@ENDTODO
   `fr`               1.00005               \(1 + \delta \rho\)
   `pr`               0.99995               \(1 - \delta \rho\)
   `collisiontype`    MRT                   Recommended
   `tau_r`            0.8                   Recommended
   `taubulk_r`        0.84                  Recommended
-->





# Boundary condition/topology modules
   -----------------------------------

 In newer revisions of version6, the update of the LB halos and possible
 adjustments of the populations at the global domain boundaries (as
 required, for instance for Lees-Edwards or different invasion types)
 takes place in the `nskg_bc_module`. To increase readability of the input
 file it is intended that different global boundary types or topologies
 should be implemented as separate modules named `nskg_bc_<boundary-type>`
 which can be selected by setting `boundary=<boundary-type>` in the
 input file and which may specify their own namelist
 `/bc_<boundary-type>/` with additional options.
 
 `boundary=='periodic'`
 
 :   At the moment, the only available setting for `boundary` is
     `'periodic'` implementing periodic boundaries in all Cartesian
     directions. Due to its simplicity, it is implemented directly in
     `nskg_bc_module` and requires no further options.
 
 `boundary=='periodic_inflow'`
 
 :   This option will be completely merged into version6 soon. At the
     moment, parts of the code are visible but still commented out.

**Goes to [[nskg_bc.F90]] periodically_wrapped?**



### Boundary conditions
 
 By default, the code uses periodic boundary conditions: when a particle
 is advected out of one side of the simulation domain, it appears
 entering the corresponding position on the opposite side.
 
 If invasive flow is enabled, by setting `boundary_cond` to a nonzero
 value, then particles which cross the edges of the simulation domain are
 recoloured. For example, to simulate oil invasion, all the blue and
 green particles crossing the boundaries may be turned into red particles
 – this method conserves total particle number, while enabling the
 simulation of invasion flow.
 
 Options are also available to modify the form of the standard periodic
 boundary conditions. Lees-Edwards boundary conditions can produce
 Couette flow.
 
 These are boundary conditions for flows, not for walls.
 
 @note moved to nskg_invasion.F90:nskg_invade()
 
 `inv_fluid = 28`: Shear flow
 
 :   Based on `inv_fluid_name = 'INV_ZOUANDHE_GENERAL'`, it generates a shear flow using one
     or two sliding walls. The walls are located in the z direction
     and the flow is along the x direction. The velocity of the
     moving wall(s) is controlled by `pr`. The argument `inv_type`
     allows the user to choose between moving the lower wall
     (`inv_type = 0`), the upper wall (`inv_type = 1`) or both walls
     (`inv_type = 2`). The physical walls (rocks) are optional and
     can be generated using `boundary_cond = 8`. Only working with
     the flags `SINGLEFLUID` and `NOSURFACTANT` turned on.
 
     @TODO Appears to no longer be present in the code. OR this is `inv_fluid_name = 'INV_SHEAR_ZOUANDHE'` nowadays.
 


# An example: Permeability calculation
  ------------------------------------

 <!---
 
 begin of former user-permeability.tex
 
 cd2ce502-ed27-4b5f-9f3d-21b3e6047432
 
 -->
 
 This is a short guide on how to calculate the single-component
 permeability \(\kappa\) of a rock geometry.
 
## Physics
 
 This method is based on the work of Narvaez et al.
 [@bib:narvaez-zauner-raischel-hilfer-harting:2010; @bib:narvaez-harting:2010]
 and is here explained in NSKG-specific terms where applicable. The use
 of MRT (`collisiontype = 'MRT'`) is
 recommended [@bib:narvaez-zauner-raischel-hilfer-harting:2010]. Zou & He
 boundary conditions (`inv_type = 1`, `inv_fluid_name = 'INV_PRESSURE'`) are
 applied [@bib:narvaez-harting:2010], setting up a small density /
 pressure difference over the sample in \(z\)-direction (`init_cond = 'INIT_CONST'`):
 $$\begin{aligned}
 \rho( z = 0 ) & = & \texttt{fr} = 1 + \delta\!\rho, \\
 \rho( z = n_z ) & = & \texttt{pr} =1 - \delta\!\rho.\end{aligned}$$ The
 density difference \(\delta\!\rho\) should be small (\(O(10^{-4})\)) to
 ensure small velocities and thus a small Reynolds number. To avoid flow
 over the periodic boundaries crosswise to the pressure gradient the
 geometry should be encapsulated by walls (`boundary_cond_name = 'DUCT_Z'`,
 `boundary_width = 1` – this does overwrite the edges of the geometry,
 but this should normally not be an issue). The pressure gradient over
 the system in \(z\)-direction is then
\begin{equation}
 \nabla p_z = 2 {c_{\rm s}}^2 \frac{\delta\!\rho}{n_z} =
 \frac{2 \delta\!\rho}{3 n_z},
\label{eq:pg}
\end{equation}
with \({c_{\rm s}}^2\) the speed of sound
 and \(n_z = \mathtt{nz}\) the length of the system in \(z\)-direction.
 Darcy's law can be rewritten in a useful form:
\begin{equation}
 \kappa_z = \mu \frac{\langle u_z \rangle}{\nabla p_z} =
 \nu \frac{\rho \langle u_z \rangle}{\nabla p_z} =
 \nu \frac{m_z / A}{\nabla p_z},
\label{eq:kappa}
\end{equation}
where
 \(A = n_x \, n_y = \mathtt{nx * nz}\) is the full cross-section of the
 sample, \(\mu = \rho \nu\) is the dynamic viscosity where
 \(\nu = {c_{\rm s}}^2 \left( \tau - \frac{1}{2} \right) = \left( 2 \tau - 1 \right) / 6\)
 is the kinematic viscosity. \(m_z\) is the mass flux in \(z\)-direction.
 Substituting the pressure gradient
 Eq. \eqref{eq:pg} into Eq. \eqref{eq:kappa} we end up with:
 $$\kappa_z = \frac{2 \tau - 1}{6} \frac{3 m_z n_z }{2 \delta\!\rho (n_x \, n_y)}.$$
 We can now readily match input parameters `nx`, `ny`, `nz`, and `tau_r`
 to \(n_x\), \(n_y\), \(n_z\), and \(\tau\), respectively. The density difference
 is related to input parameters through
 \(2 \delta\!\rho = \texttt{fr} - \texttt{fb}\). The local permeabilities
 \(\kappa_z(z)\) are dumped with the `sci_profile` option, and can be found
 in the 11th column of the output (column header `'perm'`). After a
 relatively short relaxation time \(t \approx O(10^4)\), one can average
 the permeability over \(z\) to obtain \(\kappa_z\) (easily averaged on a
 command line by using
 `grep -ve "^#" profile-z-file.asc | awk '{sum+=$11} END {print sum/NR}'`).
 Similarly, the massflux is available in column 7 (`'mf3'`). The system
 has reached equilibrium when the density (pressure) decreases
 approximately linearly over the geometry and the mass flux and
 permeability are constant. The procedure described above returns the
 permeability along the \(z\)-direction of the sample. When anisotropy is
 suspected, permeability can be easily measured over the remaining
 cardinal directions \(x\) and \(y\) by means of the `obs_rotation`
 parameter. By default this is set to `obs_rotation = 'xyz'` but by
 permuting the three letters one can rotate the rock file as it is read.
 Because the pressure drop is always in \(z\)-direction, and permeability
 is invariant under rotation around the \(z\) direction one should normally
 use `xyz`, `xzy` and `zyx` (changing the third letter). If the sample is
 not cubic, `nx`, `ny`, `nz` need to be changed accordingly as well.
 
### Units
 
 The basic SI unit of permeability is \(m^2\). Commonly used derived units
 are \(cm^2 = 10^{-4} m^2\) and the darcy (D) \(\approx 10^{-12}\(m\)^2\).
 In the benchmarks below, the values are given in lattice units, which
 then is \((\Delta x)^2\). Conversion from SI to lattice units and back is
 therefore trivial if the physical volume that a lattice site represents
 is known.
 
## Usage

<!--
NSKG setup removed as of v7.80 due to staggering outdatedness
-->
 
## Benchmarks {#benchmarks}
   ----------
 
### Square duct
 
Equilibration of the measured permeability \(\kappa\) of a square duct of size \(L = 30\). The calculcated values (symbols) tend to the theoretical value (dashed line) and finally stay constant with an error of less than 1%.

![Equilibration of the measured permeability of a square duct.](../../|media|/plots/perm-square-duct.png)
 
 For a square duct with cross-section \(L \times L\) there exists an
 analytical prediction of the permeability in the form of an infinite
 series:
\begin{equation}
 \kappa_{\mathrm{th}} = \frac{L^2}{4} \left[ \frac{1}{3} - \frac{64}{\pi^5} \sum_{n = 0}^{\infty} \frac{\tanh( (2n + 1) \frac{\pi}{2})}{(2n + 1)^5} \right] \approx \frac{L^2}{4} \cdot 0.140577
 \hbox{.}
\label{eq:kappa_th}
\end{equation}
 This can be used for validation of the code. Note that when
 approximating this result with a truncation of the series
 \(\sum_{n=0}^k\), one should use a large \(k \ge 200\) to get accurate
 results.

The code v6.5.4-394-g3f45dd9 has been compiled with the flag
 `-DSINGLEFLUID`, and executed using the input file found in
 `tests/permeability/square-duct`. For this particular setup, the channel
 width is \(L = 30\), and using
 Eq. \eqref{eq:kappa_th} we expect \(\kappa(30) \approx 31.6298\). As can
 be seen in Fig. [9](#fig:perm-square-duct){reference-type="ref" reference="fig:perm-square-duct"}, we have very good agreement with
 theory after a couple of thousand time steps.
 
 <!---
 
 end of former user-permeability.tex
 
 -->

# A list of tests regarding boundary conditions
  ---------------------------------------------

- [`md/lees-edwards-bc/single_particle_transverse_vel`](|gitlabfile|/tests/md/lees-edwards-bc/single_particle_transverse_vel)
- [`lb/lees-edwards-bc/lees-edwards-bc_ncomponents_2`](|gitlabfile|/tests/lb/lees-edwards-bc/lees-edwards-bc_ncomponents_2)
- [`lb/lees-edwards-bc/lees-edwards-bc-1proc`](|gitlabfile|/tests/lb/lees-edwards-bc/lees-edwards-bc-1proc)
- [`lb/lees-edwards-bc/lees-edwards-bc`](|gitlabfile|/tests/lb/lees-edwards-bc/lees-edwards-bc)
- [`lb/lees-edwards-bc/lees-edwards-bc-inv`](|gitlabfile|/tests/lb/lees-edwards-bc/lees-edwards-bc-inv)
- [`lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_1`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_1)
- [`lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_0`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_0)
- [`lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_2`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_2)
- [`lb/poiseuille-2D-bdist-bc/rocks-from-file`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/rocks-from-file)
- [`lb/poiseuille-2D-bdist-bc/bdist_8_bcsel_0_simple`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/bdist_8_bcsel_0_simple)
- [`lb/poiseuille-2D-bdist-bc/twofluids`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/twofluids)
- [`lb/poiseuille-2D-bdist-bc/vartau`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/vartau)
- [`lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_0_simple`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/bdist_2_bcsel_0_simple)
- [`lb/poiseuille-2D-bdist-bc/bdist_8_bcsel_0_edm`](|gitlabfile|/tests/lb/poiseuille-2D-bdist-bc/bdist_8_bcsel_0_edm)
- [`lb/invasive-flow/inv_fluid_11_pressure_bc_itype_0`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_11_pressure_bc_itype_0)
- [`lb/invasive-flow/inv_fluid_8_binary_invasion`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_8_binary_invasion)
- [`lb/invasive-flow/inv_fluid_7_binary_invasion`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_7_binary_invasion)
- [`lb/invasive-flow/inv_fluid_15_velocity_bc_itype_0`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_15_velocity_bc_itype_0)
- [`lb/invasive-flow/inv_fluid_12_velocity_bc_itype_-1`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_12_velocity_bc_itype_-1)
- [`lb/invasive-flow/inv_fluid_12_velocity_bc_itype_1`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_12_velocity_bc_itype_1)
- [`lb/invasive-flow/inv_fluid_2_water_invasion`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_2_water_invasion)
- [`lb/invasive-flow/inv_fluid_11_pressure_bc_itype_1`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_11_pressure_bc_itype_1)
- [`lb/invasive-flow/inv_fluid_13_velocity_bc_itype_6`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_13_velocity_bc_itype_6)
- [`lb/invasive-flow/inv_fluid_15_velocity_bc_itype_2`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_15_velocity_bc_itype_2)
- [`lb/invasive-flow/inv_fluid_1_oil_invasion`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_1_oil_invasion)
- [`lb/invasive-flow/inv_fluid_11_pressure_bc_ncomponents_2`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_11_pressure_bc_ncomponents_2)
- [`lb/invasive-flow/inv_fluid_15_velocity_bc_itype_1`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_15_velocity_bc_itype_1)
- [`lb/invasive-flow/inv_fluid_12_velocity_bc_itype_0`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_12_velocity_bc_itype_0)
- [`lb/invasive-flow/inv_fluid_3_surfactant_invasion`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_3_surfactant_invasion)
- [`lb/invasive-flow/inv_fluid_4_mixed_invasion`](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_4_mixed_invasion)
