title: Pseudopotential Methods (SC, Colour Gradient, …)


All pseudopotential models for Lattice-Boltzmann are based on this common code (implemented in [[nskg_psi_class]]). 


# Shan-Chen

Shan-Chen two-phase model

[[nskg_sc_class]]



# Colour gradient

[[nskg_cg_class]]

A list of tests regarding Color Gradient
----------------------------------------

- [`cg/contact_angle`](|gitlabfile|/tests/cg/contact_angle)
- [`cg/poiseuille`](|gitlabfile|/tests/cg/poiseuille)
- [`cg/laplace_leclair`](|gitlabfile|/tests/cg/laplace_leclair)
- [`perf/cg/1core`](|gitlabfile|/tests/perf/cg/1core)


# Peng-Robinson
[[nskg_pgr_class]]

