title: ELEC: Electrolytes and electrokinetics

Electrolytes plug-in (`ELEC`)
=============================

<!---

begin of former user-elec.tex

0dc4af65-da21-4ba7-bab5-a8df60ec4cf5

2f3a3058-02a4-4d7a-b235-3b63e973aff5

-->

With the `ELEC` plug-in, electrolytes can be added to the core
functionality of NSKG. This implementation is based on work by
Pagonabarraga et
al. [@bib:capuani-pagonabarraga-frenkel:2004; @bib:rotenberg-pagonabarraga-frenkel:2010; @bib:giupponi-pagonabarraga:2011-ptrsa; @bib:giupponi-pagonabarraga:2011-prl],
and the implementation is based on the codes 'elb' (cf. the Development
Documents below) and 'Ludwig' [@bib:desplat-pagonabarraga-bladon:2001].
In future, this should enable full two-way-coupling of fluids to
electric fields and optional coupling to the `MD` plug-in. Currently,
the code works as a standalone solver for the Poisson equation for a
system of mobile and fixed charges, evolving their densities and the
strength of the electromagnetic potential over time. External electric
fields can also be added. A two-way coupling to a two-component fluid
system is currently in development. The plug-in is made available by the
preprocessor switch `-DELEC`.

Current status
--------------

Currently implemented:

-   Relevant fields (\(\rho_\pm\), \(\phi\), \(\epsilon\), \(\vec{E}\)) and
    input parameters.

-   Various initialization routines for charge densities, permittivities
    and rock modifications.

-   Dumping of various scientifc output (in HDF5 and ascii formats).

-   Two Poisson solvers: SOR and P3M.

-   Charge movement of dynamic charges (not on a wall): diffusion, and
    advection when `fluid_on_elec = .true.`.

-   Code has been parallelized.

-   XDRF checkpointing (during LB timesteps only – cannot checkpoint
    during initial equilibration).

To be tested:

-   Full two-way fluid/charge coupling.

-   Support for local \(\epsilon(\vec{x})\) for P3M Poisson solver. Cf.
    section [10.10](#elec-local-permittivity){reference-type="ref" reference="elec-local-permittivity"} for details.

-   Charge advection. @warning  Currently this probably does not work
    well with anything relying on `nskg_add_force_halo`, i.e. `IBM`,
    friction-coupled point particles, constant forcing and Kolmogorov
    forcing. This list is not necessarily exhaustive.

To be implemented:

-   Complex boundary conditions for P3M Poisson solver.

-   Moving charged MD particles.

ELEC-specific compilation flags
-------------------------------

Currently two compilation flags are available for `ELEC`:
`-DELEC_NNONREST` and `-DP3M` (this does not have the `ELEC` prefix
because the P3M solver will be used for other things in the future).

-   When `-DELEC_NNONREST` is set, next-nearest neighbours are
    considered in addition to nearest neighbours for calculation of
    electric fields, charge fluxes and movement, and solving the Poisson
    equation when using SOR. In the earlier implementation (before
    9b50737), all 18 non-rest vectors were considered, while for the new
    implementation (9b50737 and beyond) only the 6 velocities linking a
    site to its neighbours are taken into account. The results should be
    physically the same, but performance and accuracy might be tweaked
    (one might try to reduce the number of expensive halo exchanges that
    have to be performed). @warning  Currently there is no convincing
    reason to use this full 18-vector implementation - however, it has
    passed all benchmark tests in
    section [10.7](#sec:ELEC-bench){reference-type="ref" reference="sec:ELEC-bench"}.

-   Setting `-DP3M` enables P3M as solver for the electric potential and
    electric field. One needs to set the input file parameters
    accordingly. Using P3M requires the availability of the FFTW 2.1.5
    library. This should be set up in the relevant `defines.PLATFORM`
    file.

Implementation guidelines
-------------------------

All source code is located in the files `nskg_elec*.F90`. While this is
not strictly enforced at the moment, files should contain no more than
one module named as the file with the extension being replaced by
`_module` and only symbols provided by `nskg_elec_interface_module` are
supposed to be accessed from non-`ELEC` modules. These symbols should
all have an `elec_` prefix so it is clear that an `ELEC` function is
called from other parts of the code.

### Tested platforms

The code has been tested to compile with all currently available defines
files. However, runtime problems might still arise, especially when
using `-DP3M`, as the FORTRAN to C interfacing is not very robust.

-   Recently tested

    -   `SUN`

    -   `SUN-INTEL`

-   Tested long time ago

    -   `CARTESIUS`

    -   `DEBIAN`

    -   `GROUCH-MPICH`

    -   `LINUX64TUE`

    -   `RPI`

-   Failure

    -   `CARTESIUS-BULLX`

    -   `GROUCH`

    -   `JUQUEEN`

-   Untested

    -   `HECTOR`

    -   `HERMIT`

    -   `JUROPA`

    -   `LEGION`

    -   `LISA`

### Current known problems

-   2013-10-21: Using `-DP3M` with `CARTESIUS-BULLX`, zero-length MPI
    messages seem to cause a problem in initialization of the P3M mesh,
    resulting in a segfault. Use `CARTESIUS` instead.

-   2013-10-21: Using `-DP3M` with `JUQUEEN` results in problems with
    the MPI communicator, probably due to the way the Cray compiler
    deals with underscores when mangling function names, which manifests
    itself when linking to C code. This issue currently does not have
    any known workarounds.

-   2013-10-22: Using `-DP3M` with `GROUCH`, zero-length MPI messages
    seem to cause a problem in initialization of the P3M mesh, resulting
    in a segfault. Use `GROUCH-MPICH` instead.

Program flow
------------

The `ELEC` module interfaces with the main code at various points, all
of which are protected with an `#ifdef ELEC` wrapper. In `nskg.F90` the
salient entry points are as follows:

  ------------------------------ -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Entry point                    Purpose
  `elec_read_input`              Read the `ELEC` input file, calculate derived parameters, display the parameters and broadcast them.
  `elec_init_timers`             Initialize the `ELEC`-specific timers.
  `elec_setup_parallel`          Set up the `ELEC` MPI data types (n.b.: the electric field is not included in the MPI halo data type).
  `elec_init_system`             Put initial values into the \(\rho_\pm\), \(\phi\) (zeros) and \(\epsilon\) arrays and exchange the halo.
  `elec_initial_equilibration`   Evolve only the `ELEC` part of the system to get to an equilibrium state with requested accuracy.
  `elec_restore_init_system`     Alternatively, when restoring from checkpoint, the system will be ready – just exchange the halo and calculate the electric field.
  `elec_force_reset`             Alternative `ELEC` version of the `nskg_force_reset` routine, which will also reset the halo.
  `elec_add_forces`              Add forces originating from `ELEC` to the fluid species. This subroutine must be called after `elec_force_reset` and after `nskg_force_apply` even though the comments might suggest one shouldn't (the force apply routine adds forces from the halo into the matching physical region of the neighbour, which is not what needs to happen to the `ELEC` forces).
  `elec_timestep`                Move the charges (diffusion, and advection if `fluid_on_elec = .true.`) according to the current system state, then recalculate the potential and electric field. This must be called in the LB loop, before fluid advection and after forces are added.
  `elec_postprocess`             Dump `ELEC`-related data to disk.
  `elec_shutdown`                Clean up allocatable arrays and such.
  ------------------------------ -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Furthermore, additional fields are added to the `nskg_site` type and
additional fields are added to the checkpoints. The global variable
`use_nskg_force` needs to be set to `.true.`– this is currently done in
`nskg.F90`.

Initial equilibration is handled as follows: first, the Poisson equation
is solved for the configuration of \(\rho_\pm\), setting \(\phi\), and the
electric field is calculated. Second, the external electric field is
temporarily disabled and the system is allowed to equilibrate by
repeated calls to `elec_timestep` (see below). Then, the external
electric field is reinstated and another equilibration cycle takes
place.

During the rest of the simulation, the `ELEC` configuration will evolve
by calling `elec_timestep` once during every LB timestep. This
subroutine consists of the following steps: first, the full `ELEC` halo
is exchanged. Then, the flux of charges according to the current
configuration of the system is calculated in `calc_fluxes_diffusion`
(and `calc_fluxes_advection` if coupling to the fluid(s) is enabled).
Charges are moved according to these fluxes in `apply_fluxes`. After
this step the configuration of \(\rho_\pm\) on the lattice has changed and
it is necessary to exchange the full halo again. With charges now
properly set, the Poisson equation is solved in `solve_poisson`.
Depending on input parameters this routine might then call the SOR or
P3M solver. The values of \(\phi\) are modified in this step, and its halo
is exchanged explicitly in the case of P3M and implicitly for SOR so the
values are correct for the next step: the calculation of the electric
field (when using the finite difference method), which can happen either
inside the P3M routine or in `calc_E_fd` (using finite difference).

ELEC input file parameters (namelist `/elec_input/`)
----------------------------------------------------

Input parameters are read from a file named as the LB input file with
“`.elec`” appended. The namelist `/elec_input/` then contains all
parameters. When input parameters are defined below, the parenthesis
after each parameter contain its type and default value. The type `real`
stands for `real(kind=rk)` in the program, which currently means double
precision. When charged colloids are desired, the `-DMD` flag is
required as well. @warning  When using `-DMD`, because the current
implementation does not yet allow the colloids to move, in the .md input
file, `fix_v` and `fix_w` should be set to `.true.`(and initial
velocities should be zero). In addition, setting `interaction = 'ladd'`
is mandatory, as this sets the correct rock states on the lattice. All
relevant parameters are expressed in lattice units. “Small” parameters
are sometimes initialized as \(\epsilon_{\mathrm{float}}\), which is to
say the machine epsilon of a real with half the precision normally used
in NSKG (currently double).

### Charges {#sec:ELEC-input-charges}

The elementary electric charge `ec` is hardcoded to be \(e = 1.0\), but is
included in calculations.

`rho_init_type` (`character(64)`, `"none"`)

:   Choice of initial charge distribution. The default value is
    `"none"`, which throws an error. Possible options are:

    `"uniform"`

    :   Sets homogeneously distributed ion densities given by the
        variables `rho_p_init` and `rho_m_init`. An additional net
        charge can be set by using `Q_fluid` and `Q_wall`, which
        represent the *total* charge on the fluid and wall sites. These
        variables get divided by the respective number of sites to
        obtain charge densities, which are set homogeneously on the ion
        type corresponding to its sign. That is, for example, if
        `Q_fluid` is positive, then the corresponding charge density is
        added as ions only to `rho_p`.

    `"slipbc"`

    :   Puts surface charge density `Q_slip` and `Q_noslip` on slip and
        noslip sites, respectively, matching the geometry of
        `inv_fluid_name = 'INV_PARTIAL_SLIP'` through the parameters `pb` and `pg`, at
        \(x = 1\). At \(x = \mathtt{tnx}\), charge densities are set to
        zero. When used together with the on-site fixed velocity
        boundary condition (`inv_fluid_name = 'INV_PARTIAL_SLIP'`) these charges will be held
        immobile, even though there is no rock present. Charges are
        placed on remaining fluid sites in order to make the system
        neutral. This initialization is used for electro-osmotic flow in
        striped channels. @warning  This initialization does not support
        rock sites.

`rho_p_init` (`real`, `0.0`)

:   When using `rho_init = "uniform"`: positive ion concentration to be
    set homogeneously on all fluid sites. When solid fraction is
    enabled, the concentration is set to be proportional to the fluid
    volume fraction.

`rho_m_init` (`real`, `0.0`)

:   When using `rho_init = "uniform"`: positive ion concentration to be
    set homogeneously on all fluid sites. When solid fraction is
    enabled, the concentration is set to be proportional to the fluid
    volume fraction.

`q_total_fluid` (`real`, `0.0`)

:   When using `rho_init = "uniform"`: total amount of charge to be
    distributed homogeneously over all fluid sites in the system.

`q_total_wall` (`real`, `0.0`)

:   When using `rho_init = "uniform"`: total amount of charge to be
    distributed homogeneously over all rock sites in the system.

`Q_colloid` (`real`, `0.0`)

:   Currently unused, as very soon the charges on colloids will be set
    by the .md configuration input file and properly updated as the
    colloid moves.

`Q_noslip` (`real`, `0.0`)

:   When using `rho_init = "slipbc"`: Surface charge density to be
    placed on no-slip stripes. This charge is then compensated for by
    opposite charges distributed over the fluid sites to attain overall
    neutrality.

`Q_slip` (`real`, `0.0`)

:   When using `rho_init = "slipbc"`: Surface charge density to be
    placed on finite slip stripes. This charge is then compensated for
    by opposite charges distributed over the fluid sites to attain
    overall neutrality.

`acc_neutrality` (`real`, `\epsilon_{\mathrm{float}}`)

:   Desired accuracy of neutrality check after initialization.

### Fluxes {#sec:ELEC-input-fluxes}

Variables related to the computation of the diffusive and advective
fluxes of ions.

`diffusion_model` (`integer`, `0`)

:   Choose a diffusion model from the following options:

    1.  Pagonabarraga style fluxes, where the fluxes are written as a
        sum of exponentials to supposely reduce spurious fluxes. This
        has been shown to be unncessary.

    2.  Fluxes in the Rempfer style, just as a simple finite difference
        discretization of advection-diffusion equation.

`solvation_model` (`integer`, `0`)

:   Model for the solvation chemical potential, one of the following
    options:

    1.  Solvation potential is proportional to the water density, such
        that \(\mu^\pm = g^\pm \rho^w\)

    2.  Solvation potential is proportional to the relative
        concentration field \(c = (\rho^w - \rho^o)/(\rho^w+\rho^o)\),
        such that \(\mu^\pm = g^\pm (1 + c)/2\).

The inverse temperature \(\beta\) is used in the diffusion equations. This
parameter is not included in the `ELEC` namelist, but is instead
calculated using `beta` = 1 / `kbT`, where `kbT` is part of the
`system` namelist

`D_therm` (`real`, `0.0`)

:   Average thermal diffusion coefficient for both positive and negative
    charges.

`delta_D_therm` (`real`, `0.0`)

:   Difference in thermal diffusion coefficients of positive and
    negative charges: `D_plus` = `D_therm` + `delta_D_therm` and
    `D_minus` = `D_therm` - `delta_D_therm`.

### Poisson solver

`poisson_solver` (`character(64)`, `'sor'`)

:   Choice of algorithm to solve the Poisson equation.

    `'sor'`

    :   Uses Successive Over-Relaxation scheme to solve the Poisson
        equation. The spectral radius will be set as
        \(r = 1 - \frac{1}{2} ( \frac{\pi}{L} )^2\), with \(L\) the largest
        dimension of the system.

    `'p3m'`

    :   When using `-DP3M`: Uses Particle-Particle-Particle-Mesh
        algorithm to solve the Poisson equation. @warning  This solver
        currently does not support boundary conditions other than
        `boundary_phi = 'periodic'`.

`maxits_SOR` (`integer`, `10000`)

:   When using `poisson_solver = 'sor'`: maximum number of iterations of
    the SOR solver per attempt to solve the Poisson equation.

`n_check_SOR` (`integer`, `1`)

:   When using `poisson_solver = 'sor'`: number of iterations of the SOR
    solver between checks for convergence (this contains an
    `MPI_Allreduce` call).

`acc_SOR` (`real`, `\epsilon_{\mathrm{float}}`)

:   When using `poisson_solver = 'sor'`: desired accuracy of the SOR
    solver.

`acc_fluxes` (`real`, `\epsilon_{\mathrm{float}}`)

:   Desired accuracy of the flux calculations. Initial equilibration
    will continue until the charge flux on every link is smaller than
    this requested accuracy.

`E_solver` (`character(64)`, `'fd'`)

:   Choice of method to calculate the electric field.

    `'fd'`

    :   Uses finite difference method to calculate the electric field
        from the gradient of the electric potential.

    `'p3m'`

    :   When using `-DP3M` and `poisson_solver = 'p3m'`: Calculate the
        electric field in Fourier space inside the P3M solver.

### Permittivity {#sec:ELEC-input-permittivity}

`local_eps` (`logical`, `.false.`)

:   If set to `.true.`, the Poisson solver will use a local
    \(\epsilon(\vec{x})\). When set to `.false.`, it uses
    \(\epsilon = \mathtt{eps\_global}\). For details, cf.
    section [10.6.1](#sec:ELEC-permittivity){reference-type="ref" reference="sec:ELEC-permittivity"}.

`eps_init_type` (`character(64)`, `'uniform'`)

:   Choice of initialization of electric permittivity:

    `'capacitor'`

    :   Sets \(\epsilon = \mathtt{eps\_uniform}\) on the lower \(z\)-half of
        the system and \(\epsilon = \mathtt{2 \cdot eps\_uniform}\) on the
        upper \(z\)-half.

    `'colloid'`

    :   Sets \(\epsilon = 1.0 / ( \pi \cdot \mathtt{bjerrum\_length} )\)
        on colloid sites, and
        \(\epsilon = 0.8 / ( \pi \cdot \mathtt{bjerrum\_length} )\) on
        fluid sites. This option should be generalized.

    `'uniform'`

    :   Sets \(\epsilon = \mathtt{eps\_uniform}\) everywhere (cf.
        `eps_global`).

`eps_global` (`real`, `0.0`)

:   When using `eps_init = 'uniform'` or `eps_init = 'capacitor'` or
    `local_eps = .false.`: sets the electric permittivity `eps_uniform`
    to be used. When set to negative value, will be calculated from the
    Bjerrum length instead:
    \(\mathtt{eps\_uniform} = \beta e^2 / 4 \pi l_B\).

`eps_r` (`real`, `0.0`)

:   When using `fluid_on_elec = .true.` and `local_eps = .true.`:
    electric permittivity of the red fluid.

`eps_b` (`real`, `0.0`)

:   When using `fluid_on_elec = .true.` and `local_eps = .true.`:
    electric permittivity of the blue fluid. Requires multicomponent
    fluid.

`eps_wall` (`real`, `0.0`)

:   When using `fluid_on_elec = .true.` and `local_eps = .true.`:
    electric permittivity of the wall sites.

### Geometry

`rock_init_type` (`character(64)`, `'none'`)

:   Choice of modification of rock state:

    `'capacitor'`

    :   Sets two walls of thickness 1 in the system perpendicular to the
        \(z\)-directions at \(z = \mathtt{tnx}/4\) and
        \(z = 3\mathtt{tnz}/4 + 1\). To be used with
        `rho_init = 'capacitor'`. Does not change the rock state of
        other sites.

    `'none'`

    :   Does not change existing rock state.

### Boundary conditions

@warning  Complex boundary conditions are not yet implemented for
`poisson_solver = 'p3m'`.

`boundary_phi` (`character(64)`, `'periodic'`)

:   Boundary conditions of \(\phi\) to be used in the Poisson solver and
    calculation of electric fields.

    `'dropz'`

    :   Apply a potential drop `phi_dropz` over the system in
        \(z\)-direction, periodic boundary condition in the others.

    `'neumann_x'`

    :   Neumann boundary conditions in \(x\)-direction (\(\nabla \phi = 0\)
        at the boundary, potentials are not necessarily the same at both
        ends), periodic boundary conditions in the others.

    `'neumann_z'`

    :   Neumann boundary conditions in \(z\)-direction (\(\nabla \phi = 0\)
        at the boundary, potentials are not necessarily the same at both
        ends), periodic boundary conditions in the others.

    `'periodic'`

    :   Periodic boundary conditions in all directions.

`phi_dropz` (`real`, `0.0`)

:   When using `boundary_phi = 'dropz'`: potential drop to be applied
    over the system.

### Couplings

`fluid_on_elec` (`logical`, `.true.`)

:   If set to `.true.`, this allows the fluids to affect the electric
    field through local fluid-composition-dependent permittivity and
    charge flux through advection.

`elec_on_fluid` (`logical`, `.true.`)

:   If set to `.true.`, this allows the electric fields to affect the
    fluids through forcing.

`delta_mu_plus` (`real`, `0.0`)

:   When using `fluid_on_elec = .true.` and multicomponent fluid: the
    solvation free energy difference between the red and blue fluids for
    the positive ion species: \(\Delta \mu_+ = \mu^r_+ - \mu^b_+\). Cf.
    definitions below Eq. 5
    in [@bib:rotenberg-pagonabarraga-frenkel:2010].

`delta_mu_minus` (`real`, `0.0`)

:   When using `fluid_on_elec = .true.` and multicomponent fluid: the
    solvation free energy difference between the red and blue fluids for
    the negative ion species: \(\Delta \mu_- = \mu^r_- - \mu^b_-\). Cf.
    definitions below Eq. 5
    in [@bib:rotenberg-pagonabarraga-frenkel:2010].

`noforce_offset_ux` (`integer`, `0`)

:   When using `elec_on_fluid = .true.` and `inv_fluid_name = 'INV_PARTIAL_SLIP'`, disable
    force application in a region
    \([\mathtt{tnx} - \mathtt{noforce\_offset\_ux} : \mathtt{tnx}]\). This
    is used to try and ignore the buildup of unphysical countercharge
    because of the presence of periodic boundary conditions. The value
    of this parameter should be based on the value of the Debye
    screening length (in particular, it should be larger).

### External electric field

`Ex` (`real`, `0.0`)

:   Magnitude of the \(x\)-component of the external electric field.
    Currently only homogeneous, time-constant fields are supported.

`Ey` (`real`, `0.0`)

:   Magnitude of the \(y\)-component of the external electric field.
    Currently only homogeneous, time-constant fields are supported.

`Ez` (`real`, `0.0`)

:   Magnitude of the \(z\)-component of the external electric field.
    Currently only homogeneous, time-constant fields are supported.

### Restoring

Variable related to the restoring operation.

`restore_elec` (`logical`, `.false.`)

:   If to restore the field related to `ELEC`, when the simulation has
    been set to restore in general.

`restore_elec_eq` (`logical`, `.false.`)

:   If the lattice data being restored was saved during equilibration.

`init_eq` (`logical`, `.true.`)

:   If to perform the initial equilibration

### Dumping

For all these integer parameters, a value of zero prevents any dumping
of that category. Currently, XDRF checkpointing is implemented for
`ELEC`, and it shares the common checkpointing parameters found in the
basic input file. @warning  Checkpoints with and without `-DELEC` are
not compatible!

`n_sci_rho_p` (`integer`, `0`)

:   Length of the interval between dumps of the positive charge scalar
    field.

`n_sci_rho_m` (`integer`, `0`)

:   Length of the interval between dumps of the negative charge scalar
    field.

`n_sci_phi` (`integer`, `0`)

:   Length of the interval between dumps of the electric potential
    scalar field.

`n_sci_eps` (`integer`, `0`)

:   Length of the interval between dumps of the local electric
    permittivity scalar field.

`n_sci_E` (`integer`, `0`)

:   Length of the interval between dumps of the electric field. This
    will be dumped as three scalar fields `Ex`, `Ey`, and `Ez`.

`n_sci_elec` (`integer`, `0`)

:   Write ASCII data of `rho_p`, `rho_m`, `phi`, `eps`, `E(:)` and
    `rock_state` at all positions. This writes one file per process.

### Debugging

`dump_elec_halo` (`logical`, `.false.`)

:   If set to `.true.`, data written through `n_sci_elec` will also
    write data inside the halo of each process.

`n_show_eq` (`integer`, `0`)

:   Length of the interval (in iteration loops) during charge
    equilibration between showing the number of performed iterations.

`n_dump_eq` (`integer`, `0`)

:   Length of the interval (in iteration loops) during charge
    equilibration between dumping a full ASCII file.

`n_show_SOR` (`integer`, `0`)

:   Length of the interval (in iteration loops) during SOR between
    showing the number of performed iterations.

`n_eq_noE` (`integer`, `-1`)

:   Number of iterations to perform during equilibration with the
    external electric field disabled. If this is set to a negative
    number, the convergence criterion as set by `acc_fluxes` is used
    instead.

`n_eq_E` (`integer`, `-1`)

:   Number of iterations to perform during equilibration with the
    external electric field enabled. If this is set to a negative
    number, the convergence criterion as set by `acc_fluxes` is used
    instead.

`dbg_check_zero_flux` (`logical`, `.false.`)

:   If set to `.true.`, total charge flux over the whole lattice will be
    checked at every timestep, to ensure that it is zero up to numerical
    accuracy.

Combining parameters
--------------------

Some combinations of parameters might have non-trivial interactions or
might not make sense. An attempt is made here to tabulate some of the
tricky cases.

### Permittivity {#sec:ELEC-permittivity}

  `local_eps `   `fluid_on_elec`   Components\*   Effect
  -------------- ----------------- -------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  `.false.`      any               any            \(\epsilon(\vec{x}) = \epsilon\) is constant throughout the system and is derived from `eps_global`.
  `.true.`       `.false.`         any            \(\epsilon(\vec{x}) = \epsilon(\vec{x})_{\mathrm{init}}\) is set as an intial condition and is stored in `N%eps`.
  `.true.`       `.true.`          single         \(\epsilon(\vec{x}) = \epsilon(\mathtt{rock\_state}(\vec{x}))\) is `eps_r` for fluid sites and `eps_wall` for rock sites (to be expanded to treat colloids and wall differently when MD is coupled).
  `.true.`       `.true.`          multi          \(\epsilon(\vec{x}) = \overline{\epsilon}( 1 -  \gamma  \phi(\vec{x}) )\)\*\* is calculated locally from the local fluid densities.

\* To be determined by the presence or absence of the compiler flag
`-DSINGLEFLUID`.

\*\* Here, \(\overline{\epsilon} = ( \epsilon_r + \epsilon_b ) / 2\),
\(\gamma = \frac{\epsilon_b - \epsilon_r}{\epsilon_b + \epsilon_r}\) and
\(\phi(\vec{x}) = \frac{\rho_r(\vec{x}) - \rho_b(\vec{x})}{\rho_r(\vec{x}) + \rho_b(\vec{x})}\).

  `local_eps`   `poisson_solver`   `E_solver`   Status
  ------------- ------------------ ------------ -----------------------
  `.false.`     `'sor'`            `'fd'`       Fully implemented.
  `.false.`     `'sor'`            `'p3m'`      Not supported.
  `.false.`     `'p3m'`            `'fd'`       Fully implemented.
  `.false.`     `'p3m'`            `'p3m'`      Not yet fully tested.
  `.true.`      `'sor'`            `'fd'`       Fully implemented.
  `.true.`      `'sor'`            `'p3m'`      Not supported.
  `.true.`      `'p3m'`            `'fd'`       Not yet fully tested.
  `.true.`      `'p3m'`            `'p3m'`      Not yet fully tested.

Benchmarks {#sec:ELEC-bench}
----------

### CPF: Equilibrium distribution of the counterion density {#elec-benchmark-cpf1}

For this benchmark test we have closely
followed [@bib:capuani-pagonabarraga-frenkel:2004]. It is used to test
the electrostatics and end result of charge movement. We consider two
charged plates with surface charge density \(\sigma\) separated by a slit
of width \(L\). The system is then made neutral by filling the slit with
charge density \(2 \sigma / L\). Please note that all definitions of
analytical profiles are symmetric in the centre of the channel (as is
the case in [@bib:capuani-pagonabarraga-frenkel:2004]), so care has to
be taken to account for the offset of \(L/2\). Also, the physical presence
of the wall will be halfway between the wall site and the neighbouring
fluid site. In this case, the result is \(L = 20\). For this simple
geometry, the charge density of the counterions is known analytically:
$$\rho(x) = \frac{\rho_0}{\cos^2(K x)} \mbox{,}$$ where
\(\rho_0 = K^2 / 2 \pi l_B\) and \(K\) is the solution of the transcendental
equation \(K L / 2 \tan(K L / 2) = \pi l_B L \sigma\). We set the Bjerrum
length \(l_B = 0.4\) and consider wall charge densities
\(\sigma = 0.003125\), \(\sigma = 0.03125\) and \(\sigma = 0.3125\), which
gives solutions of \(K = 0.02766\), \(K = 0.07854\) and \(K = 0.1395\),
respectively. All parameters in \(\rho(x)\) are now known and we can
compare with the results of the simulation. As can be seen in
Fig. [\[elec-edcd\]](#elec-edcd){reference-type="ref" reference="elec-edcd"}, agreement is very good.

@warning  What about this figure? Caption is not working, figure graphic
itself is commented out?

The code v6.5.4-452-g9de4a58 has been compiled with flags
`-DELEC -DP3M -DSINGLEFLUID`, and executed using the input files found
in `tests/elec/CPF-test-VI-B1`. @warning  The `run-simulations.sh`
script fills in the missing variables in the input files.

### Capacitor with dielectric lamellae

In this test, we calculate the potential inside a capacitor with two
dielectric lamellae. It is used to test the calculation of the potential
and electric fields and the effect of varying the dielectric constant. A
system of length \(L = 60\) is considered. A wall of thickness 1 with
surface charge \(\sigma_- = -\sigma = -1.0\) is placed in the system at
\(z = 15\) and a similar wall with \(\sigma_+ = \sigma = 1.0\) is placed at
\(z = 46\). To look at this capacitor in isolation we keep periodic
boundaries in \(x\)- and \(y\)-directions, but impose Neumann boundary
conditions for the electric potential \(\phi\) in the \(z\)-direction. In a
simple system with nonlocal dielectric constant
\(\epsilon(\vec{x}) = \epsilon\) the electric potential difference between
the two plates has a simple solution:
\(\Delta \phi = d \sigma / \epsilon\), with \(d\) the distance between the
two plates. Similarly, the electric field inside the capacitor takes the
simple form \(E_x = E_y = 0\(, \)E_z = -\sigma / \epsilon\). Outside the
capacitor the field is identically zero. We have recovered this result,
but do not show it here – instead we consider the slightly more involved
case where \(\epsilon(z) = \epsilon_1 = 1.0\( for \)z \le L/2\) and
\(\epsilon(z) = \epsilon_2 = 2.0\( for \)z > L/2\). As in
[10.7.1](#elec-benchmark-cpf1){reference-type="ref" reference="elec-benchmark-cpf1"}, the physical presence of the wall sits
halfway between two lattice sites. Similarly, in the sharp transition
from \(\epsilon_1\) to \(\epsilon_2\), the permittivity on one site is
considered to be the average of the two permittivities. As such, we
predict that
\(\Delta \phi = \frac{d_1}{\epsilon_1} + \frac{1}{\overline{\epsilon}} + \frac{d_2}{\epsilon_2}\),
where \(\overline{\epsilon} = ( \epsilon_1 + \epsilon_2 ) / 2\). Putting
in the numbers (\(d_1 = d_2 = 15\() we obtain \)\Delta \phi = 23.1667\). As
can be seen in
Fig. [\[fig:elb-capacitor\]](#fig:elb-capacitor){reference-type="ref" reference="fig:elb-capacitor"}, we recover this value very well (some
slight deviations due to discretization appear near the dielectric
interface). We also recover \(E_z = 0\(, \)E_z = -1.0\(, \)E_z = -0.5\) and
\(E_z= 0\) for the different regions around and inside the capacitor,
following the same formula as above.

The code v6.5.4-452-g9de4a58 has been compiled with flags
`-DELEC -DP3M -DSINGLEFLUID` (`-DP3M` is optional, the test uses the SOR
solver only), and executed using the input files found in
`tests/elec/test-capacitor`.

<!---

1bd42368-624e-4ecc-b204-6fdb9858ed3f

711a37c5-ad58-49cd-b6d8-92594cf43e5d

-->

### Elb: Global epsilon

In this test, we compare the field and charge distribution around a
charged colloid to the value obtained for an identical system in elb. It
is used to test the coupling with static MD particle, the external
electric field and the end result of charge movement. Some parameters
used in 'elb' have a counterpart with a different name in NSKG:

-   `ch_bjl` \(\to\) `bjerrum_length`

-   `ch_lambda` \(\to\) `debye_length`

-   `poisson_acc` \(\to\) `acc_SOR`

-   `diffusion_acc` \(\to\) `acc_fluxes`

-   `e_slope_`\(i\) \(\to\) `E`\(i\) (with \(i\) one of `x`, `y`, or `z`)

Furthermore, in NSKG, array indices run from \(1\) to \(n\) in all three
cardinal directions (Fortran style), never from \(0\) to \(n-1\). To match
the rock state of the example system, the particle is not centered in
the system (cf. md-init-benchmark-local-diel.cfg), which accounts for
the \(z\)-data seemingly being displaced compared to \(x\) and \(y\). The
plots use profiles of the form \(x \rightarrow (x,15,15)\),
\(y \rightarrow (15,y,15)\(, \)z \rightarrow (15,15,z)\). After the changes
to the SOR routine in NSKG in 9b50737 (matching the Ludwig
implementation more closely and solving some bugs) the results are no
longer exactly identical, but the physical behaviour is recovered.
Results are shown in
Fig. [\[fig:elb-global-epsilon\]](#fig:elb-global-epsilon){reference-type="ref" reference="fig:elb-global-epsilon"}.

The code v6.5.4-452-g9de4a58 has been compiled with flags
`-DELEC -DP3M -DMD -DSINGLEFLUID`, and executed using the input files
found in `tests/elec/ELB-test-global-epsilon`.

### Ludwig: Liquid junction potential {#elec-benchmark-ljp}

For this test we follow a benchmark performed in the
Ludwig [@bib:desplat-pagonabarraga-bladon:2001] user guide. It is used
to test the time behaviour of the charge diffusion / fluxes. A liquid
junction potential is obtained by initializing a system with two neutral
lamellae with charge densities \(\rho_+ = \rho_- = \rho_0 + \delta \rho\)
and \(\rho_+ = \rho_- = \rho_0 - \delta \rho\), respectively.
Additionally, the different ionic species have different diffusivities:
\(D_+ = D_0 + \delta D\( and \)D_- = D_0 - \delta D\). The charges will
diffuse from the lamella with the lower densities into the lamella with
the higher densities. Due to the different diffusivities, the system
will no longer be locally neutral, which leads to a buildup of potential
\(\Delta \phi(t)\). This in turn will balance the diffusive flux. When the
system is chosen to be sufficiently large (\(l_D / L \ll 1\)), this gives
rise to two timescales: \(\tau_s\) and \(\tau_d\). Details can be found in
[@bib:mafe-manzanares-pellicer:1988], but the most relevant formulas are
given below. The potential build-up (difference between highest and
lowest potential in the system) obeys the relation:
$$\Delta \phi(t) \simeq \Delta \phi_s \Big\{ 1 - e^{-t/\tau_s} \Big\}
\mbox{,}$$ where the saturation value
$$\Delta \phi_s = \frac{(D_+ - D_-)}{\beta e (D_+ + D_- )} \frac{2 \delta \rho}{\rho_0}
\mbox{.}$$ The saturation timescale is given by
$$\tau_s = \frac{\epsilon}{\beta e^2 (D_+ + D_-) \rho_0}
\mbox{.}$$ A more exact solution in the limit \(l_D / L \ll 1\),
\(N \to \infty\) (which can be approximated by
\(N = N_{\max} = L / \pi l_D\)) can be given:
$$\Delta \phi(t) = \Delta \phi_s  \Big\{ 1 - e^{-t/\tau_e} \Big\} \frac{4}{\pi} \Big\{ \sum^N_{m=1} \frac{\sin^3(m \pi / 2)}{m} e^{-m^2 t/\tau_d} \Big\}
\mbox{,}$$ where the decay timescale
$$\tau_d = \frac{L^2}{2\pi^2 (D_+ + D_-)}
\mbox{.}$$
!>
The parameters used in the test follow the benchmark in Ludwig:
dielectric permittivity \(\epsilon = 3.3 \cdot 10^3\),
temperature \(\beta^{-1} = 3.333 \cdot 10^{-5}\),
unit charge \(e = 1\),
diffusivities \(D = 0.01\), \(\delta D = 0.0025\),
charge densities \(\rho_0 = 0.01\) and
\(\delta \rho = 0.0001\). We test with the single system size \(L = 128\).
Putting these numbers into the formulas given above, one obtains
\(\Delta \phi_s = 1.6667 \cdot 10^{-7}\), \(\tau_s = 550\),
\(\tau_d = 41501.2\). As can be seen in
Fig. [4](#elec-ljp){reference-type="ref" reference="elec-ljp"},
agreement is very good for the buildup, while deviations start to occur
during the decay, where the observed \(\tau_d\) is off by a factor of
\(1.067\). Similar deviations are seen in results obtained by the Ludwig
code and can possibly be attributed to the fact that the analytical
solution applies to an infinitely large system with constant charge
concentrations, vanishing currents at both ends and a finite diffusive
zone of length \(L\). In these simulations, the entire system is within
the diffusive zone, which might lead to smaller effective diffusivities
or larger effective system sizes.

![Comparing theory (solid line) and simulation (dashed lines) of the
buildup and decay of potential \(\Delta \phi\) in a liquid junction, using
both sor and p3m as the Poisson solver. Also shown is the theoretical
saturation value of the potential
difference.](../|media|/plots/Ludwig-test-liquidjunction.png)

The code v6.5.4-452-g9de4a58 has been compiled with flags
`-DELEC -DP3M -DSINGLEFLUID`, and executed using the input files found
in `tests/elec/Ludwig-test-liquidjunction` (this assumes the use of 16
MPI processes).

### CPF: Electro-osmotic flow

Also for this benchmark test we have closely
followed [@bib:capuani-pagonabarraga-frenkel:2004]. It is used to test
the force coupling to a single fluid and charge advection. A system
similar to the one in section
[10.7.1](#elec-benchmark-cpf1){reference-type="ref" reference="elec-benchmark-cpf1"} is considered. An electric field \(E\) is
applied parallel to the walls. Due to the coupling to the fluid, a
velocity profile will be created. This has the exact solution
$$v_y(x) = \frac{e E \rho_0}{\eta K^2} \ln \left[ \frac{\cos(K x)}{\cos(K L /2)} \right] \mbox{,}$$
where \(\eta = 1/6\) (for \(\tau = 1\), \(\rho_r = 1\)) is the viscosity of
the fluid, electric unit charge \(e = 1\) and the other parameters are as
in the previous test. As can be seen in
Fig. [5](#elec-eof){reference-type="ref" reference="elec-eof"},
agreement is very good for lower values of \(\sigma\). As for
\(\sigma = 0.3125\) we have calculated
\(K = 0.1395 \approx K_{\max} = \pi / L = 0.1571\), the deviations are not
surprising.

![Comparing theory (dashed lines) and simulation (symbols) of the
velocity profile of a charged fluid in a slit between two charged plates
subjected to an electric field \(E\) for various charge densities
\(\sigma\), different decompositions (1 or 2 CPUs), and different Poisson
solvers (p3m and sor). Circles and squares: sor; triangles and diamonds:
p3m. The inset shows the relative error of the simulations compared to
the theory as
(\rho_{\mathrm{err}} = | \rho_{\mathrm{sim}} / \rho_{\mathrm{th}} - 1|\).
Deviations for high charge density are expected (see
text).](../|media|/plots/CPF-test-VI-B2.png)

The code v6.5.4-452-g9de4a58 has been compiled with flags
`-DELEC -DP3M -DSINGLEFLUID`, and executed using the input files found
in `tests/elec/CPF-test-VI-B1`. @warning  The `run-simulations.sh`
script fills in the missing variables in the input files.

Development document 1: Including charge dynamics in NSKG
---------------------------------------------------------

@warning  This is an only-slightly-modified copy of an original
development document and may not represent the current state of affairs.
Giovanni Giupponi, Departament de Fisica Fonamental, Universitat de
Barcelona, Carrer Martí i Franques, 08028 Barcelona (Spain). Dated: July
27, 2011.

### Phase 1

Goal is to implement charge diffusion on NSKG. Started with Stefan at
Barcelona and to be completed in parallel. We begin with solving
diffusion equation calculating fluxes as symmetrized in eq. (15)
[@bib:capuani-pagonabarraga-frenkel:2004]. Flux calculation and
therefore on lattice dynamics of charges, eq. (13)
[@bib:capuani-pagonabarraga-frenkel:2004], depend on the electrostatic
potential. The potential is calculated using SOR routine as from
Numerical Recipes. This routine solves the Poisson equation,
\(\nabla \phi = -\rho_t / \epsilon\), with \(\phi\) the electrostatic
potential, \(\rho_t\) the total electric charge and \(\epsilon\) the
dielectric constant, which has to be the same throughout the system. In
production, NSKG3D (1st phase) will be able to compute the dynamics of
charges on a lattice where the dielectric constant is local.

-   Work @ Eindhoven: Profiting from Stefan visit, the setup of NSKG3D to
    work with charges will be started.

    -   Addition of relevant fields (\(\rho_\pm\), \(\phi\)) and input
        parameters (\(\lambda_D\), \(l_B\), \(D_\pm\) accuracies, electric
        field and colloids charge) to code. Initialization of charges on
        lattice and updated dumping of output files (charges,
        potential).

    -   Include the plain SOR routine sor(\(\rho_\pm\), \(\phi\)), fluxes
        calculation and dynamics of charges (from Barcelona code) into
        NSKG3D.

-   Work @ Barcelona:

    -   Develop and test a Poisson-solver based on SOR which works with
        local \(\epsilon = \epsilon(\vec{x})\).

    -   Validating steps (in order to validate progress): 1. Compare the
        equilibrium states provided by Barcelona code and NSKG3D for a
        system with a constant \(\epsilon\). 2. Compare analytical
        solution of a system with local dielectric with NSKG3D
        computation.

### Phase 2

Update the SOR solver for the case of local dielectric constant. The
dielectric constant on a site is a function of fluids concentration
(depending on the LB model). SOR with local epsilon requires the
calculation of, at each site \(\vec{x}\), the gradient of
\(\epsilon(\vec{x})\) and the electric field (\(- \nabla \phi\)). Code is
being developed and tested @ Barcelona. Then it will be passed to
Eindhoven to implement. New round of tests comparing Barcelona-Eindhoven
results (as above).

### Phase 3

Dynamical coupling of charges and fluids. Charge dynamics is the sum of
advection (will be implemented in the same was as it is in Barcelona
code, elb, using the velocity of the fluid at each site) and diffusion.
For diffusion, the calculation of fluxes will proceed as in
[@bib:capuani-pagonabarraga-frenkel:2004], i.e. as implemented in elb,
but with an additional term in the excess chemical potential which is
due to the different affinities, or solvation energy, respect to the red
or blue fluid (in the expression for V solvation, however, only the
order parameter is used). As for the body-force on the fluid due to the
presence of charge, see (11)
[@bib:rotenberg-pagonabarraga-frenkel:2010], but only the terms with
charge should be considered, as \(\phi \nabla \phi\) term contribution
should be included in the Shan-Chen method, i.e. we are left with
\(\rho_+ + \nabla \mu^{\mathrm{ex}}_+ + \rho_- \nabla \mu^{\mathrm{ex}}_-\).
These extra forces are effectively included by modifying the Shan-Chen
method.

Development document 2: Detailed development plan
-------------------------------------------------

@warning  This is an only-slightly-modified copy of an original
development document and may not represent the current state of affairs.
Giovanni Giupponi, Departament de Fisica Fonamental, Universitat de
Barcelona, Carrer Martí i Franques, 08028 Barcelona (Spain).

Things to do to implement charge and mixture dynamics on NSKG (two-fluid
Shan-Chen binary mixture lattice-Boltzmann code). No particles for now.

### Extra terms for the dynamics of ions due to the presence of the two fluids

The dynamics of ions is caused by the combined action of diffusion and
convection. As the convective part takes into account only the local
fluid velocity and the density of charge at a point, this does not need
to be modified respect to the already implemented code in ENSKG.
Diffusive processes, on the other hand, are explicitely affected by the
presence of the two fluids via the solvation potential
\(V(\vec r) = \Delta \mu_{\pm} \frac{1 + \phi(\vec r)}{2}\), where
\(\phi(\vec r)\) is the local composition of “oil-water”, see
[@bib:rotenberg-pagonabarraga-frenkel:2010] for details. For binary
mixtures, this term has to be explicitely added to the free energy of
the ions to include all relevant thermodynamics effects and will
therefore modify the ionic chemical potentials which are used to
calculate the ionic currents (neglecting inertial effects) via the
formula (14, [@bib:capuani-pagonabarraga-frenkel:2004]). The
\(\mu^{ext}_k(\vec r , t)\) in (14,
[@bib:capuani-pagonabarraga-frenkel:2004]) will have to be updated to
include terms due to the solvation potential \(V(\vec r)\) (formula 6,
[@bib:rotenberg-pagonabarraga-frenkel:2010]).

### Code implementation

As it is now, charge fluxes are calculated in ch\_dynamics.c, around
line 1404. For each lattice site, looping over the n.n., first the local
and the n.n. electrostatic potential is booked into a variable (note the
contribution from an external electric field)

    real phi_tot    = phi_sor[index][k+1]   + \
      ( params.e_slope_x * i + params.e_slope_y * j + params.e_slope_z * k );
    real phi_tot_nn = phi_sor[indexp][kp+1] + \
      ( params.e_slope_x * ip + params.e_slope_y * jp + params.e_slope_z * kp );

Then, the discretized version of (14
[@bib:capuani-pagonabarraga-frenkel:2004]) is computed, i.e. formula (15
[@bib:capuani-pagonabarraga-frenkel:2004]). The analytical form of the
code implementation does not formally correspond to (15
[@bib:capuani-pagonabarraga-frenkel:2004]) but it is rather an
algebrical recombination that it is still there for historical reasons.
Here we have to make our changes, adding the extra term due to the
solvation potential. It is not at all a problem to pass to the routine
the extra arguments needed, i.e. the input parameters of solvation
energy differences and more importantly the composition \(\phi\) field.
Rather, a carefully checked algebric formula for lines @@@ has to be
derived and translated to code.

    double exp_dphi, exp_min_dphi;
    exp_dphi = exp( phi_tot_nn - phi_tot + jump_x + jump_y + jump_z );
    exp_min_dphi = 1.0 / exp_dphi;
    /* Flux due to electrostatic and density gradients */
    double flux_link_plus, flux_link_minus;
    @@@ flux_link_plus =  -0.5 * ( 1 + exp_min_dphi ) * \
                 ( c_plus[indexp][kp] * exp_dphi - c_plus[index][k] );
    @@@ flux_link_minus = -0.5 * ( 1 + exp_dphi ) * \
                 ( c_minus[indexp][kp] * exp_min_dphi - c_minus[index][k] );
    real f_microions = params.kbt * ( flux_link_plus + flux_link_minus );

\[UPDATE-BEG\]

After having revised the (14, 15
[@bib:capuani-pagonabarraga-frenkel:2004] formulae), there is no need to
stick to this awkward algebric fomulation of (14,15). So, we derive
general formulae for the \(j_{ki}(\vec r)\) (15,
[@bib:capuani-pagonabarraga-frenkel:2004]), which is
$$j_{ki}(\vec r) \sim  \eta_k (\vec r + \vec c_i) \big( 1.0 + e^{\beta \Delta_k^{ex}} \big) - \eta_k (\vec r) \big( 1.0 + e^{-\beta \Delta_k^{ex}} \big)$$
where
\(\Delta_k^{ex} = \mu_k^{ex}(\vec r + \vec c_i) - \mu_k^{ex}(\vec r)\).
Therefore, for a binary mixture with ions, as
\(\mu_{\pm}^{ex}(\vec r) = \pm e \Phi(\vec r) + \delta_{\pm}\frac{1+\Psi(\vec r)}{2}\)
(\(\delta_{\pm}\) are input parameters), we would code for the positive
ions

    flux_link_plus = c_plus[indexp][kp] * ( 1.0 + exp_delta_mu_plus ) -
                     c_plus[index][k] * ( 1.0 + inverse_exp_delta_mu_plus );

with

    exp_delta_mu_plus = exp( phi_tot_nn - phi_tot + jump_x + jump_y + jump_z
        + \frac{\delta_+}{2}(\phi[indexp][kp] - \phi[index][k]));

and `inverse_exp_delta_mu_plus = 1.0 / exp_delta_mu_plus`.

\[UPDATE-END\]

Finally, accumulation of fluxes on a local lattice site due the
different n.n.s is performed. It is clear that these two ifs should not
be checked here if we want to maximize performance.

    if(inside[index][k]==FLUID){
      if(inside[indexp][kp]==FLUID ){ /* both links in fluid */
        flux_link_plus  *= ( D_plus  / delta[l] );                                                                                  flux_link_minus *= ( D_minus / delta[l] );

        flux_site_plus[index][k]  += flux_link_plus / scale;
        flux_site_minus[index][k]  += flux_link_minus / scale;

### Local SOR

As it stands, we have in both codes a shaky implementation of local SOR,
the algorithm to resolve Poisson equation for the case of a not constant
dielectric. The good news is that the two implementations produces the
same results with toy set-ups, the bad news is that for more general
set-up convergence is not guaranteed: the potential gets to a converged
shape but then it increments monotonically during iterations, never
allowing to trigger the 'converged' switch. This is not really a problem
for the Physics that depends only on the electric field (which has
converged correctly). Nonetheless, this justifies the 'shaky' in shaky
implementation.

### ENSKG: Coupling to the fluid

In our implementation of the electrokinetic flow in a binary mixture, we
use an order parameter, \(\varphi\), to identify the fluid local
composition. We model the local concentration dynamics using a
convection-diffusion approach and only exploit the lattice-Boltzmann
approach to follow the barycentric fluid velocity. In this approach, the
fluid acceleration induced by the local charge and the force due to the
differential affinity of the electrolyte for one of the two fluid phases
enters always as an external field acting on the barycentric velocity,
\(\vec{F}_{\mathrm{ex}}\). In the Shan-Chen approach to multiphase fluid
flow the fluid is represented by the densities of each fluid species.
Each of them is represented by a lattice-Boltzmann distribution.
Accordingly, the one needs to identify the force that accelerates each
component. The overall force acting on the barycentric velocity emerges
from the appropriate combination of individual fluid forcing. Therefore,
in order to quantify how local electrolyte densities affect the overall
fluid flow, we have to specify the forces that will act on each
component of the fluid mixture. In order to understand how to do this
consistently, I will follow Ref. [@bib:shan-doolen:1995], where the
authors describe how to obtain the hydrodynamic regime from the
Shan-Chen model for a multicomponent fluid mixture. In this contribution
the authors identify \(\vec{F}_{\sigma}\) as the force acting locally on
component \(\sigma\). They identify the barycentric velocity \(\vec{u}\) in
terms of the velocities of each component, \(\vec{u}_{\sigma}\) (their
Eq.(12)), which allow them to show that the force which accelerates the
mean velocity is \(\vec{F}=(1/2)\sum_{\sigma} \vec{F}_{\sigma}\) (also
their Eq.(12) when the applied force does not vanish). This last
expression shows that if we are interested in accelerating the
barycentric velocity, we should distribute the overall local force
symmetrically among all the species. Hence, it is enough to take
\(\vec{F}_{\sigma}=\vec{F}_{\mathrm{ex}}\). As described in
Ref. [@bib:rotenberg-pagonabarraga-frenkel:2010], electrolytes exert
three types of forces. For a homogeneous mixture and for electrolytes
which feel the same affinity for the two solvents, only the
electrostatic force acts on the fluid. In this case we simply have
\(\vec{F}_{\mathrm{ex}}= q(\vec{r}) \vec{E}(\vec{r})\). We account for the
electrostatic asymmetry between the two solvents by including a
density-dependent electric permitivity. In
Ref. [@bib:rotenberg-pagonabarraga-frenkel:2010] this is modeled through
a \(\varphi\)-dependent \(\epsilon\). Now we should generalize the free
energy functional and introduce a dependence on the densities of the two
solvents separately to be consistent with the description of the
lattice-Boltzmann sector. The contribution of ionic species to the fluid
free energy functional writes
$$\mathcal{F} = \int d \vec{r} \sum_{\pm} \rho_{\alpha} \left[ -\mu_{\alpha} + V_{\alpha}^{\mathrm{solv}}(\vec{r}) + \frac{z_{\alpha} e}{2}\psi(\vec{r}) \right]$$
where
$$\epsilon(\vec{r})=\frac{1}{\rho_{2b}-\rho_{1b}}\left[\rho_{2b}\epsilon_1-\rho_{1b}\epsilon_2+(\epsilon_2-\epsilon_1)\rho(\vec{r})\right]$$
and the free energy of solvation can also be expressed analogously,
$$V_{\pm}^{solv} (\vec{r} )= \Delta \mu_{\pm} \frac{\rho(\vec{r})-\rho_{1b}}{\rho_{2 b}-\rho_{1b}}$$
(The contribution to the system energy associated to the immiscibility
of the two fluid phases, or the liquid and gas is accounted by the
Shan-Chen interaction term and it does not appear in the previous
expression). I have introduced the mean density,
\(\rho(\vec{r}) =\rho_1 (\vec{r})+\rho_2(\vec{r} )\) where
\(\rho_{\sigma b}\) stands for the bulk values of the densities of the two
species, \(\sigma = 1,2\). I assume these values are prescribed and known
by the interaction matrix \(G_{ab}\) which identifies the interaction
between the two fluid species in the Shan-Chen model. Therefore they can
be prescribed initially as fixed parameters. If they may change
depending on local conditions (e.g. if \(\rho_{\sigma b}\) depend on
pressure), then this approach may require a reformulation. The ionic
contribution to the system's free energy allows to derive the chemical
potential for the charged species,
\(\mu_{\pm}^{ch}= \frac{z_{\pm} e}{2}\psi(\vec{r})\) and also to obtain
the changes that the presence of ions induce in the local chemical
potential of the fluid mixture
$$\mu_{\sigma}^{\rho}=\frac{\vec{E}(\vec{r})^2}{2}\frac{\epsilon_2-\epsilon_1}{\rho_{2 b}-\rho_{1 b}}+\frac{\rho_+(\vec{r})\Delta \mu_++\rho_-(\vec{r}) \Delta \mu_-}{2}$$
We then use the fact that
\(\nabla p = \sum_{i=\pm} \rho_i\nabla \mu_i^{ch}+ \sum_{\sigma}\rho_{\sigma} \nabla \mu_{\sigma}^{\rho}\)
to identify the force that has to be exerted locally, symmetrically in
the two species, \(\vec{F}_{\mathrm{ex}}=\nabla p\). This force can be
acted on both species of Shan-Chen, as explained at the beginning of
this section.

Development document 3: Local permittivity with P3M {#elec-local-permittivity}
---------------------------------------------------

With the introduction of P3M as a Poisson solver, a new possibly problem
has come to light, in that in its current implementation, P3M does not
support local dielectric constants \(\epsilon(\vec{x})\). According to
Kevin and Gavin, there is no theoretical problem expanding P3M to
include this, but it would require rederivation of some equations and
re-implementation of these equations, which will in all likelihood fall
outside the timeframe allotted to the P3M/NSKG development. What follows
is a short explanation of a possible approximation that does not require
extensive rewrites.

Currently, the P3M routine solves a Poisson equation

$$\label{elec-maxwell}
-\nabla^2 \phi(\vec{x}) = \rho_f(\vec{x}) / \epsilon
\mbox{,}$$

where \(\phi(\vec{x})\) is the electric potential, \(\rho_f(\vec{x})\) is
the free charge in terms of the Maxwell equations (see below), which in
this case is just \(\rho_f(\vec{x}) = \rho_+(\vec{x}) - \rho_-(\vec{x})\),
and \(\epsilon\) is the electric permittivity, which does not depend on
position. In terms of the code, one can see that the right-hand-side of
the equation are the terms being summed in
`nskg_elec_poisson_solver.F90`:
`density_fft(icount) = ( N(i,j,k)%rho_p - N(i,j,k)%rho_m ) / eps_uniform`.

Now going back one step and looking at the physics of the problem,
consider the following Maxwell equation:

$$\nabla \cdot \vec{D}(\vec{x}) = \rho_f(\vec{x})
\mbox{,}$$

where in principle
\(\vec{D}(\vec{x}) = \epsilon(\vec{x})\vec{E}(\vec{x})\). Furthermore, as
the curl of the electric field is zero, we can write
\(\vec{E}(\vec{x}) = -\nabla \phi(\vec{x})\). In the above code example
the simplification \(\epsilon(\vec{x}) = \epsilon\) is used, which then
quickly allows to write
\(\nabla \cdot \vec{D}(\vec{x}) = \nabla \cdot ( \epsilon \vec{E}(\vec{x}) ) = \epsilon \nabla \cdot \vec{E}(\vec{x})\),
which in turn leads to
Eq. [\[elec-maxwell\]](#elec-maxwell){reference-type="eqref" reference="elec-maxwell"}. If we don't apply this simplification we end
up with a generalized Poisson equation:

$$-\nabla^2 \phi(\vec{x}) = \rho_f(\vec{x}) / \epsilon(\vec{x}) + \nabla \epsilon(\vec{x}) \cdot \nabla \phi(\vec{x})
\mbox{.}$$

We see that we both modify the existing right hand side term of the
equation by introducing a spatial dependence of the permittivity and add
another term altogether, according to the product rule. As the valules
for the local permittivity are readily available in the code, the
modification of the first term does not cause any problems. However, the
additional term brings complications. Again, the spatial gradient of the
permittivity is available (although it has to be calculated, currently
using the finite difference scheme used for other gradients in the code
as well), but as we are solving for the electric potential, its spatial
derivative is not known. However, the following approximation is now
proposed. Taking into account that the lattice Boltzmann method already
disallows high velocities and large changes in time, it might be
reasonable to consider the following:

$$\nabla \phi(\vec{x},t) \approx \nabla \phi(\vec{x}, t - \Delta t) + \Delta t \frac{\partial ( \nabla \phi(\vec{x},t) ) }{\partial t}
\mbox{,}$$

where we have now explicitly added the time dependence of the electric
potential. Assuming small changes in time, we could now drop the time
derivative and rewrite the generalized Poisson equation as:

$$-\nabla^2 \phi(\vec{x},t) \approx \rho_f(\vec{x},t) / \epsilon(\vec{x},t) + \nabla \epsilon(\vec{x},t) \cdot \nabla \phi(\vec{x}, t - \Delta t)$$
$$-\nabla^2 \phi(\vec{x},t) \approx \rho_f(\vec{x},t) / \epsilon(\vec{x},t) - \nabla \epsilon(\vec{x},t) \cdot \vec{E}(\vec{x}, t - \Delta t)
\mbox{,}$$

letting \(\Delta t\) represent an iteration of solving the Poisson
equation (initial equilibration allows for an arbitrary number of
iterations, and this time step can also be decoupled from the LB
timesteps). The gradient of the electric field after the previous
iteration is already stored in memory, as the electric field, so this
does not add any additional computational overhead. As at the first
iteration, no information is available on the “zeroth” iteration, we
assume \(\nabla \phi(t) = 0\).

A preliminary test has been performed, using the test “Capacitor with
dielectric lamellae” as a basis. As the Neumann boundary conditions are
not available for P3M, the absolute results are different from the ones
shown in the relevant Benchmarks section – this is to be expected. The
results for this case look promising, as can be seen in
Fig. [6](#fig:elb-capacitor-p3m){reference-type="ref" reference="fig:elb-capacitor-p3m"}. Note that this test case does not
have any moving charges, which gives iterations of the Poisson solver no
freedom to escape its initial condition of \(\vec{E} = 0\), hence this
might be considered even a pathological case. Further tests, which
include moving charges, will be performed if the method is deemed to be
possibly acceptable. The code v6.5.4-452-g9de4a58 has been compiled with
flags `-DELEC -DP3M -DSINGLEFLUID` and executed using the input files
found in `tests/elec/test-local-epsilon`.

![Electric potential in and around a periodic pair of charged plates
with two dielectric lamellae. The P3M approximation is close to the SOR
solution (which is known to be
correct).](../|media|/plots/test-local-eps.png)

To increase the accuracy of this approximation, Timm has suggested the
following: instead of dropping the time derivative of the electric
potential, one could make a similar assumption as before and instead use
the time derivative at the previous time step, which is then accurate up
to a second derivative of the potential:

$$\frac{\partial ( \nabla \phi(\vec{x},t) ) }{\partial t} \approx \frac{\partial ( \nabla \phi(\vec{x},t - \Delta t ) ) }{\partial t} + \Delta t \frac{\partial^2 ( \nabla \phi(\vec{x},t) ) }{\partial t^2}
\mbox{.}$$

To calculate the required time derivative we can use the following
(backward) form
\(\partial f(t) / \partial t = ( f(t) - f(t - \Delta t) ) / \Delta t\),
which can be plugged into the generalized Poisson equation:

$$-\nabla^2 \phi(\vec{x},t) \approx \rho_f(\vec{x},t) / \epsilon(\vec{x},t) + \nabla \epsilon(\vec{x},t) \cdot \left[ \nabla \phi(\vec{x}, t - \Delta t) + \Delta t \frac{\partial ( \nabla \phi(\vec{x},t - \Delta t ) ) }{\partial t} \right] =$$
$$-\nabla^2 \phi(\vec{x},t) \approx \rho_f(\vec{x},t) / \epsilon(\vec{x},t) - \nabla \epsilon(\vec{x},t) \cdot \left[ 2 \vec{E}(\vec{x},t - \Delta t ) - \vec{E}(\vec{x},t - 2\Delta t ) \right]
\mbox{.}$$

Now, in addition to the previous \(\nabla \phi(\vec{x},t - \Delta t)\),
which was still available as `N%E(:)`, we need
\(\nabla \phi(\vec{x},t - 2\Delta t)\), which has to be stored in memory
on the previous step. This is currently handled by an additional array
`E_prev` (only allocated when using P3M with local permittivity).

References:
[@bib:nielsen-janssen:2001; @bib:peter-vangunsteren-huenenberger:2002].

<!---

end of former user-elec.tex


A list of tests regarding ELEC
------------------------------


- [`ek/two_plates`](|gitlabfile|/tests/ek/two_plates)
- [`ek/interface/interface`](|gitlabfile|/tests/ek/interface/interface)
- [`ek/old/electroosmosis/homogeneous`](|gitlabfile|/tests/ek/old/electroosmosis/homogeneous)
- [`ek/old/electroosmosis/driven`](|gitlabfile|/tests/ek/old/electroosmosis/driven)
- [`ek/old/interface/interface`](|gitlabfile|/tests/ek/old/interface/interface)
- [`ek/old/charged-particle/constforce`](|gitlabfile|/tests/ek/old/charged-particle/constforce)
- [`ek/ext_field/ion_flux_net_noflow`](|gitlabfile|/tests/ek/ext_field/ion_flux_net_noflow)
- [`ek/ext_field/ion_flux_net_flow`](|gitlabfile|/tests/ek/ext_field/ion_flux_net_flow)
- [`ek/mass_conservation`](|gitlabfile|/tests/ek/mass_conservation)
