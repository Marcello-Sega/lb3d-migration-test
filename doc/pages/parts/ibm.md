title: Immersed boundary method
Immersed Boundary Method (IBM)
=================================

@TODO A catchy figure!

# Method in a nutshell
The Immersed Boundary Method (IBM) has shown to be a valuable tool to simulate the dynamics of deformable soft particles: one of its main advantages is the possibility to model the boundary in a continuous-like fashion.

The basic idea is having a boundary made of Lagrangian markers (i.e., massless points with coordinates \(\mathbf{r}(t)\)) that can freely move through the fixed Cartesian grid, whose velocity is interpolated from the velocity of the surrounding fluid \(\mathbf{u}(\mathbf{x},t)\),
\begin{equation}
\mathbf{\dot{r}}(t)=\int\mathbf{u}(\mathbf{x},t)\delta(\mathbf{x},\mathbf{r}(t)) d\mathbf{x}\ ,
\end{equation}
where \(\delta({\mathbf{x}},\mathbf{r}(t))\) represents the Dirac's delta distribution. The above equation makes the velocity of the surface equal to the fluid velocity, ensuring thus the no-slip boundary condition.

The momentum exchange between the fluid and the boundary makes one aware of the presence of the other. The total force density the boundary exerts on the fluid is given, once again, by an interpolation:
\begin{equation}
  \mathbf{F}(t)=\int\mathbf{F}_A(\mathbf{x},t)\delta(\mathbf{x},\mathbf{r}(t)) drds\  ,
\end{equation}
where  \(r\)  and  \(s\)  are two Lagrangian parameters needed to describe a generic surface.

Since this scheme should be implemented as a numerical method, we need to discretise the above equations. Therefore, the velocity of the \(i\)th Lagrangian marker and the total force the boundary exerts on the fluid are given by:
\begin{equation}
\begin{aligned}
\mathbf{\dot{r}}_i(t) = \sum_{\mathbf{x}} \mathbf{u}(\mathbf{x},t)\delta_D(\mathbf{x},\mathbf{r}_i(t)) \Delta\mathbf{x}\ , \\
\mathbf{F}(\mathbf{x},t) = \sum_i \mathbf{\varphi}_i(t)\delta_D(\mathbf{x},\mathbf{r}_i(t))\ , 
\end{aligned}
\end{equation}
where \(\mathbf{\varphi}_i\) is the total force on the node \(i\) (which depends on the membrane model employed, see section below), \(\delta_D(\mathbf{x})\) is a discretised version of the Dirac delta distribution \(\delta(\mathbf{x})\) and the sum runs over the Lagrangian markers used to discretise the boundary. 


# NSKG implementation

## Interpolation kernel
The discrete delta function \(\delta_D\) can be factorized in the following manner: 
\begin{equation}
\delta_D(\vec{x}) = \frac{1}{\Delta x}\phi_k(x)\phi_k(y)\phi_k(z)\  ,
\end{equation}
where the functions \(\phi_k(r)\) are called **interpolation stencils**, and the subscript \(k\) refers to the number of lattice (Eulerian) nodes involved in the spreading and interpolation operations along each coordinate axis. 

NSKG implements the following interpolation stencils, selected by specifying the `IB%interpolation_range` parameter:

* 2-points:
\begin{equation}
\phi_2(r)=
\begin{cases}
1-\vert r\vert & 0\le \vert r\vert <\Delta x\ ,\\
0 & \Delta x\le \vert r\vert\ . 
\end{cases}
\end{equation}

<!--
Until 29e7d542770415353925e6ba55f3c13c117611c0, there was a nicely typeset variant of the 3-point stencil in here. Restore, if you need it & don't feel you'd like to re-TeX it.
-->

* 4-points:
\begin{equation}
\phi_4(r)=
\begin{cases}
\frac{1}{8}\left( 3-2\vert r\vert+\sqrt{1+4\vert r\vert -4r^2}\right) & 0\le \vert r\vert<\Delta x\ ,\\
\frac{1}{8}\left( 5-2\vert r\vert-\sqrt{-7+12\vert r\vert-4r^2}\right) & \Delta x\le \vert r\vert<2\Delta x\ ,\\
0 & 2 \Delta x\le \vert r\vert\ .
\end{cases}
\end{equation}

![2-point and 4-point stencils](../../|media|/plots/ibm_stencils.png)

<!--TODO
What are the specific advantages/disadvantages? (Performance, halo extent.
ENDTODO-->


## Properties of the membrane (membrane models)
The file specified by `IB%geometry_file` defines the meshes present in the simulation.
It contains a `&mesh_properties/` entry per mesh.
Properties of each mesh are set by adding parameters to this namelist.

### Strain models
Select the strain model with the variable `strain_model`, that can have one of the following values:

* Default: no strain model (`none`): <!--TODO Describe what this is meant for ENDTODO-->

* Skalak model (`sk`): <!--TODO Describe what this is meant for ENDTODO-->
\begin{equation}
w = \frac{k_s}{8}\left(2I_1^2+4I_1-4I_2-I_2^2\right)+ \frac{k_\alpha}{8} I_2^2
\end{equation}

* Skalak alternative model (`skam`): <!--TODO Describe what this is meant for ENDTODO-->
\begin{equation}
w = \frac{k_s}{12}\left(I_1^2+2I_1-2I_2\right)+ \frac{k_\alpha}{12} I_2^2
\end{equation}

* Surface tension (`st`): 
\begin{equation}
\mathbf{\tau}_e = \sigma \mathbf{I}\ ,
\end{equation}
with \(\sigma\) representing the surface tension.

<!--TODO Is `st` placed here right? `skm`? `gsm`? ENDTODO-->

### Bending models
Select by setting `bending_model` to:

* `none` (default)
* `knm`

### Viscous model
Membrane viscosity is configured with the variable `viscous_model`:

* `none` (default)

* Boussinesq-Scrive law (`bel`):
\begin{equation}
\pmb{\tau}_\nu =  \mu_s \left[2\mathbf{e} -\text{tr}(\mathbf{e})\mathbf{P}\right] + \mu_d \text{tr}(\mathbf{e})\mathbf{P}\ ,
\end{equation}
with \(\mu_s\) and \(\mu_d\) being the shear and dilatational membrane viscosities, respectively; \(\mathbf{P}\) is the projector tensor to the 2D surface; \(\mathbf{e}\) is the surface rate of strain
\begin{equation}
\mathbf{e} =\frac{1}{2}\left\{\mathbf{P}\cdot\left[\left(\pmb{\nabla}^{\pmb{S}}\pmb{u}^{\pmb{S}}\right)+\left( \pmb{\nabla}^{\pmb{S}}\pmb{u}^{\pmb{S}}\right)^{\dagger}\right]\cdot\pmb{P}\right\}\ ,
\end{equation}
with \(\boldsymbol{\nabla}^{\boldsymbol{S}}\) the surface projection of the gradient operator and \(\mathbf{u}^{\boldsymbol{S}}\) the local membrane velocity.


# User interaction
The `&immersed_boundary/` namelist in the main input file is the central place for configuration of IBM as a whole.

Configuration takes place on the level of a [[nskg_parms_ibm]] object, which inherits its properties from [[nskg_parms_mesh_set]].

## Provide meshes
The entries in the main namelist that configure the meshes are

* `IB%meshfiles` (sources of triangulated mesh input) <!--TODO data format/typical creation route) ENDTODO-->
* `IB%geometry_file`, specifies another file, that configures the individual meshes (pre-processing, properties, placement) via `&mesh_properties/` entries.


## Interactions
### Pair forces
IBM particles have the `types` property that lets you define how the particle surface (triangles)
interacts with other objects in the simulation box (walls, rocks).

The configuration of these forces is described on the [page about interactions](../../|page|/parts/interactions.html).

### External forces
@TODO `ext_force` property in the `&mesh_properties/` namelists
<!--
see also gitlab issue #587
-->


## Output quantities
```
mesh_default_area.dat
mesh_default_bending_energy.dat
mesh_default_center.dat
mesh_default_center_weighted.dat
mesh_default_stress.dat
mesh_default_volume.dat
```
@TODO Meaning, post-processing tips, examples

(e. g. refer to an easy and insightful test)


@TODO WIP...

* Further reading/literature references/name-dropping (chances are, there is already quite some of the important stuff in the [bibliography file](|gitlabfile|/doc/nskg_literature.bib).
* Clarify the relation to the (current implementation of the) concept of Lagrangian objects (also helpful for programmers, to understand the inheritance structures of the objects).
* Simulation set-up (usage of meshes, most important namelist parameters (but not an exhaustive interface reference))
* What shape ist the output data? Typical post-processing strategies? See also the generic [**File I/O page**](../../|page|/parts/fileio.html) (we don't want to duplicate documentation, move it here if it fits better here.)
* Caveats, limitations of the method itself, and its implementation in NSKG
* A few links to the usage of IBM in NSKG (test files). Below, find just th list of all tests that match `ibm`, but there might be a few worth highlighting.
@ENDTODO


# A list of tests regarding IBM
  -----------------------------

- [`ibm/viscous_models/boussinesq_scriven_law`](|gitlabfile|/tests/ibm/viscous_models/boussinesq_scriven_law)
- [`ibm/bending_models/bending_energy/kantor_nelson_algorithm`](|gitlabfile|/tests/ibm/bending_models/bending_energy/kantor_nelson_algorithm)
- [`ibm/simple/triangle`](|gitlabfile|/tests/ibm/simple/triangle)
- [`ibm/basics/body-force-on-particle`](|gitlabfile|/tests/ibm/basics/body-force-on-particle)
- [`ibm/basics/momentum-conservation/no-force`](|gitlabfile|/tests/ibm/basics/momentum-conservation/no-force)
- [`ibm/basics/momentum-conservation/kantor-nelson`](|gitlabfile|/tests/ibm/basics/momentum-conservation/kantor-nelson)
- [`ibm/basics/momentum-conservation/skalak`](|gitlabfile|/tests/ibm/basics/momentum-conservation/skalak)
- [`ibm/basics/volume-conservation/surface-tension`](|gitlabfile|/tests/ibm/basics/volume-conservation/surface-tension)
- [`ibm/basics/wall-particle-force/wall-particle-interaction`](|gitlabfile|/tests/ibm/basics/wall-particle-force/wall-particle-interaction)
- [`ibm/basics/particle-particle-force`](|gitlabfile|/tests/ibm/basics/particle-particle-force)
- [`ibm/strain_models/skalak_alternative`](|gitlabfile|/tests/ibm/strain_models/skalak_alternative)
- [`ibm/strain_models/surface_tension`](|gitlabfile|/tests/ibm/strain_models/surface_tension)
- [`ibm/strain_models/skalak`](|gitlabfile|/tests/ibm/strain_models/skalak)
- [`basic/io/dump/ibm`](|gitlabfile|/tests/basic/io/dump/ibm)
- [`basic/io/checkpointing/ibm`](|gitlabfile|/tests/basic/io/checkpointing/ibm)
- [`perf/ibm/1core`](|gitlabfile|/tests/perf/ibm/1core)
