title: The parts of NSKG

Beyond the common introductory part of the manual, NSKG offers components that are composed to define the properties of the system to be simulated.

This directory presents the general ideas behind each of the scientific methods and their implementation structure (interrelation, configuration/setup, limitations).

We provide links “backwards” into the foundations in scientific literature, as well as “forwards” into examples of use, tests, source files, and development information.
