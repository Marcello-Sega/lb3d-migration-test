title: Quickstart: My First NSKG simulation

Let's assume you have got the compilation running, and the NSKG binary available at `/__SOURCE/PATH__/build/nskg/src/nskg`.

## Input file, Invocation

Let us start with this rather simple (yet already non-trivial) [input file](|gitlabfile|/tests/basic/simple/simple/input):

```fortran
{!../media/input_simple!}
```

This example comes from the source repository. As a such, it is included in test suite. You can directly run it yourself from your CMake build directory with `ctest -V -R basic-simple-simple`.

For running own simulations, create (or copy) this file somewhere, and instruct your local copy of NSKG to read it like
```shell
/__SOURCE/PATH__/build/nskg/src/nskg -f input
```
on the command line.

### Results of the run
The simulation runs, outputs diagnostic information to the terminal, and periodically writes its velocity profile into the files `vel{x,y,z}_simple_<timestamp>-<pid>.h5`. 

### Further Reading
For more details on invocation, like submission to cluster queues, read the [**Invocation page**](|page|/../invoking_nskg.html).


## Post-processing
This **simple Python** script is provided along this simulation [in the test directory as well](|gitlabfile|/tests/basic/simple/simple/analyse.py). It reads the velocity snapshots and compares them with analytical expectations:
```python
{!../media/analyse_simple.py!}
```

For production simulations, this kind of post-processing can be arbitrarily intricate (derivation of complex observables, visualisation, videos…).
Among the tests, you can also find [**a slightly more elaborate analysis script**](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_oil_invasion_visual/visualization.py). Execute the simulation as instructed by [its input file](|gitlabfile|/tests/lb/invasive-flow/inv_fluid_oil_invasion_visual/input) and it will show a video from the evolution of two fluid populations over simulation time.

<!--
TODO or have a look into our GitLab CI pipeline, where it is included as well.
-->

Note that most of our physics test follow the route of an NSKG simulation (defined by its `input` file), followed by post-processing done in Python.
(Although arguably much more complex), all these test are equally adequate to understand the set-up and operation of components within NSKG, and their interconnections. Hence this manual will regularly refer to test cases.
