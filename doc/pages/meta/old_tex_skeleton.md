title: Monolithic NSKG User Manual (transitional)

This is the mere skeleton of what used to be the TeX-based user
manual. For historic reasons only; obsolete once we have taught FORD
our expectations of a new, browseable structure.
For the structure, see also the [TeX user manual outline](temp.html).

General introductory stuff moved to the [NSKG !intro](../nskg_intro.html)
and [Concepts](../nskg_concepts.html) pages.

LB-MRT {#subsec:LB-MRT}
------

moved to [lb sub-page](../parts/lb.html)

Local Relaxation times
----------------------

moved to [lb sub-page](lb.html)

The `fixed_input` namelist {#fixed-input}
--------------------------

moved to [input_temp page](../parts/input_temp.html)

Boundary condition/topology modules {#fixed-input:boundary}
-----------------------------------

moved to [bc sub-page](../parts/bc.html)

Relaxation time namelists {#relaxation-time}
-------------------------

moved to [lb sub-page](../parts/lb.html)

Body force modules {#fixed-input:force}
------------------

moved to [lb sub-page](../parts/lb.html)

The `variable_input` namelist
-----------------------------

moved to [input_temp page](../parts/input_temp.html)

Invoking NSKG
------------

moved to [InvokingNSKG page](../../invoking_nskg.html)

The output files {#OUTPUT-FILES}
----------------

moved to [output page](../../output.html)

Parallel performance
====================

moved to [parallel page](../parts/parallel.html)

Numerical instabilities
=======================

moved to [lb sub-page](../parts/lb.html)

Postprocessing with parallel HDF5 {#sec:hdf5}
=================================

moved to [parallel sub-page](../parts/parallel.html)

Molecular dynamics plug-in (`MD`)
=================================

moved to [MD sub-page](../parts/md.html)


Electrolytes plug-in (`ELEC`) {#sec:ELEC}
=============================

moved to [ELEC sub-page](../parts/elec.html)


Lagrangian boundaries plug-in (`LAGR`)
======================================

moved to [ibm sub-page](../parts/ibm.html)


Multi-component implementation
------------------------------

moved to [pgr sub-page](../parts/pgr.html)

Performance
-----------

moved to [ibm sub-page](../parts/ibm.html)

Algorithm structure
-------------------

moved to [ibm sub-page](../parts/ibm.html)

Benchmark
---------

moved to [ibm sub-page](../parts/ibm.html)

Thermal fluctuations
====================

moved to [th sub-page](../parts/th.html)

Permeability calculations
=========================

moved to [bc sub-page](../parts/bc.html)

Postprocessing and visualization
================================

moved to [parallel sub-page](../parts/parallel.html)

Examples
========

Example studies can be found in the *examples* folder. Each example
usually contains the input file(s) and an analysis script including
explainatory comments.

Testing and version history
===========================

Further reading
===============

Miscellaneous references (these were in the bibliography before the
change to BibTeX, but not referenced anywhere else):
[@bib:qian-dhumieres-lallemand:1992; @bib:martys-chen:1996; @bib:olson-rothman:1997; @bib:dhumieres:1992].

