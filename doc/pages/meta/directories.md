title: The structure of the NSKG source code tree

# Directories within `libnskg/src/`

| Part    | Directory             | Docstate | Explanation/Comment                                               |
| ------- | --------------------- | -------- | ----------------------------------------------------------------- |
| core    | `core`                | S        | .                                                                 |
| core    | `globals`             | S        | .                                                                 |
| core    | `input`               | S        | .                                                                 |
| core    | `parallel`            | B        | .                                                                 |
| core    | `system`              | B        | .                                                                 |
| core    | `timer`               | T        | .                                                                 |
| physics | `ad`                  | P        | Advection                                                         |
| physics | `bc`                  | S        | Boundary conditions                                               |
| physics | `cg`                  | P        | Colour gradient                                                   |
| physics | `chkp`                | T        | Checkpoints                                                       |
| physics | `elec`                | P        | Electrolytes                                                      |
| physics | `ibm`                 | P        | Immersed boundary method                                          |
| physics | `interactions`        | B?       | Parent class for interactions                                     |
| physics | `lb`                  | P/S      | Lattice-Boltzmann                                                 |
| physics | `md`                  | P/S      | Molecular Dynamics                                                |
| physics | `offlattice_objects`  | T/B      | Objects living in physical space (rather than on the LB lattice)  |
| physics | `pgr`                 | P?       | Peng-Robinson multi-component                                     |
| physics | `psi`                 | P        | Kupershtokh pseudopotential                                       |
| physics | `rocks`               | P        | Static obstacles                                                  |
| physics | `sc`                  | P        | Shan-Chen two-phase model                                         |
| physics | `th`                  | P        | Thermal                                                           |
| physics | `walls`               | P        | Hard boundaries                                                   |
| misc    | `old`                 | L        | (get rid of this rather sooner than later, but It's Complicated™) |
| misc    | `part`                | ?        | .                                                                 |

## “Docstates”
(All these classifications are by far not fixed decisions, but to be understood as requests for comment.)

* **?:** Needs further clarification
### Scientific
* **P:** Stand-alone page
* **S:** Subdivide: worth being split up into several sub-pages
### Technical
* **T:** Technical information, probably relevant for (power) users
* **B:** Backend-only, probably irrelevant for anyone except developers
* **L:** Legacy. We're about to get rid of that anyway

Our goal is a comprehensive coverage of “P” and “B” pages within the [module registry](../parts/index.html), reasonably easy to read, and enriched with references literature and [our test suite](alltests.html).
