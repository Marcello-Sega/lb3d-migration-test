title: Caveats for overhauling old inline documentation

# Invisible functions

We have seen that after the `contains` keyword, not all subroutines/functions are automatically displayed in the module overview. E. g. [[nskg_rocks_class]].

2022-08-26 Not invisible, but hidden in procedures according to the code structure.

invisible subroutine comments:[[nskg_ad_class]] 

```
>   subroutine compute_diffusion(this, input, output)
>     !> @ TODO  MS this has to be completely reengineered with nskg_ftcs, with an eye on n_fields/local_diffusion
>     !> @ warning at the  moment it uses only the first diffusion coefficient!!!
```
Cannot be shown

# Tips for writing comments

@TODO To be merged into [FORD guide for developers](ford_primer.html)

### Comments for subroutine/function included in `Type` or `Interface`

1. For subroutine or functions **contained in `Derived Types`**, write comments for users **right below subroutine/function**, and please **write full sentences with punctuations**. Direct comments separated by blank lines or `!>` might make an end to the following comments and make them undisplayed in the manual.  It can be easier to maintain the user guide in this way. Of course comments can be scattered among the codes, but in some cases inconsistency of commenting format might cause part of comments missing in the user manual.

1. Comments after `@` of the subroutine/function will not be shown in the manual. Therefore, it is better to use other ways to emphasize contents.

1. When using special comment indicators like `TODO` and `NOTE`, either a prefix `@` or a following `:` can cause the whole line of comment invisible. Therefore, only a bare `TODO` or `NOTE` is allowed to use.

1. Variables can be safely commented with special comment indicators. Syntax follows the general Markdown syntax. 

1. Please use blank line or separate `!>` to separate different parts of the comment, either for paragraphes or blocks. This might cause a little bit more effort, but necessary for the user manual.

1. Comments in a loop inside a subroutine/function will be shown directly following previous comments. Therefore, unless necessary, please do not start comments in the code with `!>`.

### Comments for variables

1. For `Type` and `Interface`, only variables with `public` can be shown in the manual. 

1. For variables in a subroutine/function, only the ones defined in the function can be used. `Result` elements are not shown.


### General comments for subroutine/function

1. Only the subroutine/function designated `public` in the module will be shwon in the manual. Others will remain invisible. 




# Overview

Generally there are threee types of comments in this manual:

1. Instructions on the functionality or purpose of this block of codes

1. Indication on what is going on with specific part of the code

1. Special comments with indicators: NOTE, TODO, WARNING or BUG in the code that needs additional attention

Among these, the first and third one are what end-users care about, and the second is what concerns the developers. Since we are building this manual for users, we should focus the corresponding parts.

**Therefore, plenty of the second type comments will not be included in this manual and not discussed in the following guide. Newly added comments should also be user-friendly.**

The third type of comments usually have higher priority over plain comments, that corresponding content will be highlighted in a separate block color in the manual.

However, under some occasions the blocks may not show up or have lower priority than some other marks. Then authors should take care of their styles of documentation.

# FORD commenting structures

## module

To comment a module, the position of comments needs to be within the range of the module block and not within a subroutine/function/loop. Comments outside the module block will be displayed in the source code page, not in the module page.

Here is an example:

```
!> This line of comment will be displayed in the source code page.

#include "nskg.h"

!> This line of comment will be displayed in the source code page.

module nskg_invasion_module

!> This line of comment will be displayed in the module page, usually as the description for the whole module, and this is alsothe recommended way for documentation.
!> To make the comment consistent, please try putting all module description at the same place rather than scattering all over the code.

  use nskg_common_headers_macro
  use nskg_features_class
  use nskg_log_module
```

## variables

**We still need more insights into how variables should be commented.**


## subroutine/function

To tell what a subroutine/function does, please add directly under the line with `subroutine` or `function`. Example:
```
subroutine read_rocks_rb(this)
  !> red rock reading functions. 	# Text here will be displayed at the first paragraph of the subroutine description.
  class(nskg_rocks),target :: this
  character(len=1024) :: full_obs_fname_r
  character(len=1024) :: full_obs_fname_b
  character(len=1024) :: full_obs_fname_s
  full_obs_fname_r = trim(obs_folder)//'/'//trim(obs_file_r)
  full_obs_fname_b = trim(obs_folder)//'/'//trim(obs_file_b)
  full_obs_fname_s = trim(obs_folder)//'/'//trim(obs_file_s)
  !>  Text here will be displayed following the previous comment in the same paragraph.
  !>
  !>  Text here will be displayed in the following paragraph of comments.
  if
  ...
  !  This kind of inline comments without a `>` following `!` will not be displayed in the manual.
  ...
  !>  Text in a loop will not be displayed in the manual.
  end if
```
Text subjected to the subroutine/function will follow previous displayed comments in the same paragraph, if no `!>` lies in between. The same thing applies also to the third type of special comments - if you do not want your `TODO`, `NOTE`, `WARNING`, and `BUG` stick to unnecessary contents, please add a separate line of `!>` for an ending, or before the next special comment as a heading.

Since the special comments indicator have the same priority, if there are no separation in between different or the same indicator, there will be embedment in the final manual, such as a `TODO` block inside a `NOTE` block, but not vice versa. Generally authors are encouraged to not have such embedment in their comments, unless necessary.

**Notice:The four special content indicators work the same whether letters are capitalized or not, i.e. `TODO` and `todo` works the same. Whereas, capitalized indicators are recommeded because some of them can also be highlighted in code editors. To use these indicators, add `@`before the indicator.**

@TODO Content of @TODO

@NOTE Content of @NOTE

@WARNING Content of @WARNING

@BUG Content of @BUG

@NOTE note... @TODO embedded content

### Invisible comments

For some codes with structures calling functions or subrutines, the comments written in the function region might not be fully shown due to some technical problems, i.e., only the first line can be shown in the corresponding file page. If you are to show multiple lines, please **DO NOT** separate different lines with blank lines or `!>`, otherwise the rest of the comments might be lost in the page.

Example:
```
type                                                 :: nskg_advection_diffusion
      class(nskg_parms_ad), allocatable                  :: parms
      class(nskg_scalar_field), allocatable,dimension(:) :: fields
      type(nskg_ftcs_solver)                             :: ftcs
      type(nskg_laxwendroff_solver)                      :: laxwendroff
      type(nskg_scalar_field),allocatable,dimension(:)   :: local_diffusion_coeff
      type(nskg_scalar_field),allocatable,dimension(:)   :: local_background_diffusion_coeff
      type(nskg_halo),pointer                            :: halo
      integer                                           :: n_fields
      type(nskg_geometry), pointer                       :: geometry
      class(nskg_lattice), pointer                       :: lb_lattice
      class(nskg_vector_field), allocatable              :: grad_pot
      logical                                           :: initialized=.false.
   contains
      procedure, pass(this)                             :: init => init_advection_diffusion

...

   subroutine init_ad_diffusion_coeff(this)
     !> local_background_diffusion_coeff must always be allocated, to allow later on to check whether they are inited or not
     !> Note that it won't take memory, as long as the fields are not init'd.

```
Comments for subroutine init_ad_diffusion_coeff(this) will be shown in one line.


```
   subroutine init_ad_diffusion_coeff(this)
     !> local_background_diffusion_coeff must always be allocated, to allow later on to check whether they are inited or not

     !> Note that it won't take memory, as long as the fields are not init'd.

```
Only the first line of the comment for subroutine init_ad_diffusion_coeff(this) will be shown.
 

```
   subroutine init_ad_diffusion_coeff(this)
     !> local_background_diffusion_coeff must always be allocated, to allow later on to check whether they are inited or not
     !>
     !> Note that it won't take memory, as long as the fields are not init'd.

```
Only the first line of the comment for subroutine init_ad_diffusion_coeff(this) will be shown.


# Conversion to Markdown

Many of the codes were written with old TeX syntax and for the new documentation we need to use Markdown format. Therefore, we need to unify the comment syntax.

Following are examples of correct conversion:

* inline code
```
	\c XXX YYY	===>	`XXX` YYY
	\p XXX YYY	===>	`XXX` YYY
```
* bold
```
	\b XXX	===>	**XXX**
```
* item list
```
	\li XXX	===>	* XXX
```

* Change`!<` to `!>` now, because otherwise the comments will be ignored by FORD.

* FIXME: please change `FIXME` into `@TODO` or `@WARNING` according to the situation.
	
* Double `!!`: A comment starting with `!!` above the target line is the same as a comment starting with `!>` below or in the same line as the target line.









