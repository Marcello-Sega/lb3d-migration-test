title: Template for user manual pages

This page is meant for a quick start of creating contents for new pages of the NSKG user manual. Provided is a possible structure for these pages when someone is going to write something and can be altered as their will. Naturally, the content in the additional description pages should not overlap what has been present in the auto-generated pages. "As much as necessary, as little as possible". Meta-links can be really your good friends.

# Title: the name of the part you are taking care of

Basically we use the corresponding names as in the GitLab repository.

## Introduction

Introduction should contain the methods, priciples and functions of this part of codes, giving a user a brief overview of where they are, why they are here, what they are going to know and how they are going to practice. 

### Functionality


### Methods


### Priciples


## Submodules

Should contain links to related submodules, and probably some brief introductions. 

## Input parameters

Should contain the input parameters used in this part, possibly a (link to) namelist of variables. Necessary descriptions are needed.

## Outputs

Should contain the outputs related to this part, possibly a (link to) namelist of variables. Datasets and images are also possible. Please add necessary descriptions accordingly.

## Tests/Examples  

Should contain the links to tests and probably (links to) examples as 'tutorials'. Detailed instructions are necessary for users.

## FAQ

Users may have questions and encounter with problems in their work. Questions and answers are important in helping users working with the software more smoothly. Any questions from users or developers can be added in this place. The FAQ section should be frequently updated. Of course, metamedia can be and is encouraged to be used here. The FAQ part for different pages might be integrated into one, but this is not a big problem.
