title: Coding Guideline

<!--
Originally based on the commit message of 55d3ff342bf99086710445dda228c8d8bb2167ac (2020-11-23)

TODO Dedup with "./howto_fortran.md"
-->

# What we call “modern” these days
aka *The Rewrite Way of Surviving Fortran.*

This page introduces to the most important **modern (OOP) syntax**, as well as **our conventions** how to use them (since rewrite times) to keep the code in a sightly, uniform and maintainable state.


## Classes and Modules

### Parameter definition, namelist read

The idea is to follow the scheme of a class for the input parameters (associated to a namelist for backward compatibility) + the main class associated to the feature.
```fortran
   use nskg_parms_module

   implicit none

   private

   namelist /color_gradient/ cg ! BACKWARD COMPATIBILITY, remove me and the other namelists

   type,extends(nskg_parms_psi)                   :: nskg_parms_cg
       real(kind=rk),dimension(MAX_PAIRS)        :: surface_tension  = 0.1
       real(kind=rk),dimension(MAX_COMPONENTS)   :: wall_surface_tension  = 0.1
       character(len=128)                        :: model = 'leclair'
   contains
       procedure, pass(this)                     :: bcast_parms => bcast_parms_cg
       procedure, pass(this)                     :: read_namelist => read_namelist_cg
   end type

   type(nskg_parms_cg) :: cg = nskg_parms_cg(&
       surface_tension  = 0.1                ,&
       wall_surface_tension  = 0.1           ,&
       model = 'leclair'    )

   public nskg_lattice_cg, cg
```

`read_namelist` should be part of the input parameters class and should be
called only from the system class. In the future, all this should be
implemented using abstract classes.
