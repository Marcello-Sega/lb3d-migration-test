title: Problems to be solved

# Invisible functions

We have seen that after the `contains` keyword, not all subroutines/functions are automatically displayed in the module overview. E. g. [[nskg_rocks_class]].

26.08.2022 Not invisible, but hiden in procedures according to the code structure.

***
[[nskg_invasion_module]]

Subroutine `extrapolate_maxz` and `mirror_rock_maxz`, `mirror_rock_minz` are all missing in the `nskg_invasion_module`.

***
[[nskg_invasion_slip_module]]

Subroutine `nskg_invade_slip_channel(pr,pb,pg)` is invisible in the manual.

***
[[nskg_invasion_vel_module]]

Subroutine `nskg_invade_shear_bot_wall(ux_i,uy_i,uz_i,offset)` and `nskg_invade_shear_top_wall(ux_i,uy_i,uz_i,offset)` are invisible.

***
[[nskg_leesedwards_module]]

Subroutine `le_neighbours()` is invisible.

***
[[nskg_md_bc_leesedwards_module]]

Function `le_fractional_offset()` is invisible.

Subroutine `adapt_fluid_rbuf(this,rbuf,x,dv,dz_frac)`, `le_find_bottom_neighbours(so,loz,hiz)`, `le_find_top_neighbours(so,loz,hiz)`, are invisible.

***
[[nskg_cg_class]]

Derived types `type(nskg_parms_cg)` and corresponding subroutines are invisible. 

***
[[nskg_io_checkpoint_module]]

Invisible subroutine: `dump_checkpoints()`, `delete_checkpoints()`, `dump_lattice_bin()`, `restore_lattice_bin()`, `dump_lattice_MP()`, `restore_lattice_MP()`, `dump_lattice_MPbin()`, `restore_lattice_MPbin()`, `increase_restore_lattice_MPbin(cdims_old)`, `par_restore_lattice_MPbin(cdims_old)`, `equal_restore_lattice_MPbin()`, `decrease_restore_lattice_MPbin(cdims_old)`, `dump_lattice_MPrxdr()`, `dump_lattice_MPxdr(pt)`, `restore_lattice_MPrxdr()`, `restore_lattice_chunk_from_file_rxdr(filename,fdim,fbounds,lpos)`, `nskg_xdrfnskg_site(file_id,site)`, `check_changed_decomposition(ccd)`, `restore_lattice_MPhdf5()`.

***
[[nskg_features_class]]

Subroutine `set_feature(this, status)`, `check(this)`, `check_default(this)` are invisible.

***
[[nskg_gmsh]]

Function invisible: `return_number_of_vertices(gmsh_type)`

***
[[nskg_halo_class]]

Subroutine invisible: `init_neighrank(this)`

***
[[nskg_helper_module]]

Subroutine invisible: `nskg_initialize_variables()`, `Abend_fun(fil,lin)`, `nskg_catch_namelist_read_error(istat,iunit,info)`, `check_allocate(stat,msg)`, `checkmpi(ierror, msg)`, `row_from_triangular(triangular, index)`, `gaussianBoxMuller(gaussian1,gaussian2)`, `default_string(instring, defaultstring)`, `cross_product(a,b)`, `outer_product(a,b)`, `matrix_vector_product(a,b)`, `tangential_projection(a)`, `is_restoring()`, `local_coordinates(global_pos,local_pos)`, `norm(v)`, `angle(p,b)`, `unit_vector(v)`, `prettyprint_real_field(array, geometry, formatstring, print_halo)`, `nskg_report_memory(mem,filename,line)`, `dsyevc3(a, w)`, `dsyevv3(a, q, w)`.

***
[[nskg_io_hdf5_module]]

Subroutine invisible: `nskg_write_attr_phdf5(filename,dsetname,aname)`, `nskg_add_common_metadata_hdf5()`, `nskg_append_metadata_hdf5(newdata)`, `make_hdf5_metadata_hdr(msg, hdr)`, `rotate_coordinates(rotstr, ox, oy, oz, rx, ry, rz)`, `makedatestr(datestr)`

***
[[nskg_io_helper_module]]

Subroutine invisible: `append_file(inputFile, outputUnit)`

***
[[nskg_allocate_module]]

Function: `aligned_alloc(alignment, size) bind(c)`, `malloc(size) bind(c)`

Subroutine invisible: `free(p) bind(c)`

***
[[nskg_potential_hertz_class]]

Subroutine invisible: `force_and_torque_hertz_ellipsoid(this,obj1,obj2,force, torque)`

***
[[nskg_elec_class]]

Subroutine invisible: `init_elec_eps(this)`, `init_elec_fields(this)`, `SOR_iteration(this,omega,local,residual)`, `poisson_SOR(this)`

***
**nskg_elec_fluxes_module**

Subroutine invisible: `calc_fluxes_diffusion_pagonabarraga(N, flux_site_plus, flux_site_minus)`, `calc_fluxes_diffusion_rempfer(N, flux_site_plus, flux_site_minus)`, ``

Function invisible: `get_flux_idea_p(siteo, siten)`, `get_flux_idea_m(siteo, siten)i`, `get_flux_elec_p(siteo, siten, phi_nn, phi_cur, dir, s)`, `get_flux_elec_m(siteo, siten, phi_nn, phi_cur, dir, s)`, `get_flux_solv_p(siteo, siten, solv_field_nn, solv_field_cur,s)`, `get_flux_solv_m(siteo, siten, solv_field_nn, solv_field_cur, s)`, `get_flux_diel(siteo, siten, local_eps)`

***
**nskg_elec_forces_module**

Subroutine invisible: `elec_force_add_free_energy(N)`

***
**nskg_elec_helper_module**

Subroutine invisible: `elec_total_ions(rho_total_p, rho_total_m, report, init)`, `error_elec(msg)`

Function invisible: `elec_check_neutrality(N, report_) result(neutral)`, `get_op(N, i, j, k)`, `is_mobile_charge(N, i, j, k)`, `get_solvation_potential_m(N, i, j, k)`, `get_solvation_potential_p(N, i, j, k)`, `get_solvation_field(N, i, j, k)`, `get_solvation_force_gradient(N, i, j, k)`, `get_nb_phi(N, veldir, i, j, k, ip, jp, kp)`, `is_restoring_elec()`

***
**nskg_elec_init_module**

Subroutine invisible: `elec_init_lattice(N)`, `elec_restore_init_lattice(whole_N)`, `nskg_elec_allocate_E_prev(N)`, `nskg_elec_init_phi_zero(N)`, `nskg_elec_init_rho_uniform(N)`, `nskg_elec_init_rho_slipbc(N)`, `nskg_elec_init_eps_uniform(N)`, `nskg_elec_init_eps_colloid(N)`, `nskg_elec_init_rock_capacitor(N)`

***
**nskg_elec_input_module**

Subroutine invisible: `elec_restore_lattice_MPhdf5(N)`

***
**nskg_elec_output_module**

Subroutine invisible: `elec_dump_lattice_MPhdf5(N)`

***
**nskg_elec_parallel_module**

Subroutine invisible: `build_elec_site_mpitype(mpitype)`, `build_phi_site_mpitype(mpitype)`

***
**nskg_elec_poisson_solver_module**

Subroutine invisible: `solve_poisson_sor(N)`, `solve_poisson_p3m(N)`, `store_E_prev(N)`

***
**nskg_elec_timer_module**

Subroutine invisible: `elec_register_timer(tname, ti)`

***
[[nskg_interactions_class.F90]]

Subroutine invisible: `setup_interaction(this,unit,pair)`, `setup_potential_from_file(this,unit,pair,pot)`, `compute_pair_forces_mesh_mesh(mesh_set,meshid,triangleI,neighbors,pot)`, `compute_pair_forces_mesh_wall(this,wall_set,triangleI,pot)`

Function invisible: `max_types(unit,pot)`, `setup_potential_from_file(this,unit,pair,pot)`, `refresh_cell_list_mesh_triangles(this, mesh_set)`, `refresh_cell_list_mesh_vertices(this, mesh_set)`, `refresh_cell_list_md(this, md_part_set)`

***
[[nskg_bdist.F90]]

Function invisible: `nskg_bdist.F90I`

***
[[nskg_external_force_class.F90]]

Subroutine invisible: `read_namelist_external_force_constant(this)`, `read_namelist_external_force_pulsation(this)`, `read_namelist_external_force_kolmogorov(this)`, `add_external_force_constant(this)`, `add_external_force_pulsation(this)`, `add_external_force_kolmogorov(this)`

***
[[nskg_lattice.F90]]

Subroutine invisible: `l_count_fluid_volume(fluid_volume)`

***
[[nskg_relax_class.F90]]

Subroutine invisible: `set_relax_homogeneous_mrt(this)`, `set_relax_homogeneous(this)`, `set_relax_liquid_vapor(this)`, `set_relax_power_law(this)`

***
[[nskg_md_boundary_condition.F90]]

Subroutine invisible: `clip_chunks_minz_global`

***
[[nskg_md_class.F90]]

Subroutine invisible: `spread_fluid_force(this, lagrangian_object,delta,xyz)`, `init_momentum_exchange(this,rank)`, `set_momentum_exchange(this,pos,mask)`, `reduce_momentum(this,momentum,counter,request)`, `recv_momentum(this,sendcounter,recvcounter,pin,request)`, `send_momentum(this,id,pos,momentum_out,momentum,counter,request)`, `set_rock_state(this)`, `set_md_particle_properties_from_namelist(unit,particles,maxcutoff,iostat)`, `integrate_q(P)`, `integrate_w(P)`

Function invisible: `in_particle(p,x)`

***
[[nskg_md_helper.F90]]

Subroutine invisible: `scatter_rock_state_halo(rs,N)`

***
[[nskg_abstract_set_class.F90]]

Subroutine invisible: `count_global_n_elements(this)`, `pack_all(this,sendbuf)`, `unpack(this,recvbuf)`

Function invisible: `template_to_be_sent`

***
[[nskg_lagrangian_set_class.F90]]

Subroutine invisible: `interpolate_procedure(this,lagrangian_object,delta,xyz)`

***
[[nskg_mesh_class.F90]]

Subroutine invisible: `compute_node_stress(this,pos,force)`, `compute_bending_forces(this,triangle,normal_i,is_owner)`, `compute_face_centroid(this,triangle,pos1,pos2,pos3,face_centroid)`, `compute_triangle_geometry`, `get_vertex_and_neigh`, `compute_mean_normal_single_node(this, nodeid)`, `compute_inertia_tensor_single_triangle`, `compute_volume_forces(this,v1,v2,v3,is_owner)`, `compute_viscoelastic_forces`

Function invisible: `who_is_going_to_compute_triangle(this,v1,v2,v3)`, `get_vertex(this,node,index) result(vertex)`

***
[[nskg_mesh_set_class.F90]]

Subroutine invisible: `compute_mean_area_nodes(this)`, `count_elements(this,string)`, `read_vtk(this,filename)`, `read_checkpoint(this,filename)`, `printout_all(this,string)`, `dump_observable(this,observable,time_step,first_time)`

***
[[nskg_helper.F90]]

Function invisible: `iif_INTEGER(cond,true,false)`

***
[[nskg_io.F90]]

Subroutine invisible: `nskg_define_inp_file(output)`, `handle_ifort_namelist_bug`, `dump_colour_clusters()`, `dump_vel()`, `dump_density(comp)`, `dump_rock()`, `dump_vector_all(vector,name)`, `dump_vector_bin(vector,name)`, `compute_and_dump_pressure()`

***
**nskg_io_arrstats.F90**

Subroutine: `init_arrstats_data(i,asd)`, `sample_arrstats(i,asd)`, `dump_arrstats_data(i,asd,pprefix)`, `dump_arrstats_desc(i,asd,prefix)`, `restore_arrstats_desc(i,asd)`, `reset_arrstats_data(asd)`

***
[[nskg_io_stress.F90]]

Subroutine invisible: `local_fluid_momentum_transfer_xz(lfmt,grange)`, `droplet_extent(cutoff,grange)`

Function invisible: `droplet_minz(cutoff)`, `droplet_maxz(cutoff)`

***
**nskg_md.F90**

Subroutine invisible: `thermal_fluctuation(P,ddt,ddx)`, `momentum(this,m)`, `temperature(this,t)`, `rotational_kinetic_energy(this,er)`

***
**nskg_md_init.F90**

Subroutine invisible: `place_particle(pos,v,q,w,radius,mag,uid)`, `gather_rock_state(rs)`

Function invisible: `too_close_to_rock(rock_state,pos,ori)`

***
**nskg_md_magnetic.F90**

Function invisible: `mag_o(p)`

***
**nskg_md_output.F90**

Subroutine invisible: `dump_vtk(prefix,t,substep)`

***
**nskg_md_potential_gb.F90**

Function invisible: `pair_force(oi,oj,rij)`, `pair_potential(oi,oj,rij)`

***
**nskg_md_potential_interpolate.F90**

Function invisible: `fphi_potint(rin)`, `phi_potint(rsq)`

***
**nskg_md_potential_spring.F90**

Function invisible: `fphi_spring(rsq)`, `phi_spring(rsq)`

***
**nskg_md_rock_hertz.F90**

Function invisible: `pair_potential(rij)`

***
**nskg_md_rock_lj.F90**

Function invisible: `pair_potential(rij)`

***
**lextobj.F90**

Subroutine invisible: `static_particle(particle, LAGR_massdensity, friction)`, `forward_euler(particle)`, `check_nodes_vel(particle)`, `resize_particle(particle, factor)`, `forces_purge(particle)`, `forces_gravity_node(particle)`, `forces_strain(particle, k_s, k_al)`, `forces_bending(particle, k_b)`, `forces_volume(particle)`, `forces_surface(particle)`, `forces_area(particle)`, `elastic_law_neoHookean(k_s, I1, I2, w, dw_dI1, dw_dI2)`, `elastic_law_Skalak(k_s, k_al, I1, I2, w, dw_dI1, dw_dI2)`, `elastic_law_Evan_Skalak(k_s, k_al, I1, I2, w, dw_dI1, dw_dI2)`, `elastic_law_Skalak_alternative(k_s, k_al, I1, I2, w, dw_dI1, dw_dI2)`, `elastic_law_Ramanujan_Pozrikidis(k_s, I1, I2, w, dw_dI1, dw_dI2)`, `elastic_law_Mooney_Rivlin(k_s, psi, I1, I2, w, dw_dI1, dw_dI2)`, `calculate_new_normal_vectors(vector_i, vector_j, normal_ij, normal_ji)`, `bending_force_KNM(particle,kk_b)`, `bending_force_HMM(particle,kk_b)`

Function invisible: `angle_normals(particle, f_1, f_2)`, `angle_edges(particle, n_1, n_2, n_3)`, `check_convex(position1,position2,normal1,normal2)`

***
**lmesh.F90**

Subroutine invisible: `read_mesh_from_file(filename, mesh)`, `find_node_face_neighbors(mesh)`, `set_face_normals(mesh)`, `set_reference_values(mesh)`, `report_mesh_statistics(mesh)`, `check_normals(mesh, face_check, face_ref)`

Function invisible: `normal_vector(mesh, face)`, `is_node_in_face(mesh, node, face)`, `is_already_in_list(mesh, node_check, node_ref)`, `not_all_checked(mesh, is_checked)`

***
**lsuperstruct_IBM.F90**

Subroutine invisible: `interpolation_weights(dist_low, weight)`

***
**lsuperstruct_bounceback.F90**

Subroutine invisible: `check_range_int(max_separation)`, `bounceback_wrapper(mode,x, y, z, s, dist, face_velocity, lin_mom, permeability)`, `lagr_bb_handle_fresh_nodes`, `mass_change_detect()`, `mass_change_apply()`, `update_momenta_bb(particle)`, `purge_fluid(particle)`, `purge_elastic(particle)`, `wrap_to_closest_physical_site(original, closest, radius)`, `flood_fill_interior_index(x, y, z)`, `fresh_node_treatment(momentum_change, x, y, z, velocity)`, `fresh_node_treatment_border(momentum_change, x, y, z, velocity)`

***
**lsuperstruct_init.F90**

Subroutine invisible: `init_lattice_quantities()`, `init_mesh_relations`


***
**lsuperstruct_interface.F90**

Function invisible: `get_density_red(x, y, z)`, `get_density_blue(x, y, z)`, `get_density_green(x, y, z)`

***
**lsuperstruct_parallel.F90**

Subroutine invisible: `create_datatype_superstruct_node()`, `create_datatype_lextobj_node()`, `create_datatype_lextobj_face()`, `create_datatype_particle_init()`, `create_datatype_lextobj`, `create_datatype_superstruct_particle()`, `create_datatype_static_particle()`, `build_index_chunk_mpitype(ext_x, ext_y, ext_z, datatype_out)`

***
**lsuperstruct_timeloop.F90**

Subroutine invisible: `compute_swimmer_spring_forces()`

***
[[nskg_parallel.F90]]

Subroutine invisible: `FindSubdivision(nx,ny,nz,nprocs,cdims)`, `debug_report_ccoords`, `debug_report_hostnames`, `report_performance(delta_t)`

***
[[nskg_pgr_class.F90]]

Subroutine invisible: `apply_force_corrections_pgr(this)`

***
[[nskg_rocks_class.F90]]

Subroutine invisible: `init_rock_state(this)`

***
[[nskg_walls_class.F90]]

Subroutine invisible: `set_wall_properties_from_namelist(this,unit,iostat)`




# Invisible comments

## invisible subroutine comments:
[[nskg_ad_class]] 

```
   subroutine compute_diffusion(this, input, output)
     !> @ TODO  MS this has to be completely reengineered with nskg_ftcs, with an eye on n_fields/local_diffusion
     !> @ warning at the  moment it uses only the first diffusion coefficient!!!
       class(nskg_advection_diffusion), target :: this
       class(nskg_scalar_field),intent(inout)  :: input, output
       type(nskg_vector_field),save            :: flux
       integer                                :: shift(3)
       integer                                :: s,i
       if (this%lb_lattice%fields%c(1)%density%has_colloids.or.this%lb_lattice%fields%c(1)%density%has_walls) then
           if (.not.this%local_diffusion_coeff(1)%initialized) then
                 call this%local_diffusion_coeff(1)%init(this%lb_lattice%geometry)
           endif
           if (.not.flux%initialized) call flux%init(this%geometry,vdim=3)
           ! rho(x,t+1) = rho(x,t) + div ( D(x) grad rho(x,t) )
           !> We want to keep as much locality as possible (not to have to
           !> step up the halo, hence:
           ! div (D grad rho) = grad D . grad rho + D lapl rho
           !> D can vary within walls, but not within rocks
           call this%set_local_diffusion_coefficient

           call input%compute_gradient(centeredQ6)
           call input%compute_laplacian(centeredQ6)
           call this%local_diffusion_coeff(1)%compute_gradient(centeredQ6)

           output%array = this%local_diffusion_coeff(1)%array * input%lapl%array
           !> @TODO MS use BLAS implementation
           !> @TODO should distinguish the case when D(x) does not change in time (no need to recompute its gradient)
           do i=1,3
              output%array = output%array  +  input%grad%array(:,i) * this%local_diffusion_coeff(1)%grad%array(:,i)
           end do

       else
           ! rho(x,t+1) = rho(x,t) + laplacian( rho(x,t) )
           call input%compute_laplacian(centeredQ6)
           output%array = input%lapl%array * this%parms%diffusion_coeff(1)
       endif
   end subroutine
```

### Comments of subroutine/function included by `contains`

Commments right below subroutine cannot be shown, nor the comments far below.

Update:After removing the `@` before `TODO` and `warning`, the **top comments** can be shown in one paragraph; same as other examples, if a blank line or separating line, only the first line can be shown. However, other comments inside the code region cannot be shown anyway.

Update: all the contents of `!>` marked comments of a subroutine/function  after `@` cannot be shown, even if they are the top comments below subroutine/function.

Conclusion: `contained` procedures cannot have special comment indicators inside, or at least a `@` sign cannot be read.

**Suggestion: Please write full sentences with punctuations. Otherwise a comment with several sentences will be messed up. Contents after `@` might not be shown in the manual. Therefore, it is better to use other ways to emphasize contents.**


# Visible subroutine comments

### Comments of subroutine/function included by `contains`

[[nskg_ftcs_class]]

```
    subroutine step(this, field, diff_coeff, walls_diff_coeff)
        class(nskg_ftcs_solver), target        :: this
        type(nskg_scalar_field), intent(inout) :: field
        real(kind=FP),intent(in)              :: diff_coeff       !> @ TODO MS partially impl., only homogeneous
        real(kind=FP),optional,intent(in)     :: walls_diff_coeff !> @ TODO MS not implemented yet
        this%forceout%array = 0.0 !the force *must* be initialized
        select case(trim(this%method))
        case ('1st-order')
                call this%step_ftcs_dimensional_split_order_1(field, diff_coeff)
        case default
                stop 'Method not implemented in FTCS solver (nskg_ftcs_class.F90)'
        end select
    end subroutine
```

Update: Unlike direct comments below subroutine/function, comments for variables in the same line is safe to use special comment indicators with `@`.

Update: Multiple special comment indicators can be used for one variable. In the following lines, if no blank line or separate `!>` used in between, following comments will be considered as contents for the previous indicator, which is the same syntax for Markdown. Contents will **not** be automatically separated by different or same indicators.

Update: blank lines or `!>` can be used to separate special comment blocks with the same indicator.

Conclusion: Please use blank line or separate `!>` to separate different parts of the comment, either for paragraphes or blocks. This might cause a little bit more effort, but necessary for the user manual.


### Comments for variables

[[nskg_invade_sphere_evaporation_nozzle]]
```
subroutine nskg_invade_sphere_evaporation_nozzle(m_evp,in,out,m_evp_set_density, m_evp_radius,height_wall)
  !> apply a spherical boundary condition for nozzle blocking problem.
  implicit none
  real(kind=FP), intent(in) :: m_evp !> density evaporation is set to
  logical, intent(in) :: in(3), out(3)
  logical, intent(in) :: m_evp_set_density !> The density that will be enforced
  real(kind=FP), intent(in) :: m_evp_radius !> raidus of the spherical boundary condition
  integer, intent(in) :: height_wall
  real(kind=FP) :: tmp_r, tmp_s !> temp variables, needed for mass conservation
  integer :: x, y, z, tz, pevap, zfluid, zfluidsum  !> global location
  real(kind=FP) :: F(19) !>rest vesctor
  real(kind=FP) :: if1, if2                  !> Real radii
  !real(kind=FP) :: maxrad ! Maximal radius, will be taken as the radius of the  evaporation boundary
  real(kind=FP) ::  rad !> simple calculations for droplet shift, radii helper temps
  integer, dimension(3) :: base, centre, offset !> simple calculations for droplet shift, shift vectors
  real(kind=FP), dimension(3) :: r !> simple calculations for droplet shift, shift vectors
  integer :: ierror
  logical :: ismin(3), ismax(3)
  integer :: iin(3), iout(3)
  real(kind=FP) :: m_evp_use, eva_mass_r,eva_mass_b, eva_mass_g, sumred,sumblue, sumgreen 
  real(kind=FP) :: minz !> minimal z-coordinate above which evaporation boundary will be applied.    
  real(kind=FP) :: zero = 0.0

```
Some of the comments are shown, some not. This relates to certain types of varaibles. Needs further confirmation.

### Comments for modules

[[nskg_linked_list_class]]

```
module nskg_linked_list_class
  !> Author Dominik Geyer, dominik.geyer@fau.de, 19.01.2021

  !> Provide a `doubly linked list < https://en.wikipedia.org/wiki/Doubly_linked_list>`_ for the storage of arbitrary types, i.e. class(*)
  !>
  !> A doubly linked list is a linked data structure that consists of a set of sequentially linked records.
  !> Each link contains three fields two link fields (references to the previous and to the next node in the sequence of nodes)
  !> and one data field.
  !>
```
Update: When using `Author:`, the first line is invisible; using `Author` without a `:`, the first line is visible. It seems that there are many cases where `:` can be a decisive element for whether a comment can be shown.



# Weird Problems

[[nskg_laxwendroff_class]]

```
    subroutine step_laxwendroff_dimensional_split_order_1(this,field,adv)
        !> @ Note: adv is expected to be such that adv = F/rho (for forces) or adv = u (for the velocity)
        class(nskg_laxwendroff_solver), target :: this
        type(nskg_scalar_field), intent(inout) :: field
        type(nskg_vector_field), intent(inout) :: adv
        type(nskg_scalar_field), save          :: temp,temp2,upwind,r
        integer                               :: dir, s,sig
        integer,dimension(3)                  :: shift

        if(.not.temp%initialized) call temp%init(this%geometry)
        if(trim(this%method).ne.'1st-order') then
                if(.not.upwind%initialized)call upwind%init(this%geometry)
                if(.not.temp2%initialized)   call temp2%init(this%geometry)
                if(.not.r%initialized)       call r%init(this%geometry)
        endif
        select type(walls=>this%walls)
        class is(nskg_rocks)
        do dir=1,3
           where(walls%is_wall())
             adv%array(:,dir) = 0.0
           end where
        end do
        class default
           stop "internal error in step_laxwendroff_dimensional_split_order_1"
        end select
        do dir=1,3 ! xyz
          this%delta%array = 0.0
          ! dimensional splitting, 1st order
          do s=2*dir-1,2*dir  !left/right
            shift = (/ cx(s) ,  cy(s) ,  cz(s) /)
            sig = shift(dir)
            call nskg_cshift(field%array,this%neigh%array, this%geometry%width, -shift)
            call nskg_cshift(this%walls%array,this%neighwalls%array, this%geometry%width, -shift)
            call nskg_cshift(adv%array(:,dir),this%neighadv%array, this%geometry%width, -shift)
            !> zero-gradient boundary condition: put specular force across the boundary
            where(this%neighwalls%array .ne.  0)
               this%neighadv%array = -adv%array(:,dir)
            endwhere
```

Update: With `@ Note` ahead, `!> zero-gradient boundary condition: put speclar force across the boundary` will not show up in the manual. However, when `@` is removed, the "Note" line does not show up either, but the later comment line is shown, which is very strange. After experiments on other code files, this phenomenon appeared to be a specail case. For other files, with `@`, further comments neglected; with merely `Note:`, all things showed up. Wondering this related to different code functionalities.

***
[[nskg_features_class]]
```
   subroutine check_and_bcast(this, use_feature,force_active)
     !> note use_features checks for it (could be activated or not depending
     !> on the presence of an input file or a namelist) while
     !> force_active uses it and forces its activation
```

Update: with `@`, the first line is invisible; `note:` or `Note:` both can cause the first line invisible. Merely `note` can make everything show up.



***
[[nskg_invasion_module]]

```
    call nskg_invade_evaporation(m_evp,m_evp_gr,m_evp_gb,m_evp_freq_f,m_evp_freq_a,in_evp,out_evp,m_evp_set_density)
  case ('INV_EVAPORATION_SPHERE')
    !>`inv_fluid = 27`: Spherical evaporation boundary
    !> 
    !>:   `m_evp` is the density of component r outside of a sphere. The
    !>radius of sphere is 2 lattice less than the maximal possible
    !>radius inside the system; The position of this sphere are
    !>shifted with the parameters defined in drop init. See
    !>Ref. [@DennisXieJens2016].
    !> 
    if(Lattice%geometry%ncomponents .ge. 2 ) then  
       call nskg_invade_evaporation_sphere(m_evp,m_evp_set_density)
    end if 
  case ('INV_NOZZLE_EVAPORATION')
    !>`inv_fluid = 29`: Nozzle planar evaporation boundary
    !>
    !> Case 29, aimed for nozzle blocking problem of inkjet printing.
    !>
    !> Based on `inv_fluid = 26`, this boundary condition is specified
    !> to investigate the evaporation of fluid in a nozzle. `m_evp` is
    !> the imposed density of component r at the upper boundaries of
    !> the system defined by the logical variable `out_evp(3)`. To
    !> ensure the total mass of liquid component r inside a nozzle
    !> conserve, the evaporated liquid r is added back in the nozzle.
    !>
  if(Lattice%geometry%ncomponents .gt. 1 ) then  
      call nskg_invade_evaporation_nozzle(m_evp,in_evp,out_evp,m_evp_set_density,height_wall)
  else
      call error("nozzle evaporation not supported with only one fluid.")
  end if
  case ('INV_NOZZLE_EVAPORATION_SPHERE')
    !>`inv_fluid = 30`: Nozzle spherical evaporation boundary
    !> 
    !> Case 30, aimed for nozzle blocking problem of inkjet printing with spherical evaporation boundary condition
    !> 
    !> Based on `inv_fluid = 27`, this boundary condition is specified
    !> to investigate the evaporation of fluid in a nozzle. The
    !> evaporation boundary is a spherical shape, with its radius
    !> defined by `m_evp_radius`. `m_evp` is the density of component r
    !> outside of a sphere.

```
Case 27 has a bald title, while case 29 and 30 have not. This might be a result of placing below and not below a `call` command.

***
**pppm_global.F90**
```
!gjp phi_brick is not in original LAMMPS code
      real(kind=FP), allocatable :: phi_brick(:)
      real(kind=FP), allocatable :: vdx_brick(:),vdy_brick(:),vdz_brick(:)
      real(kind=FP), allocatable :: density_fft(:)
      real(kind=FP), allocatable :: greensfn(:)
      complex*16, allocatable :: workvec1(:),workvec2(:)
```
Shining yellow color blocks

***
**pppm_remap.F90**

**There is module in this file. Is it correct?**

***
**nskg_md_potential_capillary.F90**

The last half of the code are in purple color. Is that right? Please check!

***
**lsuperstruct_helper.F90**

The manual file of function `distance_vector_int_real(pos_1, pos_2)` cannot be accessed. Please check!


# Uncertain comments

[[nskg_globals.F90]]

```
  integer, parameter :: n_lp=8
  !> of surrounding lattice nodes for an arbitrary off-lattice position
  integer, save :: lp_sur(3,n_lp) =reshape([0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1],[3,n_lp])
  !> relative coordinates of these nodes
  integer, save :: opp_lp_sur(3,n_lp) = reshape([1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0],[3,n_lp])
  !> relative coordinates of the opposing nodes
```

***
[[nskg_parms_module]]

```
  logical, save :: MCMP = .false.
    !> Multi Component Multi Phase - pos. intra-comp.-, neg. inter-comp.-SC-force
    !> @WARNING Not used anywhere, but is in namelist
```

***


## Submodule

[[nskg_mesh_forces.F90]]

There is nothing on this webpage. Please check! 

***
[[nskg_mesh_geometry.F90]]

There is nothing on this webpage. Please check! 



