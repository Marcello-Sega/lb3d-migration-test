title: Allocating and Deallocating

## General background

Memory is usually organized in, roughly speaking, the following [segments](https://en.wikipedia.org/wiki/Data_segment#Program_memory):
- [data segment](https://en.wikipedia.org/wiki/Data_segment) where global and static variables are placed. Variables allocated here are accessible until the executable is running.
- [stack](https://en.wikipedia.org/wiki/Call_stack): where the variables declared within functions are stored. The size of the stack is a fixed amount of memory controlled by OS settings, quickly accessible as a LIFO. Variables allocated here are accessible only while the function is executed.
- [heap](https://en.wikipedia.org/wiki/Memory_management#DYNAMIC): where dynamically allocated variables are stored. Behaviour is usually language-dependent. 
In order not to fill the (limited) stack, large arrays should be always allocated dynamically, on the heap. Once you have allocated the memory, accessing it on the heap is as fast as on the stack. So do not worry about using dynamically allocated memory, just don't allocate/deallocate it often. 

## Fortran

- In fortran the dynamic allocation is typically done like:
```fortran
integer, allocatable, dimension(:) array
allocate(array(10))
```
while the deallocation is done with:
```fortran
deallocate(array)
```
- Note that if you allocate memory _within_ a function, it's automatically deallocated when exiting the function, you don't need to call `deallocate`


## NSKG specific considerations

In order to access arrays from the parts of the code written in c (for efficiency, see `nskg/libnskg/src/core/c_*.c`) the data stored in arrays is allocated using `malloc` through `nskg_allocate` (`libnskg/src/core/nskg_allocate.F90`). Unless you are introducing new types of arrays, you won't need to use `nskg_allocate` though. Just make use of some ready-available data type. For example, if you need a scalar array (of doubles) of the size of the lattice, use something along the lines of:

```fortran
type(nskg_scalar_array) :: my_array
call my_array%init(Lattice%geometry)
my_array%array = 0.0
my_array%array3d(1,2,3)=2.0
[...]
call my_array%destruct()
```
Note that in order to allocate/deallocate the memory, you need to call the `%init()` / `%destruct()` subroutines. This will internally use `malloc()` and `free()`. 


Note also that until you call `my_array%init()`, no memory will be used, so you can easily initialize your array only when you need it.



Of course, you can have an `allocate`d array of `type(nskg_scalar_array)`. You just need to handle things separately. Here is a full example:

```fortran
type(nskg_scalar_array), allocatable, dimension(:) :: my_array
integer :: i

allocate(my_array(2))
do i=1,2
   call my_array(i)%init(Lattice%geometry)
end do

my_array(1)%array = 1.0
my_array(2)%array = -1.0

do i=1,2
   call my_array(i)%destruct()
end do

deallocate(my_array)

```
