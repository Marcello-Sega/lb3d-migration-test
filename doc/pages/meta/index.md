title: Meta-documentation (internal)

Collecting all the things here which are helpful for the mastering of the manual (FORD syntax help, to-do lists, tutorials, tests), rather than the user/developer documentation of the software itself.
