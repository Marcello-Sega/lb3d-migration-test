title: Quick Start with FORD and inline Fortran documentation

# General idea

# References
This page does not aim at reinventing the wheel. We'll make as much use of first-hand documentation as possible.

 * [FORD homepage](https://github.com/Fortran-FOSS-Programmers/ford/)
 * [FORD's own documentation](https://forddocs.readthedocs.io/en/latest/) **NEW** as of `6.1.17-git`!!
 * [Python Markdown Extra](https://python-markdown.github.io/extensions/extra/)

# Keep in mind
## Markdown syntax
Within the comments that would become the documentation, the Markdown syntax applies. Many books have been written about the various flavours of Markdown; for now we'll stick to what Python Markdown Extra (see link above) provides.

### Docmarks
These are the symbols right after the comment sign ('!' in Fortran), that lets FORD distinguish between ordinary comments and those worth to be included into the output.

### Indentation matters!
On the one hand, FORD recognises semantics of comments by indentation level of the docmarks

### Math mode: TeX with caveats
Inline formulas use backslash-parenthesis syntax: `\(a^2\)` → \(a^2\)

Display math uses double dollars: `$$a^2$$` → $$a^2$$

Enuerated display math uses `amsmath`-style equations: `\begin{equation}a^2\end{equation}` → \begin{equation}a^2\end{equation}

## Macros

There are [a few macros (aliases) available](https://forddocs.readthedocs.io/en/latest/user_guide/writing_documentation.html#aliases) by default, like the path to the media directory (for figures etc.). Moreover, we can always define new ones to our likings.

**Important:** Macros are always expanded. Have a [look into the source file](|gitlabfile|/doc/pages/meta/ford_primer.md) to see how the macro definitions actually look like.

### Images

```
![Figure caption/alt text](../|media|/pictures/picture_file)
```

![Figure caption/alt text](../|media|/pictures/d3q19.png)

### Static Pages from source files

@TODO This still feels like an ugly hack (because it is)

### Links to files/directories in Gitlab

Linking to files in the repo might seem redundant for Fortran source files (that are more easily accessible by on-board syntax of FORD), but comes in handy for directories, tests, … or even bugs/MRs.

```
[NSKG Source Directory Hierarchy (Gitlab)](|gitlabfile|/libnskg/src/)
```

[NSKG Source Directory Hierarchy (Gitlab)](|gitlabfile|/libnskg/src/)

@TODO This is less than ideal too.
