title: Possible structure from MEEP

1. Front page

	The front page should have a short overview of the software, including what the software is, the most important parts a user should know, and how to get statrted with the manual. Following parts can be included:

	- A brief introduction of what the software is.

	- Key features

	- Other important features

	- Download (access to the software, including installation instructions)

	- Brief overview of the manual, and which part is most important for new users.

	- Discussion forum

	- Bug reports and feature requests

	- Acknowledgements

	- Support and feedback

	The following sequence of pages should be the same as described in manual introduction.

1. Introduction

	- `Table of contents`

	- Introduction to the Lattice-Boltzmann Method

	- Implementation and programming concepts

	- @todo Running NSKG

1. Download

	- `Table of contents`

	- Github source repository

	- Precompiled packages for Ubuntu

1. FAQ

	- `Table of contents`

	- General

	- Installation

	- Physics

	- Usage: ...

1. Installation/Compilation

	- `Table of contents`

1. Acknowledgements

	- Authors

	- Referencing

	- Financial support

	- History

1. Liscense and copyright


1. Stand alone pages 	

