title: Fortran Coding Guidelines

# Dependencies
1. the core modules *must not* depend on anything else in NSKG
2. the other modules (elec, ...) should depend on core only, and not on each other
3. higher level modules (interface, ...) can depend on all other modules




HOWTO Write more readable Fortran code for NSKG
===============================================

## File layout (skeleton)
```fortran
#include "nskg.h"
module nskg_XYZ_class
!> # The XYZ method
!>
!> One terse sentence about XYZ (think of the recommended formatting of a git commit
!> message). This first paragraph, rather than the headline, will appear in the module
!> overview of the online manual.
!>
!> Further details: XYZ is an important part of LB, for which we provide a few
!> sentences of context at the top of its file. People will see and read this, to
!> understand why their supervisor has told them to use it.
!>
!> This section might contain some usage instructions, historical context (both its
!> usage/coding practice, and its scientific history in the group). When the methods
!> implemented here make use of scientific literature, cite them [@bib:nskg2023] (this
!> is a BibTeX reference, don't forget to add the entry to `doc/nskg_literature.bib`).
!>
!> Two or three small paragraphs are enough at this place. It is in the nature of
!> inline documentation of source files to go in line with the programming structure,
!> rather than the physical concepts. Hence, more extensive descriptions/manuals
!> usually do not fit well here; we have the `doc/pages/parts/` hierarchy for this.
   use nskg_common_headers_macro
   use nskg_...
   use nskg_...
   use nskg_...

   implicit none
   private

   ! member variables and classes are defined here

   public nskg_xyz_public_subroutine_1   ! any part that should be available from outside
contains

   ! subroutine implementations go here

end module
```

## Vectors

Use vectors assigments! 

### Bad       
```fortran   
integer, dimension(3) :: pos
pos(1) = 1
pos(2) = 2
pos(3) = 3
```

### Good
```fortran   
integer, dimension(3) :: pos
pos = [1,2,3]
```

### An example: 
#### OLD:
```fortran
subroutine nskg_init_velz(N)
  implicit none
  type(nskg_site), dimension(0:,0:,0:) :: N
  real(kind=rk), dimension(3) :: vel_r, vel_b, vel_g ! initial velocities
  real(kind=rk) :: x_gl ! global x-position
  integer :: channel_diameter ! diameter of the channel (without rock nodes)
  real*8 :: theta, phi, r
  integer :: x, y, z, pop

  do x = 1,nx
    do y = 1,ny
      do z = 1,nz
        ! Set velocity.
        if (trim(lbinit%init_cond_name) == "INIT_VEL_Z_POIS" ) then ! Poiseuille profile
          channel_diameter = tnx - 2 * boundary_width ! subtract rocks at both sides
          x_gl = x + ccoords(1) * nx - boundary_width - 0.5d0 - 0.5d0 * channel_diameter ! x_gl shall be zero at the channel center
          vel_r(1) = 0.0d0
          vel_r(2) = 0.0d0
          vel_r(3) = vel_poiseuille_max * (1.0d0 - (2.d0 * x_gl / channel_diameter)**2)
          vel_b(1) = 0.0d0
          vel_b(2) = 0.0d0
          vel_b(3) = vel_r(3)
          vel_g(1) = 0.0d0
          vel_g(2) = 0.0d0
          vel_g(3) = vel_r(3)
        else ! => constant velocity
          vel_r(1) = 0.0d0
          vel_r(2) = 0.0d0
          vel_r(3) = pr
          vel_b(1) = 0.0d0
          vel_b(2) = 0.0d0
          vel_b(3) = pb
          vel_g(1) = 0.0d0
          vel_g(2) = 0.0d0
          vel_g(3) = pg
        end if
        if (collisiontype_id .eq. MRT_id) then
          CALL mrt_init_dist((/vel_r(1), vel_r(2), vel_r(3)/), fr, N(x,y,z)%n_r)
        else
          CALL boltz_dist(vel_r(1), vel_r(2), vel_r(3), 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, N(x,y,z)%n_r(:))
          N(x,y,z)%n_r(:) = N(x,y,z)%n_r(:) * fr
        end if
#ifdef FLUID2
        if (collisiontype_id .eq. MRT_id) then
          CALL mrt_init_dist((/vel_b(1), vel_b(2), vel_b(3)/), fb, N(x,y,z)%n_b)
        else
          CALL boltz_dist(vel_b(1), vel_b(2), vel_b(3), 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, N(x,y,z)%n_b(:))
          N(x,y,z)%n_b(:) = N(x,y,z)%n_b(:) * fb
        end if
#endif
#ifdef FLUID3
        if (collisiontype_id .eq. MRT_id) then
          CALL mrt_init_dist((/vel_g(1), vel_g(2), vel_g(3)/), fg, N(x,y,z)%n_s)
        else
          CALL boltz_dist(vel_g(1), vel_g(2), vel_g(3), 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, 0.0_8, N(x,y,z)%n_s(:))
          N(x,y,z)%n_s(:) = N(x,y,z)%n_s(:) * fg
        end if
#endif
      end do
    end do
  end do
end subroutine nskg_init_velz
```

#### NEW:
Notes:

1. being an initialization routine, we don't care much about efficiency. Better to be written in a readable format
2. vectors assigments are collected using `[ ]`. This helps understanding quickly what's going on, and is more compact
3. The maximum number of components is still fixed to 3, but in this way it will be way easier to generalize it.
4. Unnecessary details, like which model (BGK or MRT) to use, are hidden in the Lattice class function `init_dist_from_vel` 
5. The loop over different components, when setting the populations, is also hidden in `init_dist_from_vel`

<!-- There used to be a copy-pasted code snippet from the current [[nskg_init_velz]] subroutine here. This ages badly. Hence a link to the file at the moment (reducing code duplication, introducing comments, keeping things afloat with current development etc.): -->

[The state of above function in the current state (GitLab)](https://git.iek.fz-juelich.de/compflu/nskg/-/blob/5d41fb455081b803e95966559c4218cc8b107a2e/libnskg/src/old/nskg_init_functions.F90#L240)

The state of above function in the current state (this code browser): [[nskg_init_velz]]
