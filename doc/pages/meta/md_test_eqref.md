title: LaTeX-style equations & references
# Ford/Markdown/MathJax issues with LaTeX equation referencing

## LaTeX input
```latex
%% LaTeX formula
\begin{equation}
	\exp{\imath\pi}=-1
	\label{eq_euler}
\end{equation}

%% LaTeX formula reference
See \eqref{eq_euler}
```

## Markdown output (FORD dialect)
literally just a verbatim copy, according to [upstream docs](https://forddocs.readthedocs.io/en/latest/user_guide/writing_documentation.html#latex-support)

```markdown
<!-- FORD-MD formula in amsmath style --> 
\begin{equation}
	\exp{\imath\pi}=-1
	\label{eq_euler}
\end{equation}

<!-- FORD-MD formula reference --> 
See \eqref{eq_euler}
```

<!-- FORD-MD formula in amsmath style --> 
\begin{equation}
	\exp{\imath\pi}=-1
	\label{eq_euler}
\end{equation}

<!-- FORD-MD formula reference --> 
See \eqref{eq_euler}

## The issue(s)
None 🙂 (as of 2022-11). Just use amsmath-style formulas like in LaTeX.

@TODO Untested: cross-referencing between stand-alone pages, source files, and blends thereof.
