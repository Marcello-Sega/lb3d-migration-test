title: Output files


## The output files {#OUTPUT-FILES}
   ----------------

 Writing the entire state of the system every time step wastes a large
 amount of disk space, and can often slow down the simulation when many
 CPUs try to write to disk. Therefore, it is possible to choose which
 parameters are written, and how often.
 
 First of all, no outputs are written until at least time step
 ` n_sci_start`. This means that if one is interested in the long-term
 behaviour of a system, the program does not have to waste time and space
 writing short-term results which will be discarded.
 
 The science outputs are produced every `n_sci_<type>` time steps.
 Certain variables in the input file determine what sort of science data
 is written.
 
 To enable writing of a given output, the corresponding variable is set
 to `.true.`; to disable it, the variable is set to `.false.`.
 
 These output control variables are listed below.
 
 `sci_int`: Colour field
 
 :   The colour field at each site is calculated by subtracting the total
     density of blue particles from the total density of red particles.
     This writes one scalar value per lattice site.
 
 `sci_od, sci_wd`: Oil and water densities
 
 :   The total oil density and then the total water density at each site,
     i.e. two scalar values per lattice site.
 
 `sci_sur`: Surfactant density
 
 :   The total surfactant density at each site.
 
 `sci_dir`: Surfactant directions
 
 :   The net dipole moment is written as a 3-vector for each site.
 
 `sci_vel`: Z-velocity
 
 :   The total Z-component of the expression:
     $${\bf u} = \frac {\sum_\sigma \rho^\sigma {\bf u}^\sigma}
                                  {\sum_\sigma \rho^\sigma}
         \label{Z-VELOCITY}$$ is calculated and written for each site.
     Note this is a change from earlier versions of the code.
 
 `sci_flo`: Individual Z-velocities
 
 :   The Z-component of \({\bf u}^\sigma\) for each phase (in the order
     oil, water, surfactant) is written for each site: hence this
     produces three scalars for each lattice site. *Warning* earlier
     versions of the code outputted a different quantity. Note that these
     values may be misleading unless they are considered along-side the
     density of each component at the site under consideration.
 
 `sci_arrows`: Total linear momentum
 
 :   The total linear momentum of the fluid, understood as the sum of the
     linear momentum for each species is written for each site.
 
 `sci_velocities`: Total linear momentum
 
 :   As, `sci_arrows`, but the velocity components are written to
     separate files.
 
 `sci_arrstats`: Time-statistics of the velocity vector field
 
 :   This option generates output from which the time-average and the
     time-covariance matrix of the velocity vector field and the
     time-averaged fluid-site concentration – all as a function of the 3D
     lattice position – can be obtained easily and efficiently in
     post-processing.
 
 `sci_rock`: rock\_state at each lattice node
 
 :   The value of `N(x,y,z)%rock_state` is written for each lattice site
     `N(x,y,z)`.
 
 `sci_pressure`: Pressure measurement flag
 
 :   Indicates whether to compute the pressure tensor. The scalar
     pressure will also be dumped in addition to the pressure tensor
     elements. The pressure tensor is computed as:
 
     $$\begin{aligned}
             P_{ij}(\mathbf{x},t)
             &\equiv&
             \sum_{\alpha} m^\alpha
             \sum_k g_k
             \Big(c_{ki}-u_{i}(\mathbf{x},t)\Big)
             \Big(c_{kj}-u_{j}(\mathbf{x},t)\Big)
             n_k^\alpha(\mathbf{x},t)        
                                 \nonumber\\
             &+&
             \Omega \sum_{\alpha,\bar{\alpha}}
             G_{\alpha\bar{\alpha}}
             \sum_{\mathbf{x}^\prime}
             \Big[\psi^\alpha(\mathbf{x})
             \psi^{\bar{\alpha}}(\mathbf{x}^\prime)
             +
             \psi^{\bar{\alpha}}(\mathbf{x},t)
             \psi^{\alpha}(\mathbf{x}^\prime,t)\Big]
             (\mathbf{x}-\mathbf{x}^\prime)
             (\mathbf{x}-\mathbf{x}^\prime)
         \end{aligned}$$
 
     where \(\mathbf{x}\) is a lattice site, \(t\) is the time step,
     \(m^\alpha\) is the molecular mass of species \(\alpha\), \(c_{ki}\) is a
     cartesian component of the \(k\)-th molecular velocity \(\mathbf{c}_k\)
     (on the projected-FCHC lattice the method uses there are effectively
     19 velocities, \(k=0,\ldots,18\), with relevant degeneracies \(g_k\)
     such that \(\sum_k g_k = 24\), and three speeds, \(c=0,1,\sqrt{2}\)),
     \(\mathbf{x}^\prime = \mathbf{x} + \mathbf{c}_k\) is a nearest
     neighbour's lattice site, \(n^\alpha\) is the number density for
     species \(\alpha\), and \(G_{\alpha\bar{\alpha}}\) is the coupling
     matrix between species \(\alpha\) and \(\bar{\alpha}\). The factor
     \(\Omega = 1/4\) guarantees the correct form for the scalar pressure
     \(p\equiv{}P_{ii}/3\) (where Einstein convention holds), obtained from
     integrating the LB momentum balance equation over the velocity space
     [@bib:segredo-nekovee-coveney:2003]. Finally, \(u_{i}(\mathbf{x},t)\)
     is the cartesian component of the fluid's total velocity
 
     $$\mathbf{u}(\mathbf{x},t)
         \equiv
         \sum_\alpha
         \frac{\sum_k \mathbf{c}_k n_k^\alpha(\mathbf{x},t)}
         {\sum_k n_k^\alpha(\mathbf{x},t)}$$
 
     and
 
     $$\begin{aligned}
             \psi^\alpha
                 &=& n^\alpha   \qquad\mathrm{if~} \mathtt{psifunc}=0
                            \mathrm{~and~} \psi^\alpha < 1.0
                                     \nonumber\\
                         &=& 1.0    \qquad\mathrm{if~} \mathtt{psifunc}=0
                        \mathrm{~and~} \psi^\alpha > 1.0
                                     \nonumber\\
                         &=& 1 - \exp{-n^\alpha}
                        \qquad\mathrm{if~} \mathtt{psifunc}=2.
                                     \nonumber
         \end{aligned}$$
 
 
