title: User Manual


@TODO
Figure out what is a good starting point to fill this page with.

## More interesting options

- Should we put the input file parameters here right away?
- Project overview (description/**depiction** of namelist hierarchy, [commit messages](|gitlabissue|340), object orientation primer, ...?)

## Less interesting options

- ToC? Code duplication with the automated ToC at the left, danger of outdating
- Another “Welcome to our project” page with forced input, orga meta-data, etc.
@ENDTODO
