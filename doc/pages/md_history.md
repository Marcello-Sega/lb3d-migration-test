title: History of NSKG

MD implementation history
-------------------------

The plug-in started as the freely available benchmark code `ljs` that
was developed by S. Plimpton to study the parallel scalability of
spatial decomposition for the Molecular Dynamics of Lennard-Jones
systems [@bib:plimpton:1995]. The most important changes of the core MD
code present in NSKG are listed in the following.

### 2008

-   migration to Fortran 90, modular structure and user-defined MPI
    types for communication

-   addition of rotational degrees of freedom to the integrator,
    following the approach using quaternions which is described in
    [@bib:allen-tildesley:1991].

-   addition of an optional `collect`-step in the communication scheme
    that is used to reduce forces calculated for halo particles back to
    the respective owning process.

### 2009/2010?

-   dynamic allocation of most of the memory buffers

### 2011

-   MD particles may be freely added or removed during a simulation run.

-   rewriting of the MD communication. The communication layout itself
    was not changed but the code became more clear, better documented
    and more flexible which allows for the implementation of more
    complex and not purely periodic boundary conditions.

### 2012

-   decoupling of MD and LB time step. The MD step size can be an
    integer fraction of the LB step.

-   full Lees-Edwards boundary conditions available also for MD
    particles

### 2013

-   MD particles with magnetic dipole

-   External magnetic field(constant, linear, alternating).

### 2022

-   Rewrite/reimplementation into modern Fortran 2008 classes,
    increasing code share with other parts of NSKG (like IBM).
