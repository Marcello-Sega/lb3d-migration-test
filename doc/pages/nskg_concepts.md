title: Implementation and Programming Concepts

Overview of the program
=======================

The core of the code is a Lattice-Boltzmann solver, written for use on
multi-CPU architectures.

@TODO The parallel codes are written in standard
FORTRAN90, and make use of a number of features of that language that
are object-oriented in spirit.

The codes utilize the standard message
passing MPI for synchronization and communication between processors.
The code can be used in Single Data Multiple Processors (SDMP) mode,
where the load of one large task is split across processors. This mode
is used to perform large-scale calculations whose time and memory
requirements are prohibitive on a single processor. Parallelization of
the code in this mode was performed by means of a domain decomposition
strategy.

The underlying 3D lattice is partitioned into sub-domains
(boxes) and each box is assigned to one processor. Each processor is
responsible for the particles within its sub-domain and performs exactly
the same operations on these particles.

Two rounds of **communication between neighbouring sub-domains**
are required: at the propagation step,
where particles on a border node can move to a lattice point in the
sub-domain of a neighbouring processor, and in evaluating the forces. By
using a ghost layer of lattice points around each sub-domain, the
propagation and collision steps can be isolated from the communication
step. Before the propagation step is carried out the values at the
border grid points are sent to the ghost layers of the neighbouring
processor and after the propagation step an additional round of
communication is performed to update the ghost layers.

This additional
round of communication is required because of the presence of non-local
interactions in the model whose computation requires (the updated)
single-particle distribution functions at neighbouring sites.
Fig. [1](#fig:flowchart){reference-type="ref" reference="fig:flowchart"}
shows schematically one step of the parallel algorithm.

![Flow chart of the parallel LB algorithm.](../|media|/pictures/parallel.png){#fig:flowchart}

The parameters for the simulation, such as its duration, initial
conditions, and coupling parameters, are all controlled by text files,
which can be modified using any text editor.

When the code is run, it reads the input file,

@TODO Internal wiki link to the input file description

and performs the
appropriate simulation. If large numbers of input files are used, it may
be advisable to generate them using a simple shell or perl script.

The code prints a small amount of information to standard output,
usually just messages detailing the input parameters at the start, and a
brief note when a new time step is commenced. If desired, sanity checks
can be performed after user determined set of timesteps.

The program can be instructed to periodically write information
concerning the state of the simulation, such as the particle densities
and velocities, to disk. For reasons of efficiency, every time such
output is produced, each CPU can write a file containing information
about its own chunk of the simulation, and nothing else. These files are
later tied together to form a coherent whole, by an external
postprocessor. If a small loss of efficiency is acceptable, the code can
be instructed to do the postprocessing each time data is written to
disk.


This version of the code fully supports platform-independent **output formats**:

- XDR [@bib:rfc1832] (using Frans van Hoesel's XDRF library [@bib:hoesel])
- ASCII.
- Today's defacto standard is parallel HDF5 output which allows the best performance and portability if the HDF5 library is available.

@TODO Instructions how to obtain/build HDF5

If the internal postprocessing routines are not used, the external
postprocessor may be run to tie all the CPUs' outputs together to
produce output that may be visualized or used for calculation. The
output from the external postprocessor is platform-independent as it
utilizes the XDR format.




The rock field
--------------

The rock field encodes what the local lattice site contains. A rock
value of \(0\) stands for a fluid site. When the MD module is active,
rock values \(>0\) stand for colloid sites and should be equal to the
uid (unique ID) of the colloid, and values \(<0\) stand for wall
sites. When MD is inactive, rock values \(>0\) stand for wall sites.
