title: Compiling NSKG with various compilers/toolchains

We rely on `cmake` to figure out where to locate Fortran and C compilers and libraries.
If your compilers are well-behaving, and your MPI and parallel HDF5 are properly configured,
this works flawlessly. We recommend to use:

* only packages provided by your friendly Linux distribution, or
* only packages provided by your friendly compute facility operators, or
* make use of [Spack](https://spack.io/) to set up a local toolchain (set up compiler and MPI,
  e. g. `openmpi`, then install `hdf5+fortran ^openmpi`).


Compiling and running NSKG is tested (with various success) with the following compilers:

`gfortran` (from GCC)

: Well-supported, main development platform for many years (covered by compilation and test pipeline)

`ifort` (Intel Fortran Compiler Classic, part of oneAPI)

: compilation works (covered by compilation pipeline), however it seems necessary to pair it with another C compiler than `icc` (which is in the process of deprecation by upstream); `icx` is a decent choice.

`ifx` (“new” Intel Fortran Compiler, part of oneAPI)

: has reached F08 compatibility in early 2023, but is still not production-ready as of 2023.1.0 (upstream bugs are blocking compilation)

Nvidia HPC SDK (formerly PGI), Flang, AMD AOCC

: unknown status
