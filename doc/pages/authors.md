title: The authors of NSKG

# The authors of NSKG

NSKG is a team effort. Over more than a decade of existence, evolution, extension, and cleaning-up in a scientific environment (with all its inevitable staff fluctuation), a lot of people have contributed to what NSKG is today.

An attempt to compile a comprehensive list of authors.

## The NSKG author list, as of the 2012ish release v7
<wwwmtp.phys.tue.nl/nskg/nskg-manual.pdf>

E. Breitmoser, J. Chin, C. Dan, F. Dörfler, S. Frijters, G. Giupponi, N. González-Segredo, F. Günther,
J. Harting, M. Harvey, M. Hecht, S. Jha, F. Janoschek, F. Jansen, C. Kunert, M. Lujan, I. Murray,
A. Narváez, M. Nekovee, A. Porter, F. Raischel, R. Saksena, S. Schmieschek, D. Sinz, M. Venturoli,
T. Zauner


## Evolving over the HI-ERN years (2016–present)
<!-- former doc/manual/authors.tex -->
O. Aouane,
E. Breitmoser, J. Chin, C. Dan, F. Dörfler, S. Frijters, G. Giupponi, N. González-Segredo, F. Günther,
J. Harting, M. Harvey, M. Hecht, S. Jha, F. Janoschek, F. Jansen,
T. Krüger,
C. Kunert, M. Lujan, I. Murray,
A. Narváez, M. Nekovee, A. Porter, F. Raischel, R. Saksena, S. Schmieschek, D. Sinz, M. Venturoli, 
Q. Xie,
T. Zauner.

### Definitely missing
MS MZ FG FP GA NK JH BB
