# NSKG

[![pipeline status](https://git.iek.fz-juelich.de/compflu/nskg/badges/rewrite/pipeline.svg)](https://git.iek.fz-juelich.de/compflu/nskg/commits/rewrite)
[![coverage report](https://git.iek.fz-juelich.de/compflu/nskg/badges/rewrite/coverage.svg)](https://git.iek.fz-juelich.de/compflu/nskg/commits/rewrite)

NSKG is a massively parallel high performance lattice-Boltzmann code.

It is written in Fortran, uses MPI for parallelization, and [HDF5](https://support.hdfgroup.org/HDF5/) as
its data exchange file format.

## Prerequisites

Mandatory:

  * Fortran and C compilers
  * MPI with the `mpi_f08` module
  * HDF5 >= 1.8 (parallel build)
  * CMake >= 3.16

Optional:

  * Python including the [`f90nml` module](https://github.com/marshallward/f90nml) (for the tests)
  * [`FORD`](https://github.com/Fortran-FOSS-Programmers/ford/) (for building the manual)
  * [`lcov`](https://github.com/linux-test-project/lcov) (for coverage reports)
  * Valgrind (for memory leak tests)


## Obtain the sources
Clone this git repo:

```bash
git clone git@git.iek.fz-juelich.de:compflu/nskg.git
cd nskg
```

New clones are automatically on the `rewrite` branch (when in doubt, check with
`git branch` and change with `git checkout rewrite`)

## Prepare the build environment
Create a build folder and let CMake do its magic to set up the build environment

```bash
mkdir build && cd build
cmake ..
```

### Configure building
NSKG has some build-time tweakables (though by far fewer than it used to
be in former times[^1]).
We are aiming at sane defaults, but in case you want to tune them, you can
configure the build via commandline options to `cmake`:

```bash
cmake ..
```

Compile flags:

| Flag                   | Explanation                                                                |
|------------------------|----------------------------------------------------------------------------|
| `NSKG_SINGLE_PRECISION` | use single precision floats, less accuracy, more speed, tests may fail     |
| `NSKG_USE_BLAS`         | use BLAS, makes BLAS required                                              |
| `NSKG_TESTS`            | generate tests (on by default, you'll need this to run tests locally       |
| `NSKG_PLATFORM`         | select platform, e. g. `SUN`, `ONEAPI_IFORT` (see `cmake/Platforms.cmake`) |

See (and configure) the whole set of tunables with the TUI comforts that `ccmake` brings:
```bash
ccmake .
```


[^1]: An earnest object-oriented interface and semantically structured namelists have
replaced the traditional (compile-time) configuration of NSKG, in an endeavour that is called
`rewrite` for historical reasons.

## Compile & Run
Build the executable

```bash
make -j
```

You'll find the executable at `build/nskg/src/nskg`.

Optionally, you can `make install` to make it available at `${CMAKE_INSTALL_PREFIX}/bin/nskg` at the `${CMAKE_INSTALL_PREFIX}` of your choice.


### Input files, Simulation set-up
The physical setting that NSKG is supposed to simulate, is defined by Fortran
namelists, traditionally collected in a file called `input`. It defines the
simulation parameters, geometry, etc.

Local invocation of `nskg`:

```bash
mpirun -np 8 build/nskg/src/nskg -f /path/to/input
```

The documentation (see section “Manual” below) offers a much more comprehensive
introduction into application usage.


### Routine
The build directory (e. g. `~/nskg/build/`) is ephemeral and cluttered. For serious work, create a
directory `$WORK` on an appropriately dimensioned drive, and run your
simulations there:
```bash
mkdir $WORK/nskg_work && cd $WORK/nskg_work
# Create/edit your "./input" file here
mpirun -np 8 ~/nskg/build/nskg/src/nskg -f input
```

NSKG outputs fields (densities, velocities etc.) into files that use the HDF5 container format.
To have an easy way to look into such files, the repository provides the **`h5toxmf`** script
at `tools/h5toxmf/h5toxmf.py`. It writes common properties of NSKG lattice geometry into a separate
file:
```
~/nskg/build/nskg/src/nskg -f input -T flowfield

[... was told to provide files of the type "od_flowfield_t00000100.h5" by the input file]

~/nskg/tools/h5toxmf/h5toxmf.py od_flowfield_t00000100.h5
paraview od_flowfield_t00000100.xmf
```
This opens ParaView (a versatile interactive 3D visualisation application), instructed to
understand the way in which NSKG has structured the data in the HDF5 output files.


### Fortran Stack Trace/Debug output
(New with !468)

Set `CMAKE_BUILD_TYPE` to `Trace`. Use the new executable `nskg/src/nskgtrace`
instead of `nskg` to get a better clue of things in case something breaks.


## Manual
As of mid-2022, the documentation overhaul is slowly but surely taking shape.
We are using [FORD](https://github.com/Fortran-FOSS-Programmers/ford/) to
project the source code structure onto the manual, without duplicate work
on organising the manual alongside the code.

Run
```bash
make manual
```
in your CMake working path. It will create a local website, and tell you where
to point your web browser to access it. Your chance to review changes before
pushing!


## Tests
After building, you have the chance to convince yourself that the test suite of
NSKG is passing. Similarly, developers are encouraged to create simple test
cases for new code.

NSKG makes extensive use of tests (unit tests, physics tests). These are
invoked via CTest, and accordingly profit from the low entry barrier and
maintainability of the CMake universe.

```bash
ctest -V -R basic-io-io
```

All tests are stored in the [`tests/`](tests) subdirectory.

Note that some tests demand core counts (`mpirun -n 27`) which might exceed the
capabilities of machines that fit under your office desk.
In this case, expect some tests to fail due to CPU core shortage, or switch to
HPC-class hardware instead.


## File system structure
In-tree:
```
nskg/
  Source code of the main executable/frontend
libnskg/
  Implementation of all features
tests/
  Where CTests live!
doc/
  FORD Handbook: static pages and setup
tools/
  Helpers to make users' life easier (data conversion etc.)
devtools/
  Helpers to make code developers' life easier
patched_external_libs/
  External libraries that are tracked in-tree for convenience of users
performance/
  Performance numbers (MLUP/s database, scaling tests)
```

Ephemeral:
```
build/
  (user-created) The Folklore urges you to use this path for CMake building
public/
  (created by FORD) The HTML-rendered handbook
```
