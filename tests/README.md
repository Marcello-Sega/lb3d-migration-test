# Tests

| Location | Purpose |
| -------- | ------- |
| `/tests/{model-or-method-abbrev}/{category}/{test}/` | Physics tests (confirm physically sensible behaviour) |
| `/tests/unit/{test}/` | Unit tests (testbed for programming structures) |
| `/tests/basic/{category}/{test}` | Very simple system tests |
| `/tests/malformed/{test}` | Testing safeguards against malformed input |
| `/tests/tools/{tool}` | Auxiliary tools (apart from NSKG itself) |
| `/tests/memcheck/{category}` | Memory leak detection |


## Copy an existing test

Most often (and most simply), a test for a new functionalty is just a minor variation of
an already existing test. Make use of this by just copying the respective test directory.

We expect that you have a rough idea of what to test (the purpose of this document is to
explain how to get a test into the NSKG test suite/CI pipeline, not to figure out if a
test makes sense from a physical/scientific point of view).

## Modify the test you just copied

*Physics tests* need the input files and analysis scripts changed. In the analysis
script, explain briefly but completely what your test does and how you determine
if it was successful.

Some remarks:
 - try to keep the runtime of your test(s) below 2 minutes
 - keep the output low, dump only the data you need to verify the simulation run

For *Unit tests*, modify the Fortran source of the test. Use comments in the source
file to explain what your test does and how you determine if it was successful.
If it wasn't, abort your program with `MPI_Abort`.

## Adapt the CMake build configuration

The CMake build configuration is hierarchically structured. Every (sub)directory has
to have a `CMakeLists.txt` file. For intermediate directories, an
`add_subdirectory(directory)` entry is usually enough.

### Physics tests
Use the `nskg_add_test` command in the `CMakeLists.txt`:
```cmake
nskg_add_test(nproc <input-file> [<analyse-script> [WILL_FAIL] [MEMCHECK]] [-- arg1 arg2 ...])
```
Adds a test that will execute NSKG on `nproc` processors, set up by `<input-file>`.
If none of the optional arguments is present, the test success will be the exit status
of NSKG itself. `WILL_FAIL` inverts this, i. e. the test will pass when NSKG execution
fails.

If `<analyse-script>` is provided, another test is added that runs this (Python) script
on the directory where NSKG was run.

Following a `--`, arguments `arg1 arg2 ...` will be passed directly to NSKG.

Multiple calls to `nskg_add_test` are possible within a `CMakeLists.txt` file. The name of the
test/directory will be appended by a running number. Use `ctest -N -V -R {test-directory-name}`
to find out which tests are issued by a given directory/test specifier.

You can add `TIMEOUT` to terminate the execution after 60 seconds.

`MEMCHECK` is used internally to enable the Valgrind wrapper in `memcheck` tests.

### Unit tests
Use `nskg_add_unit_test` to add a test to be run by `nproc` processors (1 by default):
```cmake
nskg_add_unit_test([nproc])
```

## Adapt the Automatic Testing configuration (CI pipeline)

The configuration for automatic testing is hierarchically structured, inside the
`ci.yml`-files.

In your *category*-folder (one above your test-folder) there must be an entry of
the form
```yml
{model-or-method-abbrev}-{category}-{test}:
  extends: .base-physics-test
```
while `{test}` must correspond to your test-folder's name! Otherwise the
automatic testing won't find your test.

The hierarchy of `ci.yml` files is filled with registering all the `{category}`s themselves:
```yml
include:
  - 'tests/{category}/ci.yml'
```
inside the `ci.yml` file of their direct parent directory.

## Start a test manually

```sh
  cd build/         # go into your build folder
  cmake ..          # configure your build environment to generate tests
  make -j           # If you hadn't compiled before, do it now! Unit tests must be compiled (don't add a target)
  ctest -N -V -R {part-of-the-name-of-your-new-test}   # Search for test(s) by name
  ctest -V -R {the-name-of-your-new-test}              # Run your test
```

## Look at your coverage (local)

To judge the coverage of *just your test*, you can

```sh
  cd build/         # go into your build folder
  cmake .. -DCMAKE_BUILD_TYPE=Coverage
  make -j
  ctest -V -R {the-name-of-your-new-test}  # run your test
  ctest -V -R generate_coverage            # generate coverage information
  firefox tests/html/index.html            # open the coverage information with firefox (or any other browser)
```

(This needs `lcov` to be present on your local machine.)

## Push to your branch/MR
CI pipelines run automatically on merge requests. So if you want your test to run automatically,
wrap your branch by a MR (by following the link that is presented during push time).

Inside a MR, you can inspect how the pipelines are treating your test. In particular, access the
*coverage* by downloading the "coverage:archive" artifact, unpacking it and having a look at
`index.html` there.

## Meta-Test
There are several protective layers that prevent you from committing faulty test setups. GitLab
will complain about invalid YAML, CMake will point out `CMakeLists.txt` inconsistencies.

The project requirements are stricter, though. We do not tolerate tests that are not executed
(essentially, unmaintained code). We have a meta-test stage in the pipeline, that ensures
additional quality standards within the test suite (in particular, full integration of all
`ctest` tests in the CI pipeline).

Keep in mind, though, that there is no safeguard against stupid design errors, that make a test
run fine, but cover redundant, unspecific, or just wrong things. It is upon the responsibility of
each test author to ensure tight tolerances, prefer analytical approches over heuristics, and to
document their thoughts in the `analyse.py` script.
