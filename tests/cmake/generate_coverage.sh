#!/bin/bash
# fail on any error in this script
set -e

# run lcov to process collected data
lcov --capture --directory ../libnskg --directory ../nskg --output-file coverage.info
# (Removal of unneeded coverage report items (like /usr and /apps) happens in the ../../.gitlab-ci.yml "coverage" target.)

genhtml coverage.info --output-directory html --prefix $PWD
