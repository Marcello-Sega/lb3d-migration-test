function(nskg_add_test nprocs inputfile)

    # generate test name based on directory
    string(REPLACE "${CMAKE_SOURCE_DIR}/tests/" "" name ${CMAKE_CURRENT_SOURCE_DIR})
    string(REPLACE "/" "-" name ${name})

    # enumerate test names if there are more than one test in a CMakeLists.txt
    if(${_testsinscope})
        if("${_previoustest}" STREQUAL "")
            set(depends ${name})
        else()
            # we need to enforce dependence when testing,e.g. checkpoint/restore
            set(depends ${name}-step${_previoustest})
        endif()
        set(name ${name}-step${_testsinscope})
    endif()
    set(_previoustest "${_testsinscope}" PARENT_SCOPE)
    math(EXPR _testsinscope "${_testsinscope} + 1")
    set(_testsinscope ${_testsinscope} PARENT_SCOPE)

    # copy test folder to build dir (TODO: do this at build time?)
    file(COPY ./ DESTINATION ./ PATTERN CMakeLists.txt EXCLUDE
                                PATTERN ci.yml EXCLUDE)

    # get executable, needs to be relative in gitlab CI
    if(DEFINED ENV{CI})
        file(RELATIVE_PATH _nskg_executable
                "${CMAKE_CURRENT_BINARY_DIR}"
                "${CMAKE_BINARY_DIR}/nskg/src/nskg")
    else()
        set(_nskg_executable "$<TARGET_FILE:nskg>")
    endif()
    # get executable, needs to be relative in gitlab CI
    file(RELATIVE_PATH _linter
                "${CMAKE_CURRENT_BINARY_DIR}"
                "${CMAKE_BINARY_DIR}/tools/nmlint/nmlint.py")

    # handle extra arguments (analyse script / will_fail)
    set(i_willfail -1)
    set(b_willfail false)
    set(i_timeout -1)
    set(b_timeout false)
    set(i_memcheck -1)
    set(b_memcheck false)
    if(${ARGC} GREATER 2)
        # Search for "WILL_FAIL", write the desired behaviour into `b_willfail`, and remove argument from list
        list(FIND ARGN "WILL_FAIL" i_willfail)
        if(NOT ${i_willfail} EQUAL -1)
            list(REMOVE_AT ARGN ${i_willfail})
            set(b_willfail true)
        else()
            set(b_willfail false)
        endif()

        # Search for "TIMEOUT", write the desired behaviour into `b_timeout`, and remove argument from list
        list(FIND ARGN "TIMEOUT" i_timeout)
        if(NOT ${i_timeout} EQUAL -1)
            list(REMOVE_AT ARGN ${i_timeout})
            set(b_timeout true)
        else()
            set(b_timeout false)
        endif()

        # Search for "MEMCHECK", write the desired behaviour into `b_memcheck`, and remove argument from list
        list(FIND ARGN "MEMCHECK" i_memcheck)
        if(NOT ${i_memcheck} EQUAL -1)
            list(REMOVE_AT ARGN ${i_memcheck})
            set (b_memcheck true)
            if(CMAKE_Fortran_COMPILER_ID MATCHES Intel)
                # hard disable memcheck on Intel compilers TODO Do we still need that paranoia in 2023?
                set (b_memcheck false)
                return()
            endif()
            if(NOT DEFINED MEMCHECK_WRAPPER)
                message(FATAL_ERROR "MEMCHECK tests should go in ./tests/memcheck/" )
            endif()
            set(_nskg_executable ${MEMCHECK_WRAPPER} ${_nskg_executable})
        else()
            set (b_memcheck false)
        endif()

        # Search for extra arguments to be passed to the NSKG call (move all remaining arguments from `ARGN` into `more_args`
        set(more_args)
        list(FIND ARGN "--" i_more_args)
        if(NOT ${i_more_args} EQUAL -1)
           list(SUBLIST ARGN ${i_more_args} -1 more_args) #list(SUBLIST <list> <begin> <length> <output variable>)
           list(REMOVE_ITEM ARGN ${more_args})   # Remove all elements collected into `more_args` from `ARGN`
           list(REMOVE_AT more_args 0)           # Remove `--` at the beginning of `more_args`
        endif()

        # if it wasn't "WILL_FAIL", it was the analysis script
        if(ARGN)
            list(GET ARGN 0 analyse)
        endif()
    endif()

    # add the test
    message(STATUS "Adding nskg test ${name}")
    string(REPLACE ";" " " more_args_str "${more_args}") #mind quotes around ${more_args}
    if(${b_memcheck} STREQUAL true ) # memcheck wrapper
       # At the moment our memcheck wrapper allows only serial jobs
       # because it packages two different runs in the same script.
       # Invoking that script with mpirun makes two calls to MPI_Init.
       # So, at the moment no mpi here...
       add_test(NAME ${name} COMMAND ${_nskg_executable} -f ${inputfile} ${more_args})
    else()  # standard mpi run
       if(${b_willfail})
         add_test(NAME ${name}
           COMMAND ${MPIEXEC_EXECUTABLE} ${MPIEXEC_NUMPROC_FLAG} ${nprocs}
           ${MPIEXEC_PREFLAGS} ${_nskg_executable}
           -f ${inputfile} ${more_args} ${MPIEXEC_POSTFLAGS})
       else()
         add_test(NAME ${name}
           COMMAND bash -c "${MPIEXEC_EXECUTABLE} ${MPIEXEC_NUMPROC_FLAG} ${nprocs} \
           ${MPIEXEC_PREFLAGS} ${_nskg_executable} \
           -f ${inputfile} ${more_args_str} ${MPIEXEC_POSTFLAGS} && \
           ${Python_EXECUTABLE} ${_linter} ${inputfile}"
            )
       endif()
    endif()

    # see  ctest  --help-properties  for properties like WILL_FAIL or TIMEOUT

    # should it fail?
    set_tests_properties(${name} PROPERTIES WILL_FAIL ${b_willfail})

    # should it timeout? (one minute)
    if(${b_timeout} STREQUAL true)
       set_tests_properties(${name} PROPERTIES TIMEOUT 60)
    endif()

    if(depends)
        set_tests_properties(${name} PROPERTIES DEPENDS ${depends})
    endif()

    # add analysis
    if(analyse)
        add_test(NAME "${name}-analysis"
            COMMAND  ${Python_EXECUTABLE} ${analyse})
        set_tests_properties("${name}-analysis" PROPERTIES DEPENDS ${name})
    endif()

endfunction(nskg_add_test)

