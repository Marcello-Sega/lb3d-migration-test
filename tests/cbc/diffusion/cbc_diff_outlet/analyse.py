#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
"""
Check whether the solution produced by NSKG coincides with the heat equation for a non-trivial diffusion problem
with a set concentration at the inlet and CBC at the outlet. The initial condition is a piecewise function with a
large concentration for the first half of the domain and a small concentration for the second half.

NOTE: writing to disk currently happens after setting the inlet values and streaming. Consequently, the slice of
nodes closest to the bulk will have different values during writing than the ones that have been enforced.
 """

import h5py
import numpy as np
from glob import glob


def load_file(name: str):
    """Load .h5 file as numpy array with axes in correct order"""
    return np.swapaxes(h5py.File(sorted(glob(name))[-1], 'r')['OutArray'], 0, 2)


# Load initial conditions
d_init = np.stack([load_file('od.h5'), load_file('wd.h5')])
v_init = np.stack([load_file('velx.h5'), load_file('vely.h5'), load_file('velz.h5')])

# Load output data
d = np.stack([load_file('od_default*h5'), load_file('wd_default*h5')])
v = np.stack([load_file('velx_default*h5'), load_file('vely_default*h5'), load_file('velz_default*h5')])

# 1. Validate inlet densities
if not np.allclose(d[..., 0], d_init[..., 0]):
    raise ValueError("Wrong values for the inlet densities")

# 2. Validate fluid velocities
if not np.allclose(v, 0):
    raise ValueError("Wrong values for the velocities")

# 3. Mass conservation
if not np.allclose(np.sum(d, axis=0), 1):
    raise ValueError("Mass is not being conserved")

# 4. Validate density gradients
d_mean = np.mean(d, axis=(1, 2))
d_mean_grad = np.gradient(d_mean, axis=1)
if not (np.all(d_mean_grad[0, :] <= 0) and np.all(d_mean_grad[1, :] >= 0)):
    raise ValueError("Density gradients inconsistent")

# 5. Validate density Laplacians
d_mean_laplacian = np.gradient(d_mean, axis=1)
if not (np.all(d_mean_laplacian[0, :] <= 0) and np.all(d_mean_laplacian[1, :] >= 0)):
    raise ValueError("Density Laplacians inconsistent")

# 6. Check correspondence with the analytical solution of the heat equation
x = np.arange(32)
od_mean = d_mean[0, :]

# Compute the analytical solutions for one and two slices being set at the inlet
approx_sol1 = 0.999 - 0.248881 * np.sin(np.pi * x / 62) + 2.87479e-6 * np.sin(3 * np.pi * x / 62)
approx_sol2 = 0.999 - 1.20662e-6 * np.cos(np.pi * (9 + x) / 20) + 0.234035 * np.cos(np.pi * (29 + x) / 60)

# Verify whether the computed solution is between the two analytical solutions
b0 = np.isclose(od_mean, approx_sol1)
b1 = np.isclose(od_mean, approx_sol2)
b2 = (approx_sol1 < od_mean) & (approx_sol2 > od_mean)

if not np.all(b0 | b1 | b2):
    raise ValueError("Density does not comply with the analytical solution")

print('Success 🙂')
