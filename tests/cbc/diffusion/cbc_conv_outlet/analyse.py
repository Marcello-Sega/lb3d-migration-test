#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
"""
Check whether the solution produced by NSKG coincides with the expected solution for the convective outlet boundary
condition. The initial condition is a piecewise function with a large concentration for the first half of the domain and
a small concentration for the second half.

NOTE: writing to disk currently happens after setting the inlet values and streaming. Consequently, the slice of
nodes closest to the bulk will have different values during writing than the ones that have been enforced.
 """

import h5py
import numpy as np
from glob import glob


def load_file(name: str):
    """Load .h5 file as numpy array with axes in correct order"""
    return np.swapaxes(h5py.File(sorted(glob(name))[-1], 'r')['OutArray'], 0, 2)


# Load initial conditions
d_init = np.stack([load_file('od.h5'), load_file('wd.h5')])
v_init = np.stack([load_file('velx.h5'), load_file('vely.h5'), load_file('velz.h5')])

# Load output data
d = np.stack([load_file('od_default*h5'), load_file('wd_default*h5')])
v = np.stack([load_file('velx_default*h5'), load_file('vely_default*h5'), load_file('velz_default*h5')])

# 1. Validate inlet densities
if not np.allclose(d[..., 0], d_init[..., 0]):
    raise ValueError("Wrong values for the inlet densities")

# 2. Validate fluid velocities
if not np.allclose(v, 0):
    raise ValueError("Wrong values for the velocities")

# 3. Mass conservation
if not np.allclose(np.sum(d, axis=0), 1):
    raise ValueError("Mass is not being conserved")

# 4. Check whether the density profiles are perfectly linear
d = d[..., :-2]  # discard the last two slices which are distorted due to the outlet
d_mean = np.mean(d, axis=(1, 2))
d_mean_grad = np.gradient(d_mean, axis=1)
d_mean_grad[0, :] = -d_mean_grad[0, :]  # partially invert the gradients for easy comparison of all gradients
if not np.allclose(d_mean_grad, d_mean_grad[0, 0]):
    raise ValueError("Density gradients inconsistent")

print('Success 🙂')
