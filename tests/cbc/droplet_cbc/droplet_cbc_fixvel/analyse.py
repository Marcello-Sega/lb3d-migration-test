#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# Consistency test for convection boundary

#########################################
# this test does not check the physics  #
# only that the code reproduces the     #
# expected results.                     #
# Update with an analytical expectation.#
# if possible.                          #
#########################################

import sys, glob, h5py
import numpy as np
from scipy.optimize import curve_fit
np.seterr(all='raise')


# expected values of max density, fitting params and contact angle
# these are for consistency checks only.

# this glob should only hit one file
dens = sorted(glob.glob('od_out_t*.h5'))
try:
    i=0
    for den in dens:
        rho =  h5py.File(den, 'r')['OutArray'][:,:,1][::,::-1].T    
        i=i+1
except:
    pass
error=False
den = sorted(glob.glob('od_out_t*.h5'))[-1]
hf=h5py.File(den, 'r')
rho = np.array(hf['OutArray'])
#rho =  h5py.File(den, 'r')['OutArray'][:,:,1][::,::-1]    
nx = np.shape(rho)
print("\n")
x_droplet = 0
rho_droplet = 4.
for x in range(nx[2]):
    if np.any(rho[:, :, x] > rho_droplet):
        x_droplet = x
        break
x_droplet_ref = 35	# Value read from previous observation
if not x_droplet==x_droplet_ref:
    print("Wrong droplet position: not "+str(x_droplet_ref)+" but "+str(x_droplet))
    error=True
else:
    print ("the droplet escapes the system!!")
if error:
    print("Failure 🤦")
    raise Exception ('test failed')
print("\n\nThis is a consistency check only. Update if possible with an analytical one.")
print("Success 🙂")
