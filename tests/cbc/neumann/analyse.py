#!/usr/bin/env python3
"""
Checks whether the Neumann boundary condition enforces a gradient of zero at the end of the channel for all time steps.
"""
import f90nml
import h5py
import numpy as np
from glob import glob

f90nml.read("nskg.input")


def load_file(name: str):
    """Load a .h5 file as a numpy array with the axes in the correct order."""
    return np.swapaxes(h5py.File(sorted(glob(name))[-1], "r")['OutArray'], 0, 2)


failed = False
files = sorted(glob("*default_t*.h5"))
for file in files:
    arr = load_file(file)
    if not np.allclose(arr[:, :, -1], arr[:, :, -2]):
        failed = True
        print("Neumann boundary condition not satisfied for file: {}".format(file))

if failed:
    print("Failure 🤦")
    raise ValueError("Neumann condition was not satisfied for certain output data.")

print('Success 🙂')
