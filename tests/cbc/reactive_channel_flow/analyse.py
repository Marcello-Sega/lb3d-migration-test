#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
"""
Checks whether the analytical result for a Danckwerts boundary condition in a continuous flow reactor can be
reproduced. Here the reactor is a channel flow with conversion of reactant into product at the walls.

As an extension to this test, we also have the system set up in "input_region", which differs from the "input"
defined file by its reactive BB activity restricted to one of the two walls of the slit, i. e. we expect about
half the net reaction rate.
"""
import h5py
import numpy as np
from glob import glob
from scipy.optimize import curve_fit

nml=f90nml.read("input")
nml_region=f90nml.read("input-step1")

def load_file(name: str):
    """Load .h5 file as numpy array with axes in correct order"""
    return np.swapaxes(h5py.File(sorted(glob(name))[-1], "r")['OutArray'], 0, 2)


# Load initial conditions
rock = load_file('plates32.h5').astype(bool)
d_init = np.stack([load_file('od.h5'), load_file('wd.h5')])
v_init = np.stack([load_file('velx.h5'), load_file('vely.h5'), load_file('velz.h5')])

# Load output data
file_tag = nml["system"]["syst"]["file_tag"]
d = np.stack([load_file('od_'+file_tag+'*h5'), load_file('wd_'+file_tag+'*h5')])
v = np.stack([load_file('velx_'+file_tag+'*h5'), load_file('vely_'+file_tag+'*h5'), load_file('velz_'+file_tag+'*h5')])
file_tag = nml_region["system"]["syst"]["file_tag"]
d_region = np.stack([load_file('od_'+file_tag+'*h5'), load_file('wd_'+file_tag+'*h5')])

# 1. Validate density gradients
d_grad = np.gradient(np.sum(d[..., :-2], axis=(1, 2)), axis=1)
if not (np.all(d_grad[0, :] <= 0) and np.all(d_grad[1, :] >= 0)):
    print("Failure 🤦")
    raise ValueError("Densities not consistently decreasing/increasing")

# 2. Check whether mass conservation holds more or less for every slice. We expect noticeable deviations between the
# concentrations close to the inlet, and the steady state, the enforced populations are the equilibrium ones. Lifting
# the relative tolerance to 0.001 makes sure the test passes and still allow for noticing unreasonable mass deviations.
d_total = np.sum(d, axis=(0, 1, 2))
if not np.allclose(d_total, d_total[0], rtol=0.001):
    print("Failure 🤦")
    raise ValueError("Mass is not being conserved")


# 3. Check resemblance with the analytical solution by means of the reactant rate constant
def analytical_solution(x, c0, u, k, L):
    """Analytical solution for the advection-diffusion equation with a source term. Constant inlet concentration and
    a Danckwerts outlet boundary condition are assumed."""
    # Implicitly assumes D = 1/6
    sqrt = np.sqrt(6 * k + 9 * u * u)
    return (c0 * np.exp(3 * u * x) * (sqrt * np.cosh(sqrt * (L - x)) + 3 * u * np.sinh(sqrt * (L - x)))) / (
            sqrt * np.cosh(L * sqrt) + 3 * u * np.sinh(L * sqrt))


# Check resemblance with the analytical solution for the reactant
c_mean        = np.sum(d,        axis=(1, 2)) / np.sum(~rock, axis=(0, 1))
c_mean_region = np.sum(d_region, axis=(1, 2)) / np.sum(~rock, axis=(0, 1))
c_mean_r        = c_mean[0, :]
c_mean_r_region = c_mean_region[0, :]
c0 = c_mean_r[0]
u = np.sum(v[2, ...]) / np.sum(~rock)  # average stream-wise flow velocity

k_input = nml["rock_boundaries"]["RB"]["reaction_rate"][0] - nml["rock_boundaries"]["rb"]["reaction_rate"][1]
L = nml["system"]["SYST"]["box"][2]
x_data = np.arange(L)


def fit_k(x, k):
    """Function to find fitting values for reaction rate"""
    return analytical_solution(x, c0, u, k, L-1)


k_fit,        _ = curve_fit(fit_k, x_data, c_mean_r,        bounds=(0, 1))
k_fit_region, _ = curve_fit(fit_k, x_data, c_mean_r_region, bounds=(0, 1))

# 3.1 Check whether the reaction rate imposed on wall node level is below the reaction rate fitted for the full system
if k_fit > k_input:
    print("Failure 🤦")
    raise ValueError("Fitted reaction rate constant has a too high value")

# 3.2 Check whether the fitted reaction rate constant for the full system has the same order of magnitude as the one in
# the input file
print("Reaction rate per input file: %.6e. Value from fit: %.6e (uniform)"%(k_input,k_fit))

if not np.isclose(np.floor(np.log10(k_fit)), np.floor(np.log10(k_input))):
    print("Failure 🤦")
    raise ValueError("Fitted reaction rate constant has a different order of magnitude")

# 4 Compare region selection to the spatially uniform run. For that test, we have disabled reactivity of one wall, so
# the resultant net reaction rate is expected to be half of the uniform run.
print("Reaction rate per input file: %.6e. Value from fit: %.6e (selective)"%(k_input,k_fit_region))
region_deviation = np.abs(2*k_fit_region/k_fit-1)
if (region_deviation > 0.01):
    print("Failure 🤦")
    raise ValueError("Uniform and selective reaction rate differ too much from each other.")
if (region_deviation < 1e-7):
    print("Failure 🤦")
    raise ValueError("Selective reaction rate is suspiciously close to uniform rate.")

print('Success 🙂')
