#!/usr/bin/env python3
import f90nml

f90nml.read("nskg.input")  # this tests that nskg.input has been written correctly
#
#  Testing momentum conservation for a single particle coupled to the fluid.
#  This test is very sensitive, given the parameters chosen. Please don't change unless necessary.
#  The idea is to test momentum conservation while *no* new fluid nodes are created/distructed during the
#  motion of the particle. This is why we use a (relatively) small initial velocity. The whole particle must
#  then move less than 1/2 lu

import numpy as np
import h5py
from glob import glob
import re 
np.set_printoptions(linewidth=np.inf)

# first things first: check that the test has been run with the proper number of cores
#if not any([ re.match('^cores *= *27',a) is not None for a in open('nskg.performance.log').readlines()]):
#   print("Failure 🤦")
#   raise Exception("Wrong number of cores used")


R = 5.3
rhop = 5.0
mass_p = 4. / 3. * np.pi * R ** 3 * rhop
mu = 1. / 6.
gamma = 6. * np.pi * mu * R

tags = ["no-mc", "mc"]

failures = 0

np.seterr(all='raise')
for tag in tags:
    particle_vel = 'ladd_particles_' + tag + '_velocity.dat'
    particle_pos = 'ladd_particles_' + tag + '_position.dat'

    fluid_datax = sorted(glob("velx_" + tag + "_t00*.h5"))
    fluid_datay = sorted(glob("vely_" + tag + "_t00*.h5"))
    fluid_dataz = sorted(glob("velz_" + tag + "_t00*.h5"))
    fluid_rho_data = sorted(glob("od_" + tag + "_t00*.h5"))
    mom_f, mass = [], []

    for i in range(len(fluid_datax)):
        vx = h5py.File(fluid_datax[i], "r")["OutArray"][:]
        vy = h5py.File(fluid_datay[i], "r")["OutArray"][:]
        vz = h5py.File(fluid_dataz[i], "r")["OutArray"][:]
        m = h5py.File(fluid_rho_data[i], "r")["OutArray"][:]
        mom_f.append([np.sum(v * m) for v in [vx, vy, vz]])
        mass.append(np.sum(m))

    mom_f = np.asarray(mom_f)

    mass = np.asarray(mass)
    mom_p = np.loadtxt(particle_vel)[:, 1:] * mass_p
    # let's reshape to sum over particles and leave the cartesian directions out
    mom_p = mom_p.reshape((mom_p.shape[0], mom_p.shape[1] // 3, 3))
    mom_p = np.sum(mom_p, axis=1)

    # pos_p mainly for debugging
    pos_p = np.loadtxt(particle_pos)[:, 1:]
    mom_tot = (mom_p + mom_f)

    if np.allclose(mom_p[-1], mom_p[0]):
        failures += 1
        print("Momentum not transferred from particle to fluid, " + tag)
        print("Failure 🤦")

    # do *not* change values here. The test *must* pass with these values, also with a particle initial velocity of 0.1
    if not np.allclose(mom_tot, mom_tot[0], rtol=0, atol=1e-10):
        failures += 1
        n = np.arange(len(mom_tot)).reshape((len(mom_tot), 1))
        print('initial momentum:')
        print(mom_tot[0:1])  # if you wonder why 0:1, it's just to have the output formatted as that of mom_tot
        print('absolute difference:', np.max(np.abs(mom_tot - mom_tot[0])))
        print("Failure 🤦")

if failures > 0:
    raise Exception("Momentum not conserved with ladd coupling")
print("Success 🙂")
