#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#  Testing momentum conservation for a single particle coupled to the fluid.
#  This test is very sensitive, given the parameters chosen. Please don't change unless necessary.
#  The idea is to test momentum conservation while *no* new fluid nodes are created/distructed during the 
#  motion of the particle. This is why we use a (relatively) small initial velocity. The whole particle must 
#  then move less than 1/2 lu

import numpy as np
import h5py
from glob import glob
import re
np.set_printoptions(linewidth=np.inf)

# first things first: check that the test has been run with the proper number of cores
#if not any([ re.match('^cores *= *27',a) is not None for a in open('nskg.performance.log').readlines()]):
#    print("Failure 🤦")
#   raise Exception("Wrong number of cores used")


R = 1.0
rhop = 5.0
mass_p = 4./3. * np.pi * R**3 * rhop
mu = 1./6.
gamma = 6. * np.pi * mu * R

np.seterr(all='raise')

particle_vel = sorted(glob('point_particles_*_velocity.dat'))[-1]
particle_pos = sorted(glob('point_particles_*_position.dat'))[-1]

fluid_datax  = sorted(glob("velx_out_t00*h5"))
fluid_datay  = sorted(glob("vely_out_t00*h5"))
fluid_dataz  = sorted(glob("velz_out_t00*h5"))
fluid_rho_data = sorted(glob("od_out_t00*h5"))
mom_f,mass = [] , []

for i in range(len(fluid_datax)):
    vx = h5py.File(fluid_datax[i], "r")["OutArray"][:]
    vy = h5py.File(fluid_datay[i], "r")["OutArray"][:]
    vz = h5py.File(fluid_dataz[i], "r")["OutArray"][:]
    m = h5py.File(fluid_rho_data[i], "r")["OutArray"][:]
    mom_f.append([np.sum(v*m) for v in [vx, vy, vz]] )
    mass.append(np.sum(m))

mom_f = np.asarray(mom_f)

mass = np.asarray(mass)
mom_p = np.loadtxt(particle_vel)[:,1:] * mass_p
# let's reshape to sum over particles and leave the cartesian directions out
mom_p = mom_p.reshape((mom_p.shape[0],mom_p.shape[1]//3,3))
mom_p = np.sum(mom_p,axis=1)

#pos_p mainly for debugging
pos_p = np.loadtxt(particle_pos)[:,1:]
mom_tot = (mom_p + mom_f)

if(all(np.isclose(mom_p[-1],mom_p[0]))):
    print("Failure 🤦")
    raise Exception("Momentum not transferred from particle to fluid")

failure = np.any(~(np.isclose(mom_tot,mom_tot[0], rtol=1e-4, atol=0)))
# TODO this is not good enough. Looks like roundoff error in the force spreading?
if failure:
    n=np.arange(len(mom_tot)).reshape((len(mom_tot),1))
    print(np.hstack((n,mom_tot)))
    print('initial momentum:')
    print(mom_tot[0:1]) # if you wonder why 0:1, it's just to have the output formatted as that of mom_tot
    if failure:
        print('max relative difference:',np.max(np.abs(mom_tot/mom_tot[0]-1)))
        print("Failure 🤦")
        raise Exception("Momentum not conserved with point coupling")

# we don't check the mass, because it might be not conserved due to fluid nodes creation/distruction
else:
    print("Success 🙂")
