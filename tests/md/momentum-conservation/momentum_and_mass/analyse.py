#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#  Testing momentum conservation for a single particle coupled to the fluid.
#  This test is very sensitive, given the parameters chosen. Please don't change unless necessary.
#  The idea is to test momentum conservation while *no* new fluid nodes are created/distructed during the 
#  motion of the particle. This is why we use a (relatively) small initial velocity. The whole particle must 
#  then move less than 1/2 lu

import numpy as np
import h5py
from glob import glob

R = 5.
rhop = 3.0
mass_p = 4./3. * np.pi * R**3 * rhop
mu = 1./6.
gamma = 6. * np.pi * mu * R

np.seterr(all='raise')

particle_data = sorted(glob("ladd_particles_*_velocity.dat"))[-1]
fluid_data = sorted(glob("velz_out_t0000*h5"))
fluid_rho_data = sorted(glob("od_out_t0000*h5"))
mom_f,mass = [] , []

for i in range(len(fluid_data)):
    v = h5py.File(fluid_data[i], "r")["OutArray"][:]
    m = h5py.File(fluid_rho_data[i], "r")["OutArray"][:]
    mom_f.append(np.sum(v*m))
    mass.append(np.sum(m))

mom_f = np.asarray(mom_f)
mass = np.asarray(mass)
mom_p = np.loadtxt(particle_data)[:,3] * mass_p
mom_tot = (mom_p + mom_f)
error=False

if(np.isclose(mom_p[-1],mom_p[0])):
    print("Failure 🤦")
    raise Exception("Momentum not transferred from particle to fluid")

failure = np.abs(mom_tot/mom_tot[0]-1.) > 5e-11 # do *not* change values here. The test *must* pass with these values, also with a particle initial velocity of 0.1
if np.any(failure):
    for i in range(len(mom_tot)):
        print("t=",i+1,"Total momentum:",mom_tot[i],"Particle momentum:",mom_p[i],"Fluid momentum:",mom_f[i],"Fluid mass:",mass[i],mom_tot[i]/mom_tot[0]-1.)
    print("Failure 🤦")
    raise Exception("Momentum not conserved with ladd coupling")

failure = np.abs(mass/mass[0]-1.) > 1e-7 # do *not* change values here. The test *must* pass with these values, also with a particle initial velocity of 0.1
if np.any(failure):
    for i in range(len(mom_tot)):
        print("t=",i+1,"Total momentum:",mom_tot[i],"Particle momentum:",mom_p[i],"Fluid momentum:",mom_f[i],"Fluid mass:",mass[i],mass[i]/mass[0]-1.)
    print("Failure 🤦")
    raise Exception("Fluid mass not conserved with ladd coupling")

else:
    print("Success 🙂")
