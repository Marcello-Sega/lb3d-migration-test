#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

import numpy  as np
import f90nml
from glob import glob
import h5py

mdinp = f90nml.read('particles_init.cfg')
R1 = mdinp['md_particle'][0]['R_orth']
R2 = mdinp['md_particle'][1]['R_orth']

den = h5py.File("od_default_t00000001.h5", "r")["OutArray"][:,32,32]

if not(np.sum(den==0)==7):
    print("The particles have not the size/position they should have. This test relies on accurate positionin")
    print("Failure 🤦")
    exit(1)

if not(np.sum(np.abs(den[1:]-den[:-1])>0) == 2):
    print("The particles are separated by fluid")
    print("Failure 🤦")
    exit(1)

forces = np.loadtxt("ladd_particles_default_force.dat")[:,1:]
if not np.all(np.isclose(forces,0.0)) :
    print("Failure 🤦")
    exit(1)

print('Success 🙂')

