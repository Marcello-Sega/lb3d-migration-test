#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#  This test is similar to friction-coefficient/ladd as it keeps the position of the particle fixed, and
#  measures the force acting on it. This time, however, we fix the velocity to a finite value (basically,  
#  just changing the reference frame) to check that the force correction for the moving boundaries gives
#  the same result as in the case of zero force.

import numpy as np
import h5py
from glob import glob
import re
import f90nml

np.seterr(all='raise')

np.set_printoptions(linewidth=np.inf)
inp = f90nml.read('input')
geometry_file= inp['md_input']['md']['geometry_file']
geom = f90nml.read(geometry_file)

pr       = inp['lb_initialization']['lbinit']['pr']
R        = geom['md_particle']['R_orth'] +0.5 # taking into account the actual hydrodynamic boundary
rhop     = geom['md_particle']['density']
velp     = geom['md_particle']['velocity'][2]
tau      = inp['relax_homogeneous']['relax']['homogeneous']['tau']
rho      = inp['lb_initialization']['lbinit']['fr']
box      = np.prod(inp['system']['syst']['box'])
normalize= np.sum(h5py.File(sorted(glob('od_*h5'))[-1], 'r')['OutArray'][:,:,:])/(box*rho)
U        = np.mean(h5py.File(sorted(glob('velz_*h5'))[-1], 'r')['OutArray'][:,:,:])/ normalize
mu       = (tau-0.5)/3. * rho
vol      = 4./3. * np.pi * R**3 
c        = vol / box
gamma    = 6. * np.pi * mu * R * (U-velp)
hasimoto = 1 - 1.7601 * c**(1./3.) + c - 1.5593 * c**2. 
# https://doi.org/10.1016/0301-9322(82)90047-7  Sangani, Acrivos, Int. J. Multiphase Flow Vol.8, No. 4, pp. 343-360,1~2
# we take just 11 coefficients out of 30. We are anyway at low enough volume fractions, it should be identical to Hasimoto's result
cmax     = np.pi/6. 
coeff    = np.asarray([1., 1.418649, 2.012564 , 2.331523, 2.564809,2.584787, 2.873609, 3.340163, 3.536763, 3.504092, 3.253622, 2.689757])
chi      = pow(c/cmax,np.arange(coeff.shape[0])/3.)
sangani  = 1./np.sum(coeff*chi)
force    = np.mean(np.loadtxt(sorted(glob("ladd_particles_*_force.dat"))[-1])[-1000:,1:],axis=0)
result   = gamma/force[2]
# K = F / ( 6 pi mu U R) 
print('Inlet velocity     =', pr)
print('Mean velocity      =', U)
print('Particlevelocity   =', velp)
print('Volume fraction    =', c)
print('Reynolds number    =', (U-velp) * (2*R) * rho/mu)
print('Stokes estimate    =',1.0)
print('Sangani estimate   =',sangani)
print('Hasimoto estimate  =',hasimoto)
print('Simulation result  =',result)

if not np.isclose(pr,U,atol=5e-3,rtol=0):
    print("Failure 🤦")
    raise Exception("Actual fluid velocity not as expected")
    
if(not all(np.isclose(force[[0,1]],0.0,atol=1e-10,rtol=0.0))):
    print("Failure 🤦")
    raise Exception("Lateral force present"+str(force[[0,1]]))

if(not np.isclose(result,hasimoto,atol=0,rtol=0.1)):
    print("Failure 🤦")
    raise Exception("Wrong friction coefficient")

print("Success 🙂")
