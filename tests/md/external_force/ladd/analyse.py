#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Let two particles pass by each other under external force, and watch if the fluid forces behave
# as expected (friction-limited velocity along motion direction from Hasimoto expansion, zero
# elsewhere). Note that we need to fix the orientation for this to work.

import numpy  as np
import f90nml
from glob import glob
import h5py

mdinp = f90nml.read('particles_init.cfg')
R = mdinp['md_particle'][0]['R_orth']
F = mdinp['md_particle'][0]['external_force'][0]
F2 = mdinp['md_particle'][1]['external_force'][0]
if np.abs(F+F2)> 1e-16 : raise (ValueError('something is wrong in the input file, forces along x do not cancel out'))
if np.abs(F)< 1e-6 : raise (ValueError('something is wrong in the input file, force along x is too small'))

inp = f90nml.read('input')
box = np.asarray(inp['system']['syst']['box'])
if not np.all(box==box[0]) : raise (ValueError('not a cubic box'))

# Use that data points from the end of the velocity trajectories for analysis (the system needs some 3000 lattice time units for equilibration)
N_last_snapshots = 90    # unit: inp['md_input']['md']['n_dump']

# Here we assume tau=1 (nu=1/6) and density=1. We could read the logged parameters, since some are the defaut ones.
eta = 1./6.
c0 = 6*np.pi*eta*R
c1 = R/box[0] ;
# Hasimoto friction expansion
gamma =  1. / ((1./c0) *  (1. - 2.837*c1 + 4.19*c1**3 - 27.4*c1**6)) ;
target_vel = F / gamma
vel = np.loadtxt('ladd_particles_default_velocity.dat',usecols=[1,2,3])
if np.any(vel[-N_last_snapshots:].std(axis=0)> np.array([ 1e-3, 1e-3, 1e-12])) : raise (ValueError('🤦 velocity drift is present'+str(vel[-N_last_snapshots:].std(axis=0))))
av_vel = vel[-N_last_snapshots:].mean(axis=0)
if not np.isclose(av_vel[0], target_vel, rtol=3e-2)  : raise (ValueError('🤦 wrong x velocity'))
# close to zero we need absolute tolerance
if not np.isclose(av_vel[1], 0, atol=1e-5)  : raise (ValueError('🤦 wrong y velocity')) 
if not np.isclose(av_vel[2], 0, atol=1e-12) : raise (ValueError('🤦 wrong z velocity'))

print("Success 🙂")
