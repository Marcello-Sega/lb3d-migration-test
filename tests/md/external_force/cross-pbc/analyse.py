#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
# This is an empirical test only (hence the high tolerance) as there's no analytically prescribed value for the efficient friction coefficient
# See Ahlrichs & Dunweg  https://doi.org/10.1063/1.480156 (see discussion around fig 2 and Eq. 20)

import numpy  as np
import f90nml
from glob import glob
import h5py

mdinp = f90nml.read('particles_init.cfg')
R = mdinp['md_particle']['R_orth']+.5 # effective radius, including bounce-back
F = mdinp['md_particle']['external_force'][0]
if np.abs(F)< 1e-6 : raise (ValueError('something is wrong in the input file, force along x is too small'))
# Here we assume tau=1 (nu=1/6) and density=1. We could read the logged parameters, since some are the defaut ones.
eta = 1./6.
gamma = 6 * np.pi * eta * R

#Ahlrichs-Dunweg correctionfactor
# 1/gamma_eff = 1/gamma + 1/(25 * eta)
# In the end we must see F/V = gamma_eff, with V velocity relative to the fluid
gamma_eff = 1./( 1./gamma + 1./(25. * eta))
target_vel = F/gamma_eff

# Select relevant time period (after transient)
times = np.loadtxt('point_particles_default_velocity.dat',usecols=[0])
times_without_transient = times > 2000

# Load trajectories
vel   = np.loadtxt('point_particles_default_velocity.dat',usecols=[1,2,3])[times_without_transient]
force = np.loadtxt('point_particles_default_force.dat',   usecols=[1,2,3])[times_without_transient]
stddev_vel = vel.std(axis=0)
stddev_force = force.std(axis=0)
print('# (DD) stddev of velocity:', stddev_vel[0])
print('# (DD) stddev of force:', stddev_force[0])
if np.any(stddev_vel   > np.array([ 2e-5, 1e-12, 1e-12])) : raise (ValueError('🤦 velocity drift is present'))
if np.any(stddev_force > np.array([ 3e-4, 1e-15, 1e-15])) : raise (ValueError('🤦 force drift is present'))
U = [np.mean(h5py.File(sorted(glob('vel'+x+'_*h5'))[-1], 'r')['OutArray'][:,:,:]) for x in ['x', 'y', 'z']]
relv = np.mean(vel[:,:],axis=0) - U
print('# (DD) mean relative velocity:',relv,'. Target:',target_vel,0.0,0.0)
if not np.isclose(relv[0], target_vel ,rtol=1e-1)  : raise (ValueError('🤦 wrong x velocity'))
# close to zero we need absolute tolerance
if not np.all(np.isclose(relv[1:], 0 ,atol=1e-9))  : raise (ValueError('🤦 wrong y or z velocity'))

print("Success 🙂")
