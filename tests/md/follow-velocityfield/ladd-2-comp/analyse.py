#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# Here we place a particle with zero velocity in a flow with a velocity (which we
# keep steady via inlet b.c.). The particles should adjust to the set z-velocity
# with a good accuracy. Furthermore in the perpendicular direction no velocity should devolop.
# Note that the instanteneous velocity does show some peaks due to the creation/destruction of
# new fluid nodes. Hence we average

import numpy as np
import h5py
from glob import glob
import re
import f90nml

inp = f90nml.read('input')
pr  = inp['lb_initialization']['lbinit']['pr']
vol =  np.prod(inp['system']['syst']['box'])
np.seterr(all='raise')

atol_vel = 1e-10
rtol_vel = 2e-3
tolerance_pos = 1e-14
# Compare the first particle (Ladd-like coupled to the fluid)
vel = np.loadtxt(sorted(glob('ladd_particles_*_velocity.dat'))[-1])
pos = np.loadtxt(sorted(glob('ladd_particles_*_position.dat'))[-1])


rho           = h5py.File(sorted(glob("od_*_t0000*h5"))[-1],'r')["OutArray"][:,:,:]
rho           = rho + h5py.File(sorted(glob("wd_*_t0000*h5"))[-1],'r')["OutArray"][:,:,:]
normalization = np.sum(rho[rho>1e-10]) / vol
pr_sim        = np.mean(h5py.File(sorted(glob("velz_*_t0000*h5"))[-1],'r')["OutArray"][:,:,:])/ normalization
print('fluid momentum:',pr_sim,pr)
N = 500 # the solution oscillates around the target momentum, *and* there are glitches
        # when rock sites are updated.
# NOTE: the ladd particle gets a kick whenever its discretized boundary changes as it moves
# With this setup it's enough to check the last frame velocity because it's not jumping. 
# If the initial conditions/ sampling frequency is changed, this may fail. 

for dir in [1,2]:
    if not np.isclose(np.mean(vel[-N:,dir]),0.0,rtol=0,atol=atol_vel):
        print("Failure 🤦")
        raise Exception("Ladd-coupled: lateral velocity larger than tolerance")

print(np.mean(vel[-N:,3]),pr_sim)
for dir in [3]:
    if not np.isclose(np.mean(vel[-N:,dir]),pr_sim,rtol=rtol_vel,atol=0.0):
        print("Mean along ",dir,":", mean,"differs from ",pr_sim,"(",np.abs(1-mean/pr_sim),"relative diff) more than the target tolerance",rtol_vel)
        print("Failure 🤦")
        raise Exception("Ladd-coupled: velocity larger than tolerance")

for dir in [1,2]:
    if not np.isclose(pos[0,dir],pos[-1,dir],rtol=0,atol=tolerance_pos):
        print("Failure 🤦")
        raise Exception("Ladd-coupled: Particle moved too much in the x/y direction") 

print("Success 🙂")

