#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# Here we place a particle with zero velocity in a flow with a velocity (which we
# keep steady via inlet b.c.). The particles should adjust to the set z-velocity
# with a good accuracy. Furthermore in the perpendicular direction no velocity should devolop.
# The ranom nois part of the langevin thermostat for the particles is switched off.

import numpy as np
import h5py
from glob import glob
import re
import f90nml

inp = f90nml.read('input')
pr  = inp['lb_initialization']['lbinit']['pr']
vol = np.prod( inp['system']['syst']['box'])
np.seterr(all='raise')

atol_vel = 5e-10
rtol_vel = 2e-2
tolerance_pos = 1e-6
# Compare the first particle (point-like coupled to the fluid)
vel = np.loadtxt(sorted(glob('point_particles_*_velocity.dat'))[-1])
pos = np.loadtxt(sorted(glob('point_particles_*_position.dat'))[-1])

rho           = h5py.File(sorted(glob("od_*_t0000*h5"))[-1],'r')["OutArray"][:,:,:]
normalization = np.sum(rho[rho>1e-10]) / vol
pr_sim        = np.mean(h5py.File(sorted(glob("velz_*_t0000*h5"))[-1],'r')["OutArray"][:,:,:])/ normalization

if(not np.isclose(pr,pr_sim,rtol=1e-6,atol=0)):
        print('set fluid velocity',pr)
        print('measured fluid velocity',pr_sim)
        print('relative deviation',abs(pr/pr_sim-1))
        print("Failure 🤦")
        raise Exception("Fluid mean velocity differes from what expected")

for dir in [1,2]:
    if not np.isclose(np.mean(vel[-1,dir]),0.0,rtol=0,atol=atol_vel):
        print('lateral velocity:',np.mean(vel[-1,dir]))
        print("Failure 🤦")
        raise Exception("Friction-coupled: lateral velocity larger than tolerance")

for dir in [3]:
    if not np.isclose(np.mean(vel[-1,dir]),pr_sim,rtol=rtol_vel,atol=0.0):
        print('fluid velocity:',pr_sim)
        print('particle velocity',np.mean(vel[-1,dir]))
        print('relative deviation',np.mean(vel[-1,dir])/pr_sim -1 )
        print("Failure 🤦")
        raise Exception("Friction-coupled: velocity larger than tolerance")

for dir in [1,2]:
    if not np.isclose(pos[0,dir],pos[-1,dir],rtol=0,atol=tolerance_pos):
        print("Failure 🤦")
        raise Exception("Friction-coupled: Particle moved too much in the x/y direction") 

print("Success 🙂")

