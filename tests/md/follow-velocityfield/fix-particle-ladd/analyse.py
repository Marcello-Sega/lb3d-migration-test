#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#  Testing momentum conservation for a single particle coupled to the fluid.
#  This test is very sensitive, given the parameters chosen. Please don't change unless necessary.
#  The idea is to test momentum conservation while *no* new fluid nodes are created/distructed during the 
#  motion of the particle. This is why we use a (relatively) small initial velocity. The whole particle must 
#  then move less than 1/2 lu

import numpy as np
import h5py
from glob import glob
import re
np.set_printoptions(linewidth=np.inf)

# first things first: check that the test has been run with the proper number of cores
#if not any([ re.match('^cores *= *27',a) is not None for a in open('nskg.performance.log').readlines()]):
#    print("Failure 🤦")
#   raise Exception("Wrong number of cores used")


R = 5.3
rhop = 3.0
mass_p = 4./3. * np.pi * R**3 * rhop
mu = 1./6.
gamma = 6. * np.pi * mu * R

np.seterr(all='raise')

mom_p = np.loadtxt('ladd_particles_out_velocity.dat')[:,:]
pos_p = np.loadtxt('ladd_particles_out_position.dat')[:,:]

fail = not np.all(np.isclose(mom_p[:,[4,6]],0.0))
fail = fail or not np.all(np.isclose(pos_p[-1,[4,6]],pos_p[0,[4,6]]))
if fail:
    print("Failure 🤦")
    raise Exception("Momentum transferred to fixed direction ")
start_t = 20 # the flow pushes in directio z, it takes time to develop fluctuations orthogonally
fail = np.any(np.isclose(mom_p[start_t:,[3,5]],0.0))
fail = fail or np.any(np.isclose(pos_p[-1,[3,5]]-pos_p[0,[3,5]],0.0))
if fail:
    print("Failure 🤦")
    raise Exception("Momentum not transferred to free direction")

print("Success 🙂")
