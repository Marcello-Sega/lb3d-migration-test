#!/usr/bin/env python3
"""
Two lamellae with a particle initialized on the interface but not at an equilibrium. The particle should start
moving towards its equilibrium position.
"""
import numpy as np
import f90nml

f90nml.read("nskg.input")  # this tests that nskg.input has been written correctly

forces = np.loadtxt("ladd_particles_default_force.dat")
positions = np.loadtxt("ladd_particles_default_position.dat")
velocities = np.loadtxt("ladd_particles_default_velocity.dat")

# 1. Check whether the particle is motionless in the x- and y-direction
assert np.allclose(forces[:, 1:3], 0)
assert np.allclose(positions[:, 1:3], positions[1, 1])
assert np.allclose(velocities[:, 1:3], 0)

# 2. Check the movement of the particle in the z-direction
assert np.all(np.diff(positions[:, 3]) < 0), "Particle not moving towards the equilibrium position"
assert np.all(velocities[:, 3] < 0), "Particle not moving towards the equilibrium position"

print('Success 🙂')
