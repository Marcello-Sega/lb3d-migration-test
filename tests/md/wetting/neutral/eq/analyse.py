#!/usr/bin/env python3
"""
Two lamellae with a particle initialized exactly on the interface. The particle should stay exactly at its equilibrium
position. The long simulation duration is necessary to relax the initial shockwave from the rough initialisation.
"""
import numpy as np
import h5py
from glob import glob
import f90nml

f90nml.read("nskg.input")  # this tests that nskg.input has been written correctly

forces = np.loadtxt("ladd_particles_default_force.dat")
positions = np.loadtxt("ladd_particles_default_position.dat")
velocities = np.loadtxt("ladd_particles_default_velocity.dat")

# 1. Check whether the particle is motionless
assert np.allclose(forces[:, 1:4], 0)
assert np.allclose(positions[:, 1:4], positions[0, 1])
assert np.allclose(velocities[:, 1:4], 0)

# 2. Check whether there is a halo of increased density around the particle
# See figure 1 in the paper
# "From bijels to Pickering emulsions: A lattice Boltzmann study"
# https://dx.doi.org/10.1103/PhysRevE.83.046707
d0 = h5py.File(sorted(glob("od_*_t0000*h5"))[-1], 'r')["OutArray"][:, :, :]
d1 = h5py.File(sorted(glob("wd_*_t0000*h5"))[-1], 'r')["OutArray"][:, :, :]

# The values for fb,fr and pb,pr in the input file were chosen so that the system is in the binodal part of the phase diagram,
# and the simulation will stay in the demixed phase. The value of target_density used here, is determined by letting the
# simulation run for a long time, and then looking into the sum density fields.
target_density = 1.1005
assert np.all(d0 + d1 < target_density *1.01)   # Transient variation of densities is below 1% after some 700ish time steps.

print('Success 🙂')
