#!/usr/bin/env python3
"""
Two lamellae with a particle initialized exactly on the interface. The particle should stay exactly at its equilibrium
position. However, in comparison with the equilibrium test for neutral wetting the particle should repel both fluids
here. Hence, we expect a smaller density at the nodes neighbouring the rocks.
"""
import numpy as np
import numpy.ma as ma
import h5py
from glob import glob
import f90nml

f90nml.read("nskg.input")  # this tests that nskg.input has been written correctly

# 1. Check whether the particle is motionless
forces = np.loadtxt("ladd_particles_default_force.dat")
positions = np.loadtxt("ladd_particles_default_position.dat")
velocities = np.loadtxt("ladd_particles_default_velocity.dat")

assert np.allclose(forces[:, 1:4], 0)
assert np.allclose(positions[:, 1:4], positions[0, 1])
assert np.allclose(velocities[:, 1:4], 0)

# 2. Check whether the total fluid density is lower around the rocks
d0 = np.swapaxes(h5py.File(sorted(glob("od_*_t0000*h5"))[-1], 'r')["OutArray"][:, :, :], 0, 2)
d1 = np.swapaxes(h5py.File(sorted(glob("wd_*_t0000*h5"))[-1], 'r')["OutArray"][:, :, :], 0, 2)
rocks = np.swapaxes(h5py.File(sorted(glob("rock_*_t0000*h5"))[-1], 'r')["OutArray"][:, :, :], 0, 2) != 0

# Compute which nodes neighbour rock nodes
neighbours = np.zeros(rocks.shape, dtype=bool)
shift = [-1, 0, 1]
for x in shift:
    for y in shift:
        for z in shift:
            neighbours = neighbours | np.roll(rocks, (x, y, z), (0, 1, 2))
neighbours = ~rocks & neighbours

# Compare the average densities of the bulk and the neighbouring nodes
neighbour_density = ma.array(d0 + d1, mask=~neighbours)
bulk_density = ma.array(d0 + d1, mask=rocks | neighbours)

surface_density_ratio = np.average(neighbour_density)/np.average(bulk_density)
assert surface_density_ratio < 0.9

print('Success 🙂')
