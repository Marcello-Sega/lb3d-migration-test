#!/usr/bin/env python3
"""
Two lamellae with a particle initialized on the interface. Due to the non-neutral wetting the particle should start
moving towards its equilibrium position with a contact angle that is not equal to 90 degrees. Here, the position of the
particle after the initial movement is compared with the reference value computed when this test was merged. Under the
assumption that the current version of the code is correct this can be seen as an implicit contact angle test.
"""

import numpy as np
import f90nml

f90nml.read("nskg.input")  # this tests that nskg.input has been written correctly

forces = np.loadtxt("ladd_particles_default_force.dat")
positions = np.loadtxt("ladd_particles_default_position.dat")
velocities = np.loadtxt("ladd_particles_default_velocity.dat")

# 1. Check whether the particle is motionless in the x- and y-direction
assert np.allclose(forces[:, 1:3], 0)
assert np.allclose(positions[:, 1:3], positions[1, 1])
assert np.allclose(velocities[:, 1:3], 0)

# 2. Check whether the particle is moving upwards in the z-direction
assert np.all(positions[1:, 3] > 16), "Particle not moving upwards"

# 3. Check whether the value for the final 200 timesteps is close to a reference value computed when this test was
# merged. This assumes that the implementation was correct at this time and can be seen as an implicit contact angle
# test.
mean_reference_position = 17.3708
assert np.allclose(positions[800:, 3], mean_reference_position,
                   rtol=1e-3), "Contact angle different from the reference value"

print('Success 🙂')
