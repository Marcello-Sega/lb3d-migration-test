#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# Here we test the implementation of the repulsive plane on one MD-particle

import numpy as np
import f90nml as nml

np.seterr(all='raise')
parts  = nml.read('init.cfg')['md_particle']
plane  = nml.read('input')['moving_plane']
plane_velocity = plane['mp']['rep_plane_velocity_z']

data = np.genfromtxt('point_particles_default_velocity.dat', skip_header=0)
data_late = data[-4:,:]   # v_z transient is initially oscillating with an exponential envelope, that has a time constant of about 140, and starts to fluctuate after about 1000 time steps.
avg_vz = np.mean(data_late[:,3])
avg_vxy = np.mean(data_late[:,[1,2]])
if not np.isclose(avg_vz,plane_velocity,rtol=2e-3,atol=0):
    print("Failure 🤦")
    raise Exception("Z-velocity %e not the same as expected %e" % (avg_vz,plane_velocity))

if not np.isclose(avg_vxy,0.0):
    print("Failure 🤦")
    raise Exception("X/Y-velocity %e larger than zero" % (avg_vxy))


print("Success 🙂")
