#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# We place two obstacle-type particles (fixed, allowed to overlap), in a way that they
# push upon a third, mobile, particle in an isoscele triangle, i. e. the y components
# of the interaction forces must add up to zero.
#   _____
#  /     \
#  |Obst1|··..
#  X=====X  ..(Test)
#  |Obst2|··
#  \_____/
#
# Note that the actual position of the particles is important to make sure they belong
# to different ranks when run with 2 procs (don't change these conditions)


import numpy as np
import h5py, glob

Failure = False

nml = f90nml.read("input")
file_tag = nml["system"]["syst"]["file_tag"]
s_traj  = "ladd_particles_"+file_tag+"_position.dat"
s_force = "ladd_particles_"+file_tag+"_force.dat"

m_traj  = np.loadtxt(s_traj,  usecols=(1,2,3, 4,5,6, 7,8,9))
m_force = np.loadtxt(s_force, usecols=(1,2,3, 4,5,6, 7,8,9))
motion_obstacles = np.diff(m_traj[:, 0:6], axis=0)
force_tracer = m_force[:, 6:]

# Test for obstacles being fixed
if not np.isclose(motion_obstacles,0.0).all():
    Failure = True
    print("Obstacle particles are moving")

# Test for force direction. The vast majority of force is acting in +x direction.
tol_yz=1e-15
strongly_in_plus_x=1.
if force_tracer[0, 0]<strongly_in_plus_x:
    Failure = True
    print("In first time step, force", force_tracer[0], "is not pointing strongly in +x direction")
# y, z directions should only experience forces at the limit of numerical noise
for t, V in enumerate(force_tracer):
    if not (np.isclose(np.abs(V[1]), 0.0, atol = tol_yz)):
        Failure = True
        print("y (parallel) velocity of", force_tracer[t], "too large in time step", t)
    if not (np.isclose(np.abs(V[2]), 0.0, atol = tol_yz)):
        Failure = True
        print("z (lateral) velocity of",  force_tracer[t], "too large in time step", t)
if Failure:
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
