#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Test for when the position of a particle is fixed the particle should not move
import numpy as np
import h5py, glob

# check fluid moves
vel = h5py.File(sorted(glob.glob('velx_*h5'))[-1], 'r')['OutArray'][:,:,:]
if np.isclose(np.mean(vel), 0.0):
    print("Failure 🤦")
    raise Exception("There is no velocity in the fluid")

# check particle velocity is fixed
data = np.genfromtxt('ladd_particles_default_velocity.dat', skip_header=0)
avg = np.mean(data,axis=0)[1:]
if not all(np.isclose(avg,0.0)):
     print("Failure 🤦")
     raise Exception("Velocity different from zero: (%e, %e, %e)"%(avg[0], avg[1], avg[2]))

print("Success 🙂")
