#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# A Ladd sphere spins up under an external torque (rotation axis is hard-coded to +y direction as of now)

import numpy as np
import f90nml
from glob import glob
import h5py

lbinp = f90nml.read('input')
mdinp = f90nml.read(lbinp["md_input"]["md"]["geometry_file"])

file_tag = lbinp["system"]["syst"]["file_tag"]
iterations = lbinp["system"]["syst"]["n_iteration"]
dump_interval = lbinp["md_input"]["md"]["n_dump"]
torque_in   = mdinp["md_particle"]["external_torque"][1]
R_orth      = mdinp["md_particle"]["R_orth"]
md_mass     = mdinp["md_particle"]["density"]*4./3.*np.pi*(R_orth)**3
md_inertia  = 2./5*md_mass*(R_orth)**2

Failure = False

# Check unit length of quaternion
quaternion_log = np.loadtxt("ladd_particles_"+file_tag+"_quaternion.dat", usecols=(1,2,3,4))
orientation_log = np.loadtxt("ladd_particles_"+file_tag+"_orientation.dat", usecols=(1,2,3))
if (np.any(~np.isclose(np.linalg.norm(quaternion_log, axis=1), 1.))):
    Failure = True
    print("Quaternions not normalised!")

# Torque points in +y direction. Recover history of rotation angle
rotated_by_q = 2*np.arctan2(quaternion_log[:, 2],quaternion_log[:, 0])
rotated_by_o = np.arctan2(orientation_log[:, 0],orientation_log[:, 2])
if np.any(~np.isclose(rotated_by_q, rotated_by_o)):
    Failure = True
    print("Quaternions and orientations change differently")

# Rotation angle should increase 
# We could do a np.polyfit() here, but two numerical differentiations do the trick just as well.
rotation_diff2 = np.diff(rotated_by_q,n=2)      # ~ measured torque as second derivative of orientation, see below for units
rotation_diff2_std  = np.std (rotation_diff2)

if not np.isclose(rotation_diff2_std, 0., atol=1e-11):
    Failure = True
    print("Uneven spin-up (standard deviation of effective torque too large)")

# Apparent inertia and effective torque
torque_correct_units = (md_inertia/dump_interval**2)*rotation_diff2
if ~np.all(np.isclose(torque_correct_units-torque_in, 0., atol=1e-8)):
    print("Measured effective torque ", torque_correct_units," differs from input torque ", torque_in)
    Failure = True


if Failure:
    print("Failure 🤦")
    exit(1)

print('Success 🙂')
