#!/usr/bin/env python3
#
# Plug in euler angle in aerospace zyx-convention (psi theta phi) -> (z y x)
# [Kuipers 1999 page 86 Eq 4.4 and page 166 - 168] in arc length
# in our case (psi, theta, phi) = (np.pi/2, np.pi/3, np.pi/4)
# and check whether the output in nml files is correct
#
import f90nml
import numpy as np
from glob import glob

f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
np.seterr(all='raise')
np.set_printoptions(linewidth=np.inf)

def quaterniontoEulerAngleZYX(q):
    '''
    Parameters
    ----------
    q: np.array([float, float, float, float])
        unit quaternion, i.e. |q| = 1
        
    Returns
    -------
    eulerAngle: np.array([float, float, float])
        Euler angle in aereospace convention $(\psi, \theta, \phi)$ -> $(z y x)$,
        i.e. R = R_x^\phi R_y^\theta R_z^psi, in arc length
        [Kuipers 1999 page 86 Eq 4.4 and page 166 - 168] 
    
    Example
    -------
    >>> quaterniontoEulerAngleZYX(np.array([0.70105738, -0.09229596,  0.56098553,  0.43045933]))*180/np.pi
    array([90.00000131, 60.00000025, 45.00000108])  # in degree now
    '''       
    return np.array([np.arctan2(2*q[1]*q[2]+2*q[0]*q[3], 2*q[0]**2 + 2*q[1]**2 - 1),\
                     np.arcsin(2*q[0]*q[2] - 2*q[1]*q[3]),\
                     np.arctan2(2*q[2]*q[3]+2*q[0]*q[1], 2*q[0]**2 + 2*q[3]**2 - 1)])

# read geometry file
inp = f90nml.read('input')
geometry_file = inp['md_input']['md']['geometry_file']
geom = f90nml.read(geometry_file)
euler_zyx = geom['md_particle']['euler_zyx']
# read out data from nml file
data = f90nml.read(sorted(glob('md-cfg_*_t00000001.nml'))[0])
euler_zyx_data = data['md_particle']['euler_zyx']
quaternion_data = data['md_particle']['quaternion']

if not np.all(np.isclose(euler_zyx,euler_zyx_data,atol=1e-10,rtol=1e-10)):
    print("Failure 🤦")
    raise Exception("Input ouput routine is broken for the Euler angle in nml files")    
if not np.all(np.isclose(euler_zyx,quaterniontoEulerAngleZYX(quaternion_data),atol=1e-10,rtol=1e-10)):
    print("Failure 🤦")
    raise Exception("Euler angle to match to quaternions in nml files")    
print("Success 🙂")    
