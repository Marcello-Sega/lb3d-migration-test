#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
# We check that the orientation property of a particle is recomputed appropriately
# when a particle is communicated across ranks. 

# If it is not, the force of the fluid on  the two identical particles (one place in a domain, the
# other across two) will be different. Note that the particles need to have a different R_orth
# and R_para, otherwise a wrong orientation does not make a difference.

import numpy as np
from glob import glob
data = np.loadtxt('ladd_particles_default_force.dat')[1:].reshape(2,3)
if not  np.all(np.isclose(data[0],data[1],atol=1e-15, rtol=0)):
    print("Failure 🤦")
    exit(1)

print('Success 🙂')
