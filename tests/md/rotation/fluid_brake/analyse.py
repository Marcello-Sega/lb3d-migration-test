#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# A rotating Ladd sphere loses its angular velocity to fluid forces

import numpy as np
import f90nml
from glob import glob
import h5py

Failure = False

lbinp = f90nml.read('input')
mdinp = f90nml.read(lbinp["md_input"]["md"]["geometry_file"])

file_tag = lbinp["system"]["syst"]["file_tag"]
iterations = lbinp["system"]["syst"]["n_iteration"]
dump_interval = lbinp["md_input"]["md"]["n_dump"]
R_orth      = mdinp["md_particle"]["R_orth"]
angular_velocity_in      = mdinp["md_particle"]["angular_velocity"]
md_mass     = mdinp["md_particle"]["density"]*4./3.*np.pi*(R_orth)**3
md_inertia  = 2./5*md_mass*(R_orth)**2
position = mdinp["md_particle"]["pos"]

# particle
angular_velocity = np.loadtxt("ladd_particles_"+file_tag+"_angular_velocity.dat", usecols=(1,2,3))
if not np.all(np.isclose(angular_velocity[0]-angular_velocity_in, 0., atol=1e-13)):
    print("Angular velocity at the beginning", angular_velocity[0]," is not close enough to the initial value, ", angular_velocity_in)
    Failure = True

if np.all(np.isclose(angular_velocity[-1]-angular_velocity[0], 0., atol=1e-13)):
    print("Angular velocity is not changing at all. Are you sure that you have *not* set `fix_rotation=T T T`?")
    Failure = True

if not np.all(np.isclose(angular_velocity[-1], 0., atol=1e-5)):
    print("Angular velocity does not reach zero but",angular_velocity[-1])
    Failure = True

if Failure:
    print("Failure 🤦")
    exit(1)

print('Success 🙂')
