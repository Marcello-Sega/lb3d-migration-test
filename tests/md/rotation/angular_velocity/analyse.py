#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# A prolate spheroid, rotating along an axis different from its director
# Also tests the 'nml' MD dump format

import numpy as np
import f90nml
from glob import glob
import h5py

lbinp = f90nml.read('input')
mdinp = f90nml.read(lbinp["md_input"]["md"]["geometry_file"])

file_tag = lbinp["system"]["syst"]["file_tag"]
time_unit = lbinp["md_input"]["md"]["n_dump"]
angular_velocity_in = mdinp["md_particle"]["angular_velocity"]
R_orth              = mdinp["md_particle"]["R_orth"]
R_para              = mdinp["md_particle"]["R_para"]

Failure = False

# Tests for 'nml' output format: rotation velocity
files_nml = sorted(glob("md-cfg_"+file_tag+"_t*.nml"))
last_quaternion=np.zeros(4)
l_omega=[]
for S in files_nml:
    fileinp = f90nml.read(S)
    quaternion = np.array(fileinp["md_particle"]["quaternion"])
    quaternion_increment = 2*np.linalg.norm(quaternion-last_quaternion)/time_unit
    l_omega += [quaternion_increment]
    last_quaternion = quaternion
v_omega = np.array(l_omega[1:])
angular_velocity_sim_mean = np.mean(v_omega)
angular_velocity_sim_std  = np.std( v_omega)

if (not np.isclose(angular_velocity_sim_std,0., atol=1e-10)):
    print("Unsteady rotation (from reading quaternion history)", angular_velocity_sim_std)
    Failure = True

if (not np.isclose(angular_velocity_sim_mean,np.linalg.norm(angular_velocity_in),atol=1e-6)):
    print("Wrong angular velocity average (from reading quaternion history)", angular_velocity_sim_mean-np.linalg.norm(angular_velocity_in))
    Failure = True

# Tests for simulation output: spheroid volume
files_rock = sorted(glob("rock_state_"+file_tag+"_t*.h5"))[1:]  # TODO Excluding the t=0 snapshot, known to be broken, will open a separate issue for that soon™.
volume = 4./3.*np.pi*R_orth*R_orth*R_para
for S in files_rock:
    rock = np.array(h5py.File(S, "r")["OutArray"], dtype=int)
    rock_size = np.sum(rock)
    # tolerance value to account for Ladd volume fluctuations
    if (not np.isclose(rock_size,volume,rtol=0.07)):
        print("Wrong particle volume", rock_size, volume, "rockfile", S)
        Failure = True


if Failure:
    print("Failure 🤦")
    exit(1)

print('Success 🙂')
