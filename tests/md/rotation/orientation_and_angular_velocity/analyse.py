#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# A very simple tests: makes sure that the angular velocity written to file is computed in the
# lab reference frame. If that is not the case, changin sign to the initial orientation (even on zero elements) 
# would change the angular velocity

import numpy as np
import f90nml
from glob import glob

lbinp = f90nml.read('input')
mdinp = f90nml.read(lbinp["md_input"]["md"]["geometry_file"])

# Tests for 'nml' output format: rotation velocity
file_nml = sorted(glob("md-cfg_*t*.nml"))[-1]

mdout = f90nml.read(file_nml)

w1 = mdout['md_particle'][0]['angular_velocity']
w2 = mdout['md_particle'][1]['angular_velocity']
if(not(np.all(w1==w2))):
    print("Failure 🤦")
    exit(1)

print('Success 🙂')
