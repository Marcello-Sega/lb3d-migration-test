#!/usr/bin/env python3
import f90nml

f90nml.read("nskg.input")  # this tests that nskg.input has been written correctly

"""
In this test the mass correction is tested for a simulation of a Ladd particle in a single component fluid. In 
particular,it is checked whether new fluid nodes are initialized with the correct values.
"""

import os
import numpy as np
import numpy.ma as ma
import re
from glob import glob
import h5py

# 1. Extract the dumped iterations from the .h5 files
iterations = []
for file in os.listdir(os.getcwd()):
    if file.endswith(".h5"):
        t = re.search('_t(.+?).h5', file)
        iterations.append(int(t.group(1)))

iterations = np.sort(np.unique(np.array(iterations))).astype(int)


# 2. Function to load any particular .h5 density file based on the iteration number
def load(t):
    """Return a numpy array with the data of the .h5 density file with the corresponding iteration number"""
    return np.swapaxes(h5py.File(sorted(glob("od_default_t*" + str(t) + "*h5"))[0], 'r')["OutArray"][:, :, :], 0, 2)


# 3. Function to compute the local min, mean and max values of a density field
def compute_local_min_mean_max_new_nodes(dens, new_fluid_nodes):
    """Computes the local minima, mean and maxima based on the six nearest neighbours"""
    dens_shifted = np.empty((6, np.sum(new_fluid_nodes)))
    indices = np.array(np.where(new_fluid_nodes)).T

    for i in range(np.sum(new_fluid_nodes)):
        idx = 0
        for axis in (0, 1, 2):
            for shift in (-1, 1):
                ind = np.copy(indices[i])
                ind[axis] = ind[axis] + shift
                # Ensure that densities of newly initialized nodes do not end up in the results
                if np.any(np.all(ind == indices, axis=1)):
                    dens_shifted[idx, i] = 0
                else:
                    dens_shifted[idx, i] = dens[tuple(ind)]
                idx += 1

    mask = np.isclose(dens_shifted, 0)
    dens_shifted = ma.array(dens_shifted, mask=mask)

    dens_min = np.array(ma.amin(dens_shifted, 0))
    dens_mean = np.array(ma.mean(dens_shifted, 0))
    dens_max = np.array(ma.amax(dens_shifted, 0))

    return dens_min, dens_mean, dens_max


# 4. Function to compute which nodes have been transformed from rock to fluid
def compute_new_fluid_nodes(dens_old, dens_new):
    """Computes which nodes have been transformed from rock into fluid"""
    mask_old = np.isclose(dens_old, 0)
    mask_new = np.isclose(dens_new, 0)

    return mask_new ^ (mask_old | mask_new)


# 5. Loop through all the density files and check if the densities of new fluid nodes are corrected for any mass changes
initial_mass = np.sum(load(0))
mass_correction_constant = f90nml.read("input")["md_input"]["md"]["mass_correction_constant"]
box_volume = np.prod(f90nml.read("input")["system"]["syst"]["box"])
failed = False

for idx in range(len(iterations) - 1):
    dens_old = load(iterations[idx])
    dens_new = load(iterations[idx + 1])

    current_mass = np.sum(dens_old)
    new_fluid_nodes = compute_new_fluid_nodes(dens_old, dens_new)

    if np.sum(new_fluid_nodes) > 0:
        # We only test stuff when a Ladd particle frees some fluid nodes.
        dens_min, dens_mean, dens_max = compute_local_min_mean_max_new_nodes(dens_new, new_fluid_nodes)
        dens_sim = dens_new[new_fluid_nodes]

        # Directly test whether the equation for the mass correction is applied correctly
        ratio = 1 - mass_correction_constant * (current_mass - initial_mass) / box_volume
        dens = dens_mean * ratio

        smaller = dens < dens_min
        dens[smaller] = dens_min[smaller]

        larger = dens > dens_max
        dens[larger] = dens_max[larger]

        # NOTE: we need to use an additional tolerance here since bounce-back with moving particles is not perfectly
        # mass conserving. This is not reflected into the density output at the same time step. Hence, the ratios used
        # by the mass scaler at a particular time step can be slightly different from the ratios that can be computed
        # from the density files. Furthermore, this issue worsens when the mass correction constant is large. Hence,
        # for this test only a relatively small mass correction constant is used.
        if not np.allclose(dens_sim, dens, atol=0.001, rtol=0.001):
            print("Mass conservation violated in time step {}".format(idx))
            failed = True

if failed:
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
