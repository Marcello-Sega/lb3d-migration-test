#!/usr/bin/env python3
"""
In this test the mass correction is tested for a simulation of a Ladd particle with non-neutral wetting in a
multi-component fluid. In particular, it is checked whether the total mass of both components remains close to the
initial masses.
"""
import os
import numpy as np
import re
from glob import glob
import h5py
import f90nml

f90nml.read("nskg.input")  # this tests that nskg.input has been written correctly

# 1. Extract the dumped iterations from the .h5 files
iterations = []
for file in os.listdir(os.getcwd()):
    if file.endswith(".h5"):
        t = re.search('_t(.+?).h5', file)
        iterations.append(int(t.group(1)))

iterations = np.sort(np.unique(np.array(iterations))).astype(int)

# 2. Compare the mass change after the final iteration with a reference value
rho0init = np.sum(np.swapaxes(h5py.File(sorted(glob("od_default_t*h5"))[0], 'r')["OutArray"][:, :, :], 0, 2))
rho1init = np.sum(np.swapaxes(h5py.File(sorted(glob("wd_default_t*h5"))[0], 'r')["OutArray"][:, :, :], 0, 2))

stat_start = 1000  # The time step after which the statistics are used
idx = np.where(iterations == stat_start)
rho0stat = rho1stat = 0

for i in iterations[int(idx[0]):]:
    rho0stat += np.sum(
        np.swapaxes(h5py.File(sorted(glob("od_default_t*" + str(i) + "*h5"))[0], 'r')["OutArray"][:, :, :], 0, 2))
    rho1stat += np.sum(
        np.swapaxes(h5py.File(sorted(glob("wd_default_t*" + str(i) + "*h5"))[0], 'r')["OutArray"][:, :, :], 0, 2))

rho0stat /= len(iterations[int(idx[0]):])
rho1stat /= len(iterations[int(idx[0]):])

if ~np.isclose(rho0stat / rho0init, 1, rtol=5e-4, atol=5e-4):
    print("Failure 🤦")
    raise Exception("Mass conservation for component 0 not working correctly.")
if ~np.isclose(rho1stat / rho1init, 1, rtol=1e-4, atol=1e-4):
    print("Failure 🤦")
    raise Exception("Mass conservation for component 1 not working correctly.")

print("Success 🙂")
