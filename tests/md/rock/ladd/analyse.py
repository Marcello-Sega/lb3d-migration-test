#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Interaction of Ladd particles with static rocks

import numpy  as np
import f90nml
from glob import glob
import h5py

mdinp = f90nml.read('md.nml')
potinp = f90nml.read('potential.cfg')
R = mdinp['md_particle']['R_orth']
pos = mdinp['md_particle']['pos'][0]
cutoff =  potinp['potential_lj']['cutoff']
epsilon =  potinp['potential_lj']['epsilon']
sigma =  potinp['potential_lj']['sigma']

# note that the interaction with rock walls goes into fluid force at the moment
force = np.loadtxt('ladd_particles_default_force.dat')[1:]

mg = np.mgrid[1:2,1:9,1:9]-0.5
r = np.asarray([4.,4.5,4.5]).reshape(3,1,1,1) - mg
r = r.reshape(3,-1)
dist_ = np.sqrt(r[0,:]**2+r[1,:]**2+r[2,:]**2)
dist = dist_[dist_<3.8]

def lj_force(x):
    d=np.sqrt(np.sum(x**2,axis=0))
    f = 48.*epsilon*(sigma/d)**6*((sigma/d)**6-0.5)*x/d**2
    f[:,d>cutoff]=0.0
    return np.sum(f,axis=1)


if np.all(np.isclose(lj_force(r),force)):
    print('Success 🙂')
else:
    print("Failure 🤦")
    exit(1)

