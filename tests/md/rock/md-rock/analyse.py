#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Force between different types of particles, and a flat rock wall.
#
# This will test:
#   * working MD-rocks part of the interaction matrix
#   * coexistence of Ladd and point particles
#   * coexistence of central forces and surface interactions for either
#   * force cut-off obedience
#   * force sign convention
#   * constancy of equilibrium positions (we place particles close to the equilibrium, but slightly shifted to closer distances, so that the resulting force will be small but repulsive, i. e. have the same sign as a pair of point particles of that distance) TODO: Think of a better test/analysis scenario for equilibrium
#
# See "./input" for the box layout
#
# TODO The majority of cases within this test are currently disabled. See <https://git.iek.fz-juelich.de/compflu/nskg/-/issues/549>

import sys, os.path, string 
from glob import glob
import numpy as np
import f90nml as nml

effective_wall_surface = 0.     # TODO If I had to add +0.5 (effective rock surface position) I would not have been puzzled, and explained this somewhere in the manual.
file_tag =    nml.read('input')['system']['syst']['file_tag']
wallx = float(nml.read('input')['rock_boundaries']['rb']['width'])+effective_wall_surface
boxx  = float(nml.read('input')['system']['syst']['box'][0])
parts = nml.read('md_particles.nml')['md_particle']
pot   = nml.read('potential.cfg')['potential_lj']
cutoff,epsilon,sigma = pot['cutoff'],pot['epsilon'],pot['sigma']
partx = np.array([I['pos'][0] for I in parts])
partR = np.array([I['R_orth'] for I in parts])
part_is_surface = np.array([(I['distances']=="surfaces") for I in parts])
x_invert = partx>0.5*boxx
wall_particle_centre_distance = (partx*(1-2*x_invert)+x_invert*boxx)-wallx
wall_particle_surface_distance = wall_particle_centre_distance - partR*part_is_surface

def force(dist):
    force_formula = 48.*epsilon*(sigma/dist)**6*((sigma/dist)**6-0.5)/dist
    if (dist<cutoff):
        return force_formula
    else:
        return 0.

particle_forces_theo = np.array([force(F) for F in wall_particle_surface_distance])

#### TODO ↓ Re-enable point particles here ↓
####force_sim_point = np.loadtxt('point_particles_'+file_tag+'_force.dat', ndmin=2)[0, 1:]
force_sim_ladd  = np.loadtxt ('ladd_particles_'+file_tag+'_force.dat', ndmin=2)[0, 1:]
####force_sim = np.array(force_sim_point.tolist() + force_sim_ladd.tolist())
force_sim = np.array(force_sim_ladd.tolist())
N = (np.shape(force_sim)[0]//3)     # number of (point, ladd) particles

# Divide x and lateral components, and test for the absence of lateral forces
force_sim_x = np.zeros(N)
force_sim_lateral = np.copy(force_sim)
for i in np.arange(N):
    force_sim_lateral[3*i]=0.
    force_sim_x[i] = force_sim[3*i]*(1-2*x_invert[i])
if not all(np.isclose(force_sim_lateral, 0, atol=1e-14)):
    print("Lateral forces detected in face-on MD-rocks interactions")
    print(force_sim_lateral)
    print("Failure 🤦")
    exit(1)

fail = False
print("# particle surface_distance test_status force_simul force_formula")
for I, Ft in enumerate(particle_forces_theo):
    Fs = force_sim_x[I]
    print(I, wall_particle_surface_distance[I], end=' ')
    #"""
    # A loose rule: forces must match by being zero/nonzero, and their sign
    if (np.isclose(Fs, 0)):
        if (np.isclose(Ft, 0)):
            print("All fine, forces are both zero", end=' ')
        else:
            print("Zero simulation force, but finite expected.", end=' ')
            fail = True
    else:
        if (np.isclose(Ft, 0)):
            print("Finite simulation force, but zero expected.", end=' ')
            fail = True
        else:
            if (Fs*Ft<0):
                print("Wrong force sign", end=' ')
                fail = True
            else:
                print("All fine, forces have the same sign", end=' ')
    #"""
    """
    # A strict rule: forces must match quantitatively
    if not np.isclose(Ft, Fs, rtol=1e-4):
        print("Force from simulation deviates from analytic force for particle", I+1, "at", parts[I]['pos'])
        print("measured force:", Fs)
        print("expected force:", Ft)
        if np.nonzero(Ft):
            print("force mismatch factor:", Fs/Ft)
        print("distance:", wall_particle_surface_distance[I])
        print("interaction range:", cutoff)
        fail = True
    """
    print(Fs, Ft)

if fail:
    print("Particle-wall distances:")
    print(wall_particle_surface_distance)
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
