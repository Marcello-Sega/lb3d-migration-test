#!/usr/bin/env python3
import f90nml
inp = f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
log = open(inp['system']['syst']['logfile'],'r').readlines()
cdims =    inp['system']['syst']['cdims']
if not ((cdims[0]==1) and (cdims[1]==1) and (cdims[2]==2)):
    print("Error: this needs to be run with a specific domain decomposition")
    exit(1)
#
# Force between a particle and a flat rock wall (loaded from file)
import sys, os.path, string 
from glob import glob
import numpy as np
import f90nml as nml

parts = nml.read('pos.cfg')['md_particle']
pot   = nml.read('potential.cfg')['potential_lj']
cutoff,epsilon, sigma  = pot['cutoff'],pot['epsilon'],pot['sigma']

def potential(dist):
    return 4*epsilon*((sigma/dist)**12-(sigma/dist)**6) 

def force(dist):
    return 48*epsilon*(sigma/dist)**6*((sigma/dist)**6-0.5)/dist



wall = inp['system']['syst']['box'][1] - 0.5
wall2 = 0.5
p1,p2,p3,p4 = parts[0],parts[1],parts[2],parts[3]
p1['pos'] = np.asarray(p1['pos'])
p2['pos'] = np.asarray(p2['pos'])
p3['pos'] = np.asarray(p3['pos'])
p4['pos'] = np.asarray(p4['pos'])
if not all (p1['pos'][:2] == p2['pos'][:2]):
    print('This test assumes the x and y coordinates of both particle to be the same')
    exit(1)
if not (np.mod(p1['pos'][2] - p2['pos'][2],1)==0):
    print('This test assumes the z coordinates of the particles to be separated by a multiple of a LB lattice unit')
    exit(1)

boxZ= inp['system']['syst']['box'][2]
if not all ([p1['pos'][2] < boxZ/2, p1['pos'][2]>0, p2['pos'][2]>boxZ/2, p2['pos'][2]<boxZ, p4['pos'][2]>boxZ/2]):
    print('This test assumes the z coordinates of the particles to be in different parallelization domains')
    exit(1)

dist  = p1['pos'][1] - wall    # no abs() here to make sure we adhere to our own force direction convention
force = force(dist)
pot   = potential(dist)

forces = np.genfromtxt(sorted(glob('ladd_particles_*_force.dat'))[-1])
force_sim  = forces[1:4]  # particle 1: hits only 1 voxel -> we can compute the force analytically
force_sim2 = forces[4:7]  # particle 2: like particle 1 
force_sim3 = forces[7:10] # particle 3: hits many voxels -> we just check that it's the same as particle 4
force_sim4 = forces[10:13]# partcile 4: see particle 3.

# While we do not know a priori (we could do it algorithmically) how many particle/wall voxels particles 3 and 4 
# have, their role here is to let us check that a particle that has a center assigned to one rank gets the correct
# contributions from the interactions with voxels on the other rank. To make sure this is the case, the 1st non-halo 
# voxel should be within the interaction range. Here we try to make sure that p4, which belongs to rank 1, extends 
# its interaction range into rk 0 Since this depends also on the position along x relative to the nodes, we require 
# a bit of buffer in this test.
x,y,z = p4['pos'][0:3]
y=y-wall2
rk1node = boxZ/2 - 0.5 - 2.0 # -2.0 makes sure (with some buffer) that it's in rk0 *and* not in the halo of rk1 
z=z-rk1node
if y**2 + z**2 >= (p4['r_orth']+cutoff)**2:
    print(y,z)
    print('This test assumes the fourth particle to interact with LB boundaris in the other parallelization domain. Apparently some parameters have been changed disregaring this requirement.')
    exit(1)


if not ( all(np.isclose(force_sim, force_sim2)) and  all(np.isclose(force_sim3, force_sim4))) :
    print('The forces on the particles are different (should be exactly the same)')
    exit(1)
if not all(np.isclose(force_sim[[0,2]],[0,0])):
    print('The forces on the particles along x and z must be zero')
    exit(1)

if np.isclose(force_sim[1],0):
    print("Force from simulation zero. Something is probably wrong with particle placement.")
    print("distance :",dist)
    print("Failure 🤦")
    exit(1)

if not np.isclose(force, force_sim[1],rtol=1e-4):
    print("Force from simulation deviates from analytic force")
    print("simulation force:",force_sim)
    print("analytical force:",force)
    print("distance :",dist)
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
