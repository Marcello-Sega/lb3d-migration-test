#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Interaction of point particles with static rocks
#
# A point particle is placed next to a flat rock (created via
# `RB%add_walls`), with which it interacts via the LJ potential. The
# distance is chosen so that a number of rock nodes (9 to be precise)
# exerts a force on the MD particle, and this force is compared with
# quantitative prediction.
#
# @TODO: Note that the interaction with rock walls is handled by the
# fluid force (Euler/LB lattice nodes) at the moment.
# Since the reduction of f_fluid is not implemented for point particles,
# this test fails in parallel.
# One way out, that does not require communication, is to restrict the
# interaction range to the LB halo.
#
# Resolve https://git.iek.fz-juelich.de/compflu/nskg/-/issues/645 and
# remove this comment once a solution is found.
# @ENDTODO

import numpy  as np
import f90nml
from glob import glob
import h5py

def lj_force(x):
    d=np.sqrt(np.sum(x**2,axis=0))
    f = 48.*epsilon*(sigma/d)**6*((sigma/d)**6-0.5)*x/d**2
    f[:,d>cutoff]=0.0
    return np.sum(f,axis=1)

mdinp   = f90nml.read('md.nml')
potinp  = f90nml.read('potential.cfg')
R       = mdinp['md_particle']['R_orth']
pos     = mdinp['md_particle']['pos'][0]
cutoff  = potinp['potential_lj']['cutoff']
epsilon = potinp['potential_lj']['epsilon']
sigma   = potinp['potential_lj']['sigma']

Failure = False

file_tags = ["np1", "np2"]  # Perform multiple simulation runs with different numbers of CPUs
#file_tags = ["np1"]  # FIXME As of 2023-05, we have weakened this test, by disabling the multi-core stage. See #645 and the comment at the top of this file.
for file_tag in file_tags:
    force = np.loadtxt('point_particles_'+file_tag+'_force.dat')[1:]

    mg = np.mgrid[1:2,1:9,1:9]-0.5
    r = np.asarray([4.,4.5,4.5]).reshape(3,1,1,1) - mg
    r = r.reshape(3,-1)
    dist_ = np.sqrt(r[0,:]**2+r[1,:]**2+r[2,:]**2)
    dist = dist_[dist_<3.8]

    if not np.all(np.isclose(lj_force(r),force)):
        print('In case '+file_tag+', forces differed from prediction')
        Failure = True
        print('Measured:', force)
        print('Expected:', lj_force(r))

if Failure:
    print("Failure 🤦")
    exit(1)

print('Success 🙂')
