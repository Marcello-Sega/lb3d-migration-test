#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#

import numpy as np
import h5py
from glob import glob
import re
import f90nml

inp    = f90nml.read('input')
pinp   = f90nml.read('init.cfg')
kBT    = inp['system']['syst']['kBT']
nsteps = inp['system']['syst']['n_iteration']
m_dens = pinp['md_particle']['density']
radius = pinp['md_particle']['R_orth']
mass   = m_dens * 4./3. * np.pi * radius**3 
np.seterr(all='raise')

# Check that m<v²> = kBT

# Note that we flatten the array, so we are effectively computing the contribution
# of *one* degree of freedom (anskgit with 3 times the statistics). This is why 
# the prefactor is kBT and not 3kBT.

v = np.loadtxt('./point_particles_default_velocity.dat')[:,[1,2,3]].flatten()
vmean, vstd = np.mean(v), np.std(v)

if (not np.isclose(mass * vstd**2, kBT, rtol=1e-1, atol=0)):
        print('Wrong kinetic energy ', mass * vstd**2, 'vs', kBT)
        print("Failure 🤦")
        raise ValueError()

# (JH7r 2023-01-10) I measured the exponential decay of velocity autocorrelation, the time constant is around 50±10. We use this as a typical time scale between samples, to decide when are sufficiently decorrelated to be counted independently.
corrtime = 50.
vsigma = np.abs(vmean/vstd)
vsigma_tolerance = 1./np.sqrt(nsteps/corrtime)  # What we still consider an acceptable mean velocity. The measured value converges towards zero like the inverse square root of the number of time steps (counting statistics), with a pre-factor due to correlation of the time series.
vsigma_frac = vsigma/vsigma_tolerance   # This is the fraction of how much the current simulation run eats up of the theoretical variance budget.

if (vsigma_frac > 1.):
        print('Expectation value of mean velocity ', vmean, 'exceeds estimation by a factor', vsigma_frac, '. Either the MD particles move wrongly, or you have exceptional bad luck with your random seed.')
        print("Failure 🤦")
        raise ValueError()

print("Success 🙂")
