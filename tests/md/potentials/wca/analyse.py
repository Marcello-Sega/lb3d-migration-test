#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# We test the WCA potential and force acting on point particles
# The particles are fixed
# The resulting force is compared to the analytic expression
# Much code shared with the LJ potential ../lj/analyse.py

import sys, os.path, string
from glob import glob
import numpy as np
import f90nml as nml

file_tag = nml.read("input")["system"]["syst"]["file_tag"]
epsilon = float(nml.read("potential.cfg")["potential_wca"]["epsilon"])
sigma = float(nml.read("potential.cfg")["potential_wca"]["sigma"])
cutoff = 2.0**(1.0/6.0)*sigma   # WCA currently has its internal cutoff apart from the generic `cutoff` property

def force(dist):
    if dist <= cutoff:
        return 48.*epsilon*(sigma/dist)**6*((sigma/dist)**6-0.5)/dist
    print("Analytical potential evaluated outside cut-off. Test probably unreliable")
    print("Failure 🤦")
    exit(1)

particles_nml = nml.read('pos.cfg')['md_particle']
N_pairs = len(particles_nml)//2
particles_pos_orig = np.array([particles_nml[i]['pos'] for i in np.arange(2*N_pairs)])
# Distances between pairs (odd-indexed minus even-indexed columns)
dists  = np.linalg.norm((particles_pos_orig[1::2] - particles_pos_orig[::2]), axis=1)
# Analytical force
forces_anal = np.asarray([force(dist) for dist in dists])

# Now, finally, gather pairs of simulation points and compare forces with analytical value
zcols=tuple(3*(np.arange(2*N_pairs, dtype=int)+1))  # 0-based indexes of z components
force_history = np.loadtxt(sorted(glob('point_particles_'+file_tag+'_force.dat'))[-1], usecols=zcols)[-1]    # Only loads the z components of the forces. This array now has, for each pair, lower particles at the even column indexes (repulsive forces pointing down), and upper particles at odd indexes (repulsive forces pointing up)
for I, lower in enumerate(2*np.arange(N_pairs, dtype=int)):
    upper=lower+1
    forces_sim_lower = -force_history[lower]	# sign change due to inverted direction
    forces_sim_upper =  force_history[upper]
    if not (all(np.isclose([forces_sim_lower, forces_sim_upper], forces_anal[I]))):
        print("Force from simulation deviates from analytic force (particles %d and/or %d)"%(lower+1,upper+1))	# index shift to 1-based
        print("simulation:", forces_sim_lower, forces_sim_upper)
        print("analytical:", forces_anal[I])
        print("distances :", dists)
        print("Failure 🤦")
        exit(1)

print("Success 🙂")
