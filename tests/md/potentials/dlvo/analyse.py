#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# We test the dlvo potential energy and force acting on a particle of radius R=2 moving at constant
# velocity 0.05 away from a second particle with velocity 0. The force and potential as functions
# of distance are compared to the corresponding analytical functions.
# The particle starts at a distance of 3.8 and moves up to a distance of 5.8 over 40 time steps.
# Because the forces in the first time step are outputted before compute_forces and are thus zero,
# we start comparing with analytics at distance 3.85
# Note that nofluiddynamics is active and amass_r is 0 in order for the friction force to be zero
#
# TODO: Currently rewrite does not output potential energies, so the parts of this test that checks
# the potential energy is removed.
#
# TODO: Currently the "nskg.input" debug output in rewrite does not output namelists that were not
# part of "input". Hence we have to hard-code default parameters of the potential setup.
#
# TODO: This is a very much stripped-down test. It just sets up a particle pair close to its
# potential minimum, and checks that this point is stable, without checking forces or energies
# at all. Potential parameters and particle positions are fine-tuned to have the minimum at a
# place that is convenient to analyse in a test simulation, rather than with physically sensible
# values in mind.
# This test should be extended in the future™, to test all aspects of the DLVO interaction.

from glob import glob
import numpy as np
from sys import exit

file_tag = f90nml.read("input")["system"]["syst"]["file_tag"]
r = f90nml.read("md_particles.nml")["md_particle"][0]["R_orth"]
ham  =         f90nml.read("potential.nml")["potential_dlvo"]["a_ham"]
zeta = 5.     #f90nml.read("potential.nml")["potential_dlvo"]["zeta_large"]
deb  =         f90nml.read("potential.nml")["potential_dlvo"]["debye_dlvo"]
eps  =         f90nml.read("potential.nml")["potential_dlvo"]["eps_dlvo"]
k_hertz = 1.  #f90nml.read("potential.nml")["potential_dlvo"]["k_hertz"]
dconst = 1.01 #f90nml.read("potential.nml")["potential_dlvo"]["d_const"]

# TODO: The DLVO potential (implemented in Python) used to be part of this test. See 5654ae057191d345c18714f4e998e18108275cc2 to forward-port it in case you need it.
dists_sim = np.loadtxt("ladd_particles_"+file_tag+"_position.dat", usecols=(1,4))[1:]
#forces_sim = np.loadtxt("ladd_particles_"+file_tag+"_force.dat", usecols=(1,4))[1:]
dists_sim = dists_sim[:, 1] - dists_sim[:, 0]

# Simple test: particle, placed at the potential minimum, should stay there
N_late =  10
surface_dists_late = dists_sim[-N_late:] - 2*r
expected_minimum = 0.7277834    # extracted from a test simulation
late_distance_mean   = np.mean(surface_dists_late)
late_distance_stddev = np.std( surface_dists_late)

if not np.isclose(late_distance_mean, expected_minimum, rtol=1e-6):
    print("Particle position does not approach to expected potential minimum", late_distance_mean, expected_minimum)
    print("Failure 🤦")
    exit(1)
if not np.isclose(late_distance_stddev, 0., atol=2e-6):
    print("Variance of particle position does not converge", late_distance_stddev)
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
