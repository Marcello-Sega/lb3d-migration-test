#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Here the Lubrication correction is tested. Setup: three pairs of Ladd particles, with one static and one mobile particle. Measure the force on the static particle (We are only checking the force that the resting particle feels due to the mobile particle moving towards/away from it. Moving particles have drag forces, which we don't want to compensate here.).
from glob import glob
import numpy as np
import f90nml as nml

file_tag = nml.read('input')['system']['syst']['file_tag']
cutoff         = 2./3   # No cut-off provided by "./potential.cfg", using default one
tau = np.array(nml.read('input')['relax_homogeneous']['relax']['homogeneous']['tau'])
rho = np.array(
    [nml.read('input')['lb_initialization']['lbinit']['fr'], nml.read('input')['lb_initialization']['lbinit']['fb']])
nu = (tau - 0.5) / 3


def force(d, v):
    if d > cutoff:
        print("Attempt to evaluate lubrication force outside cut-off. Check your input coordinates.")
        print("Failure 🤦")
        exit(1)
    elif d < 0:
        print("Attempt to evaluate lubrication force at negative distance. Check your input coordinates.")
        print("Failure 🤦")
        exit(1)

    return (6 * np.pi * np.sum(rho * nu) * (R1 * R2) ** 2 / (R1 + R2) ** 2) * (1 / d - 1 / cutoff) * v


parts = nml.read('pos.cfg')['md_particle']
R1 = parts[0]['R_orth']
R2 = parts[1]['R_orth']  # silently assuming that each pair of particles has the radii R1, R2

# Let's analyze the pairs of particles that are supposed to be within cutoff distance
dists = np.asarray([abs(parts[i]['pos'][2] - parts[i+1]['pos'][2])-R1-R2     for i in 2*np.arange(0, len(parts) // 2)])    # subtracting the sum of radii due to `distances="surfaces"`
vels  = np.asarray([   (parts[i+1]['velocity'][2] - parts[i]['velocity'][2]) for i in 2*np.arange(0, len(parts) // 2)])
forces = np.asarray([force(d, v) for d, v in zip(dists, vels)])

forces_sim = np.genfromtxt('ladd_particles_'+file_tag+'_force.dat'   )[-1, 3::6]

# Check force direction relative to the velocity vector. This detects sign errors in the formulae. The force will always try to move a particle into the direction of the other, i. e. the momentum transfer (force) will have the same sign as the relative velocity (in lab frame). `np.multiply` is poor man's scalar product (when the quantities are already scalars, but I want to vectorise).
if np.any(np.multiply(forces    , vels) <= 0.):
    print("Analytical forces not parallel with velocities. Check analyse.py formula.")
    print("Failure 🤦")
if np.any(np.multiply(forces_sim, vels) <= 0.):
    print("Simulation forces not parallel with velocities. Check Fortran formula.")
    print("Failure 🤦")

if not all(np.isclose(forces, forces_sim)):
    print("Force from simulation deviates from analytic force ")
    print("simulation:", forces_sim)
    print("analytical:", forces)
    print("distances :", dists)
    print("relative velocity:", vels)
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
