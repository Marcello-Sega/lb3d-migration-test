#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# Force between a particle and a flat rock wall (loaded from file)
import sys, os.path, string 
from glob import glob
import numpy as np
import f90nml as nml


parts = nml.read('pos.cfg')['md_particle']
pot   = nml.read('potential.cfg')['potential_hertz']
cutoff,K  = pot['cutoff'],pot['K']

def potential(dist):
    if dist > cutoff : return 0
    return K * (cutoff-dist)**2.5

def force(dist):
    if dist > cutoff : return 0
    return -2.5*K*(cutoff-dist)**1.5

wall = 0.5 # from the 'SLIT_Y' rock initialisation with RB%width=1
if not np.all(np.isclose(parts['pos'],[8.5,5.4,15.5])):
    print("Error, the particle is not in the expected position")
    # the position is chosen so that with a cutoff of 1 only one rock node
    # are in the interaction range
    exit(1)

dist  = parts['pos'][1]-wall
force = -force(dist)    # the interaction partner of the particle is to the left (negative x direction), so the minus sign here ensures that the force is antiparallel to the connection vector, in accordance to our force sign convention.
pot   = potential(dist)

force_sim = np.genfromtxt(sorted(glob('ladd_particles_default_force.dat'))[-1])[2]
if not np.isclose(force, force_sim,rtol=1e-4):
    print("Force from simulation deviates from analytic force ")
    print("simulation force:",force_sim)
    print(np.genfromtxt('ladd_particles_default_force.dat')[2])
    print("analytical force:",force)
    print("distance :",dist)
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
