#!/usr/bin/env python3
# Hertz potential test
# NOTE that the particles in this test are configured to let forces act on their
# surface (MD namelist parameter `distances="surfaces"`).
from glob import glob
import numpy as np
import f90nml as nml
parts = nml.read('pos.cfg')['md_particle']
r1= parts[0]['R_orth']
r2= parts[1]['R_orth']
pot = nml.read('potential.cfg')['potential_hertz']
K,cutoff = pot['K'] , pot['cutoff']

def force(r1,r2,r):
    return 0 if r > cutoff + r1 + r2 else -2.5 * K * (cutoff+r1 + r2 - r) ** 1.5

def potential(r1,r2,r):
    return 0 if r > cutoff + r1 + r2 else K * (cutoff + r1 + r2 - r) ** 2.5


dist = parts[1]['pos'][2] - parts[0]['pos'][2]
forces_sim = np.genfromtxt('ladd_particles_out_force.dat')[3]
if not np.isclose(force(r1,r2,dist), forces_sim):
    print("Force from simulation deviates from analytic force ")
    print("simulation:", forces_sim)
    print("analytical:", force(r1,r2,dist))
    print("distance  :", dist, 'surface distance:', dist -r1 -r2)
    print("cutoff    :", cutoff)
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
