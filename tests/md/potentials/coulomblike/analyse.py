#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# We test the coulomblike potential on a pair of point particles, apart 0.5, with repulsive force

import numpy as np
import f90nml as nml

# read input data
parts  = nml.read('particle_init.cfg')['md_particle']
pot  = nml.read('potential.cfg')['potential_coulomblike']

k    = pot['strength'] 
rcut = pot['cutoff']
F    = np.loadtxt('point_particles_default_force.dat')[3]
r    = np.abs(parts[1]['pos'][2] - parts[0]['pos'][2])
F_an = -k*(1/r**2 - 1/rcut**2)
if not np.isclose(F,F_an):
    print("Wrong force",F,"expected:",F_an)
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
