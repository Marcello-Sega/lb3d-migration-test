#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# The linear potential (constant force).
#
# This is a non-trivial test setup that creates a a core-shell interaction as a compound interaction (via interaction superposition, using two `types`), composed of two instances of linear potentials.
# The inner force gently pushes particles apart from each other (positive `strength`). Leaving this region, a particle feels an attractive force, that will pull it towards the other again. The particle distance will hence stabilise at the exact cut-off value of the core.

import sys, os.path, string
from glob import glob
import numpy as np
import f90nml as nml

file_tag = nml.read("input")["system"]["syst"]["file_tag"]

# extract properties of the potential
interactions = nml.read("input")["interactions"]["inter"]["potential_file"]
potentials_nml = nml.read(interactions)
shell_ranges    = []
shell_strengths = []
for O in potentials_nml["potential_linear"]:
    shell_ranges    += [O["cutoff"]]
    shell_strengths += [O["strength"]]
shell_ranges    = np.array(shell_ranges)
shell_strengths = np.sort(np.array(shell_strengths))
# extract properties of particles
particle_radii_sum = 0.
particle_nml = nml.read(nml.read("input")["md_input"]["md"]["geometry_file"])
for O in particle_nml["md_particle"]:
    particle_radii_sum += float(O["R_orth"])

particle_pos   =  np.loadtxt("ladd_particles_"+file_tag+"_position.dat", usecols=(4, 1)) # both the mobile and the static particle
particle_force = -np.loadtxt("ladd_particles_"+file_tag+"_force.dat",    usecols=(4))    # The minus sign accounts for the fact that the interaction partner (fixed particle) is at +x direction relative to the particle under observation
particle_pos_late   = particle_pos[  -100:, 0]
particle_force_late = particle_force[-100:]
static_pos = particle_pos[0, 1]

# Check that particle position is correct and does not change much
pos_expected  = static_pos - particle_radii_sum - np.min(shell_ranges)
pos_found_avg = np.mean(particle_pos_late)
if not np.isclose(pos_found_avg, pos_expected, atol=1e-3):
    print("Particle has not the correct place: x position off by", pos_found_avg-pos_expected)
    print("Failure 🤦")
    exit(1)
pos_found_stddev = np.std(particle_pos_late)
if not np.isclose(pos_found_stddev, 0., atol=1e-3):
    print("Particle is not close to steady state: x position stddev", pos_found_stddev)
    print("Failure 🤦")
    exit(1)

# Check that forces are present (zero on average, right maximum values)
force_mean = np.mean(particle_force_late)
if not np.isclose(force_mean, 0., atol=1e-2):
    print("Particle is not close to steady state: finite x force", force_mean)
    print("Failure 🤦")
    exit(1)
force_stddev = np.std(particle_force_late)  # This is expected to be (and stay) large
if np.isclose(force_stddev, 0., atol=0.2):
    print("Particle does not feel substantial forces.")
    print("Failure 🤦")
    exit(1)
force_minmax = [np.min(particle_force_late), np.max(particle_force_late)]   # We expect these to be roughly around the values of the potential; we have net forces, not just the ones from the potential, so some deviation is in order. The difference in potential height is chosen to detect sign errors, though. Just to be sure™.
if not np.all(np.isclose(shell_strengths, force_minmax, rtol=0.3)):
    print("Particle forces not in line with potential strengths.")
    print("Force min", np.min(particle_force_late), "vs potential", shell_strengths[0])
    print("Force max", np.max(particle_force_late), "vs potential", shell_strengths[1])
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
