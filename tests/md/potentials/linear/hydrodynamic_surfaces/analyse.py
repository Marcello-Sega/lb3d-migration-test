#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Testing the radius extension (aka "hydrodynamic_surface") for Ladd particles (using the linear potential)
#
# The code offers the option to increase particle radii (from the perspective of interaction potentials). We test this by placing Ladd particles inside and outside the extended interaction radii, and see if forces are applied or not.

import sys, os.path, string
from glob import glob
import numpy as np
import f90nml as nml

file_tag = nml.read("input")["system"]["syst"]["file_tag"]

# extract properties of the potential
interactions = nml.read("input")["interactions"]["inter"]["potential_file"]
potential = nml.read(interactions)["potential_linear"]
linear_range    = potential["cutoff"]
linear_strength = potential["strength"]
particle_nml = nml.read(nml.read("input")["md_input"]["md"]["geometry_file"])
particle_radii = []
for O in particle_nml["md_particle"]:
    particle_radii += [O["R_orth"]]
if not (np.isclose(particle_radii[1]-particle_radii[2], 0.0)):
    print("Particles 2 and 3 don't have the same radius. Aborting")
    print("Failure 🤦")
    exit(1)
hydrodynamic_surface_distance = particle_radii[0]+particle_radii[1] + 1.0

particle_pos_in    =  np.loadtxt("ladd_particles_"+file_tag+"_position.dat", usecols=(1, 4, 7))
particle_forces    = -np.loadtxt("ladd_particles_"+file_tag+"_force.dat",    usecols=(   4, 7))
particle_distances = np.array([particle_pos_in[1]-particle_pos_in[0], particle_pos_in[2]-particle_pos_in[0]])
particle_distances_inside_extended_cutoff = (np.abs(particle_distances) < hydrodynamic_surface_distance + linear_range)
if (not particle_distances_inside_extended_cutoff[0]) or particle_distances_inside_extended_cutoff[1]:
    print("Particle 2 and/or 3 not inside/outside interaction radius like expected (position)")
    print("Failure 🤦")
    exit(1)

finite_force = ~np.isclose(np.abs(particle_forces), 0.0)
if np.any(np.logical_xor(particle_distances_inside_extended_cutoff, finite_force)):
    print("Particle 2 and/or 3 not inside/outside interaction radius like expected (simulation force)")
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
