#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Here the van der Waals force is tested. The resulting potential and force are compared to the analytic expression.
# The VdW forces are surface based, hence we are testing them with Ladd particles, with `distances=surfaces"`

import sys, os.path, string
from glob import glob
import numpy as np
import f90nml as nml

file_tag = nml.read("input")["system"]["syst"]["file_tag"]
particles_nml = nml.read('pos.cfg')['md_particle']
R1 = particles_nml[0]["R_orth"]
R2 = particles_nml[1]["R_orth"]
cutoff = nml.read('potential.cfg')['potential_vdw']['cutoff']
AH = nml.read('potential.cfg')['potential_vdw']['AH']  # Hamaker coefficient


# z is the centre-to-centre distance in the force formulas. Both the simulation and the analysis should respect that.
def force(z):
    return - (AH / 6) * (
            (64 * R1 ** 3 * R2 ** 3 * z) / ((z ** 2 - (R1 + R2) ** 2) ** 2 * (z ** 2 - (R1 - R2) ** 2) ** 2))


def potential(z):
    x1 = z ** 2 - (R1 + R2) ** 2
    x2 = z ** 2 - (R1 - R2) ** 2
    return - (AH / 6) * (2 * R1 * R2 / x1 + 2 * R1 * R2 / x2 + np.log(x1 / x2))

N_pairs = len(particles_nml)//2
particles_pos_orig = np.array([particles_nml[i]['pos'] for i in np.arange(2*N_pairs)])
# Distances between pairs (odd-indexed minus even-indexed columns)
dists  = np.linalg.norm((particles_pos_orig[1::2] - particles_pos_orig[::2]), axis=1)
# Analytical force
forces_anal = np.asarray([force(dist) for dist in dists])

failed = False

# Test if vectors are in z direction only
force_vectors = np.reshape(np.loadtxt(sorted(glob('ladd_particles_'+file_tag+'_force.dat'))[-1])[-1, 1:(N_pairs*6+1)], (2*N_pairs, 3))
for I, V in enumerate(force_vectors):
    if not (np.all(np.isclose(V[0:2], 0.))):
        print("Force vector", V, "(particle", I+1, ") is not parallel to interaction axis.")
        print("Failure 🤦")
        failed = True

# Now, finally, gather pairs of simulation points and compare forces with analytical value
force_z = force_vectors[:, 2]
for I, lower in enumerate(2*np.arange(N_pairs, dtype=int)):
    upper=lower+1
    force_sim_lower = -force_z[lower]	# sign change due to inverted direction
    force_sim_upper =  force_z[upper]
    for J, force_sim_tested in enumerate([force_sim_lower, force_sim_upper]):
        if not (np.isclose(force_sim_tested, forces_anal[I])):
            print("Force from simulation deviates from analytic force (particle %d)"%(I+J+1))	# index shift to 1-based
            print("simulation:", force_sim_tested, force_sim_tested/forces_anal[I])
            print("analytical:", forces_anal[I])
            print("distances :", dists)
            failed = True

if failed:
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
