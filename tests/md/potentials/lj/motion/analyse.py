#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# Test that two particles interact properly after crossing the boundary between two processors
# Note 1: the point particle moving includes the friction force, one needs then to check the other one
# Note 2: the force was calculated the step before, we need a shift of one timestep wrt to the positions
from glob import glob
import numpy as np
import f90nml as nml


epsilon = nml.read('potential.cfg')['potential_lj']['epsilon']
sigma   = nml.read('potential.cfg')['potential_lj']['sigma']
cutoff  = nml.read('potential.cfg')['potential_lj']['cutoff']

def force_th(dist):
    f = 48*epsilon*(sigma/dist)**6*((sigma/dist)**6-0.5)/np.abs(dist)
    f[np.where(np.abs(dist)>cutoff)]=0.0
    return f

parts = np.loadtxt('point_particles_default_position.dat')[:-1,:]
force = np.loadtxt('point_particles_default_force.dat')[1:,6]
dists = parts[:,6] - parts[:,3]

if all(np.isclose(force, 0)):
        print("Force always zero, something is wrong")
        exit(1)

if not all(np.isclose(force, force_th(dists))):
        print("Force from simulation deviates from analytic force ")
        print("Step Distance     simulation    analytical  rel_difference")
        analytical = force_th(dists)
        for i in range(len(force)): print(i,dists[i],force[i],analytical[i], abs(force[i]/analytical[i]-1) )
        print("Failure 🤦")
        exit(1)

print("Success 🙂")
