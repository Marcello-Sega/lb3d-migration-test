#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# We test the lj potential and force acting on a Ladd particle of radius R=3 
# The particles are fixed, and separated with a distance dist=10
# The resulting potential and force are compared to the analytic expression

import sys, os.path, string 
from glob import glob
import numpy as np
import f90nml as nml

pot =  nml.read('potential.cfg')['potential_lj']
sigma  = pot['sigma']
epsilon= pot['epsilon']
cutoff = pot['cutoff']

def potential(dist):
    if abs(dist)>cutoff : return 0
    return 4*epsilon*((sigma/dist)**12-(sigma/dist)**6) 

def force(dist):
    if abs(dist)>cutoff : return 0
    return -48*epsilon*(sigma/dist)**6*((sigma/dist)**6-0.5)/dist

parts  = nml.read('pos.cfg')['md_particle']
#let's analyze the pairs of particles that are supposed to be within cutoff distance
#careful not to get the sign wrong here
dist  = np.abs(parts[0]['pos'][2] -  parts[1]['pos'][2]) 
f     = force(dist)
pot   = potential(dist)

forces_sim = np.genfromtxt(sorted(glob('ladd_particles_*_force.dat'))[-1])[3]
if not np.isclose(f, forces_sim):
    print("Force from simulation deviates from analytic force ")
    print("simulation:",forces_sim)
    print(np.genfromtxt('ladd_particles_out_force.dat')[-1][6::6])
    print("analytical:",f)
    print("distances :",dist)
    print("LJ sigma: ",sigma)
    print("LJ eps : ",epsilon)
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
