#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# We test the capillary force acting on a pair of particles with radius R=8. The position of the particles are fixed. The force are compared to the corresponding analytical functions.
# Note that nofluiddynamics is active and amass_r is 0 in order for the friction force to be zero
#
# NOTE: We are only testing the Rabinovich flavour [@bib:rabinovich2005capillary] of capillary force here.
# The user can alternatively choose the Willett capillary force [@bib:willett2000capillary], for which the test would look identical, up to replacing the `interaction="Rabinovich"` by `interaction="Willett"` (and adapting the analytical formula).

import sys, os.path, string, glob
import numpy as np

potfile = f90nml.read("input")["interactions"]["inter"]["potential_file"]
potdef = f90nml.read(potfile)
radius, contact_angle, volume_fraction, sigma_surf   = [ potdef['potential_capillary'][s] for s in ['radius', 'contact_angle', 'volume_fraction', 'sigma_surf' ]]

# Capillary force model calculation
def force_rabinovich(dist):
    pi=np.pi
    delta=contact_angle*pi/180
    V = volume_fraction*4.0*pi*radius*radius*radius/3.0
    rsq = np.abs(dist)-2.0*radius
    d=0.5*rsq*(-1.0+np.sqrt((1.0+2.0*V/(pi*radius*rsq**2.0))))
    F_r=-2.0*pi*radius*sigma_surf*np.cos(delta)/(1.0+rsq/(2.0*d))
    return F_r


data_location = np.genfromtxt('point_particles_rabinovich_position.dat',usecols=[1,2,3,4,5,6]).reshape(-1,2,3)[-1]
dx = data_location[1]-data_location[0]
dist = np.linalg.norm(dx)
data_force = np.genfromtxt('point_particles_rabinovich_force.dat',usecols=[1,2,3,4,5,6]).reshape(-1,2,3)[-1]
f = -data_force[0].dot(dx)/np.linalg.norm(dx) # This computes the magnitude but also the direction (sign) of the force. A positive scalar product between force and the unit vector towards the interaction partner means that the force is attractive. The explicit minus sign then changes forces into our convention of negative value = attractive force.
if not np.isclose(force_rabinovich(dist),f) : 
    print('Force does not match analytical value')
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
