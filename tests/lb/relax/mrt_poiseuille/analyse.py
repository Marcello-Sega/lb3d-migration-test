#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#
# Parabolic flow using MRT
# --------------------------------------------------
# compare poiseuille flow against analytic solution
# using sum of squared errors of the velocity profile
#
# We impose a parabolic flow along z in channel 
# confined by two parallel walls at y=0 and y=ny and
# compare the velocity profile with the analytical solution
# The MRT parameters are chosen such that we can recover the BGK model
# omega = 1/tau = 1, omegabulk = 1/taubulk = 1.19
# s03 = 1.4, s05 = 1.2, s11 = 1.4, and s17=1.98 
#

import sys, glob, h5py
import numpy as np

nml = f90nml.read("input") 
v = 1./6.
f = nml['extforce_constant']['acc'][2]
rho = 1. 
tol = 1e-6

# this glob should only hit one file
with h5py.File(sorted(glob.glob('velz_default_*h5'))[-1],'r') as hf:
    # one slice is enough, cut out walls
    u = hf['OutArray'][1,1:-1,1]

# width of the channel
nz = len(u)

# distance from center to walls
H = nz/2.

# centered y coordinates
y = np.arange(nz) - H + 0.5

# hagen poiseuille flow
u_a = (f*H**2)/(2*rho*v)*(1-(y/H)**2)

e = sum((u-u_a)**2)

if e >= tol:
    print("analyse failed with an sse for u of %2.2e" % e)
    print("Failure 🤦")
    sys.exit(1)

print("sse for u %2.2e" % e)
print("success 🙂")
