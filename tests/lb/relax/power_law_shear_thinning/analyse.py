#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Power law fluid with n=0.5 (shear-thinning fluid):
# --------------------------------------------------
# We impose a parabolic flow along z in channel 
# confined by two parallel walls at y=0 and y=ny and
# compare the velocity profile with the analytical solution
# TODO:
# To obtain the right peak velocity and profile, two parameters
# need to be adjusted: the consistency index (k) through tau0
# and the body force.
#

from glob import glob
import numpy as np
import  h5py, sys, os
#
def velocity_profile(y,f,W,n,k):
    """
    - arguments:
    y: sampling points of y positions
    f: body force along a given direction
    W: channel width
    n: flow index
    k: force scale (?)
    - return value:
    uan:  analytical velocity profile of a power-law fluid between two parallel walls
    umax: midplane velocity (peak velocity)
    """
    c = 1.0+(1.0/n)
    b = 1/n
    w  = W/2.0
    umax = 1.0/c * (f/k)**b * w**c 
    uan  = umax * ( 1.0 - (abs(y)/w)**c )
    return uan, umax
def body_force(u0,W,k,n):
    """
    - arguments:
    u0: desired maximum velocity
    W: channel width
    k: flow consistency (for n=1, k= dyna_visc)
    n: flow index
    - return value:
    fb:  body force along a given direction
    """
    c = 1.0+(1.0/n)
    w  = W/2.0
    fb = (c * u0 / (w**c * k**(-1.0/n)))**n
    return fb

files   = sorted(glob('velz*h5'))
Ly      = f90nml.read("input")['system']['syst']['box'][1]
border  = f90nml.read("input")['rock_boundaries']['rb']['width']
tau0    = f90nml.read("input")['relax_power_law']['relax']['power_law']['tau']
flowidx = f90nml.read("input")['relax_power_law']['relax']['power_law']['flow_index']

ygrid = np.arange(0, Ly)
y_center = 0.5*(Ly-border)  # to be subtracted from `ygrid` to change from LB grid to central symmetry coordinates
cs2 = 1/3.
rho=1.
nu0=cs2*(tau0-0.5)
k=nu0
u0=0.05
W=Ly-2*border
fb=body_force(u0,W,k,flowidx)
flow_from_theory, umax = velocity_profile(ygrid-y_center, fb, W, flowidx, k)

flow_from_simulation = h5py.File(files[-1], 'r')['OutArray'][0,:,0][:]

rock_mask = np.ones_like(flow_from_simulation, dtype=bool)
rock_mask[ :border] = False
rock_mask[-border:] = False
absolute_difference = flow_from_theory - flow_from_simulation 
relative_difference = np.abs(np.divide(absolute_difference, flow_from_theory))

# Analysis target 1: the shape of simulation should match theory (quotient should be flat)
shape_difference = np.divide(flow_from_theory[rock_mask], flow_from_simulation[rock_mask])
shape_scale = np.mean(shape_difference)
shape_deviations = np.abs(shape_difference/shape_scale-1.)
shape_noise = np.std(shape_deviations)
if (not (np.all(np.isclose(shape_deviations, 0, atol=2e-2)))) or (not (np.isclose(shape_noise, 0, atol=3e-3))):
    print("🤦 Shape deviations of flow profile too large: maximum deviation %.1e, shape noise %.1e"%(np.max(shape_deviations), shape_noise))
    exit(1)

# Analysis target 2: maximum flow should be somewhere around what we expect from theory
maxflow_from_simulation = flow_from_simulation[Ly//2]
maxflow_from_theory     = flow_from_theory    [Ly//2]
if not (np.isclose(maxflow_from_simulation/maxflow_from_theory, 1., rtol=1e-2)):
    print("🤦 Maximum flow velocity of flow profile deviates too much")
    exit(1)


print("Success 🙂")
