#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# test ternary fluid then compare the density at one lattice with expected value

import sys, glob, h5py
import numpy as np

t= 1000
rhosur = 0.385771 
tol = 1e-6

# this glob should only hit one file
for fn in glob.glob('sur_out_t%08d.h5' % t):
    with h5py.File(fn, 'r') as hf:
        # one slice is enough, cut out walls
        u = hf['OutArray'][10,1,1]

e = (u-rhosur)
if e >= tol:
    print("analyse failed with an sse for u of %2.2e" % e)
    print("Failure 🤦")
    sys.exit(1)

print("sse for  u %2.2e" % e)
print("success 🙂")
