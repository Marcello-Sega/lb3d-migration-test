#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Testing Kolmogorov flow (body force, spatially modulated with a transversal sinusoidal profile)

import sys, os.path, string, glob, h5py
import numpy as np

file_tag = f90nml.read("input")["system"]["syst"]["file_tag"]
L = np.array(f90nml.read("input")["system"]["syst"]["box"], dtype=int)
acc = np.array(f90nml.read("input")["extforce_kolmogorov"]["acc"], dtype=float)

l_velx = sorted(glob.glob("velx_"+file_tag+"_t00*h5"))
l_vely = sorted(glob.glob("vely_"+file_tag+"_t00*h5"))
l_velz = sorted(glob.glob("velz_"+file_tag+"_t00*h5"))

def avec(fx, fy, fz):
    # velocity vector field from components, lateral average ± stddev
    v = np.zeros(tuple(L.tolist()+[3]))
    v[:, :, :, 0] = h5py.File(fx, 'r')["OutArray"]
    v[:, :, :, 1] = h5py.File(fy, 'r')["OutArray"]
    v[:, :, :, 2] = h5py.File(fz, 'r')["OutArray"]
    return (np.mean(v, axis=(0,2)), np.std(v, axis=(0,2)))

# 1D-ified data from last snapshot
vmean,vstd = avec(l_velx[-1], l_vely[-1], l_velz[-1])

max_vel_in_sinus_direction = np.max(np.abs(vmean[:, 1]))
if max_vel_in_sinus_direction > 1e-15:
    print("Maximum flow speed perpendicular to Kolmogorov flow direction", max_vel_in_sinus_direction, "is too high")
    print('Failure 🤦')
    sys.exit(1)

# Some simple statistics. The standard deviation in slices perpendicular to the modulation is zero up to numerical noise, so we will use this as a criterion to test if we really have only flow in these planes, but not across them.
max_stddev_perp = np.max(vstd)
if max_stddev_perp > 5e-19:
    print("Detected unexpected numerical noise perpendicular to modulation direction", max_stddev_perp)
    print('Failure 🤦')
    sys.exit(1)

# Sinusoid test
max_vel_ypos = np.argmax(vmean[:, 0]*np.sign(acc[0]))   # Using component '0' here, need to include information about sign of the original force, since A·sin(x+π) is indistinguishable from -A·sin(x)
max_vel      = vmean[max_vel_ypos]
for I, V in enumerate(vmean):
    residual = np.linalg.norm(V-max_vel*np.sin(2*np.pi*(I+0.5)/L[1]))
    if (residual > 1e-15):
        print("Substantial deviation", residual, "from Kolmogorov sinusoid at position", I)
        print('Failure 🤦')
        sys.exit(2)

print("Success 🙂")
