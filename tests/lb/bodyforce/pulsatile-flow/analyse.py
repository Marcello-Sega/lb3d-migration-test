#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
# This is a simple test for a Womersley flow in a straight pipe
# The pipe is loaded from a rock file.
# We are imposing a pulsatile body force and measuring the 
# velocity profile along the cross-section. 
# The results are compared to the analytical profile.
# Note that for scientific production (higher accuracy), the system needs to be larger.

import sys, os.path, string, glob, argparse, re, h5py
import numpy as np
import scipy.special as sf
from scipy.special import jv
#
def womersley_profile(u,rcoord,time,fb,den,freq,wom,phi):
    """
    analytical velocity profile of a pulsatile flow in a straight pipe
    Arguments
    rcoord: radial coord array
    time:   time array
    fb:     body force
    den:    density
    freq:   frequency
    wom:    Womersley number 
    phi:    phase shift  
    return the velocity profile (u) along the pipe cross-section
    """
    for i in range(0,np.size(rcoord)):
        for j in range(0,np.size(time)):    
            A = jv(0, ((1.0/np.sqrt(2)) * (-wom + 1j*wom) * rcoord[i]))
            B = jv(0, ((1.0/np.sqrt(2)) * (-wom + 1j*wom)))
            u[i,j]  = np.real( (fb/(1j*freq*den)) * (1-A/B) * np.exp(1j*(freq*time[j]+phi)) )
# Numerical parameters
R, L          = 20, 80
tnx, tny, tnz = int(2*R), int(2*R), L
tau, rho, cs  = 1.0, 1.0, 1/np.sqrt(3)
nu            = cs**2 * (tau - 0.5)    
fz, wom       = 1e-5, 10.0
omega         = (nu/R**2) * wom**2 
period        = 2*np.pi/omega
print('freq=',omega,'period=',period,'Womersley number=',wom,'body force=',fz)
fn = glob.glob('velz_*.h5'); fn.sort()
nt = int(np.size(fn)) 
time = np.zeros(nt, dtype=float)
vz = np.zeros((int(R*2),nt),dtype=float)
cut = int(R)
for i in range(0,nt):
    with h5py.File(fn[i], 'r') as hf:
        vz[:,i] = np.swapaxes(np.array(hf['OutArray'][0:tnz,0:tny,0:tnx]),0,2)[::1,cut,cut]
    time[i] = np.array(re.findall("\d{8}", fn[i])[0])
time = np.sort(time) - np.sort(time)[0]
rcoord = np.linspace(-1,1,np.size(vz[:,0]))
nrc    = len(rcoord)
cutr = nrc//2
u    = np.zeros((nrc,nt),dtype=float)
womersley_profile(u,rcoord,time,fz,rho,omega,wom,0)
# Phase shift because of the difference in the initial condition
# between analytics and numerics
dt=[ (time/period)[np.argmax(a[cut,:])]  for a in [u, vz]]
phi = -2.*np.pi*(dt[1]-dt[0])
womersley_profile(u,rcoord,time,fz,rho,omega,wom,phi)
# Mean squared error
mse = 1/len(vz[cutr,:]) * np.sum((vz[cutr,:]-u[cutr,:])**2)
if (mse < 1e-9):
    print("Success 🙂") 
else:    
    print('Failure 🤦')         
    sys.exit(1)
