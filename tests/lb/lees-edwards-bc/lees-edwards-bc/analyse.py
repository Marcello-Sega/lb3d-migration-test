#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# test shear flow with Lees-Edwards boundary conditions
# gradient of z-velocity in x direction, at xmin and xmax w = shear_u
# compare the velocity profile with analytical solution

import sys, glob, h5py
import numpy as np

t= 1000
shear_u = 0.005
tol = 1e-6

# one slice is enough, get full domain in x as there are no walls
w = h5py.File(sorted(glob.glob('velz_out_t*1000.h5'))[0],'r')['OutArray'][1,1,:]
# size of the system
nx = len(w)
# x coordinates
x = np.arange(nx) 
# liner shear flow (analytical solution)
w_a = shear_u*((x)/(nx-1.)-(1.-(x)/(nx-1.)))
e = sum((w-w_a)**2)

if e >= tol:
    print("analyse failed with an sse for w of %2.2e" % e)
    print("Failure 🤦")
    sys.exit(1)

print("sse for  w %2.2e" % e)
print("success 🙂")
