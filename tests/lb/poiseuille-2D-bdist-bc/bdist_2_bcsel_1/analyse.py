#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#  compare poiseuille flow against analytic solution
#  using sum of squared errors of the velocity profile
#

import sys, glob, h5py
import numpy as np

t= 10000
v = 1./6.
f = 1e-4 
rh = 1. 
tol = 1e-6

# this glob should only hit one file
for fn in glob.glob('velz_out_t%08d.h5' % t ):
    with h5py.File(fn, 'r') as hf:
        # one slice is enough, cut out walls
        u = hf['OutArray'][1,1:-1,1]

# width of the channel
nz = len(u)

# distance from center to walls
H = nz/2.

# centered y coordinates
y = np.arange(nz) - H + 0.5

# hagen poiseuille flow
u_a = (f*H**2)/(2*rh*v)*(1-(y/H)**2)

e = sum((u-u_a)**2)

if e >= tol:
    print("analyse failed with an sse for u of %2.2e" % e)
    print("Failure 🤦")
    sys.exit(1)

print("sse for  u %2.2e" % e)
print("success 🙂")
