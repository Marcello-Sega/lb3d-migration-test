#!/usr/bin/env python3

import h5py
import numpy as np
R=3
for R in [1, 3 , 7, 15, 31, 63, 127, 255, 511, 1023 ]:
    nx=2*R+2
    rocks = np.zeros([nx,nx,2])
    vx=np.arange(0,nx)
    vz=np.arange(0,2)
    x,y,z = np.meshgrid(vx,vx,vz,indexing='ij')
    rocks[np.sqrt((x-nx//2+0.5)**2+(y-nx//2+0.5)**2)>=R] = 1.0
    rocks =  np.swapaxes(rocks, 0, 2)
    with h5py.File('cylinder.'+str(R)+'.h5','w') as f:
        f['OutArray']=rocks

# use this bash line to generate the input files from 'input'
#for i in 1 3 7  17 31 63 128 256 500 1000; do n=$[ $i * 2 + 2 ] ; acc=`echo $i | awk '{print(0.1*4./6./($1)**2)}'`; sed "s/^n\([xy]\) =.*/n\1 = $n/" input | sed "s/g_accn.*/g_accn = $acc/" | sed "s/cylinder.1.h5/cylinder.$i.h5/" >| input.$i ; done 
