#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
# Poiseuille flow test, checking 'pois-buildup' (build-up of a paraboloid profile by prolonged flow), and 'pois-init' (initialisation with `lbinit%init_cond_name = "INIT_VEL_Z_POIS"`)
import sys, h5py
import numpy as np
from glob import glob

relative_tolerance=4e-2

res=[]
for fn in sorted(glob('velz_pois-*_t*.h5')):
    hf=h5py.File(fn, 'r') 
    u = np.swapaxes(hf['OutArray'][:,:,:],0,2)[:,:,0]
    try:
        with h5py.File(fn, 'r') as hf:
            attr = list(hf['OutArray'].attrs.keys())[0]
            acc = f90nml.read("input-buildup")["extforce_constant"]["acc"][2]
            # one slice is enough
            u = np.swapaxes(hf['OutArray'][:,:,:],0,2)[:,:,0]
            nx,ny = u.shape
            vx=np.arange(0,nx)
            R = nx//2-1
            eta = 1./6.
            umax = acc * R**2 / 4.0  / eta
            x,y = np.meshgrid(vx,vx,indexing='ij')
            rad = np.sqrt((x-nx//2+0.5)**2+(y-nx//2+0.5)**2)
            sol = umax * (1-rad**2/R**2)
            sol[rad >=R] = 0.0  # Mask the part that is covered by the rockfile
            error_all    = np.sqrt(np.sum((u-sol)**2)/np.sum((sol)**2))
            error_centre = np.max(u)/np.max(sol)-1.
            res += [(R, error_all, error_centre, fn)]
            if ((np.abs(error_centre) > relative_tolerance) or (np.abs(error_all) > relative_tolerance)):
                print("Failure 🤦")
                raise(ValueError("Not a Poiseuille profile in file", fn))
    except(ValueError):
        print("Failure 🤦")
        raise(ValueError("Wrong flow"))
    except:
        print("Failure 🤦")
        print('error opening or processing file',fn)
        sys.exit(1)

if len(res)<1:
    print("Failure 🤦")
    raise(ValueError("No tests conducted. Check your simulation setup"))

print()
print('Radius | CentreError | AllError | File' )
print('------ | ----------- | -------- | ----' )
for T in res:
        print(T[0], "| %.2f%%"%(100.*T[2]), "| %.2f%% |"%(100.*T[1]), T[3])
print('Success 🙂')
