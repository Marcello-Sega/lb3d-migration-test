#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Test for velocity boundary conditions
#
# The fluid is initialized with the z velocity `pr`, and the boundaries at `z=1` and `z=nz` keep
# the flow steady. Hence we expect the flow field to not change at all.

import sys, glob, h5py
import numpy as np

# Test fails if any field in any flow component at any time deviates more than that from the
# expected value (0 for x and y, `pr` for z)
tol = 1e-15

# prescribed velocity
pr = f90nml.read("input")['lb_initialization']['lbinit']['pr']

#get x velocity 
vel_x = []
data = sorted(glob.glob("velx_out_t0000*h5"))
for i in range(len(data)):
    v = h5py.File(data[i], "r")["OutArray"][:]
    vel_x.append(v)
vel_x = np.asarray(vel_x)

#get y velocity 
vel_y = []
data = sorted(glob.glob("vely_out_t0000*h5"))
for i in range(len(data)):
    v = h5py.File(data[i], "r")["OutArray"][:]
    vel_y.append(v)
vel_y = np.asarray(vel_y)

#get z velocity 
vel_z = []
data = sorted(glob.glob("velz_out_t0000*h5"))
for i in range(len(data)):
    v = h5py.File(data[i], "r")["OutArray"][:]
    vel_z.append(v)
vel_z = np.asarray(vel_z)

if np.any(np.abs(vel_x)>tol):
    print("Failure 🤦")
    raise Exception("Finite x velocity.")

if np.any(np.abs(vel_y)>tol):
    print("Failure 🤦")
    raise Exception("Finite y velocity.")

if np.any(np.abs(vel_z-pr)>tol):
    raise Exception("To quick changes in z velocity.")

print("success 🙂")
