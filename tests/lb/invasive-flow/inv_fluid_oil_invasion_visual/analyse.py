#!/usr/bin/env python3
#
# This script tests correctness of file I/O configuration.
#
# The main purpose of this directory is to serve as an example for visualisation.
# Call
#   ./visualization.py
# after the simulation has run, to have a nice illustration of invasive two-phase flow.
#

import sys, glob, h5py
import numpy as np
import f90nml

input_file = f90nml.read('input')
file_tag       =     input_file['system']['syst']['file_tag']
iterations     = int(input_file['system']['syst']['n_iteration'])
outdir         = '.' # input_files[1]['system']['syst']['folder']
dump_n_density = np.array(input_file['lattice_boltzmann']['LB']['dump_n_density'], dtype=int)

dumped_files = np.array((iterations * np.divide(1., dump_n_density))+1, dtype=int)

# First component: o[il]
od_files = glob.glob(outdir+'/od_'+file_tag+'_t*.h5')
if not (len(od_files)==dumped_files[0]):
    print("number of od files wrong: %d != %d"%(len(od_files), dumped_files[0]))
    print("Failure 🤦")
    raise ValueError()

# Second component: w[ater]
wd_files = glob.glob(outdir+'/wd_'+file_tag+'_t*.h5')
if not (len(wd_files)==dumped_files[1]):
    print("number of wd files wrong: %d != %d"%(len(wd_files), dumped_files[1]))
    print("Failure 🤦")
    raise ValueError()

print("number of dumped files correct: (%d, %d)"%(len(od_files), len(wd_files)))

print("success 🙂")
