#!/usr/bin/env python3
#
# This example visualizes in a video of a slice in the yz plane for oil (red) 
# invading a water (blue) filled channel. The flow is caused by an external forcing.
#

import sys, glob, h5py
import numpy as np
import pylab as pl


files = sorted(glob.glob('data/wd_out_*')) # get all water density output files
wd_list = [];

for fn in files:
    with h5py.File(fn, 'r') as hf:
        wd_list.append(np.tensordot(hf['OutArray'][:,1:-1,1],np.array([0,0,1]),axes=0)) # store water density multiplied by color blue (rgb array [0,0,1])

files = sorted(glob.glob('data/od_out_*')) # get all oil density output files
od_list = [];

for fn in files:
    with h5py.File(fn, 'r') as hf:
        od_list.append(np.tensordot(hf['OutArray'][:,1:-1,2],np.array([1,0,0]),axes=0)) # store oil density multiplied by color red (rgb array [1,0,0])

# show animation
img = None
for i in range(len(wd_list)):
    if img is None:
        img = pl.imshow(np.swapaxes(od_list[i]+wd_list[i],0,1))
        pl.xlabel('x')
        pl.ylabel('y')
    else:
        img.set_data(np.swapaxes(od_list[i]+wd_list[i],0,1))
    pl.pause(.1)
    pl.draw()
