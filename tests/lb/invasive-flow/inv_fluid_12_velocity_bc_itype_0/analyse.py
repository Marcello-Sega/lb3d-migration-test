#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# test for velocity boundary conditions
# constant velocity at z=1 and z = nz is set, the velocity fields at every time step are tested
#

import sys, glob, h5py
import numpy as np

tol = 1e-12

# prescribed velocity
pr = 0.001

#get x velocity 
vel_x = []
data = sorted(glob.glob("velx_out_t0000*h5"))
for i in range(len(data)):
    v = h5py.File(data[i], "r")["OutArray"][:]
    vel_x.append(v)
vel_x = np.asarray(vel_x)

#get y velocity 
vel_y = []
data = sorted(glob.glob("vely_out_t0000*h5"))
for i in range(len(data)):
    v = h5py.File(data[i], "r")["OutArray"][:]
    vel_y.append(v)
vel_y = np.asarray(vel_y)

#get z velocity 
vel_z = []
data = sorted(glob.glob("velz_out_t0000*h5"))
for i in range(len(data)):
    v = h5py.File(data[i], "r")["OutArray"][:]
    vel_z.append(v)
vel_z = np.asarray(vel_z)

if np.any(np.abs(vel_x)>tol):
    print("Failure 🤦")
    raise Exception("Too large x velocity.")

if np.any(np.abs(vel_y)>tol):
    print("Failure 🤦")
    raise Exception("Too large y velocity.")

if np.any(np.abs(vel_z-pr)>tol):
    print("Failure 🤦")
    raise Exception("To large deviation of z velocity.")

print("success 🙂")
