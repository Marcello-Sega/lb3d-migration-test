#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Test for velocity boundary conditions
#
# Three fluids are initialized with the z velocity `pr`, and the boundaries at `z=1` and `z=nz` keep
# the flow steady. Hence we expect the (sum of the) flow fields to not change at all.

import sys, glob, h5py
import numpy as np

# Test fails if any field in any flow component at any time deviates more than that from the
# expected value (0 for x and y, `pr+pb+pg` for z)
tol = 1e-6

# prescribed velocities
pr = f90nml.read("input")['lb_initialization']['lbinit']['pr']
pb = f90nml.read("input")['lb_initialization']['lbinit']['pb']
pg = f90nml.read("input")['lb_initialization']['lbinit']['pg']

#get x velocity 
vel_x = []
data = sorted(glob.glob("velx_out_t0000*h5"))
for i in range(len(data)):
    v = h5py.File(data[i], "r")["OutArray"][:]
    vel_x.append(v)
vel_x = np.asarray(vel_x)

#get y velocity 
vel_y = []
data = sorted(glob.glob("vely_out_t0000*h5"))
for i in range(len(data)):
    v = h5py.File(data[i], "r")["OutArray"][:]
    vel_y.append(v)
vel_y = np.asarray(vel_y)

#get z velocity 
vel_z = []
data = sorted(glob.glob("velz_out_t0000*h5"))
for i in range(len(data)):
    v = h5py.File(data[i], "r")["OutArray"][:]
    vel_z.append(v)
vel_z = np.asarray(vel_z)

#get density fields
od = []
data = sorted(glob.glob("od_out_t0000*h5"))
for i in range(len(data)):
    r = h5py.File(data[i],"r")["OutArray"][:]
    od.append(r)
od =  np.asarray(od[:])

wd = []
data = sorted(glob.glob("wd_out_t0000*h5"))
for i in range(len(data)):
    r = h5py.File(data[i],"r")["OutArray"][:]
    wd.append(r)
wd =  np.asarray(wd[:])

sd = []
data = sorted(glob.glob("sur_out_t0000*h5"))
for i in range(len(data)):
    r = h5py.File(data[i],"r")["OutArray"][:]
    sd.append(r)
sd =  np.asarray(sd[:])

if np.any(np.abs(vel_x)>tol):
    print("Failure 🤦")
    raise Exception("Too large x velocity.")

if np.any(np.abs(vel_y)>tol):
    print("Failure 🤦")
    raise Exception("Too large y velocity.")

if np.any(np.abs(vel_z-(od*pr+wd*pb+sd*pg)/(od+wd+sd))>tol):
    print("Failure 🤦")
    raise Exception("Too large deviation of z velocity.")

print("success 🙂")
