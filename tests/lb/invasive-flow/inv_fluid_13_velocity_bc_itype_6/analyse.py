#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# test moving a bottom fluid layer (z-coordinate) with a constant velocity, then 
# compare the velocity profile with analytical solution

import sys, os, glob, h5py
import numpy as np

# system size
nx=32;ny=32;nz=32;
# wall thickness
bw=0;
# wall velocity
uwall=0.01

field_x  = glob.glob(os.path.join('velx_*.h5'))
field_x.sort();

with h5py.File(field_x[-1], 'r') as hf:
   out_array = np.array(hf['OutArray'][:,:,:])
   out_array = np.swapaxes(out_array,0,2)
# numerical profile 
vx = out_array[int(nx/2.),int(ny/2.),bw:nz-bw]

# analytical profile of a shear flow with a single moving wall
gdot = 2*uwall/(nz)
ux    = gdot * np.linspace(1,-1,vx.shape[0])*vx.shape[0]/2

# error on the profile
e = sum((ux-vx)**2)

if e >= 1e-6:
    print("analyse failed with an sse for u of %2.2e" % e)
    sys.exit(1)

print("sse for v %2.2e" % e)
print("success :)")
