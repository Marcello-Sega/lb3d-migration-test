#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# test of mixed invasion. Initially, oil, water and surfactant densities are set to 0.3,0.3,0.4 everywhere. 
# A flow is forced along z-direction, while faces normal to x and y are rocks. At the z boundary sites, 
# particles are converted in the ratio given by pr, pb and pg. The system thus should converge to 
# a state where the densities correspond to these values.
#

import sys, glob, h5py
import numpy as np

t= 500
tol = 1e-2
pr = 0.15
pb = 0.55
pg = 0.3

# get density fields at one slice in the y-z plane
for fn in glob.glob('wd_out_t%08d.h5' % t ):
	with h5py.File(fn, 'r') as hf:
		wd_f = (hf['OutArray'][:,1:-1,7])

for fn in glob.glob('od_out_t%08d.h5' % t ):
	with h5py.File(fn, 'r') as hf:
		od_f = (hf['OutArray'][:,1:-1,7])
		
for fn in glob.glob('sur_out_t%08d.h5' % t ):
	with h5py.File(fn, 'r') as hf:
		sd_f = (hf['OutArray'][:,1:-1,7])

e = pg-np.mean(sd_f)+np.std(sd_f)+pb-np.mean(wd_f)+np.std(wd_f)+np.std(od_f)+pr-np.mean(od_f)


if e >= tol:
    print("analyse failed with a total error of %2.2e" % e)
    print("Failure 🤦")
    sys.exit(1)

print("success 🙂")
