#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# test of surfactant invasion. Initially, oil and water densities are set to 0.5 everywhere, 
# while surfactant density is zero everywhere. A flow is forced along z-direction, while 
# faces normal to x and y are rocks. At the z boundary sites, water and oil particles are 
# converted to surfactant particles, which therefore simulates the invasion of surfactant 
# in the water/oil-filled channel. The system thus should converge to a state where surfactant
# density is 1 everywhere while oil and water are absent.
#

import sys, glob, h5py
import numpy as np

t=500
tol = 1e-2

# get density fields at one slice in the y-z plane
for fn in glob.glob('wd_out_t%08d.h5' % t ):
	with h5py.File(fn, 'r') as hf:
		wd_f = (hf['OutArray'][:,1:-1,7])

for fn in glob.glob('od_out_t%08d.h5' % t ):
	with h5py.File(fn, 'r') as hf:
		od_f = (hf['OutArray'][:,1:-1,7])
		
for fn in glob.glob('sur_out_t%08d.h5' % t ):
	with h5py.File(fn, 'r') as hf:
		sd_f = (hf['OutArray'][:,1:-1,7])

e = 1-np.mean(sd_f)+np.std(sd_f)+np.mean(wd_f)+np.std(wd_f)+np.std(od_f)+np.mean(od_f)

if e >= tol:
    print("analyse failed with a total error of %2.2e" % e)
    print("Failure 🤦")
    sys.exit(1)

print("success 🙂")
