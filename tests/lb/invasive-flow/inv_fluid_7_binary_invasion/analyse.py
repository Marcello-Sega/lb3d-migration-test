#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# test of binary invasion, inv_fluid_name = 'INV_BINARY_INVASION_RED'. Initially, the domain is filled with oil (red) and water (blue),
# density=0.5 each. At z=0,1 all particles are turned red, while at z = nz,nz-1 all particles 
# are turned blue. This leads to a linear density gradient for both water and oil.
#

import sys, glob, h5py
import numpy as np

t= 500
tol = 1e-2

# get density fields at one slice in the y-z plane
for fn in glob.glob('wd_out_t%08d.h5' % t ):
	with h5py.File(fn, 'r') as hf:
		wd_f = (hf['OutArray'][1:-1,1:-1,7])

for fn in glob.glob('od_out_t%08d.h5' % t ):
	with h5py.File(fn, 'r') as hf:
		od_f = (hf['OutArray'][1:-1,1:-1,7]) #discard side walls and half of buffer region

L = len(wd_f)
z = np.linspace(0,L,L)
wd_a = z/L
od_a = 1.-z/L

e = sum((wd_a-np.average(wd_f,axis=1))**2.+(od_a-np.average(od_f,axis=1))**2)

if e >= tol:
    print("analyse failed with a total error of %2.2e" % e)
    print("Failure 🤦")
    sys.exit(1)

print("success 🙂")
