#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# test for pressure boundary conditions with 2 components
# a pressure driven flow in a infinite wide channel limited by two parallel plates is examined
# the expected linear pressure gradient (density gradient) is tested as well as the parabolic
# velocity profile along the channel height
#

import sys, glob, h5py
import numpy as np

inp=f90nml.read("input")

t = inp["system"]["syst"]["n_iteration"]
tol = 8e-5

box = inp["system"]["syst"]["box"]
h = box[0]-2*inp["rock_boundaries"]["rb"]["width"]  # wall distance
l = box[2]      # channel length


# pressure boundary values
fr = inp["lb_initialization"]["lbinit"]["f_inv_pressure"][0]
fb = inp["lb_initialization"]["lbinit"]["f_inv_pressure"][1]
pr = inp["lb_initialization"]["lbinit"]["p_inv_pressure"][0]
pb = inp["lb_initialization"]["lbinit"]["p_inv_pressure"][1]

# get density fields
for fn in glob.glob('od_out_t%08d.h5' % t ):
    with h5py.File(fn, 'r') as hf:
        od_f = (hf['OutArray'][:,:,1:-1])

for fn in glob.glob('wd_out_t%08d.h5' % t ):
    with h5py.File(fn, 'r') as hf:
        wd_f = (hf['OutArray'][:,:,1:-1])

#get z velocity at one slice in the x-y plane
for fn in glob.glob('velz_out_t%08d.h5' % t ):
    with h5py.File(fn, 'r') as hf:
        vel_z = (hf['OutArray'][20,:,1:-1])

drhodz = (pr+pb-fr-fb)/l # expected linear density gradient

# check pressure gradient (sign)
o_pressure_linear = np.mean(od_f,axis=(1,2))
w_pressure_linear = np.mean(wd_f,axis=(1,2))
o_pressure_gradient = np.diff(o_pressure_linear)
w_pressure_gradient = np.diff(w_pressure_linear)
o_density_drop = np.sum(o_pressure_gradient)
w_density_drop = np.sum(w_pressure_gradient)
if ((abs(o_density_drop-(pr-fr)) >= tol) or (abs(w_density_drop-(pb-fb)) >= tol)):
    print("Density drop has wrong direction:")
    print("fluid 1:", o_density_drop, "should be", pr-fr, "(off by factor", o_density_drop/(pr-fr), ")")
    print("fluid 2:", w_density_drop, "should be", pb-fb, "(off by factor", w_density_drop/(pb-fb), ")")
    print("Failure 🤦")
    sys.exit(1)

# check pressure gradient (values)
e = np.sum((drhodz*np.linspace(0,l,l)+fr+fb - np.mean(od_f+wd_f,axis=(1,2)))**2)

if e >= tol:
    print("too large errors in pressure gradient ({})".format(e))
    print("Failure 🤦")
    sys.exit(1)

# expected velocity profile
x = np.linspace(0.5,h-0.5,h)
u = -drhodz*x*(h-x)

# check velocity profile
e = np.sum((u-np.mean(vel_z,axis=0))**2)

if e >= tol:
    print("too large errors in velocity profile ({})".format(e))
    print("Failure 🤦")
    sys.exit(1)

print("success 🙂")
