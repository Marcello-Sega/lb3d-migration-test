#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# test of oil invasion. Initially, water density is set to 1 everywhere, while oil density  
# is zero everywhere. A flow is forced along z-direction, while faces normal to x and y
# are rocks. At the z boundary sites, water particles are converted to oil particles, which
# therefore simulates the invasion of oil in the water-filled channel. The system thus
# should converge to a state where oil density is 1 everywhere while water is absent.
#

import sys, glob, h5py
import numpy as np

tol = 1e-2

# get density fields at one slice in the y-z plane
wd = h5py.File(sorted(glob.glob('wd_out_*.h5'))[-1], 'r')['OutArray'][:,1:-1,7]
od = h5py.File(sorted(glob.glob('od_out_*.h5'))[-1], 'r')['OutArray'][:,1:-1,7]

e = np.mean(wd)+np.std(wd)+np.std(od)+1-np.mean(od)

if e >= tol:
    print("analyse failed with a total error of %2.2e" % e)
    print("Failure 🤦")
    sys.exit(1)

print("success 🙂")
