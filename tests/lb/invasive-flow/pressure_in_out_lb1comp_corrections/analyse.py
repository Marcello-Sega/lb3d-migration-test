#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# test for pressure boundary conditions (including diagonal corrections from [@bib:hecht-harting:2010])
# a pressure driven flow in a infinite wide channel limited by two parallel plates is examined
# the expected linear pressure gradient (density gradient) is tested as well as the parabolic
# velocity profile along the channel height
#

import sys, glob, h5py
import numpy as np

inp=f90nml.read("input")

t = inp["system"]["syst"]["n_iteration"]
tol = 5e-3

box = inp["system"]["syst"]["box"]
h = box[0]-2*inp["rock_boundaries"]["rb"]["width"]  # wall distance
l = box[2]      # channel length


# pressure boundary values
fr = inp["lb_initialization"]["lbinit"]["f_inv_pressure"]
pr = inp["lb_initialization"]["lbinit"]["p_inv_pressure"]

# get density field
for fn in glob.glob('od_out_t%08d.h5' % t ):
    with h5py.File(fn, 'r') as hf:
        od_f = (hf['OutArray'][:,:,1:-1])

#get z velocity at one slice in the x-y plane
for fn in glob.glob('velz_out_t%08d.h5' % t ):
    with h5py.File(fn, 'r') as hf:
        vel_z = (hf['OutArray'][20,:,1:-1])

drhodz = (pr-fr)/l # expected linear density gradient

# check pressure gradient
e = np.sum((drhodz*np.linspace(0,l,l)+fr - np.mean(od_f,axis=(1,2)))**2)

if e >= tol:
    print("too large errors in pressure gradient ({})".format(e))
    print("Failure 🤦")
    sys.exit(1)

# expected velocity profile
x = np.linspace(0.5,h-0.5,h)
u = -drhodz*x*(h-x)

# check velocity profile
e = np.sum((u-np.mean(vel_z,axis=0))**2)

if e >= tol:
    print("too large errors in velocity profile ({})".format(e))
    print("Failure 🤦")
    sys.exit(1)

print("success 🙂")
