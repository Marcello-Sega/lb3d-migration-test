#!/usr/bin/env python3
"""
Test to check whether the `INV_RECOLOR` boundary condition works with periodic boundary conditions and negative widths.
"""
import f90nml

f90nml.read("nskg.input")  # this tests that nskg.input has been written correctly

import sys, glob, h5py
import numpy as np

od = np.swapaxes(h5py.File(sorted(glob.glob('od_out_*.h5'))[-1], 'r')['OutArray'], 0, 2)
wd = np.swapaxes(h5py.File(sorted(glob.glob('wd_out_*.h5'))[-1], 'r')['OutArray'], 0, 2)

failure = False

if not (np.allclose(od[-2:-1], 1) and np.allclose(od[0:2], 1)):
    failure = True
    print("Incorrect oil densities in recoloring region.")

if not np.all(od[2:-2] < 0.99):
    failure = True
    print("Incorrect oil densities in buffer region.")

if not (np.allclose(wd[-2:-1], 0) and np.allclose(wd[0:2], 0)):
    failure = True
    print("Incorrect water densities in recoloring region.")

if not np.all(wd[2:-2] > 0.01):
    failure = True
    print("Incorrect water densities in buffer region.")

if failure:
    print("Failure 🤦")
    sys.exit(1)

print("success 🙂")
