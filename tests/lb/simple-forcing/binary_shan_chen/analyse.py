#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# The test checks the final average velocity against the analytical solution:
#
# v(t) = g*t
#

import numpy as np
import h5py
import glob
from sys import exit

np.seterr(all='raise')

# values from input-file
n_iteration = 100
fx = 0.001
fy = 0.002
fz = 0.003
fr = 0.4
fb = 0.25
#
# Check correct velocity output
#
# read h5 output
fields = {}
for k in ['velx_out','vely_out','velz_out']:
    for f in glob.glob(k+'*'+str(n_iteration)+'.h5'):
        with h5py.File(f, 'r') as h5f:
            fields[k] = np.swapaxes(h5f["OutArray"][:],0,2)

# get analytical solution
# -1.0 because the steps is one less than n_iteration

# compare solutions
eps = 10e-13 # Numerical fluctuations?

v_a = np.array([fx, fy, fz]) * (n_iteration*1.0-1.0+0.5)

# get simulated solution for oil
v_s = np.array([ np.mean(fields['velx_out']),
                 np.mean(fields['vely_out']),
                 np.mean(fields['velz_out'])])

if ( (np.abs(v_a-v_s) > eps ).any() ):
    print("oil velocity failed: |{} - {}| > {}".format(v_a,v_s,eps))
    print("Failure 🤦")
    exit(1)


#
#   Check density output and conservation
#
# read h5 output for densities
fields = {}
for k in ['od','wd']:
    for f in glob.glob(k+'*'+str(n_iteration)+'.h5'):
        with h5py.File(f, 'r') as h5f:
            fields[k] = np.swapaxes(h5f["OutArray"][:],0,2)

d_s = np.array([np.mean(fields['od']),np.mean(fields['wd'])])
d_a = np.array([fr,fb])
if ( (np.abs(d_a-d_s) > eps ).any() ):
    print("density failed: %e %e > %e" % (tuple(np.abs(d_a-d_s))+(eps,)))
    print("Failure 🤦")
    exit(1)

print("success 🙂")
