#!/usr/bin/env python3
#
#This is a simple test where we impose a simple shear flow along the x direction.
#Planar walls at z=1 and z=tnz
#We compare the average value of the profile of the xz-component of the deviatoric stress tensor
#with the analytical prediction: \sigma_xz = \mu \dot{\gamma}, where \mu is the fluid viscosity
# and \dot{\gamma} is the shear rate.
#The purpose is to test if the xz-component stress calculation is correct.
#Note that, with the aim of doing a quick test, the distant between the two moving walls is small
# and this strong confinement produces an error aroun 4% in the stress measurement.

import numpy as np
from numpy import loadtxt
from sys import exit

#> Theoretical prediction
u_wall = float([ a for a in open('./input').readlines() if 'pr ' in a][0].split()[-1])
H = int([ a for a in open('./input').readlines() if 'syst%box ' in a][0].split()[-1])
u_wall = abs(u_wall)
print(u_wall,H)
mu = 1./6.
dotGamma = 2.*u_wall/H
stress_theory = mu*dotGamma

#> Loading the last dumped xz stress profile data
stress_profile = loadtxt("stress_xz_profile_2000.dat")
stress = np.mean(stress_profile)

print('[Test]: An homogeneous fluid is subject to a simple shear flow.')
print('The profile of the Xz-component of the deviatoric stress is computed and dumped.\n')

print('Measured stress: ',stress,', with a theoretical value of ',stress_theory)
if not np.all(np.isclose(stress,stress_theory,rtol=4e-6, atol=4e-6)):
    print('Results are different from the reference case! Something is wrong!')
    print("Failure 🤦")
    exit(1)
else:
    print('\nSuccess 🙂')
