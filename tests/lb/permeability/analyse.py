#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input")  # this tests that nskg.input has been written correctly

"""
Test to check whether NSKG can reproduce the square duct permeability benchmark.

The prediction from several simulations is compared with the theoretical value.
"""

import h5py
import sys
import numpy as np
import numpy.ma as ma
from glob import glob


input_files = ['input-step0', 'input-step1', 'input-step3']  # 'input-step2',
failed = False

for file in input_files:
    inp = f90nml.read(file)
    box = inp["system"]["syst"]["box"]
    rb_width = inp["rock_boundaries"]["rb"]["width"]
    t_final = inp["system"]["syst"]["n_iteration"]
    tag = inp["system"]["syst"]["file_tag"]

    # Compute the pressure difference from the ext_force or INV_PRESSURE boundary condition
    if "extforce_constant" in inp:
        delta_p = inp["extforce_constant"]["acc"][2]
        forcing = "EXTERNAL FORCE"
    else:
        delta_p = (inp["lb_initialization"]["lbinit"]["f_inv_pressure"] - inp["lb_initialization"]["lbinit"]["p_inv_pressure"]) / (
                3 * (box[2] - 1))
        forcing = "PRESSURE DIFFERENCE"

    # Compute the relaxation time for the BGK and MRT cases
    if "relax_homogeneous" in inp:
        tau = inp["relax_homogeneous"]["relax"]["homogeneous"]["tau"]
        collision_operator = "BGK"
    else:
        tau = inp["relax_mrt_homogeneous"]["relax"]["mrt_homogeneous"]["omega"]
        collision_operator = "MRT"


    def permeability(t, delta_p):
        """Compute the permeability at time step t"""
        try:
            od = np.swapaxes(
                h5py.File(sorted(glob('od_' + tag + "_t*" + str(t) + '.h5'))[0], "r")["OutArray"][:], 0, 2)
            velz = np.swapaxes(
                h5py.File(sorted(glob('velz_' + tag + "_t*" + str(t) + '.h5'))[0], "r")["OutArray"][:], 0, 2)
        except IndexError:
            print("Files not found for simulation with input file {}!".format(file))
            return -1

        mask = np.invert(od != 0)
        od = ma.array(od, mask=mask)
        velz = ma.array(velz, mask=mask)

        visc = (1 / 3) * (tau - 0.5)
        rho = np.average(od)
        u = np.average(velz)

        return visc * rho * u / delta_p


    k_sim = permeability(t_final, delta_p)
    k_th = (box[0] - 2 * rb_width) * (box[1] - 2 * rb_width) * 0.140577 / 4

    print("Input file: {}".format(file))
    print("Fluid forcing: {}, Collision operator: {}".format(forcing, collision_operator))
    print("Theoretical permeability value: {:2f}, Simulation permeability value {:2f}".format(k_th, k_sim))
    print("Deviation: {:2f}%".format(np.abs(k_th - k_sim) * 100 / k_th))

    # The bound could be set somewhat looser, but we definitely want to be within the 1% range as described in the old
    # NSKG manual.
    if not np.isclose(k_th, k_sim, rtol=0.002):
        failed = True
        print("Simulation permeability is not close to the theoretical value!")
    print("\n")

if failed:
    print("Failure 🤦")
    sys.exit(1)

print("success 🙂")
