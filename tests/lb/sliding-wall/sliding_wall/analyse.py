#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# test moving a bottom wall (z-coordinate) with a constant velocity, then 
# compare the velocity profile with analytical solution

import sys, glob, h5py
import numpy as np

v = 0.01 
tol = 1e-6

# this glob should only hit one file
fn = sorted(glob.glob('velx_out_*.h5'))[-1]
with h5py.File(fn, 'r') as hf:
   # one slice is enough, cut out walls
   u = hf['OutArray'][1:15,1,1]
#  width of the channel
nz = len(u)
# z coordinates
z = np.arange(nz) 
#liner shear flow
u_a = np.arange(0.5,nz+0.5,1.0)[::-1] * v/(nz)

e = np.std(u-u_a)

if e >= tol:
    print("analyse failed with an sse for u of %2.2e" % e)
    print('Failure 🤦')
    sys.exit(1)

print("success 🙂")
