#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# This tests checks the mass-conservation of the bounce-back script for different interpolation ranges. All should be up
# to numerical precision. The test is a bit annoying since the HDF5-files itself introduce a small numerical error. Easily
# seen when double checking with exact output from fortran itself (note that sanity-check only by default does single
# precision real).
#

import numpy as np
import h5py
import glob
from scipy.stats import norm

kBT = 1e-5
N = 10
eps_mean_vel = 1e-7
tol_err_kbt = 0.7
np.seterr(all='raise')

def read_h5(filename):
    return np.swapaxes(h5py.File(filename, "r")["OutArray"][:], 0, 2)

# Read input-file parameters
files_velx = sorted(glob.glob("velx_*"))
files_vely = sorted(glob.glob("vely_*"))
files_velz = sorted(glob.glob("velz_*"))


eff_kbt = []
mean_vel = []

from scipy.stats import norm
# Anaylyse each outputted timestep individually
for fvx, fvy, fvz in zip(files_velx, files_vely, files_velz):
    vx = read_h5(fvx).ravel()
    vy = read_h5(fvy).ravel()
    vz = read_h5(fvz).ravel()

    # From kinetic gas theory we can get the temperature via the degrees of freedom and average kinetic energy of mean squared velocity
    vel = ( vx**2 + vy**2 + vz**2 )
    kBT_eff = np.mean(vel)/3.0

    # By fitting with normal distribution we can also get the temperature from the variance
    mu_x, sigma = norm.fit(vx)
    kBT_eff_x = sigma**2
    mu_y, sigma = norm.fit(vy)
    kBT_eff_y = sigma**2
    mu_z, sigma = norm.fit(vz)
    kBT_eff_z = sigma**2

    eff_kbt.append([kBT_eff, kBT_eff_x, kBT_eff_y, kBT_eff_z])
    mean_vel.append([mu_x, mu_y, mu_z])


mean_vel = np.array(mean_vel)
eff_kbt = np.array(eff_kbt)
mean_vel = np.mean(mean_vel,axis=0)
if any(abs(mean_vel) > eps_mean_vel):
    print("Failure 🤦")
    raise Exception("Mean velocity too large (should be close to zero)! "+str(mean_vel))

for i in range(4):
    if abs((np.mean(eff_kbt[N:,i])/kBT)-1.0) > tol_err_kbt:
        print("Failure 🤦")
        raise Exception("Error in effective kBT larger than tolerated error! %e > %e" % (np.mean(eff_kbt[N:,i])/kBT-1.0, tol_err_kbt) )
print('mean vel (should be close to zero): ', mean_vel)
print('effective kBT ', eff_kbt[-1,:], 'target: ',kBT)
print("Success 🙂")
