#!/usr/bin/env python3

# This file will be installed in the build directory of each performance
# test as analyse.py
# Alternatively, if within a git repo, this can be used as-is, e.g.,
# (cd lb/1core ; python3 ../../analyse.1core.in.py )
# 

import pandas as pd
import numpy as np

keys=['&shan_chen','&peng_robinson','&color_gradient','&advection_diffusion','&immersed_boundary']
perffile=open('./nskg.performance.log').readlines()
mlups=[float(line.split('=')[-1]) for line in perffile[-10:] if 'Mlups/core' in line][0]
try:
    DB = pd.read_csv('@CMAKE_BINARY_DIR@/performance/db/db.csv')
except:
    try:
        import os
        stream = os.popen('git worktree list  --porcelain | grep worktree')
        DB=pd.read_csv(stream.read().split()[-1]+'/performance/db/db.csv')
    except:
        print("Failure 🤦")
        raise(FileNotFoundError('db.csv not found'))

modelname = [ line.split(':')[-1].strip() for line in open('/proc/cpuinfo').readlines() if 'model name' in line ][0]


DB = DB[DB.processor.str.strip()==modelname]
if(len(DB)==0):
    print('Error: processor',modelname ,'not in database')
    print("Performance = ",str(mlups),"Mlups/core")
    print("Failure 🤦")
    exit(1)

selector=[ key in ''.join(perffile) for key in keys ]
select=(DB[keys] == selector).all(axis=1)
DB=DB[select]
target = np.max(DB['Mlups/core'].values)

# Some CPUs throttle their frequency, which distorts measurement. But there is a sysfs interface to compensate for this.
try:
    throttle = float(open('/sys/devices/system/cpu/intel_pstate/max_perf_pct').readline())/100.0
except:
    throttle = 1.0

print('throttle = ',throttle)

target = target * throttle

# 5% grace interval around the target Mlups performance
mlups_tolerance=0.05
print("PERFORMANCE_LEVEL: "+str(mlups)+" Mlups/core, target: "+str(target))
if mlups < target * (1-mlups_tolerance) :
    print("Failure 🤦")
    raise RuntimeError("Performance target missed by "+str(100.*(1.-mlups/target))+"%. If this is caused by busy queues, re-trigger this test.")

else:
    print("Performance = "+str(mlups)+" in line ( within "+str(100.*(1.-mlups/target))[:4]+"%) with target "+str(target), " Mlups/core")
    print("Success 🙂")
