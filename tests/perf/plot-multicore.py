#!/usr/bin/env python3
import matplotlib as mpl
mpl.use('Agg') #we might not have a DISPLAY
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from glob import glob
for ptype  in ['','core']:
    for precision in ["", "SP"]:
        ax = plt.axes()
        files=glob(precision+"RESULTS*")
        if len(files)>0:
            for f in files:
                lines=open(f,'r').readlines()
                name = ' '.join(lines[np.argwhere(["Model name" in l for l in lines])[0,0]].split(':')[-1].split())
                if (f == 'RESULTS' or f=='SPRESULTS'):
                    name = 'This run: '+name
                data = pd.read_csv(f,sep='\s+',comment="#",header=None, names=['N','lups','lupscore'])
                data.plot(ax=ax,x='N',y='lups'+ptype,label=name, style='o-')
            
            plt.xlabel('number of cores')
            if(ptype == 'core'):
                plt.ylabel('MLUPS/core')
            else:
                plt.ylabel('MLUPS')
            plt.savefig(precision+'mlups'+ptype+'.pdf')
            plt.savefig(precision+'mlups'+ptype+'.png')
            plt.close()
