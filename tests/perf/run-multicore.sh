#!/bin/bash
CPS=`lscpu  | grep "^Core.*per.*socket" | awk '{print $NF}'`
SKT=`lscpu  | grep "^Socket" | awk '{print $NF}'`
cores=$[ $CPS  * $SKT ]
NX=`grep "\<nx\> *=" input.template | awk -F '=' '{print $2}'`
case @NSKG_SINGLE_PRECISION@  in
    ON) prefix="SP" ;;
     *) prefix="" ;;
esac

lscpu  | sed 's/^/#/' >| ${prefix}RESULTS
@SLURM_SRUN_COMMAND@ -n 1 @CMAKE_BINARY_DIR@/nskg/src/nskg  2>/dev/null | grep -A4 "Starting NSKG" | sed 's/^/#/' >> ${prefix}RESULTS
echo "#size $NX x $NX x (ncores x $NX) "  >> ${prefix}RESULTS
echo "#cores Mlups Mlups/core" >> ${prefix}RESULTS
for N in `seq 1 $cores` ; do 
	M=$[ $NX * $N ] ;  
	sed -e "s/NNN/$N/" -e "s/MMM/$M/" input.template >| input.$N && 
	# this should fill the first socket and then go on
        @SLURM_SRUN_COMMAND@ -n $N --cpu-bind=verbose,rank --cpus-per-task=1 --ntasks-per-node=$N   @CMAKE_BINARY_DIR@/nskg/src/nskg -f input.$N && 
	echo $N `tail LOG_$N   | grep Mlups | awk '{print $3}' | xargs` >> ${prefix}RESULTS
done
