#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
import numpy as np
import h5py
from glob import glob

# numerical solution obtained independently, physical domain [0,32] + b.c. with 
# neutralizing charge. Diffusion coefficient 0.3, epsilon 2, initial homogeneous
# charge density 0.1. Converged after 5k iteratino of ftcs+lax-wendroff. Coulomb
# solved with fft. NSKG must use wall thickness 2. SOR tolerance 1e-3 is enough.

sol = np.fromstring('0.50070641, 0.28914811, 0.17979164, 0.12450933, 0.09294579,\
                     0.07313171, 0.05987322, 0.05059869, 0.04390444, 0.03896873,\
                     0.03528498, 0.03252865, 0.03048637, 0.02901609, 0.02802393,\
                     0.02745048, 0.02726284, 0.02745048, 0.02802393, 0.02901609,\
                     0.03048637, 0.03252865, 0.03528498, 0.03896873, 0.04390444,\
                     0.05059869, 0.05987322, 0.07313171, 0.09294579, 0.12450933,\
                     0.17979164, 0.28914811, 0.50070641', sep=',')

rho=h5py.File(sorted(glob('./elec_*rho_p*h5'))[-1],'r')['OutArray'][:,2,2]

if np.all(np.isclose(sol,rho[2:-2],atol=0.,rtol=1e-2)):
    print('Success 🙂')
else:
    print('Failure 🤦')
    print(np.isclose(sol,rho[2:-2],atol=0.,rtol=1e-2))
    print('got     :',rho[2:-2])
    print('expected:',sol)
    raise ValueError('Wrong charge density profile')
