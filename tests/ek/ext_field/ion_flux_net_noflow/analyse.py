#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
# We test a single component fluid with a homogeneous distribution of positive and negative ions in an
# external electric field in z direction. The fluid should not move.
 

import sys
import glob
import numpy as np
import h5py

files = sorted(glob.glob('./velz_output_*'))
velz = h5py.File(files[-1], 'r')["OutArray"][32,2,2]

if not np.isclose(velz,0.0):
    print('velz',velz)
    print('Failure 🤦')
    raise ValueError('Velocity caused to ion flux is not zero.')

print('Success 🙂')

