#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
# We test a single component fluid with a homogeneous distribution of positive charged ions in an
# external electric field in z direction. The ion flux causes a linearly increasing fluid velocity.
# The value of the fluid velocity is checked after 1000 time steps (-1 + 0.5 to fit to the LB scheme).
# 

import sys
import glob
import numpy as np
import h5py

nit = 100
Ez = 0.01
rhop = 0.1
rhom = 0.2

files = glob.glob('./velz_output_*')
files.sort()
h5f = h5py.File(files[-1], 'r')
velz = h5f["OutArray"][32,2,2]
# a positive field acting on positive charges results in a positive
# velocity of the charges (and likewise of the dragged fluid)
v_an = Ez*(rhop-rhom)*(nit-1+0.5)

if np.abs(velz - v_an) > 1.e-5 :
    print('Velocity induced by the ion flux does not match the analytical value.')
    print('velz',velz)
    print('v_an',v_an)
    print('Failure 🤦')
    sys.exit(1)

print('Success 🙂')

