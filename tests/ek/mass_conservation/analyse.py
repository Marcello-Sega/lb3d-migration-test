#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
""""
Test conservation of masses of fluids and the total charge 
with
 - rock walls (RB%add_walls='SLIT_Z') 
 - ElecOnFluidOnElec

homogenous, initial positiv charge = 0.1  (negative charge = 0.0)
"""

import numpy as np
import h5py
import glob

o_den = sorted(glob.glob("od*.h5"))
ch_den = sorted(glob.glob("*elec_rho_p*.h5"))



init = [np.sum(h5py.File(o_den[0], 'r')['OutArray'][:,:,:]), np.sum( h5py.File(ch_den[0], 'r')['OutArray'][:,:,:])]
end = [np.sum( h5py.File(o_den[-1], 'r')['OutArray'][:,:,:]), np.sum( h5py.File(ch_den[-1], 'r')['OutArray'][:,:,:])]


#check if quantities deviate from initial values by more than e-3 after 10 steps
if np.all(np.isclose(end, init ,atol=0.,rtol=1e-3)):
    print('Success 🙂')
else:
    print("inital mass, charge =", init)
    print("mass, charge after 10 iterations =", end)
    print('Failure 🤦')
    raise ValueError('mass or charge not conserved!')

