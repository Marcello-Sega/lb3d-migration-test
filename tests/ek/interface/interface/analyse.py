#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# We consider a planar interface between two fluids, with ions.
# We test the ratio of the ion concentration in each phase, which
# should be:
#
#   n^i_w / n^i_o = exp(-delta_mu_p*(rho^o_w - rho^o_o)).
#
# Where the subscripts denote the values at the bulk of the corresponding phase.

import sys
import glob
import numpy as np
import h5py

files = glob.glob('./elec_rho_m*')
files.sort()
h5f = h5py.File(files[-1], 'r')
rhom = np.swapaxes(h5f["OutArray"][:], 0, 2)

files = glob.glob('./od*')
files.sort()
h5f = h5py.File(files[-1], 'r')
od = np.swapaxes(h5f["OutArray"][:], 0, 2)

rhom = rhom[2,2,:]
od = od[2,2,:]

ratio = np.mean(rhom/rhom[0] / np.exp(2.0*(od-od[0])))

if  np.abs(od[15]/od[50]-1.) < 0.1 :
    print('Error, the oil did not separate')
    print('Failure 🤦')
    sys.exit(1)

error = np.abs(ratio - 1.)
if (error > 0.02):
    print('Error is more than 2%! {}'.format(error))
    print('Failure 🤦')
    sys.exit(1)

print('Success 🙂')

