#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# Consistency test for wetting of CG

#########################################
# this test does not check the physics  #
# only that the code reproduces the     #
# expected results.                     #
# Update with an analytical expectation #
# for the contact angle, if possible.   #
#########################################

import sys, glob, h5py
import numpy as np
from scipy.optimize import curve_fit

R_     =  30.924314197861833
C_     = -14.367936493242686
theta_ = 117.68538420898058 # expected theoretical value: 120.0

# this glob should only hit one file
den = sorted(glob.glob('od_out_t*.h5'))[-1]
rho =  h5py.File(den, 'r')['OutArray'][1,:,:][::,::-1].T
rho[rho<0.4]=-1
rho[rho>0.6]=-1
y,x = np.mgrid[0:rho.shape[0],0:rho.shape[1]]
# extract x&y coordinates of the rim, and shift them so that they are
# centered along the x axis, and start from 0 along the y one.
px,py = x[rho>0]-x.shape[1]//2,-y[rho>0]+x.shape[0]
# sort just for pretty plotting
sort = np.argsort(px)
px,py=px[sort],py[sort]

# the function to be fitted
def arc(x,R,C):
    return C + np.sqrt(R**2-x**2)

# initial estimate
c=(max(px)-min(px))/2.
d=max(py)-2
R=c**2/d/2. + d/2.
C=d-R

try:
    [R,C] , pcov = curve_fit(arc, px, py, p0=(R,C))
    theta = np.arccos(C/R)*180./np.pi
except:
    print("Failure 🤦")
    raise RuntimeError("Fitting procedure failed")
print(R,C,theta)
if not np.isclose(R,R_):
    print("Failure 🤦")
    raise RuntimeError("Wrong radius: not "+str(R_)+" but "+str(R))
if not np.isclose(C,C_):
    print("Failure 🤦")
    raise RuntimeError("Wrong center: not "+str(C_)+" but "+str(C))
if not np.isclose(theta,theta_):
    print("Failure 🤦")
    raise RuntimeError("Wrong contact angle: "+str(theta)+" expected: "+str(theta_))
else:
    print('Contact angle OK:',theta)

print("Success 🙂")

