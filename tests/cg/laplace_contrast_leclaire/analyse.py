#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# Simple Laplace test

import numpy as np
import h5py
import glob
import sys

np.seterr(all='raise')
error = False

def read_h5(filename):
    return np.swapaxes(h5py.File(filename, "r")["OutArray"][:], 0, 2)

rad = []
pdif = []
minval = 1e-7
# Get filenames of all files
fod= sorted(glob.glob("od_*h5" ))
fwd= sorted(glob.glob("wd_*h5" ))

od = read_h5(fod[-1])
wd = read_h5(fwd[-1])
mw = np.sum(read_h5(fwd[0])) , np.sum(wd)
mo = np.sum(read_h5(fod[0])) , np.sum(od)
mass = mw[0]+mo[0],mw[1]+mo[1]
dmass = np.abs(mass[1]/mass[0]-1)
testn=1
if dmass > 1e-12:
    print("Test", testn, " ERROR: total mass is not conserved, change=",dmass)
    error = True
else:
    print("Test",testn, " PASSED: total mass is conserved, change=",dmass)
testn += 1
dmass = np.abs(mw[1]/mw[0]- 1)
if dmass > 1e-12:
    print("Test",testn," ERROR: water mass is not conserved, change=",dmass)
    error = True
else:
    print("Test",testn, " PASSED: water mass is conserved, change=",dmass)

testn+=1
minw  = np.min (wd) 
if minw > minval:
    print("Test",testn,  "ERROR: wrong minimum water density, expected ",minval, "got", minw)
    error=True
else:
    print("Test", testn, " PASSED: water minimum density <",minval)


testn+=1
mino  = np.min (od) 
if mino > minval:
    print("Test",testn,  "ERROR: wrong minimum oil density, expected ",minval, "got", mino)
    error=True
else:
    print("Test", testn, " PASSED: oil minimum density <",minval)
testn+=1
surft = 0.1
rad = 10.
# the init files (thought for the case without density contrast) are not
# really correct, as they are not flat, but the max value will be used by the 
# algorithm to determine the values of alphas, so here we do the same

# Caveat for users: always start from step-like profiles to avoid these problems
rho_o = np.max(h5py.File('init_density_od_800.h5', "r")["OutArray"][:,:,:])
rho_w = np.max(h5py.File('init_density_wd_800.h5', "r")["OutArray"][:,:,:])

alpha_w = 1/3. # w(19), set to the component with minimum density (w, here)
alpha_o = 1. - (1-alpha_w)   * rho_w/rho_o

cs2_o = 0.5 * (1-alpha_o) # eq (11) 0.1103/PhysRevE.95.033306
cs2_w = 0.5 * (1-alpha_w)
pjump = (np.max (od) * cs2_o  -  np.max (wd) * cs2_w)
#pjump  = (np.max (od) -  np.max (wd))
if np.abs(pjump * rad / surft - 1 ) > 0.12:
    print("Test",testn,"ERROR: wrong Laplace pressure jump. Expected:", surft/rad, "got:",  (np.max (od) -  np.max (wd) ) /3.,'maxs',np.max (od) ,np.max (wd) )
    print("Test",testn,"ERROR: wrong Laplace pressure jump. Expected:", surft/rad, "got:", pjump, 'rad=',rad, 'surf tens=',surft,'cs2',cs2_o,cs2_w)
    error = True
else:
    print("Test",testn,"PASSED: Laplace pressure jump. Expected:", surft/rad, "got:", pjump)


if error:
    print("Error(s) occurred while comparing files:",fod[0],fod[-1],fwd[0],fwd[-1])
    print("Failure 🤦")
    sys.exit(1)
else:
    print("Success 🙂")

