#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Evaporating film, check position of interface after some time. Only the last 10%
# of the simulation is carried out, restoring from checkpoints for that purpose.

import h5py
import numpy as np
from scipy.optimize import curve_fit
import glob


# Get filenames of all files
od = np.array(h5py.File(sorted(glob.glob("od_film*h5"))[-1], 'r')["OutArray"][:])
wd = np.array(h5py.File(sorted(glob.glob("wd_film*h5"))[-1], 'r')["OutArray"][:])
colarr = od - wd

# System sizes
nz, ny, nx = colarr.shape
ns = nx * ny * nz


def get_interface(a):
    # Find the interface by looking for the flip in the prefactor of the
    # colour field and interpolate
    for hnoty in range (5,ny):
        if a[int(nx/2)][hnoty][int(nz/2)]<0:
            break
        hnot=(hnoty- \
             a[int(nx/2)][hnoty][int(nz/2)]/ \
             (a[int(nx/2)][hnoty][int(nz/2)] - \
                a[int(nx/2)][hnoty-1][int(nz/2)]))
    return hnot


sim_pos = get_interface(colarr.copy()) # Don't touch the original array
#print("interface position: ",sim_pos)

##calculate theoretical prediction (Ref: D. Hessling, Q. Xie and  J.
#Harting. Diffusion dominated evaporation in multicomponent lattice
#Boltzmann simulations, J. Chem. Phys. 146, 054111 (2017))
xh=128.0
x0=99.665054082
t0=100000.0
D=0.1184
grarho = 0.036-0.02
rhoma=0.7
drho=grarho/rhoma
d0=5.0
T0=100000
tmea=110000
theory_pos=(xh-((xh-x0)**2+2*D*drho*(tmea-t0))**0.5)

if abs(theory_pos-sim_pos)/theory_pos > 0.002:
    print("interface position of an evaporating film (%e) deviates more than %f%% from expected value %e" % (sim_pos, 0.002*100.0, theory_pos))
    print("Failure 🤦")
    exit(1)
else:
    print("Success 🙂")

