#!/usr/bin/env python3
#
# Check the contact angle for Shan-Chen 2 components,
# shan-chen forcing scheme and sukop wetting.
# Uses as reference the approximation
# proposed by Sukop et al.
#
# Huang, H., Thorne Jr, D. T., Schaap, M. G., & Sukop, M. C. (2007).
# Proposed approximation for contact angles in Shan-and-Chen-type
# multicomponent multiphase lattice Boltzmann models.
# Physical Review E, 76(6), 066701.

import sys, glob, h5py
import numpy as np
from scipy.optimize import curve_fit

rho_A = 1.021198921843392
rho_B = 0.018984502335334538
rock=4
# note that this formula is different from that of the
# shan-chen forcing because the two forcing scheme
# yield a different phase diagram. The shan-chen
# one is tau-dependent and with tau=1 the effectiv
# coupling parameter that remaps guo forcing to the 
# shan-chen one is 1/2 (for this reason also G_AB here
# is twice that one in the shan-chen forcing case.
# See for details: 
# Benzi, R., Sbragaglia, M., Succi, S., Bernaschi, M., & Chibbaro, S. (2009). 
# Mesoscopic lattice Boltzmann modeling of soft-glassy systems: theory and simulations. 
# J. Chem. Phys. 131(10), 104903.

cosThetaEstim = 0.0
thetaEstim = np.arccos(cosThetaEstim)
print("Expected theta =",180.*thetaEstim/np.pi)
thetaEstim = 180.*thetaEstim/np.pi

# this glob should only hit one file
den = sorted(glob.glob('od_out_t*.h5'))[-1]
rho =  h5py.File(den, 'r')['OutArray'][1,:,:]

# we mark the nodes where the rocks are, and we select
# only the edge of the interface
rho[:,0:rock+1] = -1
rho[rho>0.6] = -1
rho[rho<0.5] = -1

# We perform now a fit of the droplet edge to extract the contact angle
y,x = np.mgrid[0:rho.shape[0],0:rho.shape[1]]

# extract x&y coordinates of the rim, and shift them so that they are
# centered along the x axis, and start from 0 along the y one.
py,px = x[rho>0]-rock,y[rho>0]-x.shape[0]/2+0.5
# sort just for pretty plotting
sort = np.argsort(px)
px,py=px[sort],py[sort]

# the function to be fitted                                                                                                                                   
def arc(x,R,C):
    return C + np.sqrt(R**2-x**2)

c=(max(px)-min(px))/2.
d=max(py)-rock
R=c**2/d/2. + d/2.
C=d-R

try:
    [R,C] , _ = curve_fit(arc, px, py,p0=(R,C))
    theta = 180.-np.arccos(C/R)*180./np.pi
    print("From fit: R,C,theta=",R,C,theta)
except:
    print("Failure 🤦")
    raise RuntimeError("Fitting procedure failed")

# let's check that we are not furhter apart than 1.3 deg from the expected esitmate
# Note: we start from a state that is at theta=88.5, so don't change sensitivity here
if not np.isclose(thetaEstim,theta,atol=1.3,rtol=0.0):
    print("Failure 🤦")
    raise RuntimeError("Wrong contact angle: "+str(theta)+" expected: "+str(thetaEstim))
else:
    print('Contact angle OK:',theta)

print('Success 🙂')
