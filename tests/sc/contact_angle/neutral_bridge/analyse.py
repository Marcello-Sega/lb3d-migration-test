#!/usr/bin/env python3
# this tests that by using the neutral wetting condition based on mirroring
# the values of the pseudopotential psi (gw_wet_neutral), the resulting 
# density profile of a liquid bridge connecting two flat walls does 
# not depend on the distance from the wall.
import h5py
import numpy as np
from glob import glob
h5f = h5py.File(sorted(glob('./od*h5'))[-1], 'r')
od = np.swapaxes(h5f["OutArray"][:], 0, 2)

# We take two cuts, one where the density is high, one where it is low
# We also exclude two points on the right and two on the left (the wall)
d=od[7,1,2:-2]
if not np.all(np.isclose(d/np.max(d)-1, 0.0, atol=1e-9,rtol=1e-9)):
    print(d)
    print("Failure 🤦")
    raise ValueError('The system is not homogeneous in the direction normal to the wall')
d=od[24,1,2:-2]
# along the cut at lower density the accuracy is lower.
if not np.all(np.isclose(d/np.max(d)-1, 0.0, atol=1e-7,rtol=1e-9)):
    print(d)
    print("Failure 🤦")
    raise ValueError('The system is not homogeneous in the direction normal to the wall')

# The next check is actually stricter than the one above (which can wrongly pass also in case of a homogeneous fluid), 
# but it's a consistency one. You might want to adapt it if you change the number of integration steps in the test, 
# or if you want to extend this to other multicomponent systems.
if not np.all(np.isclose([1.11438 , 0.00312], 
                         [np.max(od),np.min(od[od>0])], atol=1e-5,rtol=1e-5)):
    print("Failure 🤦")
    raise ValueError('the system did not demix as expected')

print('Success 🙂')
