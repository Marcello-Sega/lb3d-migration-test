#!/usr/bin/env python3
#
# Check the contact angle for Shan-Chen 2 components,
# shan-chen forcing scheme and sukop wetting.
# Uses as reference the approximation
# proposed by Sukop et al.
#
# Huang, H., Thorne Jr, D. T., Schaap, M. G., & Sukop, M. C. (2007).
# Proposed approximation for contact angles in Shan-and-Chen-type
# multicomponent multiphase lattice Boltzmann models.
# Physical Review E, 76(6), 066701.

import sys, glob, h5py
import numpy as np
from scipy.optimize import curve_fit

G_AB = 2.
G_WA = -0.1
G_WB = 0.1
rho_A = 1.021198921843392
rho_B = 0.018984502335334538
rock=4
cosThetaEstim = (G_WB-G_WA)/(0.5*G_AB*(rho_A-rho_B))
thetaEstim = np.arccos(cosThetaEstim)
print("Estimated theta =",180.*thetaEstim/np.pi)
thetaEstim = 180.*thetaEstim/np.pi

# this glob should only hit one file
den = sorted(glob.glob('od_out_t*.h5'))[-1]
rho =  h5py.File(den, 'r')['OutArray'][1,:,:]

# we mark the nodes where the rocks are, and we select
# only the edge of the interface
rho[:,0:rock+1] = -1
rho[rho>0.6] = -1
rho[rho<0.4] = -1

# We perform now a fit of the droplet edge to extract the contact angle
y,x = np.mgrid[0:rho.shape[0],0:rho.shape[1]]

# extract x&y coordinates of the rim, and shift them so that they are
# centered along the x axis, and start from 0 along the y one.
py1,px1 = x[rho>0.]-rock,y[rho>0.]+0.5
sort = np.argsort(px1)
px1,py1=px1[sort],py1[sort]

#not consider the lower half of the droplet
#in this way, for hydrophobic surfaces
#the fitting procedure is more precise
pxMin = np.min(px1)
cond = np.where(px1 == pxMin)
pyMin = py1[cond]
pyMIN = np.average(pyMin, weights=None)
                        
#general procedure to center along the x axis                    
pyMax = np.max(py1)
cond = np.where(py1 == pyMax)
pxMax = px1[cond]
pxMAX = np.average(pxMax, weights=None)

#putting all together
py = py1[py1>=(pyMIN)]
px = px1[py1>=(pyMIN)] - pxMAX
# sort just for pretty plotting
sort = np.argsort(px)
px,py=px[sort],py[sort]

# the function to be fitted                                                                                                                                   
def arc(x,R,C):
    return C + np.sqrt(R**2-x**2)

c=(max(px)-min(px))/2.
d=max(py)-rock
R=c**2/d/2. + d/2.
C=d-R
#print("Estimated R,C,theta =",R,C,180.*(np.arcsin(c/R))/np.pi)

try:
    [R,C] , _ = curve_fit(arc, px, py,p0=(R,C))
    theta = 180.-np.arccos(C/R)*180./np.pi
    print("From fit: R,C,theta=",R,C,theta)
except:
    print("Failure 🤦")
    raise RuntimeError("Fitting procedure failed")

# let's check that we are not furhter apart than 1 deg from the expected esitmate
if not np.isclose(thetaEstim,theta,atol=2.7,rtol=0.0):
    raise RuntimeError("Wrong contact angle: "+str(theta)+" expected: "+str(thetaEstim))
else:
    print('Contact angle OK:',theta)

print('Success 🙂')
