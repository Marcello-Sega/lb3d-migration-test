#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# Simple Laplace test:
# Simulate a droplet of blue fluid inside a bulk solvent of red without
# any additional forcing.
#
# For this testcase we therefore allow a reasonably large offset and tolerance,
# which where obtained empirically. If results

import numpy as np
import h5py
import sys
from glob import glob

# All empirical with a 1% allowed tolerance between them
# Values are obtained by equilibrating for 200000 timesteps, and then sampling the next 25000-timesteps. Test itself runs
# 10000 timesteps but no significant difference was observed anymore for this shorter time-range (i.e. well equilibrated)
st_empirical = 0.18094613663549453
G_AB = 3.
tolerance_gamma = 0.01
speedOfSound2 = 1./3.

np.seterr(all='raise')

#function to read h5 files and invert the x and z axes
def read_h5_lb(filename):
    hf=h5py.File(filename, 'r')
    return np.swapaxes(hf["OutArray"][:], 0, 2)

#read files
waterLaplace = read_h5_lb(sorted(glob('wd_out_*.h5'))[-1])
oilLaplace = read_h5_lb(sorted(glob('od_out_*.h5'))[-1])
pressure = read_h5_lb(sorted(glob('pxx_*.h5'))[-1])

#initialisation
radius = 0.
pressureIN = 0.
pressureOUT = 0.
pressIdealIN = 0.
pressIdealOUT = 0.
pressInteractIN = 0.
pressInteractOUT = 0.
meanDens = 0.

#compute pressure difference and radius ---> real surface tension
sizeHalf = [int(s/2) for s in np.array(oilLaplace.shape)]

pressIdealIN = (oilLaplace[tuple(sizeHalf)]+waterLaplace[tuple(sizeHalf)])*speedOfSound2
pressInteractIN = G_AB*oilLaplace[tuple(sizeHalf)]*waterLaplace[tuple(sizeHalf)]*speedOfSound2
pressureIN = pressIdealIN+pressInteractIN

pressIdealOUT = (oilLaplace[1,1,1]+waterLaplace[1,1,1])*speedOfSound2
pressInteractOUT = G_AB*oilLaplace[1,1,1]*waterLaplace[1,1,1]*speedOfSound2
pressureOUT = pressIdealOUT+pressInteractOUT

#sizeHalf = [int(s/2) for s in np.array(oilLaplace.shape)]
meanDens = (np.max(oilLaplace) + np.min(oilLaplace))/2.0
od = oilLaplace[:,sizeHalf[1],sizeHalf[2]]
droplet = np.zeros_like(od)
droplet[np.where(od > meanDens)] = 1
radius = sum(droplet)/2.0

st_laplace = radius*(pressureIN-pressureOUT)


if not np.isclose(st_empirical,st_laplace,atol=0.01,rtol=0.0):
    print("Failure 🤦")
    raise RuntimeError("Surface tension: "+str(st_laplace)+" out of tollerance range. Expected one: "+str(st_empirical))
else:
    print('Surface tension measured from Laplace experiment OK:',st_laplace)

print('Success 🙂')
