#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
# -*- coding: utf-8 -*-

import h5py
import numpy as np
from scipy.optimize import curve_fit
import glob

od = np.array(h5py.File(sorted(glob.glob("od_lens*h5"))[-1], "r")["OutArray"][:])
sd = np.array(h5py.File(sorted(glob.glob("sur_lens*h5"))[-1], "r")["OutArray"][:])
colarr = (od-sd)
# System sizes
nz, ny, nx = colarr.shape
ns = nx * ny * nz
zhigh =int(nz/2)
zlow = 5

ydata = np.linspace(0, ny, ny)
def get_interface(color_field):
    # Find the interface by looking for the flip in the prefactor of the colour field and interpolate
    pos = []
    for hnotx in range (0,nx):     
        hnot = nz/2.0-1.0
        for hnotz in range (zlow,zhigh):
            if color_field[hnotz][0][hnotx]==0:
                hnot = hnotz
            elif color_field[hnotz][0][hnotx]<0  and color_field[hnotz-1][0][hnotx]>0:
                #do interpolation to get the exact interface position )
                hnot=(hnotz- \
                color_field[hnotz][0][hnotx]/ \
                (color_field[hnotz][0][hnotx] - \
	             color_field[hnotz-1][0][hnotx]))
                break
        pos.append(hnot)
    return pos

#get a quarter of the interface shape for extract contact angle
def get_quarter(interface):
    pos_quarter = []
    for i in range (int(nx/2), nx):
        posm=nz/2 -1.0-interface[i]
        if (posm > 0.0):
            pos_quarter.append(posm)
    return pos_quarter

# function to descrie a spherical cap 
def spherical_cap(x, R, theta):
    return  ((R/np.sin(theta))**2-x**2)**0.5-R*np.cos(theta)/np.sin(theta)

# function to calcuate the neumann angles.
#droplet is fluid 1, lower liquid is fluid 2, and upper liquid is fluid 3
def neumann_angles(g12,g13,g23):
    theta1 = np.arccos((g13**2-g12**2+g23**2)/(2*g23*g13))
    theta2 = np.arccos((g12**2-g13**2+g23**2)/(2*g23*g12))
    return theta1, theta2

#surface tensions
g12, g13, g23 =  0.046, 0.046, 0.046

interface = get_interface(colarr.copy()) # Don't touch the original array
interface_quarter = get_quarter(interface)
ydata2 = np.linspace(0, len(interface_quarter), len(interface_quarter))
theta= neumann_angles(g12,g13,g23)

init_vals = [len(interface_quarter), theta[0]]
pt, pcov = curve_fit(spherical_cap, ydata2, interface_quarter, init_vals)

if abs(theta[0]-pt[1])/theta[0] > 0.02:
    print("contact angle of lens (%e) deviated more than %f%% from expected value %e" % (pt[1]/np.pi*180, 0.02*100.0, theta[0]/np.pi*180))
    print("Failure 🤦")
    exit(1)
else:
    print("Success 🙂")

