#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Very basic consistency test for the interface.
# Compares with a precomputed 1D numpy array of oil density across an interface of length 100
# computed with an interaction parameter of 0.075 over 10^5 timesteps.
# If maximum pointwise deviation of newly generated data to reference data is >=0.01, something
# has changed in the Shan Chen interactions.

import h5py
import numpy as np
from sys import exit
import glob

force=h5py.File(sorted(glob.glob('fz_od_mixtest_t00000002.h5'))[0],'r')['OutArray'][:]
dens=h5py.File(sorted(glob.glob('od_mixtest_t00000002.h5'))[0],'r')['OutArray'][:]

if not np.all(np.isclose(np.loadtxt('force-1.dat').flatten(),force.flatten())):
    print('wrong force')
    print("Failure 🤦")
    exit(1)
if not np.all(np.isclose(np.loadtxt('dens-1.dat').flatten(),dens.flatten())):
    print('wrong density')
    print("Failure 🤦")
    exit(1)

print('success🙂')

