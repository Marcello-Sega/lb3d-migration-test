#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Very basic consistency test for the interface.
# Compares with a precomputed 1D numpy array of oil density across an interface of length 100
# computed with an interaction parameter of 0.075 over 10^5 timesteps.
# If maximum pointwise deviation of newly generated data to reference data is >=0.01, something
# has changed in the Shan Chen interactions.

import h5py
import numpy as np
from sys import exit
import glob
failed=False
g_br = 2.7
wforce=h5py.File(sorted(glob.glob('fz_wd_mixtest_t00000001.h5'))[0],'r')['OutArray'][:]
oforce=h5py.File(sorted(glob.glob('fz_od_mixtest_t00000001.h5'))[0],'r')['OutArray'][:]
odens=h5py.File(sorted(glob.glob('od_mixtest_t00000001.h5'))[0],'r')['OutArray'][:]
wdens=h5py.File(sorted(glob.glob('wd_mixtest_t00000001.h5'))[0],'r')['OutArray'][:]

# it's a planar surface, we take the gradient along x + 4 other equal contribution for other vector pairs
if not np.all(np.isclose((np.roll(wdens,1,axis=0)-np.roll(wdens,-1,axis=0) ) * g_br * odens * (1./18. + 4./36.)  , oforce )) :
    print((np.roll(wdens[:,0,0],1)-np.roll(wdens[:,0,0],-1) ) * g_br * odens[:,0,0]* 5  , '\n\n', oforce[:,0,0] ) 
    print('wrong oil force')
    failed = True
if not np.all(np.isclose((np.roll(odens,1,axis=0)-np.roll(odens,-1,axis=0) ) * g_br * wdens * (1./18. + 4./36.)  , wforce )) :
    print((np.roll(odens[:,0,0],1)-np.roll(odens[:,0,0],-1) ) * g_br * wdens[:,0,0]* 5  , '\n\n', wforce[:,0,0] ) 
    print('failed water force')
    failed = True

if failed :
    print('failure')
    print("Failure 🤦")
    exit(1)
else:
    print('success🙂')
#
