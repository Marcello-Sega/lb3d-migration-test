#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Very basic consistency test for the interface.
# Compares with a precomputed 1D numpy array of oil density across an interface of length 100
# computed with an interaction parameter of 0.075 over 10^5 timesteps.
# If maximum pointwise deviation of newly generated data to reference data is >=0.01, something
# has changed in the Shan Chen interactions.

import h5py
import numpy as np
from sys import exit
import glob

files=sorted(glob.glob('od*.h5'))

data=h5py.File(files[-1],'r')["OutArray"][:]
data=np.average(data,axis=(1,2))
dataref=np.load('reference.npy')

maxdev=np.amax(np.absolute(data-dataref)/dataref)
#print('maxdev={}'.format(maxdev))

if maxdev>=0.01:
    print('Interface deviates by 1% or more ('+str(maxdev*100.)+'%) from the nskg state of 2019-04-18.')
    print(data)
    print(dataref)
    print("Failure 🤦")
    raise Exception('Test failed')
else:
    print("Success 🙂")
