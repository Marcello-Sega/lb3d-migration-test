#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# gas density test for thermal LBM forcing with Peng-Robinson EOS. At default temperature 0.75Tc (hard-coded for now) uniform density with fluctuations is initialised and phase separation occurs. In the end, densities of gas and liquid are compared with theoretical values.

import numpy as np
import h5py
import glob
import sys


# All empirical with a 1% allowed tolerance between them
# Values are obtained by equilibrating for 200000 timesteps, and then sampling the next 25000-timesteps. Test itself runs
# 10000 timesteps but no significant difference was observed anymore for this shorter time-range (i.e. well equilibrated)


tolerance = 0.0001

deno = sorted(glob.glob('od_mixtest_t*.h5'))[-1]
denw = 'wd'+deno[2:]

u1 = h5py.File(deno, 'r')['OutArray'][1,1,:]
u2 = h5py.File(denw, 'r')['OutArray'][1,1,:]
    
min_u1 = np.min(u1)
max_u1 = np.max(u1)

u1ref = [  0.      ,    0.   ,       0.     ,     0.    ,      0.    ,      0.,
   0.00273395,  0.0055662 ,  0.00814557,  0.01067878,  0.01315527,  0.01555099,
   0.01785607,  0.0200481 ,  0.02211806,  0.02404583,  0.02582346,  0.02743369,
   0.02886989,  0.0301181 ,  0.03117319,  0.0320249 ,  0.03266975,  0.03310145,
   0.03331825,  0.03331803,  0.03310081,  0.03266869,  0.03202347,  0.03117141,
   0.03011603,  0.02886758,  0.02743121,  0.02582087,  0.02404319,  0.02211545,
   0.02004559,  0.01785372,  0.01554887,  0.01315343,  0.01067727,  0.00814442,
   0.00556546,  0.00273369,  0.        ,  0.        ,  0.        ,  0.         , 0.,
   0.        ]
u2ref = [ 0.  ,        0. ,         0. ,         0.  ,        0.  ,        0.  ,
   0.09828418 , 0.09545191,  0.0928725 ,  0.09033923 , 0.08786266 , 0.08546684,
   0.08316165 , 0.08096948,  0.07889937,  0.07697143 , 0.07519362 , 0.07358318,
   0.07214677 , 0.07089832,  0.069843  ,  0.06899102 , 0.06834592 , 0.06791394,
   0.06769688 , 0.0676968 ,  0.06791375,  0.06834557 , 0.06899052 , 0.06984229,
   0.07089741 , 0.07214559,  0.07358171,  0.07519181 , 0.07696926 , 0.07889678,
   0.08096645 , 0.08315815,  0.08546285,  0.08785817 , 0.09033422 , 0.09286699,
   0.0954459  , 0.09827764,  0.        ,  0.         , 0.         , 0.        ,  0.,
   0.       ]



error = False


for i in range(u1.shape[0]):
    if abs(u1[i]-u1ref[i]) > tolerance:
        print("oil distribution in reactive bc test is wrong")
        print("at i = ", i)
        print (u1[i])
        print (u1ref[i])
        error = True
for i in range(u2.shape[0]):
    if abs(u2[i]-u2ref[i]) > tolerance:
        print("water distribution in reactive bc test is wrong")
        print("at i = ", i)
        print (u2[i])
        print (u2ref[i])
        error = True



if error:
    print("Failure 🤦")
    sys.exit(1)
else:
    print ("Success 🙂")



