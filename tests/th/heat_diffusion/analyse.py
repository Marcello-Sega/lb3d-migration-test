#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# gas density test for thermal LBM forcing with Peng-Robinson EOS. At default temperature 0.75Tc (hard-coded for now) uniform density with fluctuations is initialised and phase separation occurs. In the end, densities of gas and liquid are compared with theoretical values.

import numpy as np
import h5py
import glob
import sys


# All empirical with a 1% allowed tolerance between them
# Values are obtained by equilibrating for 200000 timesteps, and then sampling the next 25000-timesteps. Test itself runs
# 10000 timesteps but no significant difference was observed anymore for this shorter time-range (i.e. well equilibrated)

critical_rho = 2.5312748582
tolerance = 0.01

fn = sorted(glob.glob('temp_mixtest_t*.h5'))[-1]
den = 'od'+fn[4:]

u = h5py.File(fn, 'r')['OutArray'][1,:,1]
    
min_u = np.min(u)
max_u = np.max(u)



error = False
for i in range(16):
    if abs(u[i]-(0.95-0.2*i/15))/(0.95-0.2*i/15) > tolerance:
        print("temperature distribution in heat diffusion test is wrong")
        print("at i = ", i)
        print (u[i])
        print ((0.95-0.2*i/15))
        error = True


if error:
    print("Failure 🤦")
    sys.exit(1)
else:
    print("Success 🙂")



