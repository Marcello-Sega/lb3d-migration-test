#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# Consistency test for wetting of CG

#########################################
# this test does not check the physics  #
# only that the code reproduces the     #
# expected results.                     #
# Update with an analytical expectation #
# for the contact angle, if possible.   #
#########################################

import sys, glob, h5py
import numpy as np
from scipy.optimize import curve_fit
np.seterr(all='raise')


# expected values of max density, fitting params and contact angle
# these are for consistency checks only.
rho_ = 6.67882168868
R_ = 29.640999307562726
C_ = -8.8735757492414251
theta_ = 107.41966607492505

# this glob should only hit one file
dens = sorted(glob.glob('od_out_t*.h5'))
try:
    i=0
    for den in dens:
        rho =  h5py.File(den, 'r')['OutArray'][:,:,1][::,::-1].T    
        i=i+1
except:
    pass

den = sorted(glob.glob('od_out_t*.h5'))[-1]
rho =  h5py.File(den, 'r')['OutArray'][:,:,1][::,::-1].T    

# let's first check min and max density
print("\n")
if not np.isclose(np.min(rho[1:100,3:97]),0., 0.1): # this is actually a strict requirement of CG
    print("Wrong minimum density: not zero but "+str(np.min(rho))) 
    print('Minimum density OK: ',np.min(rho))
if not np.isclose(np.max(rho),rho_,rtol=1e-1,atol=1e-4):
    print("Wrong maximum density: not "+str(rho_)+" but "+ str(np.max(rho)))
else:
    print('Maximum density OK: ',np.max(rho))

# We perform now a fit of the droplet edge to extract the contact angle
a=rho.copy()
a[rho>0.6*np.max(rho)]=np.nan
a[rho<0.4*np.max(rho)]=np.nan
y,x = np.mgrid[0:a.shape[0],0:a.shape[1]]
# extract x&y coordinates of the rim, and shift them so that they are
# centered along the x axis, and start from 0 along the y one.
px,py = x[np.isfinite(a)]-x.shape[1]//2,-y[np.isfinite(a)]+x.shape[0]
# sort just for pretty plotting
sort = np.argsort(px)
px,py=px[sort],py[sort]

# the function to be fitted
def arc(x,R,C):
    return C + np.sqrt(R**2-x**2)




error=False

c=(max(px)-min(px))/2.
d=max(py)-2
R=c**2/d/2. + d/2.
C=-d+R
print("Estimated R,C,theta =",R,C,180.*(np.arcsin(c/R))/np.pi)
try:
    [R,C] , pcov = curve_fit(arc, px, py, p0=(R,C))
    theta = np.arccos(C/R)*180./np.pi
    print("From fit: R,C,theta=",R,C,theta)
except:
    print("Fitting procedure failed")

if not np.isclose(R,R_,0.1):
    print("Wrong radius: not "+str(R_)+" but "+str(R))
    error=True
if not np.isclose(C,C_,0.1):
    print("Wrong center: not "+str(C_)+" but "+str(C))
    error=True
if not np.isclose(theta,theta_,0.1):
    print("Wrong contact angle: "+str(theta)+" expected: "+str(theta_))
    error=True
else:
    print('Contact angle OK:',theta)
if error:
    print("Failure 🤦")
    raise RuntimeError ('test failed')
print("\n\nThis is a consistency check only. Update if possible with an analytical one.")
print("Success 🙂")



