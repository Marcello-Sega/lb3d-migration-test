#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# gas density test for thermal LBM forcing with Peng-Robinson EOS. At default temperature 0.75Tc (hard-coded for now) uniform density with fluctuations is initialised and phase separation occurs. In the end, densities of gas and liquid are compared with theoretical values.

import numpy as np
import h5py
import glob
import sys

# All empirical with a 1% allowed tolerance between them
# Values are obtained by equilibrating for 200000 timesteps, and then sampling the next 25000-timesteps. Test itself runs
# 10000 timesteps but no significant difference was observed anymore for this shorter time-range (i.e. well equilibrated)

rho_gas = 0.04297
rho_liq = 3.034
tolerance_rho= 0.1


critical_rho = 2.5312748582


den = sorted(glob.glob('od_mixtest_t*.h5'))[-1]
# one slice is enough, cut out walls
u = h5py.File(den, 'r')['OutArray'][1,:,:]
    
min_u = np.min(u)/critical_rho
max_u = np.max(u)/critical_rho
    

error = False
if abs(min_u-rho_gas)/rho_gas > tolerance_rho:
    print("Gas density (%e) deviated more than %f%% from expected value %e" % (min_u, tolerance_rho*100.0, rho_gas))
    error = True
if (max_u-rho_liq)/rho_liq> tolerance_rho:
    print("Liquid density (%e) deviated more than %f%% from expected value %e" % (max_u, tolerance_rho*100.0, rho_liq))
    error = True

if error:
    print("Failure 🤦")
    sys.exit(1)
else:
    print("Success 🙂")

