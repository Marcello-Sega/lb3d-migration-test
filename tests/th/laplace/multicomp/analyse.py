#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# gas density test for thermal LBM forcing with Peng-Robinson EOS. At default temperature 0.75Tc (hard-coded for now) uniform density with fluctuations is initialised and phase separation occurs. In the end, densities of gas and liquid are compared with theoretical values.

import numpy as np
import h5py
import glob
import sys

# All empirical with a 1% allowed tolerance between them
# Values are obtained by equilibrating for 200000 timesteps, and then sampling the next 25000-timesteps. Test itself runs
# 10000 timesteps but no significant difference was observed anymore for this shorter time-range (i.e. well equilibrated)

rho_gas = 0.06522/2.5312748582
rho_liq = 7.71856/2.5312748582
rho2_gas = 0.00599
rho2_liq = 4.4972e-5
tolerance_rho= 0.01


critical_rho = 2.5312748582


den1 = sorted(glob.glob('od_mixtest_t*.h5'))[-1]
den2 = sorted(glob.glob('wd_mixtest_t*.h5'))[-1]
# one slice is enough, cut out walls
u1 = h5py.File(den1, 'r')['OutArray'][1,:,:]
u2 = h5py.File(den2, 'r')['OutArray'][1,:,:]
    
min_u1 = np.min(u1)/critical_rho
max_u1 = np.max(u1)/critical_rho
min_u2 = np.min(u2)
max_u2 = np.max(u2)
    

error = False
if abs(min_u1-rho_gas)/rho_gas > tolerance_rho:
    print("Gas density (%e) deviated more than %f%% from expected value %e" % (min_u1, tolerance_rho*100.0, rho_gas))
    error = True
if (max_u1-rho_liq)/rho_liq> tolerance_rho:
    print("Liquid density (%e) deviated more than %f%% from expected value %e" % (max_u1, tolerance_rho*100.0, rho_liq))
    error = True
    
if abs(max_u2-rho2_gas)/rho2_gas > tolerance_rho:
    print("O2 density in gas (%e) deviated more than %f%% from expected value %e" % (max_u2, tolerance_rho*100.0, rho2_gas))
    error = True
if (min_u2-rho2_liq)/rho2_liq> tolerance_rho:
    print("O2 density in liquid (dissolved) (%e) deviated more than %f%% from expected value %e" % (min_u2, tolerance_rho*100.0, rho2_liq))
    error = True

if error:
    print("Failure 🤦")
    sys.exit(1)
else:
    print("Success 🙂")

