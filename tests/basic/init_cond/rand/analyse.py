#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
import h5py
import numpy as np
from glob import glob

# bounds testing
# NOTE: if this test succeeds this does not guarantee that the distribution is really random!
od = h5py.File(sorted(glob('od_default*h5'))[-1], 'r')['OutArray'][...]
if not np.all(od >= 0) and np.all(od <= 0.01):
    raise (ValueError('🤦 wrong bounds for component 1'))

wd = h5py.File(sorted(glob('wd_default*h5'))[-1], 'r')['OutArray'][...]
if not np.all(wd >= 0) and np.all(wd <= 1.0):
    raise (ValueError('🤦 wrong bounds for component 2'))

sd = h5py.File(sorted(glob('sur_default*h5'))[-1], 'r')['OutArray'][...]
if not np.all(sd >= 0) and np.all(sd <= 100.0):
    raise (ValueError('🤦 wrong bounds for component 3'))

print('Success 🙂')
