#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
import h5py
import numpy as np
from glob import glob

nml=f90nml.read("input")
tpl=[0]*int(nml["lb_initialization"]["lbinit"]["fr1"]) + [1]*int(nml["lb_initialization"]["lbinit"]["fr2"])
lamella_binary = np.array(tpl+tpl)

od = nml["lb_initialization"]["lbinit"]["fr"] + lamella_binary*(nml["lb_initialization"]["lbinit"]["pr"]-nml["lb_initialization"]["lbinit"]["fr"])
if not np.allclose(od, h5py.File(sorted(glob( 'od_default*h5'))[-1], 'r')['OutArray'][1, :, 1]):
    raise (ValueError('🤦 wrong density of component 1'))
wd = nml["lb_initialization"]["lbinit"]["fb"] + lamella_binary*(nml["lb_initialization"]["lbinit"]["pb"]-nml["lb_initialization"]["lbinit"]["fb"])
if not np.allclose(wd, h5py.File(sorted(glob( 'wd_default*h5'))[-1], 'r')['OutArray'][1, :, 1]):
    raise (ValueError('🤦 wrong density of component 2'))
sd = nml["lb_initialization"]["lbinit"]["fg"] + lamella_binary*(nml["lb_initialization"]["lbinit"]["pg"]-nml["lb_initialization"]["lbinit"]["fg"])
if not np.allclose(sd, h5py.File(sorted(glob('sur_default*h5'))[-1], 'r')['OutArray'][1, :, 1]):
    raise (ValueError('🤦 wrong density of component 3'))

print('Success 🙂')
