#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
import h5py
import numpy as np
from glob import glob

nml=f90nml.read("input")
tpl=[[1,0,0]]*int(nml["lb_initialization"]["lbinit"]["fr1"]) + [[0,1,0]] + [[0,0,1]]*int(nml["lb_initialization"]["lbinit"]["fr2"]) + [[0,1,0]]
lamella_components = np.array(tpl+tpl)

od = lamella_components[:, 0]*nml["lb_initialization"]["lbinit"]["fr"] + lamella_components[:, 1]*nml["lb_initialization"]["lbinit"]["qr"] + lamella_components[:, 2]*nml["lb_initialization"]["lbinit"]["pr"]
if not(np.all(np.isclose(od,h5py.File(sorted(glob( 'od_default*h5'))[-1], 'r')['OutArray'][:,1,1]))):
    raise(ValueError('🤦 wrong density of component 1'))
wd = lamella_components[:, 0]*nml["lb_initialization"]["lbinit"]["fb"] + lamella_components[:, 1]*nml["lb_initialization"]["lbinit"]["qb"] + lamella_components[:, 2]*nml["lb_initialization"]["lbinit"]["pb"]
if not(np.all(np.isclose(wd,h5py.File(sorted(glob( 'wd_default*h5'))[-1], 'r')['OutArray'][:,1,1]))):
    raise(ValueError('🤦 wrong density of component 2'))
sd = lamella_components[:, 0]*nml["lb_initialization"]["lbinit"]["fg"] + lamella_components[:, 1]*nml["lb_initialization"]["lbinit"]["qg"] + lamella_components[:, 2]*nml["lb_initialization"]["lbinit"]["pg"]
if not(np.all(np.isclose(sd,h5py.File(sorted(glob('sur_default*h5'))[-1], 'r')['OutArray'][:,1,1]))):
    raise(ValueError('🤦 wrong density of component 3'))

print('Success 🙂')
