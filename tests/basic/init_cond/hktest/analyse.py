#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
import h5py
import numpy as np
from glob import glob

od0 = np.array([1., 111., 111., 111., 1., 111., 111., 111.])
od1 = np.array([111., 1., 1., 1., 111., 1., 1., 1.])
od_file = h5py.File(sorted(glob('od_default*h5'))[-1], 'r')['OutArray']
if not (np.allclose(od0, od_file[0, 0, :]) and np.allclose(od1, od_file[0, 1, :])):
    raise (ValueError('🤦 wrong density of component 1'))

wd0 = np.array([2., 222., 222., 222., 2., 222., 222., 222.])
wd1 = np.array([222., 2., 2., 2., 222., 2., 2., 2.])
wd_file = h5py.File(sorted(glob('wd_default*h5'))[-1], 'r')['OutArray']
if not (np.allclose(wd0, wd_file[0, 0, :]) and np.allclose(wd1, wd_file[0, 1, :])):
    raise (ValueError('🤦 wrong density of component 2'))

sd0 = np.array([3., 333., 333., 333., 3., 333., 333., 333.])
sd1 = np.array([333., 3., 3., 3., 333., 3., 3., 3.])
sd_file = h5py.File(sorted(glob('sur_default*h5'))[-1], 'r')['OutArray']
if not (np.allclose(sd0, sd_file[0, 0, :]) and np.allclose(sd1, sd_file[0, 1, :])):
    raise (ValueError('🤦 wrong density of component 3'))

print('Success 🙂')
