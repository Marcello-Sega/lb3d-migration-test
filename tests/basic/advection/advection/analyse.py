#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#
import sys, glob, h5py
import numpy as np
error="Velocity not advected properly"
error_den="Mass not conserved"
initial=np.array([[[0.,0.,0.,0.],[0.,0.,0.,0.]],[[0.30769231,0.,0.30769231,0.],[0.,0.,0.,0.]]])


U = []
rho = [] 
for fn in sorted(glob.glob('velz_out_t*.h5')):
    with h5py.File(fn, 'r') as hf:
        U.append(hf['OutArray'][:])

for idx,u in enumerate(U[1:]):
    expected = np.roll(initial,idx,axis=0)
    if not np.all(np.isclose(u,expected,1e-16)):
        print ('vel(t='+str(idx)+'):')
        print (u)
        print ('expected:')
        print(expected)
        print("Failure 🤦")
        raise RuntimeError(error)

for fn in sorted(glob.glob('od_out_t*.h5')):
    with h5py.File(fn, 'r') as hf:
        rho.append(hf['OutArray'][:])

if not np.isclose(np.sum(rho[-1]), np.sum(rho[0])):
    print ("mass difference: "+str(np.sum(rho[0])  - np.sum(rho[-1])))
    print("Failure 🤦")
    raise RuntimeError(error_den)

print ("Success 🙂")
