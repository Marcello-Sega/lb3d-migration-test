#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# This test generates a simple shear flow via a moving wall on the top (z-direction). 
# Check wheter a wall is placed at z = 12 [l.u.] via the velocity profile (zero below the wall, non zero above the wall).

import h5py
import numpy as np
from glob import glob

# resting rock at z = 0, 12 and moving rock at z = 15 => fluid velocity zero
# no velocity field below 10, when wall is placed, velocity profile of a simple shear flow above 10.
velX = [0., 0.,  0.,  0., 0.,  0.,  0., 0., 0., 0., 0., 0., 0.,  3.3333335e-02,  1.0000000e-01,  0.]
if not np.allclose(velX, h5py.File(sorted(glob('velx*.h5'))[-1], 'r')['OutArray'][:,8,8]):  # array format (z,y,x)
    raise (ValueError('wrong velocity profile'))

print('Success')
