#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# This test generates a simple shear flow via a moving wall on the top (z-direction). 
# Check wthere the particle is placed at position (8, 8, 5)

import h5py
import numpy as np
from glob import glob

# resting rock at z = 0 and moving rock at z = 15 => fluid velocity zero
# md particle at position (8, 8, 5) with radius 3 => velocity zero on this nodes
velX = [0., 0., 0., 0., 0., 0., 0., 0., 0.01275856, 0.03098489, 0.04560565, 0.05921471, 0.07280587, 0.08652047, 0.1, 0. ]
if not np.allclose(velX, h5py.File(sorted(glob('velx*.h5'))[-1], 'r')['OutArray'][:,8,8]):  # array format (z,y,x)
    raise (ValueError('wrong velocity profile'))

print('Success 🙂')
