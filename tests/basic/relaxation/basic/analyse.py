#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Simulation setup with constant velocity, nothing happens. After `relaxation%stop_local_velocity%at` time steps, all velocities are set to zero.
# This test checks if this is really the case.

import sys, glob, h5py
import numpy as np
np.set_printoptions(suppress=True)

file_tag        = f90nml.read("input")["system"]["syst"]["file_tag"]
target_velocity = f90nml.read("input")["lb_initialization"]["lbinit"]["pr"]
reset_time      = f90nml.read("input")["relaxation"]["stop_local_velocity"]["at"]-1 # Python is 0-based, so we have to subtract 1 here

U = []
for time,fn in enumerate(sorted(glob.glob('velz_'+file_tag+'_t*.h5'))[1:]):
    with h5py.File(fn, 'r') as hf:
        U.append(np.mean(hf['OutArray'][:]))
U = np.asarray(U)

if(not all(np.isclose(U[:reset_time],target_velocity))):
    print("Failure 🤦")
    raise RuntimeError("Initial velocity not correct")
if(not all(np.isclose(U[reset_time:],0.0))):
    print("Failure 🤦")
    raise RuntimeError("Velocity after reset not correct")
print("Success 🙂")
