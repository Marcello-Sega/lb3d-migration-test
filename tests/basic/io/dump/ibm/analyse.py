#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#This is a simple test checking the correct dumping of IBM particle vtk files

import numpy as np
from sys import exit
from glob import glob

outfiles=sorted(glob('ibm_particles_*.vtk'))

with open('reference.vtk','r') as f1, open(outfiles[1],'r') as f2:
    for l1 in f1.readlines():
        l2 = f2.readline()

if not (l1==l2):
    print('Results are different from the initial mesh! Something is wrong!')
    print("Failure 🤦")
    exit(1)
else:
    print('\nSuccess 🙂')
