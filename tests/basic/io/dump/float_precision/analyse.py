#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Runs the same system twice, once with native precision, once with forced reduced
# precision, and check if the data type of dumps (densities, velocities, population
# checkpoints) meets our expectations.

import numpy as np
import h5py
from sys import exit

for qty in ['od', 'velx', 'chkp_c1']:
    is_dump = (not ('chkp' in qty))
    fname_native = qty+"_fpnative_t00000001.h5"
    fname_single = qty+"_fpsingle_t00000001.h5"
    Onative = h5py.File(fname_native, 'r')['OutArray']
    Osingle = h5py.File(fname_single, 'r')['OutArray']

    # Fail if data types are accidentally identical
    if (Onative.dtype == Osingle.dtype) and is_dump:  # checkpoints are always in machine precision; checking would cause false positives
        print('Warning: Choice of data dump formats not respected (This is okay if NSKG has been compiled with single-precision)')
        print(fname_native, ': ', Onative.dtype)
        print(fname_single, ': ', Osingle.dtype)

    # Fail if single-precision dumps (not checkpoints!) are not actually single-precision
    if (Osingle.dtype != np.float32) and is_dump:
        print('Error: Wrong FP type for single-precision dump', fname_single)
        print("Failure 🤦")
        exit(1)

    # Fail if native builds have irregular float formats. (This includes the case of wrong checkpoint precision)
    if Onative.dtype != np.float64:    # TODO Change this to CMake macro that puts desired compile-time precision here.
        print('Error: Wrong FP type for double-precision dumps (This is okay if NSKG has been compiled with single-precision)', fname_native)
        print("Failure 🤦")
        exit(1)

    # Nice gimmick: since we have chosen density = π, we can see loss of precision via
    # a simple subtraction (not a test, since it won't fail).
    if ('od' in qty):
        print("Absolute noise level, native precision: ", np.mean(np.array(Onative, dtype=np.float64)-np.pi))
        print("Absolute noise level, single precision: ", np.mean(np.array(Osingle, dtype=np.float64)-np.pi))

print('Success 🙂')
