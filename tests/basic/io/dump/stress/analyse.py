#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

#This is a simple test checking the correct dumping of stresses

import numpy as np
from sys import exit
import glob

stressdumps = sorted(glob.glob('stress_TestStressDump_*.asc'))

print('%d stress dumps found.'%len(stressdumps))

if len(stressdumps)==0:
    print("Failure 🤦")
    exit(1)
elif len(stressdumps)>1:
    print(stressdumps)
    print("More than one stress dump, won't guess if the test just created one. Do you have to clean your ctest directory?")
    print("Failure 🤦")
    exit(1)
else:
    print('\nSuccess 🙂')
