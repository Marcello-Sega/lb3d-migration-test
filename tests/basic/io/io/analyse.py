#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
# This set-up tests if file I/O snippets end up at the right place. (The physics part of this test is simple.)
import sys, glob, h5py
import numpy as np
import f90nml

error="not loaded / saved properly"

# Read parameters from namelists
inputfile = f90nml.read('input')
input_filetag = inputfile['system']['syst']['file_tag']
input_folder  = inputfile['system']['syst']['folder']

# Containers for reference data (as stored into h5 files in this directory)
data =  np.swapaxes(np.arange(3*3*4).reshape((3,3,4)) ,0,2)
rho =[ [None],[None] ]
vel =[ [None],[None],[None] ]

fn = sorted(glob.glob(input_folder+'/od_'+input_filetag+'_t*.h5'))[-1]
with h5py.File(fn, 'r') as hf:
        rho[0] = hf['OutArray'][:]

fn = sorted(glob.glob(input_folder+'/wd_'+input_filetag+'_t*.h5'))[-1]
with h5py.File(fn, 'r') as hf:
        rho[1] = hf['OutArray'][:]

fn = sorted(glob.glob(input_folder+'/velx_'+input_filetag+'_t*.h5'))[-1]
with h5py.File(fn, 'r') as hf:
        vel[0] = hf['OutArray'][:]
fn = sorted(glob.glob(input_folder+'/vely_'+input_filetag+'_t*.h5'))[-1]
with h5py.File(fn, 'r') as hf:
        vel[1] = hf['OutArray'][:]
fn = sorted(glob.glob(input_folder+'/velz_'+input_filetag+'_t*.h5'))[-1]
with h5py.File(fn, 'r') as hf:
        vel[2] = hf['OutArray'][:]

if not np.all(np.isclose(1.0*rho[0] ,data)):
    print("Failure 🤦")
    raise RuntimeError("Oil "+error)
if not np.all(np.isclose(1.0*rho[1] ,2.*data)):
    print("Failure 🤦")
    raise RuntimeError("Water "+error)
# the elements of the velocity must be zero where the density is zero.
cond = rho[0]+rho[1] > 0
if not np.all(np.isclose(vel[0][cond], 0.01)):
    print('velx:',vel[0])
    print("Failure 🤦")
    raise RuntimeError("velx "+error)
if not np.all(np.isclose(vel[1][cond], 0.02)):
    print('vely:',vel[1])
    print("Failure 🤦")
    raise RuntimeError("vely "+error)
if not np.all(np.isclose(vel[2][cond], 0.03)):
    print('velz:',vel[2])
    print("Failure 🤦")
    raise RuntimeError("velz "+error)
print ("Success 🙂")

