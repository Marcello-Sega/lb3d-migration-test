#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# A Python script to analyse a simple NSKG simulation run
# This script is run as a test within the test suite, but it also is part of the manual, as a simple example for the usage of Python, h5py and f90nml in conjunction with NSKG.
#
# (A line of context or two, at this place, makes reading this file more inviting to your future self.)

import sys, glob, h5py
import numpy as np

# This reads the input file (to extract information from there, rather than just copy-pasting values here)
inputfile = f90nml.read('./input')
acceleration = inputfile['extforce_constant']['acc']
acceleration_z = acceleration[2]

epsilon = 1e-8     # tolerance

files = sorted(glob.glob('velz_simple_t*.h5'))
print("# Timestep VelocityMeasured VelocityExpected Deviation")
for I, S in enumerate(files):
    with h5py.File(S, 'r') as hf:
        average_velocity = np.mean(hf['OutArray'][:])                   # Read fluid velocity from file
        velocity_deviation = average_velocity - acceleration_z*(I+0.5)  # Compare velocity to expectation (constant acceleration × time)
        print(I, average_velocity, acceleration_z*(I+0.5), velocity_deviation)
        if (np.abs(velocity_deviation) > epsilon):                      # Decide when a deviation is too large to be tolerated
            print("Deviation from constant acceleration detected after time step", I, "file:", S)
            print("Failure 🤦")
            raise RuntimeError()

print("Success 🙂")
