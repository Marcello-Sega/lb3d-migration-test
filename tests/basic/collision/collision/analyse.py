#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#
import sys, glob, h5py
import numpy as np
np.set_printoptions(suppress=True)
error="Velocity not advected properly or bounce-back boundary conditions not working"
expected=np.array([[[0.,0.,0.,0.],[0.,0.,0.,0.],[0.,0.,0.,0.],[0.,0.,0.,0.]],[[0.,0.,0.,0.],[0.,0.,0.,0.],[0.,0.,0.,0.],[0.,0.,0.,0.]],[[0.,0.,0.,0.],[0.,0.,0.,0.],[0.,0.,0.,0.],[0.,0.,0.,0.]],[[-0.30769231,0.,-0.30769231,0.],[0.,0.,0.,0.],[0.,0.,0.,0.],[0.,0.,0.,0.]]])

U = []
for time,fn in enumerate(sorted(glob.glob('velz_out_t*.h5'))[1:]):
    with h5py.File(fn, 'r') as hf:
        U.append(hf['OutArray'][:])
        print ("time = "+str(time))
        print (U[-1])

if not np.all(np.isclose(U[1],expected,1e-7)):
    print("expected at t=1")
    print(expected)
    print("got:")
    print(U[1])
    print("difference:")
    print(U[1]-expected)
    print("Failure 🤦")
    raise RuntimeError(error)

if not np.all(np.isclose(U[7], expected,1e-7)):
    print("expected at t=4")
    print(expected)
    print("got:")
    print(U[7])
    print("difference:")
    print(U[7]-expected)
    print("Failure 🤦")
    raise RuntimeError(error)

print("Success 🙂")
