#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
# this test just checks that the simulation runs and produces
# output without crashing
import sys, glob, h5py
import numpy as np
np.set_printoptions(suppress=True)

U = []
for time,fn in enumerate(sorted(glob.glob('velz_out_t*.h5'))[1:]):
    with h5py.File(fn, 'r') as hf:
        U.append(hf['OutArray'][:])
        print ("time = "+str(time))
        print (U[-1])

print("Success 🙂")
