#!/usr/bin/env python3
#
if not 'Error: a triangle edge is larger than the halo' in open('LOG').readlines()[-1]:
    print("Failure 🤦")
    raise RuntimeError("Error not handled appropriately")
print("Success 🙂")
