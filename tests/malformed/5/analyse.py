#!/usr/bin/env python3
#
if not 'The domain decomposition is not compatible with halo size' in open('LOG').readlines()[-2]:
    print("Failure 🤦")
    raise RuntimeError("Error not handled appropriately")
print("Success 🙂")
