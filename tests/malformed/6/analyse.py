#!/usr/bin/env python3
#
if not 'Emergency stop. Ladd particle acceleration too large' in open('LOG').readlines()[-1]:
    print("Failure 🤦")
    raise RuntimeError("Error not handled appropriately")
print("Success 🙂")
