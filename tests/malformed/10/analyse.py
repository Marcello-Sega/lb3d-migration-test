#!/usr/bin/env python3
#
# We have to crawl through a few lines at the end of the log, since we don't exactly know which line the desired error will be located at:
for L in open('LOG').readlines()[-16:]:
    if 'Your results are known to be physically invalid' in L:
        print("Success 🙂")
        exit(0)
print("Failure 🤦")
raise RuntimeError("Error not handled appropriately")
