#!/usr/bin/env python3
#
if not 'Cannot match namelist object name %wrong_input' in open('LOG').readlines()[-1]:
    print("Failure 🤦")
    raise RuntimeError("Error not handled appropriately")
print("Success 🙂")
