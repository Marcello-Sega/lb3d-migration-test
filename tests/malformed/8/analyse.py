#!/usr/bin/env python3
#
if not 'FATAL ERROR: Domain decomposition smaller than twice the needed halo (set by particle radii and interaction cutoff)' in open('LOG').readlines()[-1]:
    print("Failure 🤦")
    raise RuntimeError("Error not handled appropriately")
print("Success 🙂")
