#!/usr/bin/env python3
#
if not 'One of the Ladd particles does not statisfy the stability criterion' in open('LOG').readlines()[-1]:
    print("Failure 🤦")
    raise RuntimeError("Error not handled appropriately")
print("Success 🙂")
