#!/usr/bin/env python3
#
if not 'FATAL ERROR: Ladd particles overlap. Check your potentials' in open('LOG').readlines()[-1]:
    print("Failure 🤦")
    raise RuntimeError("Error not handled appropriately")
print("Success 🙂")
