#!/usr/bin/env python3
#
if not 'problem reading relax_homogeneous' in open('LOG').readlines()[-1]:
    print("Failure 🤦")
    raise RuntimeError("Error not handled appropriately")
print("Success 🙂")
