#include "nskg.h"
program t_hdf5
  !> Unit test: HDF5 (parallel I/O)
  !>
  !> This tests the speed of accessing a small file.
  !> Note that the y,z domain is not divisible by two,
  !> forcing to slice it along x. Parallel read on 
  !> such a file written serially has trouble of NFS
  !> (the time for the 4x101x101 file should be ~1.5 sec 
  !> vs ~1E-3 in all other cases)
  use nskg_io_hdf5_module
  use nskg_parallel_module

  use nskg_parms_module
  use nskg_system_class

  implicit none
  real(kind=FP), dimension(:,:,:),allocatable :: array,array2
  real:: start_t,end_t
  integer i
  call features%init         
 
  SYST%box = [4, 101, 101]
  LB%ncomponents = 1
  allocate(SYS)
  SYST%hdf_use_parallel_read = .true. ! will be changeed below
  call SYS%init(.false.,SYST)

  allocate(array(nx,ny,nz))
  allocate(array2(nx,ny,nz))
  if (myrankc.eq.0) then
      print *, ''
      print *, ''
      print *, '=== test reading files written before ==='
      print *, ''
  endif
  do i=1,2
      !!!!SYST%hdf_use_parallel_read  = .not. SYST%hdf_use_parallel_read
      call cpu_time(start_t)
      call read_array_hdf5('density_generated_by_one_proc.h5',array)
      call cpu_time(end_t)
      if(myrankc.eq.0) print*,'read file generated serially, SYST%hdf_use_parallel_read=',SYST%hdf_use_parallel_read, ' time:', end_t - start_t

      call cpu_time(start_t)
      call read_array_hdf5('density_generated_by_two_procs.h5',array2)
      call cpu_time(end_t)
      if(myrankc.eq.0)print*,'reading file generated in parallel, SYST%hdf_use_parallel_read=',SYST%hdf_use_parallel_read, ' time:', end_t - start_t
      if(any(abs(array-array2).gt.1e-6)) then
              print*, 'arrays differ at line',__LINE__
              call MPI_Finalize()
              call exit(1)
      else
              if(myrankc.eq.0)print*, 'arrays are the same'
      endif
  end do

  if (myrankc.eq.0) then
      print *, ''
      print *, ''
      print *, '=== test writing a new file  ==='
      print *, ''
  endif
  call dump_scalar_phdf5(array,'density.h5')
  do i=1,2
      !!!!SYST%hdf_use_parallel_read  = .not. SYST%hdf_use_parallel_read
      call cpu_time(start_t)
      call read_array_hdf5('density.h5',array2)
      call cpu_time(end_t)
      if(myrankc.eq.0)print*,'reading new file generated using,',nprocs,'procs, SYST%hdf_use_parallel_read=',SYST%hdf_use_parallel_read,' time:',  end_t - start_t
      if(any(abs(array-array2).gt.1e-6)) then
              print*, 'arrays differ at line',__LINE__,'by at most',maxval(abs(array-array2))
              call MPI_Finalize()
              call exit(1)
      else
              if(myrankc.eq.0)print*, 'arrays are the same'
      endif
  end do

  call MPI_Barrier(comm_cart)
  if(myrankc.eq.0) then
          print *, ''
          print*, 'Success 🙂'
  endif
  call MPI_Finalize()

end program
