#include "nskg.h"
program t_bdist
  !> Unit test: Boltzmann distribution
  use nskg_features_class
  use nskg_system_class
  use nskg_lb_class
  use nskg_bc_module, only: nskg_bc_init,nskg_bc_advection_post
  use nskg_parms_module
  use nskg_parallel_module
  use nskg_parallel_globals_module
  use nskg_init_functions_module
  use nskg_log_module
  use nskg_helper_module
  use nskg_advection_module
  use nskg_relax_class
  use nskg_bdist_class
  use nskg_bdist_module, only: boltz_dist

  implicit none
  integer i,x,y,z

  real(kind=FP), allocatable :: initial(:,:)
  real(kind=FP), allocatable :: buffer_bdist(:,:)
  real(kind=FP)  tu(3)
  real(kind=FP)  uc, uu,tol
  real(kind=FP)  eqpop(19)
  character(len=CLIARG) :: arg
  call features%init         
  allocate(SYS)
  LB%ncomponents = 2
  LB%forcing_model ='guo' ! to use the barycentric velocity.
  !! read arguments from command line 
  call get_command_argument(1, arg)

  if(FP>4) then
         tol=1e-8
  else
         tol=1e-5
  endif 

  SYST%box = [2,2,2]
  features%relaxation_homogeneous%active=.true.
  
  call SYS%init(.false., SYST)
  allocate(initial(Lattice%geometry%sites,3))
  allocate(buffer_bdist(Lattice%geometry%sites,19))

  call RANDOM_NUMBER(initial)

  call nskg_init_sequence()
  lbinit%fr = 1.0
  call nskg_bc_init() !TODO MS SHOULD THIS BE HIDDEN IN nskg_init_system?
  Lattice%fields%total_density%is_required = .true.
  Lattice%fields%barycentric_velocity%is_required = .true.
  call Lattice%fields%outdate()
  call Lattice%fields%compute()
  call Lattice%bdist%compute()
  do x=1-halo_extent,nx+halo_extent
     do y=1-halo_extent,ny+halo_extent
        do z=1-halo_extent,nz+halo_extent
           tu = Lattice%fields%barycentric_velocity%array3d(x,y,z,1:3) 
           call boltz_dist(tu, (/0.0, 0.0, 0.0/), eqpop)
           eqpop = eqpop * Lattice%fields%c(1)%density%array3d(x,y,z)
           ! we use the first component to store the difference with the Boltzmann distribution computed with the old function
           Lattice%bdist%pop(1)%array3d(x,y,z,1:19) = Lattice%bdist%pop(1)%array3d(x,y,z,1:19)  - eqpop(:) 
           do i = 1, 19
                uc = dot_product(tu,c(i,:))
                uu = dot_product(tu,tu)
                Lattice%bdist%pop(2)%array3d(x,y,z,i) = eqpop(i) -  w(i) * Lattice%fields%c(1)%density%array3d(x,y,z) * &
                        & (1+uc*3.0 + uc*uc * 9./2. - uu*3./2.)
           end do
        enddo
     enddo
  enddo
  
  if (any(abs(Lattice%bdist%pop(2)%array_) > tol)) then
        write(*,*) 'The maximum difference with the boltzmann distribution is:',maxval(abs(Lattice%bdist%pop(2)%array_))
        stop 1
  endif 
  if (any(abs(Lattice%bdist%pop(1)%array_) > tol)) then
      write(*,*) 'Failure... The population differ by:', Lattice%bdist%pop(1)%array3d(1,1,1,1:19)
      stop 2
  endif 
  
  write(*,*) 'Success 🙂'

  
  
  call Lattice%fields%shifted_u%destruct()
  call Lattice%destruct()

  call MPI_Finalize()
end program
