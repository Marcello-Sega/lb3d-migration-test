#include "nskg.h"
program t_ad_elec
  !> Unit test: ELEC (Electrokinetics)
  use nskg_system_class, only: SYS, SYST
  use nskg_features_class
  use nskg_parms_module
  use nskg_init_functions_module
  use nskg_globals_module
  use nskg_fields_class
  use nskg_ad_class
  use nskg_elec_class
  use nskg_parms_lb_class
  integer :: i,nsteps=20
  real(kind=FP),dimension(64):: a,bp,bm
  real(kind=FP) :: meanp,meanm,meana,stdp,stdm,stda,diffusion_coeff
  integer :: initial_width = 15
  real(kind=FP) :: zdom(64) = (/(i, i=1,64)/)
  class(nskg_density_field), pointer :: f
  call features%init         
LB%ncomponents = 1
  features%elec%active = .true.
  halo_extent = 2
  diffusion_coeff = 0.1
  ek%diffusion_coeff = diffusion_coeff
  ek%elec_on_fluid = .false.
  allocate(SYS)
  SYST%box = [4,4,64]
   call SYS%init(.false.,SYST)
  SYS%Lattice%fields%barycentric_velocity%is_required=.true.
  SYS%Lattice%fields%total_density%is_required=.true.
  f => SYS%Lattice%fields%c(1)%density

  call nskg_init_const

  ! set up and compute by hand the diffusion in 1d
  ! This is so simple that it does not represent a problem and can be used as reference.
  a = 0.
  a(nz/2-(initial_width-1)/2:nz/2+(initial_width-1)/2) = 1./initial_width
  do i=1,nsteps
      a = a + (- 2 * a + (cshift(a,-1) + cshift(a,1))) * diffusion_coeff
  end do

  meana = sum(a(1:nz)*zdom(1:nz))
  stda = sum(a(1:nz)*zdom(1:nz)*zdom(1:nz))-(meana)**2

  ! let's compare it now with what the nskg code does

  ! make sure we switch to the homogeneous AD equation
  ! here we also have zero barycentric velocity field (=no advection)
  ! with the has_walls and has_colloids flags switched off, nskg uses
  ! the homogeneous diffusion algorithm.
  f%has_walls = .false.
  f%has_colloids = .false.
  SYS%elec%parms%diffusion_coeff = diffusion_coeff
  call SetAndSolveAD(bp,bm)
  if (any(abs(a-bp).gt.1e-10) .or. any(abs(a-bm).gt.1e-10)) error stop "Error #1 (wrong behaviour of the AD homogeneous solver)"

  ! Check AD for small velocities
  ! Diffusion has to be present to control the advection induced diffusion
  f%has_colloids = .false.
  SYS%elec%parms%diffusion_coeff = diffusion_coeff

  SYS%Lattice%fields%barycentric_velocity%array(:,3) = 0.2
  call SetAndSolveAD(bp,bm)
  if (sum(bp)-1..gt.1e-10) error stop "Error #4.1 (pos. ions) (wrong behavior in the advection (integral not conserved, vel<1)"
  if (sum(bm)-1..gt.1e-10) error stop "Error #4.2 (neg. ions) (wrong behavior in the advection (integral not conserved, vel<1)"
  meanp = sum(bp(1:nz) *zdom(1:nz))
  meanm = sum(bm(1:nz) *zdom(1:nz))
  stdp = sum(bp(1:nz) *zdom(1:nz)*zdom(1:nz))-meanp**2
  stdm = sum(bm(1:nz) *zdom(1:nz)*zdom(1:nz))-meanm**2
  if (abs(meanp-(nz/2+nsteps*0.2)).gt.1e-10) error stop "Error #5.1 (pos. ions) (wrong behavior in the advection (position, vel<1)"
  if (abs(meanm-(nz/2+nsteps*0.2)).gt.1e-10) error stop "Error #5.2 (neg. ions) (wrong behavior in the advection (position, vel<1)"
  ! we allow here a deviation of the diffusion width of 15%, but this generally depends a lot on the resolution of the distribution!
  if (abs(stdp-stda)/stda.gt.0.15) error stop "Error #6.1 (pos.ions, wrong behavior in the advection (width, vel<1)"
  if (abs(stdm-stda)/stda.gt.0.15) error stop "Error #6.2 (neg.ions, wrong behavior in the advection (width, vel<1)"

  ! now let's add Coulomb force in an external electric field
  ! setting beta=1,D=0.1 and a potential gradient of 2 should yield a velocity of 0.2
  SYS%Lattice%fields%barycentric_velocity%array(:,3) = 0.
  SYS%elec%phi%array = 0.0
  SYS%elec%parms%diffusion_coeff = 0.1
  do i=1-halo_extent,nz+halo_extent
      SYS%elec%phi%array3d(:,:,i) = -i*2.0
  end do
  call SetAndSolveAD(bp,bm)
  if (abs(sum(bp)-1).gt.1e-8) error stop "Error #7.1 (pos. ions) (wrong behavior in the Coulomb forcing, integral not conserved)"
  if (abs(sum(bm)-1).gt.1e-8) error stop "Error #7.2 (neg. ions) (wrong behavior in the Coulomb forcing, integral not conserved)"
  meanp = sum(bp(1:nz) *zdom(1:nz))
  meanm = sum(bm(1:nz) *zdom(1:nz))
  stdp = sum(bp(1:nz) *zdom(1:nz)*zdom(1:nz))-meanp**2
  stdm = sum(bm(1:nz) *zdom(1:nz)*zdom(1:nz))-meanm**2
  ! note: E = -grad(phi) = 2 z => F= qE > 0 acts on positive charges, that move towards larger z
  if (abs(meanp-(nz/2+nsteps/5)).gt.1e-7) error stop "Error #8.1 (pos. ions) (wrong behavior in the Coulomb forcing, position)"
  if (abs(meanm-(nz/2-nsteps/5)).gt.1e-7) error stop "Error #8.2 (neg. ions) (wrong behavior in the Coulomb forcing, position)"
  if (abs(stdp-stda)/stda.gt.0.15) error stop "Error #9.1 (pos.ions, wrong behavior in the Coulomb forcing, width)"
  if (abs(stdm-stda)/stda.gt.0.15) error stop "Error #9.2 (neg.ions, wrong behavior in the Coulomb forcing, width)"

  ! now let's activate simultaneously advection and  Coulomb force
  ! setting beta=1,D=0.1 and a potential gradient of 1, together with a flow velocity of 0.1 should yield an effective velocity of 0.2
  ! CAUTION: for negative ions, the Coulomb forcing and the advection will cancel each other, so the mean position must not change!
  SYS%Lattice%fields%barycentric_velocity%array(:,3) = 0.1
  SYS%elec%phi%array = 0.0
  SYS%elec%parms%diffusion_coeff = 0.1
  do i=1-halo_extent,nz+halo_extent
      SYS%elec%phi%array3d(:,:,i) = -i*1.0
  end do
  call SetAndSolveAD(bp,bm)
  if (abs(sum(bp)-1).gt.1e-8) error stop "Error #10.1 (pos. ions) (wrong behavior in the Coulomb forcing with advection, integral not conserved)"
  if (abs(sum(bm)-1).gt.1e-8) error stop "Error #10.2 (neg. ions) (wrong behavior in the Coulomb forcing with advection, integral not conserved)"
  meanp = sum(bp(1:nz) *zdom(1:nz))
  meanm = sum(bm(1:nz) *zdom(1:nz))
  stdp = sum(bp(1:nz) *zdom(1:nz)*zdom(1:nz))-meanp**2
  stdm = sum(bm(1:nz) *zdom(1:nz)*zdom(1:nz))-meanm**2
  if (abs(meanp-(nz/2+nsteps/5)).gt.1e-7) error stop "Error #11.1 (pos. ions) (wrong behavior in the Coulomb forcing with advection, position)"
  if (abs(meanm-(nz/2)).gt.1e-7) error stop "Error #11.2 (neg. ions) (wrong behavior in the Coulomb forcing with advection, position)"
  if (abs(stdp-stda)/stda.gt.0.15) error stop "Error #12.1 (pos.ions, wrong behavior in the Coulomb forcing with advection, width)"
  if (abs(stdm-stda)/stda.gt.0.15) error stop "Error #12.2 (neg.ions, wrong behavior in the Coulomb forcing with advection, width)"

  write(*,*) "Success 🙂"
  call MPI_Finalize()

contains
  subroutine SetAndSolveAD(bufferp,bufferm)
     integer :: i
     real(kind=FP),intent(inout), dimension(64) :: bufferp,bufferm
     SYS%elec%fields(1)%array = 0.0
     SYS%elec%fields(1)%array3d(:,:,nz/2-(initial_width-1)/2:nz/2+(initial_width-1)/2) = 1./initial_width
     SYS%elec%fields(2)%array = 0.0
     SYS%elec%fields(2)%array3d(:,:,nz/2-(initial_width-1)/2:nz/2+(initial_width-1)/2) = 1./initial_width

     do i=1,nsteps
      call SYS%elec%phi%compute_gradient()
      call SYS%elec%do_advection_diffusion_elec()
     end do
     bufferp = SYS%elec%rhop%array3d(nx/2,ny/2,1:nz)
     bufferm = SYS%elec%rhom%array3d(nx/2,ny/2,1:nz)
  end subroutine

end program
