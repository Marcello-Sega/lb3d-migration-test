# NSKG unit tests

Every folder in this directory contains

   - one single fortran source file of the same name as the folder
   - a CMakeLists.txt calling the function below to add the test to ctest

The single source file compiles into a single self-contained executable: A
unit test.
