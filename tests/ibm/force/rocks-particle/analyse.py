#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#This is a simple test for an IBM particle interacting with a LB rock wall
#The purpose is to test if the particle will move as expected
#in this minimal setup.

import numpy as np
from numpy import loadtxt
from sys import exit

#> Loading and reading particle data
d0 = loadtxt("reference.dat")

#> Loading and reading particle data
d1 = loadtxt("mesh_default_center.dat")

print('[Test]: An IBM particle with the skalak strain model interacting with a wall.')
print('Strain is switched on and bending is off.\n')
print('Volume conservation is switched on.\n')

#> We are tesing if after 10 time steps the particle is deforming and moving
#> similarly to the reference case calculated previously.

if not np.all(np.isclose(d0,d1,rtol=1e-6, atol=1e-6)):
    print('Results are different from the reference case! Something is wrong!')
    print("Failure 🤦")
    exit(1)
else:
    print('\nSuccess 🙂')
