#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#This is a simple test where we apply a body force on the particle along the z direction.
#The purpose is to test if the particle will move as expected in this minimal setup
# TODO is it possible to have an analytical estimate of the displacement?
import pandas as pd
from sys import exit
from glob import glob
import numpy as np

#> Loading and reading particle data
df = pd.read_csv('mesh_default_center_weighted.dat', sep='\s+', names=['time','x','y','z'], header=None)

#> Check if the displacement along the z axis is larger than 0
diff = df.z.values[1] - df.z.values[0]

print('[Test]: particle driven by a body force along the z direction.')
print('The body force is applied on the surface of the particle and not on the fluid.')
print('Strain and bending forces are switched off.\n')

print('Particle initial position:              [px, py, pz] = ',[df.x.values[0],df.y.values[0],df.z.values[0]])
print('Particle position after 25  time steps: [px, py, pz] = ',[df.x.values[1],df.y.values[1],df.z.values[1]])

#> We are tesing if after 100 time steps the particle manages to move or not
if (not np.isclose(diff, 0.330691)):
    print('The particle is not moving as expected! Something is wrong!')
#   TODO add these back once output implemented.
#    print('Force after 100 time steps:    [fx, fy, fz] = ',[d1.force_x.item(),d1.force_y.item(),d1.force_z.item()])
#    print('Velocity after 100 time steps: [vx, vy, vz] = ',[d1.vel_x.item(),d1.vel_y.item(),d1.vel_z.item()])
    print("Failure 🤦")
    exit(1)
else:
    print('\nSuccess 🙂')
