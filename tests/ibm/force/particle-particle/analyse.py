#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#This is a simple test for three IBM particles of which two interacting
# with a Coulomb-like interaction
#We compare the state of the particle after 100 time steps with a
#reference state computed previously.
#The purpose is to test if the particle will deform as expected
#in this minimal setup.

import numpy as np
from numpy import loadtxt
from sys import exit

#> Loading and reading particle data
d0 = loadtxt("reference.dat",skiprows=2)

#> Loading and reading particle data
d1 = loadtxt("mesh_default_area.dat",skiprows=2)

print('[Test]: Two interacting IBM particles via a Coulomb-like force')
print('Skalak strain is switched on and bending is off.\n')
print('Volume conservation is switched off.\n')
print('It is the case of an elastic capsule.\n')

#> We are tesing if after 100 time steps the particle is deforming and moving
#> similarly to the reference case calculated previously.

if not np.all(np.isclose(d0,d1,rtol=1e-6, atol=1e-6)):
    print('Results are different from the reference case! Something is wrong!')
    print("Failure 🤦")
    exit(1)
else:
    print('\nSuccess 🙂')
