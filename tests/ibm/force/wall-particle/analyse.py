#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#This is a simple test for an IBM particle interacting with a wall
#Planar IBM walls at wall_pos_z=2.5 and z=tnz-2.5
#An IBM droplet with a specific surface tension is placed at x=y=Lz/2 and z=R+sigma+wall_pos_z
# with R being the droplet radius and sigma the Lennard-Jones interaction parameter
#We compare the state of the particle after 1000 time steps with a
#reference state computed previously.
#The purpose is to test if the particle will move and deform as expected
#in this minimal setup.

import numpy as np
from numpy import loadtxt
from sys import exit

#> Loading and reading particle data
d0 = loadtxt("mesh_reference_center_weighted.dat",skiprows=2)

#> Loading and reading particle data
d1 = loadtxt("mesh_test_center_weighted.dat",skiprows=2)

print('[Test]: An IBM particle with the surface tension strain model interacting with a wall.')
print('Strain is switched on and bending is off.\n')
print('Volume conservation is switched on.\n')
print('It is the case of an elastic droplet.\n')

#> We are tesing if after 1000 time steps the particle is deforming and moving
#> similarly to the reference case calculated previously.

if not np.all(np.isclose(d0,d1,rtol=1e-3, atol=1e-3)):
    print('Results are different from the reference case! Something is wrong!')
    print("Failure 🤦")
    exit(1)
else:
    print('\nSuccess 🙂')
