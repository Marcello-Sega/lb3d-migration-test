#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#  Testing momentum conservation for a single particle coupled to the fluid.
#  The initial momentum is non-zero, due to constant fluid velocity and the particle being at rest.
#  This test is very sensitive, given the parameters chosen. Please don't change unless necessary.

import numpy as np
import h5py
import glob

input = f90nml.read("input")
file_tag = input["system"]["syst"]["file_tag"]
vol = np.product(input["system"]["syst"]["box"])
v_i = input["lb_initialization"]["lbinit"]["pr"]
np.seterr(all='raise')
tolerance = 1e-11


fluid_data     = sorted(glob.glob("velz_"+file_tag+"*_t000*.h5"))
fluid_rho_data = sorted(glob.glob(  "od_"+file_tag+"*_t000*.h5"))
mom,mass = [] , []

for i in range(len(fluid_data)):
    v = h5py.File(fluid_data[i], "r")["OutArray"][:]
    m = h5py.File(fluid_rho_data[i], "r")["OutArray"][:]
    mom.append(np.sum(v*m))

mom = np.asarray(mom)
print(mom)
if np.abs(mom[0]-v_i * vol) > tolerance:
    print('initial momentum:',mom[0])
    print('momentum prescribed:',v_i * vol)
    print("Failure 🤦")
    raise RuntimeError("Initial momentum not set properly")

if np.any(np.abs(mom-mom[0]) > tolerance):
    for i in range(len(mom)):
        print("Total momentum:",mom[i])
    print("Failure 🤦")
    raise RuntimeError("Momentum not conserved with ibm coupling")

 
print("Success 🙂")
