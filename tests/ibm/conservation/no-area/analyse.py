#!/usr/bin/env python3

"""
Test for surface deviations with a Neo-Hookean law:
---------------------------------------------------
This script reads the particle area evolution from the output file "mesh_default_area.dat"
and checks that the particle is expanding.

A neo-Hookean model is used in this test. The conservation of area is the result of the 
penalty function on the surface of the particle. The penalty function on the surface 
is switched off.  

"""

import numpy as np
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Load data from files
data = np.loadtxt("mesh_default_area.dat")

# Extract relevant data
time = data[:, 0]
area = data[:, 1]


expansion_factor = np.abs(area[-1]/area[0] - 1)
expected_factor  = 0.012498 # measured

# Check that the calculated area corresponds to the reference area
if np.isclose(expansion_factor,expected_factor, rtol=1e-3, atol=1e-3):
    print("\nGlobal area of the particle is expanding as expected")
else:
    print("\nGlobal area of the particle is not expanding as expected")
    print("Failure 🤦")
    exit(1)

print('Success 🙂')
