#!/usr/bin/env python3

"""
This test aims to verify the correctness of a rotation applied to an ellipsoid by comparing the calculated inclination angles between the rotated ellipsoid's principal axes and the reference unit vector (in the z-direction), and compares them to the reference angle. principal axes with a reference angle. The test reads the reference angle from a file called "geometry.dat" and the principal axes data from a file called "mesh_default_inertia_axes.dat" at two different time steps (iteration 0 and 10). It checks that the calculated angles do not change over time and that they correspond to the reference angle. By validating these conditions, the test ensures that the rotation is correctly applied and that the principal axes' inclination angles are consistent with the expected reference angle.
"""

import re
import numpy as np

# Read reference angle from geometry.dat
with open("geometry.dat", "r") as f:
    content = f.read()
    angle_match = re.search(r"angle=(\d+(\.\d+)?)", content)
    reference_angle = float(angle_match.group(1))

# Read principal axes from mesh_default_inertia_axes.dat
principal_axes = []
with open("mesh_default_inertia_axes.dat", "r") as f:
    for line in f:
        data = list(map(float, line.strip().split()))
        time = data[0]
        axes = np.array(data[1:]).reshape(3, 3).T
        principal_axes.append((time, axes))

# Calculate inclination angles and compare with reference angle
unit_vector_reference_axis = np.array([0, 0, 1])
previous_inclination_angles = None
for time, axes in principal_axes:
    inclination_angles = []
    for i in range(3):
        unit_vector_principal_axis = axes[:, i]
        cos_theta = np.dot(unit_vector_principal_axis, unit_vector_reference_axis)
        inclination_angle = np.arccos(cos_theta) * 180 / np.pi
        inclination_angles.append(inclination_angle)
    print(f"Inclination angles at time {time}: {inclination_angle}")

    if time > 0:
        assert np.allclose(inclination_angles, previous_inclination_angles, rtol=1e-6, atol=1e-6), "The orientation angle is changing in time while no force is applied on the particle. Something is wrong!"
    previous_inclination_angles = inclination_angles

# Check that the calculated angle corresponds to the reference angle
if np.isclose(inclination_angle, reference_angle, rtol=1e-6, atol=1e-6):
    print("\nThe calculated angle: "+ str(inclination_angle) +" corresponds to the reference angle "+str(reference_angle)+".")
    print('Success 🙂')
else:
    print("\nThe initial angle in `geometry.dat`:" + str(reference_angle) + " is different from the printed angle: " + str(inclination_angle) + ". Something is wrong!")
    print("Failure 🤦")
    exit(1)
