#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#  Testing volume conservation for a single droplet coupled to the fluid.
#  The initial momentum is roughy equal to 4*pi*R^3/3

import pandas as pd  
import numpy as np
from numpy import loadtxt
from sys import exit

#> Loading and reading particle data 
df = pd.read_csv('mesh_default_volume.dat',sep='\s+', header=None, names=['time', 'volume'])
if len(df) == 1:
    print("At least two timesteps are needed")
    print("Failure 🤦")
    exit(1)

#  This test is very sensitive, given the parameters chosen. Please don't change unless necessary.
diff = 100.*(df.volume.values[-1] - df.volume.values[0])/df.volume.values[0]

print('[Test]: elastic droplet with a given surface tension is not subjected force')

print('Particle initial volume:  = ',df[['volume']].values[0])
print('Particle volume after 500 time steps:  = ',df[['volume']].values[-1])

#> We are tesing if after 500 time steps the particle conserves its volume
if (abs(diff) > 0.1):
    print('The particle is not conserving its volume! Something is wrong!')
    print("Failure 🤦")
    exit(1)
else:
    print('\nSuccess 🙂')
