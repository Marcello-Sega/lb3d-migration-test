#!/usr/bin/env python3

"""
Test for the penalty for surface deviations:
--------------------------------------------
This script reads the particle area evolution from the output file "mesh_default_area.dat", 
and checks that in the last frame this is within tolerance of 1) that of  the initial frame
and 2) a reference value from the file "reference.dat"

A neo-Hookean model is used in this test. The conservation of area is the result of the 
penalty function on the surface of the particle.  

"""

import numpy as np
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Load data from files
data = np.loadtxt("mesh_default_area.dat")
ref_data = np.loadtxt("reference.dat")

# Extract relevant data
time = data[:, 0]
area = data[:, 1]
ref_area = ref_data[:, 1]  # assuming that the area data is in the second column


# Compute relative error on the area in (%) for the last data point
rel_err = 100 * np.abs(area[-1]/area[0] -1)

# Check that the calculated area corresponds to the reference area
if np.isclose(area[-1], area[0], rtol=1e-3, atol=1e-3):
    print("\nGlobal area of the particle is conserved, with a relative error of {:.6f}% with respect to the stress-free area".format(rel_err))
else:
    print("\nGlobal area of the particle is not conserved, with a relative error of {:.6f}% with respect to the stress-free area. Something is wrong!".format(rel_err))
    print("Failure 🤦")
    exit(1)

# To add another layer of security, compare the area with the reference value 
if np.allclose(area, ref_area, rtol=1e-6, atol=1e-6):
    print("\nTime evolution of the area matches the reference data.")
else:
    print("\nTime evolution of the area does not match the reference data. Something is wrong!")
    print("Failure 🤦")
    exit(1)

print('Success 🙂')
