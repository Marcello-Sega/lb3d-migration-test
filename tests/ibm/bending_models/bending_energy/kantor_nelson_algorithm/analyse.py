#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
# 
#We compute numerically the bending energy of a spherical particle, and compare it to the analytical value.
#

import pandas as pd 
import numpy as np 
from numpy import loadtxt

#> tolerence
tol = 3.

#> Bending modulus
kb=5.556e-4

#> Bending energy of a sphere
ana_ergb = 8*np.pi*kb

#> Loading and reading particle data 
erg_b = loadtxt("mesh_default_bending_energy.dat",skiprows=2)

#> Relative error  
err = 100.0*(erg_b[1] - ana_ergb)/ana_ergb

print('[Test]: bending energy of the particle is measured in a quiescent fluid.')
print('Strain force is switched off.\n')

print('Analytic bending energy:  Eb = ',ana_ergb)
print('Numerical bending energy: Eb = ',erg_b[1])

#> We are tesing if after 200 time steps the particle manages to move or not 
if (err.item() > tol):
    print('Error too large! Relative error (in %)= ', err)
    print("Failure 🤦")
    exit(1)
else:
    print('\nSuccess 🙂')
