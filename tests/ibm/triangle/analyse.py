#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Just tests that IBM is switched on, and a mesh can be read
# This test fails surprisingly often, so let's be extra verbose about why.
import sys

s_logfile='log'    # verbatim copy from "./input", not "./nskg.input"
try:
    unit=open(s_logfile)
except FileNotFoundError:
    print('File '+s_logfile+' not readable')
    print('Failure 🤦')
    sys.exit(1)

nmlread = f90nml.read("nskg.input")
s_log_via_nml = nmlread["system"]["syst"]["logfile"]

if s_logfile != s_log_via_nml:
    print('Warning: log file definition mismatch between input and output: "' + s_logfile + '" != "' + s_log_via_nml + '"')

l_logfile = unit.readlines()
joined = ''.join(l_logfile)

string_to_search_for = "Registering mesh, IBM, EBF timers"
found_ibm_timers = string_to_search_for in joined

if found_ibm_timers:
    print("Success 🙂")
else:
    print('Did not find "'+string_to_search_for+'" in output')
    print(joined)
    print('Failure 🤦')
    sys.exit(1)
