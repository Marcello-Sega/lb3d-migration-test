#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly
#
#This is a simple test where we impose a simple shear flow along the x direction.
#Planar walls at z=1 and z=tnz
#An IBM particle with a specific strain model is placed at x=Lz/2 and is fixed to the center.
#We compare the state of the particle after 1500 time steps with a
#reference state computed previously.
#The purpose is to test if the particle will move and deform as expected
#in this minimal setup.
#
#Ref. paper: 
#Krüger, Timm, Fathollah Varnik, and Dierk Raabe. 
#"Efficient and accurate simulations of deformable particles immersed in a fluid using a 
#combined immersed boundary lattice Boltzmann finite element method." 
#Computers & Mathematics with Applications 61.12 (2011): 3485-3505.

import numpy as np
from numpy import loadtxt
from sys import exit

#> Loading and reading particle data
d0 = loadtxt("reference.dat",skiprows=3)

#> Loading and reading particle data
d1 = loadtxt("mesh_default_main_axes.dat",skiprows=3)

print('[Test]: An IBM particle with the Skalak old strain model subject to a simple shear flow.')
print('Strain is switched on and bending is off.\n')

#> We are tesing if after 1500 time steps the particle is deforming and moving
#> similarly to the reference case calculated previously.

if not np.all(np.isclose(d0,d1,rtol=1e-3, atol=1e-3)):
    print(d0)
    print(d1)
    print('Results are different from the reference case! Something is wrong!')
    print("Failure 🤦")
    exit(1)
else:
    print('\nSuccess 🙂')
