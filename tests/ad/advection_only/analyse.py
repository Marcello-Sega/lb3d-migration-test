#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# AD test: advection only (zero diffusion coefficient). The AD field is initialized with a cube,
# which the advection moves once through the system in z direction. After one loop across
# periodic boundaries, it arrives at its position from `loop_time` snapshots earlier, and we
# compare if the block has stayed (roughly) intact.

import sys, os.path, string
from glob import glob
import numpy as np
import f90nml as nml
import h5py

input_file = nml.read("input")
file_tag = input_file["system"]["syst"]["file_tag"]
loop_time = 1.0*input_file["system"]["syst"]["box"][2]/input_file["lb_initialization"]["lbinit"]["pr"]/input_file["advection_diffusion"]["ad"]["dump_interval"]
print("Determined AD repeating period of ", loop_time, " snapshots per cycle (as determined from box size and z velocity of the fluid)")
if (not np.isclose(loop_time, int(loop_time))):
    print("Test configured wrongly, loop time not an integer")
    exit(1)
loop_time = int(loop_time)

fields = sorted(glob('ad_field1_'+file_tag+'_t*.h5'))
files = (fields[-1], fields[-1-loop_time])
last_field  = np.array(h5py.File(files[1],'r')['OutArray'])
first_field = np.array(h5py.File(files[0],'r')['OutArray'])

avg_deviation = np.mean(np.abs(last_field-first_field))
print("Comparing fields ", files)

tolerance = 2e-3    # This limit was determined by comparing the periodic recurrences. If the cube is not aligned with its past, the `avg_deviation` grows up to about 0.074, i. e. some 40 times the minimum value. Due to the finite precision of the integration, the dynamics of this tolerance cannot be much larger.
if (not np.isclose(avg_deviation, 0, atol=tolerance)):
    print("AD field densities between periods differ by", avg_deviation, "on average")
    print("Failure 🤦")
    exit(1)

print("Success 🙂")
