#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# Note: do not increase the tolerance (atol=1e-14)

from glob import glob
import numpy as np

restored_files = sorted(glob('ladd_particles_step_*.dat'))
reference_files = sorted(glob('ladd_particles_full_*.dat'))

for i,f in enumerate(restored_files):
    print('analysing', restored_files[i])
    reference = np.loadtxt(reference_files[i])
    restored  = np.loadtxt(restored_files[i])
    if not np.all(np.isclose(reference,restored, rtol=1e-14, atol=1e-14)):  # We need a finite rtol to avoid test failures in case of cancellation noise at values on the order of unity.
        print('reference ',reference_files[i])
        print(reference)
        print('restored ',restored_files[i])
        print(restored)
        print("Failure 🤦")
        exit(1)

print("success 🙂")
