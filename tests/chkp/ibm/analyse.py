#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

import numpy as np
from glob import glob
from sys import exit

reference_files = sorted(glob('mesh_full_*.dat'))
restored_files  = sorted(glob('mesh_default_*.dat'))

for i,f in enumerate(reference_files):
    reference = np.loadtxt(reference_files[i])
    restored = np.loadtxt(restored_files[i])
    if not np.all(np.isclose(reference,restored,atol=1e-7,rtol=1e-5)):
       print("Failure 🤦")
       exit(1)
print("success 🙂")
