#!/usr/bin/env python3
import f90nml
f90nml.read("nskg.input") # this tests that nskg.input has been written correctly

# -  run 10 timesteps in a row (full) and
#         5 and then 5 after loading from checkpoint (step)
# -  load output densities for fields in `check`
# -  compare values of last timestep
#

import sys, glob, h5py
import numpy as np

# checked nskg output variables
check = ['od', 'sur', 'wd' , 'velocity']

# last timestep (as zero-padded string to be used in the filename)
timestep = '10'.rjust(8, '0')

# will which values were recovered sucessfully from checkpoint
success = {}

for var in check:
    fullfn = '{v}_full_t{t}.h5'.format(v=var,t=timestep)
    stepfn = '{v}_step_t{t}.h5'.format(v=var,t=timestep)
    with h5py.File(fullfn, 'r') as full, h5py.File(stepfn, 'r') as step:
        success[var] = (full['OutArray'][:] == step['OutArray'][:]).all()
        if(not success[var]):
            print(fullfn)
            print(full['OutArray'][:,:,1]) 
            print(stepfn)
            print(step['OutArray'][:,:,1]) 
            f_different = np.sum(1.-1*(full['OutArray'][:] == step['OutArray'][:]))/np.size(full['OutArray'])
            print('(EE) Wrong restart for', var, 'with', 100*f_different, '% wrong fields')

if not all(success.values()):
    print("not all fields recovered properly:")
    print(success)
    print("Failure 🤦")
    raise RuntimeError("Wrong start from checkpoint")

print('success 🙂')
