# Meshes

Find mesh particles of various resolution here, both in gmsh ASCII mesh format (`*.msh`), and VTK (`*.vtk`).

`sph_ico_<N>.*`

: Sphere (icosahedral triangluation), `<N>` triangles, radius 1, diameter 2

`rbc_ico_<N>.*`

: Biconcave disk (model for erythrocytes/red blood cells, icosahedral triangluation), `<N>` triangles, diameter 2, maximum thickness 0.655, central thickness 0.207
