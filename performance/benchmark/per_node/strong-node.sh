#!/bin/bash
# for jureca and Intel; export I_MPI_FABRIC=shm:ofa
CORESPN=24
TYPE=strong_node
DATE=`date +%Y%m%d%H%M%S`
OUTCSV=out/${TYPE}.${DATE}.csv
ACCOUNT=${ACCOUNT:-jiek11}

mkdir -p "in"
mkdir -p "out"
mkdir -p "logs"

echo nodes,cores,corespn,size,time,lu > $OUTCSV

for MULT in 1 2 4 8 16 32 64 128 256
do
  SIZEX=$[ 18 * $CORESPN ]
  SIZEY=$[ 18 * $CORESPN ]
  SIZEZ=$[ 18 * $CORESPN / 2 ]
  NITER=`echo | awk "{ print int(12000. * 5 * ${MULT} / 256.)}"`
  CORES=$[ $MULT * $CORESPN ]
  NODES=$[ ($CORES+$CORESPN-1)/ $CORESPN ]
  SIZE=$NODES
  OUTLOG="logs/${TYPE}.${DATE}.s${SIZE}.c${CORES}.log"

  echo "running for $TYPE scaling with system size $SIZEX on $CORES cores with $CORESPN cores per node"

  sed -e s/NN/${SIZE}/ \
      -e s/NX/${SIZEX}/ \
      -e s/NY/${SIZEY}/ \
      -e s/NZ/${SIZEZ}/ \
      -e s/NITER/${NITER}/ \
      input >| "in/input.${SIZE}"

  (
    srun --contiguous --ntasks-per-core=1 --job-name=weak_core --time=10:00 --export=ALL \
        -N $MULT -n $CORES -o $OUTLOG -A $ACCOUNT \
        ../nskg/src/nskg -f "in/input.${SIZE}"

    TIME=`grep -m 1 "Total.*time" $OUTLOG | awk '{ print $5 }'`
    LU=`grep -o ">.*site updates" $OUTLOG | awk '{ print $2 }'`
    echo $NODES,$CORES,$CORESPN,$SIZEX,$TIME,$LU > $OUTCSV.$MULT
  ) &
done

wait

cat $OUTCSV.* > $OUTCSV

rm $OUTCSV.*
rm -f coords_*.txt
