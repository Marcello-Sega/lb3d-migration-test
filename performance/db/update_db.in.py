#!/usr/bin/env python3
# Add statistics to the NSKG performance database, by reading "nskg.performance.log" supplemental output files of NSKG. For more documentation, see the message of git commit 18286c39472fa00b9130d219a82a93f2d7da8647

# Usage (inside the build directory):
#	./update_db.py path/to/nskg.performance.log [path/to/another/nskg.performance.log]

import argparse
parser = argparse.ArgumentParser(description='Add statistics to the performanc database of NSKG.')
parser.add_argument('files', type=str, nargs='+',
                   help='files to be parsed')

args = parser.parse_args()

import pandas as pd
from glob import glob

DB='db.csv'
def add_set(files):
    df=None
    def add(file):
        df2 = pd.read_csv(file,sep='=',comment='!',header=None,names=['key','val'])
        df2.val = df2.val.str.replace('"',"").replace("'","").replace("\s+"," ",regex=True)
        df2.key = df2.key.str.replace("\s+","",regex=True)
        df2 =df2.drop(df2[df2.key=='/'].index)
        df2 =df2.drop(df2[df2.key==''].index)
        keys,vals=df2.key.values, [[e] for e in df2.val.values]
        dct=dict(zip(keys,vals))
        df2 = pd.DataFrame.from_dict(dct)
        df2.fillna(True,inplace=True)
        return df2

    for file in files:
        try:
            tmp = add(file)
            df=df.append(tmp,ignore_index=True)
            df.fillna(False,inplace=True)
        except:
            df = add(file)
            
    for f in df.columns:
        df[f]=pd.to_numeric(df[f],errors='ignore',downcast='integer')
        
    return df
new =add_set(args.files)
try:
	df = pd.read_csv(DB).replace('False',False).replace('.false.',False).replace('.true.',True).replace('True',True)
	df2 = df.append(new,ignore_index=True).fillna(False)
except:
	df2 = new.fillna(False).replace('False',False).replace('.false.',False).replace('.true.',True).replace('True',True)
labels=df2.columns.difference(['Mlups','Mlups/core','logfile','Version'])
maxes=df2.groupby(labels.to_list())['Mlups'].transform(max)
df2 = df2.loc[df2['Mlups'] == maxes].drop_duplicates()
df2.to_csv(DB,index=None)

