#!/usr/bin/env python3
import argparse

parser = argparse.ArgumentParser(description='Print statistics from the performanc database of NSKG.')
parser.add_argument('--group', type=str, default=None, nargs='+', help='select one (or more) groups (choices: LB SC CG AD TH)')
parser.add_argument('--version', action='store_true', help='add version information')

try:
    from tabulate import tabulate
    parser.add_argument('--style', type=str, default='psql', help='output style, e.g. rst, github, latex, html, ... see pypi.org/project/tabulate/')
except:
    pass
args = parser.parse_args()

import pandas as pd
DB = pd.read_csv('@CMAKE_CURRENT_BINARY_DIR@/db.csv')

keys=['&shan_chen','&peng_robinson','&color_gradient','&advection_diffusion','&thermal']
printk=['processor','ncomponents','Mlups/core']
if args.version:
    printk+=['Version']
    print(printk)

selector={
        'LB' :['nothing' in e for e in keys],
        'SC' :['shan_chen' in e for e in keys],
        'CG' :['color_gradient' in e for e in keys],
        'PGR':['peng_robinson' in e for e in keys],
        'TH' :['thermal' in e for e in keys],
        'AD' :['advection_diffusion' in e for e in keys]
        }

if args.group is None:
    print(DB.set_index(keys).sort_index()[printk])

else:
    for group in args.group:
        try:
            select=(DB[keys] == selector[group] ).all(axis=1)
            print(group)
            if args.style == 'rst':
                print('#'*len(group))
            try:
                print(tabulate(DB[select][printk], headers='keys', tablefmt=args.style,showindex='never'))

            except:
                print(DB[select][printk])
        except:
            print('problem selecting',group)

