# Performance DB

Introduces a database of performances, to be used
to organize in a more systematic way the performance
tests in the pipelines. The database is stored in
csv format in the file `./db.csv`

This file should be updated only using the
`./update_db.py` script (in the cmake build directory). This script parses
the performance log files `ib3d.performance.log`, 
produced by the `nskg` executable, and updates `db.csv` when
a higher performance is detected.

After running the performance tests, it is
possible to sweep them all with, e.g.,

```
find -name 'nskg.performance.log'|xargs python3 update_db.py
```

The results can be shown, e.g. using

```
python3 ./print_db.py --group LB  --style=rst

=========================================  ===========  ==========
processor                                  ncomponents  Mlups/core
=========================================  ===========  ==========
Intel(R) Xeon(R) Gold 6140 CPU @ 2.30GHz             1       25.17
Intel(R) Xeon(R) CPU E5-2695 v3 @ 2.30GHz            1       15.48
AMD EPYC 7662 64-Core Processor                      1       24.79
=========================================  ===========  ==========
```
