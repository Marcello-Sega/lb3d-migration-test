add_library(nskg_bclb nskg_leesedwards.F90
                      nskg_invasion_inv.F90
                      nskg_invasion_vel.F90
                      nskg_invasion_slip.F90
                      nskg_invasion_misc.F90
                      nskg_invasion_evap.F90
                      nskg_invasion.F90
)

target_link_libraries(nskg_bclb nskg_md nskg_parms_bc ${MPI_Fortran_LIBRARIES}) # Yes, md depends on lb...FIXME?

target_include_directories( nskg_bclb PUBLIC ${CMAKE_CURRENT_BINARY_DIR}
                                     PRIVATE $<TARGET_PROPERTY:nskg_md,INTERFACE_INCLUDE_DIRECTORIES>
                                             ${MPI_Fortran_MODULE_DIR})
