#include "nskg.h"

module nskg_laxwendroff_class
!> Lax-Wendroff PDE solver
!>
!> Flux limiters:
!>
!> * none
!> * Superbee
!> * Van Leer
!>
!> Select via
!> ```fortran
!> solver%init(..., method='1st-order{,-superbee,-vanleer}', ...)
!> ```
!>
!> @TODO Literature references

   use nskg_common_headers_macro
   use nskg_globals_module, only:  cx,cy,cz
   use nskg_geometry_class
   use nskg_rocks_class, only: nskg_rocks
   use nskg_scalar_field_class
   use nskg_vector_field_class
   use nskg_array_class
   use nskg_halo_class, only: nskg_halo
   use nskg_cshift_module, only: nskg_cshift
   use nskg_finite_difference_abstractclass,only: nskg_finite_difference_solver

   implicit none
   private

   type, extends(nskg_finite_difference_solver) :: nskg_laxwendroff_solver
      class(nskg_scalar_array),pointer          :: neighadv
   contains
      procedure, pass(this)                    :: step
      procedure, pass(this)                    :: init
      procedure, pass(this)                    :: step_laxwendroff_dimensional_split_order_1
   end type

   public nskg_laxwendroff_solver

contains
      subroutine init(this,geometry,halo,method,walls,&
                     & delta,  neighwalls, neighadv,forceout)
      class(nskg_laxwendroff_solver),target      :: this
      type(nskg_geometry), target                :: geometry
      type(nskg_halo), target                    :: halo
      character(len=*),intent(in)               :: method
      class(nskg_scalar_field), optional, target :: delta
      class(nskg_scalar_array), optional, target :: walls, neighwalls
      class(nskg_scalar_array), optional, target :: neighadv
      class(nskg_vector_array), optional, target :: forceout

      call this%basic_constructor(geometry = geometry,halo = halo,&
              & delta = delta, walls = walls, neighwalls=neighwalls, &
              & method = method, forceout = forceout)
      if (.not.present(neighadv)) then
              allocate(this%neighadv)
              call this%neighadv%init(geometry)
      else
              this%neighadv=> neighadv
      endif
      end subroutine

    subroutine step(this, field, adv)
        class(nskg_laxwendroff_solver), target  :: this
        class(nskg_scalar_field), intent(inout) :: field
        class(nskg_vector_field), intent(inout) :: adv
        this%forceout%array = 0.0 !the force *must* be initialized
        select case(trim(this%method))
        case ('1st-order','1st-order-superbee','1st-order-vanleer')
                call this%step_laxwendroff_dimensional_split_order_1(field, adv)
        case default
                error stop 'Method not implemented in FTCS solver (nskg_laxwendroff_class.F90)'
        end select
    end subroutine

    subroutine step_laxwendroff_dimensional_split_order_1(this,field,adv)
        !> @Note: adv is expected to be such that adv = F/rho (for forces) or adv = u (for the velocity)
        class(nskg_laxwendroff_solver), target :: this
        type(nskg_scalar_field), intent(inout) :: field
        type(nskg_vector_field), intent(inout) :: adv
        type(nskg_scalar_field), save          :: temp,temp2,upwind,r
        integer                               :: dir, s,sig
        integer,dimension(3)                  :: shift
        if(.not.temp%initialized) call temp%init(this%geometry)
        if(trim(this%method).ne.'1st-order') then
            if(.not.upwind%initialized) call upwind%init(this%geometry)
            if(.not.temp2%initialized)  call temp2%init(this%geometry)
            if(.not.r%initialized)      call r%init(this%geometry)
        endif
        if (this%has_walls) then
           select type(walls=>this%walls)
           class is(nskg_rocks)
           do dir=1,3
              where(walls%is_wall())
                adv%array(:,dir) = 0.0
              end where
           end do
           class default
              error stop "internal error in step_laxwendroff_dimensional_split_order_1"
           end select
        endif
        do dir=1,3 ! xyz
          this%delta%array = 0.0
          ! dimensional splitting, 1st order
          do s=2*dir-1,2*dir  !left/right
            shift = (/ cx(s) ,  cy(s) ,  cz(s) /)
            sig = shift(dir)
            call nskg_cshift(field%array,this%neigh%array, this%geometry%width, -shift)
            call nskg_cshift(adv%array(:,dir),this%neighadv%array, this%geometry%width, -shift)
            if (this%has_walls) then
                call nskg_cshift(this%walls%array,this%neighwalls%array, this%geometry%width, -shift)
                !> zero-gradient boundary condition: put specular force across the boundary
                where(abs(this%neighwalls%array)>0.0)
                   this%neighadv%array = -adv%array(:,dir)
                endwhere
            endif
            if (trim(this%method).eq.'1st-order') then
                temp%array =  0.25 *  &
                        & (this%neighadv%array + adv%array(:,dir)) * &
                        & (field%array * (1.0 + sig * adv%array(:,dir)) + &
                        &  this%neigh%array * (1.0 - sig * this%neighadv%array))
                ! now we need the force by averageing +1/2 and -1/2, hence the factor 0.5:
                this%forceout%array(:,dir) = this%forceout%array(:,dir) + temp%array * 0.5
                this%delta%array =  this%delta%array + sig * temp%array
            else
                ! upwind
                upwind%array = 0.0
                where(sig*adv%array(:,dir).ge.0)
                    upwind%array = field%array
                endwhere
                where(sig*this%neighadv%array.lt.0)
                    upwind%array = upwind%array + this%neigh%array
                endwhere
                !> @TODO optimize here, if needed.
                ! r =  (state - np.roll(state, 1))
                ! cond = np.where(grad<0)
                ! r[cond] =  (np.roll(state,-2) - np.roll(state,-1))[cond]
                ! denominator = np.roll(state,-1) - state + 1e-12
                ! r/=denominator

                call nskg_cshift(field%array,r%array, this%geometry%width,abs(shift))
                call nskg_cshift(field%array,temp%array, this%geometry%width,-abs(shift))
                call nskg_cshift(temp%array,temp2%array, this%geometry%width,-abs(shift))
                r%array = field%array - r%array
                where(adv%array(:,dir) .lt.0)
                     r%array =  temp2%array - temp%array
                endwhere
                temp%array = temp%array - field%array
                where(abs(temp%array) .lt. 1e-24)
                        temp%array  = 1e-24
                endwhere
                where (r%array .gt. 1e24)
                        r%array = 1e24
                endwhere
                where (r%array .lt. -1e24)
                        r%array = -1e24
                endwhere
                r%array = r%array / temp%array
                if (trim(this%method).eq.'1st-order-superbee') then
                    ! cond = np.logical_and(r>=0,r<=0.5)
                    ! psi[cond] = 2 * r[cond]
                    ! cond = np.logical_and(r>0.5, r<1.0)
                    ! psi[cond] = 1.0
                    ! cond = np.logical_and(r>1.0, r<2.)
                    ! psi[cond] = r[cond]
                    ! cond = r>2.0
                    ! psi[cond] = 2.0
                    where (r%array.lt.0) ! can this happen?
                            temp2%array = 0.0
                    endwhere
                    where (r%array.ge.0 .and. r%array.le.0.5)
                            temp2%array = 2.0 * r%array
                    endwhere
                    where (r%array.gt.0.5 .and. r%array.lt.1.0)
                            temp2%array = 1.0
                    endwhere
                    where (r%array.gt.1.0 .and. r%array.lt.2.0)
                            temp2%array = r%array
                    endwhere
                    where (r%array.gt.2.0)
                            temp2%array = 2.0
                    endwhere
                else if (trim(this%method).eq.'1st-order-vanleer') then
                    ! cond = np.where(r>=0)
                    ! psi[cond] = 2. * (r / (1+r))[cond]
                    where (r%array.lt.0) ! can this happen?
                            temp2%array = 0.0
                    endwhere
                    where (r%array.ge.0)
                            temp2%array = 2.0 * r%array / (1. + r%array)
                    endwhere
                endif

                if (sig.lt.0) call nskg_cshift(temp2%array,temp2%array, this%geometry%width,abs(shift))

                !flux_p = 0.5 * state * (1 + grad) +  0.5 * np.roll(state * (1-grad),-1)
                temp%array  = 0.5 * field%array * (1.0 + sig * adv%array(:,dir)) + &
                              0.5 * this%neigh%array * (1.0 - sig * this%neighadv%array)
                !flux_p = (psi_p) * (flux_p - low_p) + low_p
                temp%array  =  temp2%array * ( temp%array - upwind%array) + upwind%array

                ! grad_pa = (np.roll(grad,-1) + grad) * 0.5
                ! grad_m  = (np.roll(grad, 1) + grad) * 0.5

                ! force_p = flux_p * grad_p
                ! force_m = flux_m * grad_m

                temp%array  =  temp%array * 0.5* ( this%neighadv%array   + adv%array(:,dir))

                ! now we need the force by averageing +1/2 and -1/2, hence the factor 0.5:
                this%forceout%array(:,dir) = this%forceout%array(:,dir) + temp%array * 0.5
                this%delta%array =  this%delta%array + sig * temp%array
            endif
          end do
          if (this%has_walls) then
            where(abs(this%walls%array) < tiny(1.0))
                 field%array = field%array - this%delta%array
            endwhere
          else
                 field%array = field%array - this%delta%array
          endif
        end do
    end subroutine
end module

