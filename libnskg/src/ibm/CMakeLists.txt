add_library(nskg_ibm
         nskg_ibm_class.F90
)
target_link_libraries(nskg_ibm nskg_mesh ${MPI_Fortran_LIBRARIES})
target_include_directories( nskg_ibm PUBLIC  ${CMAKE_CURRENT_BINARY_DIR} ${MPI_Fortran_MODULE_DIR})

