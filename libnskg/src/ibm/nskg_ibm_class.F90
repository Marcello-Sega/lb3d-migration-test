#include "nskg.h"

module nskg_ibm_class
!> # The Immersed Boundary method (IBM), high-level frontend
!>
!> IBM introduces soft, triangulated surfaces (“membranes”), and calculates momentum exchange with the LB fluid.
!>
!> For a wrap-up of the concepts and methods, and a introduction into the user interface, see the [dedicated
!> documentation page](../|page|/parts/ibm.html).
!>
!> The IBM implementation of NSKG makes heavy use of fragmentation and modularization, in order to encourage
!> code reuse between different parts of the code.
!> IBM membranes are sets of offlattice objects; the IBM structure itself is built around the set of meshes
!> of the individual IBM particles. Meshes consist of (Lagrangian) points, enriched with the information
!> about their interconnection etc...
!> Dynamics, internal & external forces, interaction with the LB fluid, etc., are implemented at the lowest
!> possible level. Hence the IBM frontend itself just consists of the parts of the user interface and the
!> integration steps peculiar to IBM, and not reused by related methods like EBF.

  use nskg_common_headers_macro
  use nskg_log_module
  use nskg_features_class
  use nskg_globals_module
  use nskg_mesh_set_class, only: nskg_parms_mesh_set,nskg_mesh_set
  use nskg_timer_module
  use nskg_io_interface, only: nskg_try_open_namelist

  implicit none

  private

  type,extends(nskg_parms_mesh_set) :: nskg_parms_ibm
  contains
    procedure, nopass              :: read_namelist => read_namelist_ibm
    procedure, nopass              :: log_parms => log_parms_ibm
  end type        

  type,extends(nskg_mesh_set)       :: nskg_ibm
  contains        
      procedure, pass(this)        :: integration_step
  end type        

  class(nskg_ibm), pointer          :: IBM

  type(nskg_parms_ibm),target :: IB  = nskg_parms_ibm(&
    shortname='ibm',longname        = 'Immersed Boundary Method',&
    interpolation_range             = 2                         ,&
    time_step_dump_vtk              = 0                         ,&
    halo                            =-1.0                       ,&
    number_random_lagrangian_object = 0                         ,&
    meshnumbers                     = 0                         ,&
    meshfiles                       = ''                        ,&
    geometry_file                   = ''                        ,&
    restore_file                    = ''                        ,&
    fix_position                    = .false.                   ,&
    compute_inertia_ellipsoid       = .false.                   ,&
    time_step_dump_mesh_properties  = 0                         ,&
    steady_step                     = 0                         ,&
    soft_warning                    = .false.                    &
  )
  
  namelist /immersed_boundary/ IB

  public nskg_parms_ibm, IBM,IB,nskg_ibm

contains

   subroutine log_parms_ibm()
       log_parms_template(IB,immersed_boundary)
   end subroutine

  subroutine read_namelist_ibm()
    if (nskg_try_open_namelist(unit=42, file=features%ibm%inp_file)) then
      read_catch_namelist_error_macro(42,immersed_boundary,'immersed_boundary')
      close(unit=42)
    endif
  end subroutine


  subroutine integration_step(this, use_fluid)
     class(nskg_ibm)    :: this
     logical, optional :: use_fluid
     logical           :: use_fluid_local
     use_fluid_local=.false.
     if(present(use_fluid)) use_fluid_local = use_fluid
     TIMEIT(ti_ibm_compute_vertices_velocity, call this%compute_vertices_velocity(use_fluid_local) )
     TIMEIT(ti_ibm_exchange_halo            , call this%exchange_halo() )
     TIMEIT(ti_ibm_forward_euler            , call this%forward_euler_massless )
  end subroutine

end module
