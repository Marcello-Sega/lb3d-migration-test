#include "nskg.h"

module nskg_types_module


  use nskg_common_headers_macro
!> Defines the datatypes `nskg_site`, and `radial_model`.
!>
!> @Warning: If you change the nskg_site type, then you MUST ALWAYS change the
!> `MPI_Type` definitions in nskg_parallel.f90, subroutine `nskg_parallel_init()`

  use nskg_globals_module

  implicit none
  public

  !! Used for: initializing droplets
  type radial_model
    real(kind=FP) :: n_r, n_b, n_s
    integer :: dip
  end type radial_model

  !! @todo Used for?
  type bc_check
    sequence ! Contiguous in memory, please..
    integer, dimension(19) :: n
  end type bc_check

  contains

end module nskg_types_module
