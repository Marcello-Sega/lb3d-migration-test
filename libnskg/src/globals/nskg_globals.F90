#include "nskg.h"

module nskg_globals_module

  use nskg_common_headers_macro
  !> This module defines constants, needed/shared by many parts of the program

  implicit none

  public
  character(len=256) :: iomsg
    !> to read namelists, see macro in libnskg/src/nskg.h
  character(len=4096), dimension(256) :: tmpstr
    !> to write namelists  to nskg.input, see macro in libnskg/src/nskg.h
  integer            :: iostat
    !> same as iomsg
  public makedatestr

  integer, parameter :: MAX_COMPONENTS = 3
    !> Maximum number of LB components
  integer, parameter :: MAX_PAIRS = MAX_COMPONENTS*(MAX_COMPONENTS-1)/2
    !> Maximum number of interfaces between LB components (3 for 3 components)
  integer, parameter :: MAX_INTERACTIONS = MAX_COMPONENTS*(MAX_COMPONENTS+1)/2
    !> Maximum number of interaction combinations LB components (6 for 3 components)
  integer, save :: myrankc
    !> MPI rank of the current process (Comm_Cart)

  real(kind=FP), parameter :: pi = 3.1415926535897932384626433832795028841971693993751
    !> π
#ifdef ESMALL_ZERO
  real(kind=FP), parameter :: esmall = 0.0
#else
  real(kind=FP), parameter :: esmall = tiny(1.0)
#endif

  integer, parameter :: nd = 3
    !> Number of spatial dimensions of the system. (This is just here to avoid
    !> the magic constant `3` to appear everywhere; in NSKG, the three dimensions
    !> are hard-coded (down to the naming).

  !> These parameters are a result solely of the lattice chosen.

  !> Number of different lattice vectors.
  !>
  !> Contains the number of independent vectors (19), which
  !> corresponds to the size of the population arrays of each site.
  !> When operating on all elements of such an array, run the loop
  !> from `1` to `nvecs`.
  integer, parameter :: nvecs = 19
    !> Number of vectors (populations)
  integer, parameter :: nnonrest = nvecs-1
    !> Number of non-resting vectors (moving populations)

  !> c(x,y,z) are the the x,y,z components of the lattice vectors
  !> [as described in the manual](../|page|/nskg_intro.html#sec:nskg_algorithm).
  integer, parameter :: cx(nvecs) = &
    (/ 1,-1, 0, 0, 0, 0, 1, 1, 1, 1,-1,-1,-1,-1, 0, 0, 0, 0, 0 /)
  integer, parameter :: cy(nvecs) = &
    (/ 0, 0, 1,-1, 0, 0, 1,-1, 0, 0, 1,-1, 0, 0, 1, 1,-1,-1, 0 /)
  integer, parameter :: cz(nvecs) = &
    (/ 0, 0, 0, 0, 1,-1, 0, 0, 1,-1, 0, 0, 1,-1, 1,-1, 1,-1, 0 /)

  real(kind=FP), parameter,private :: w0 = 1.0/3.0
  real(kind=FP), parameter,private :: w1 = 1.0/18.0
  real(kind=FP), parameter,private :: w2 = 1.0/36.0
  real(kind=FP), parameter :: w(nvecs) = &
    (/ w1, w1, w1, w1, w1, w1, &
       w2, w2, w2, w2, w2, w2, w2, w2, w2, w2, w2, w2, &
       w0 &
    /)
    !> Lattice weights

  !> contains the index of the vector pointing in the opposite
  !> direction to c(i).
  integer, parameter :: bounce(nvecs) = &
    (/  2, 1, 4, 3, 6, 5, &
       12,11,14,13, 8, 7, 10, 9, 18,17,16,15, &
       19 &
    /)

  !> contains the index of the vector pointing in the specular-reflection
  !> direction to c(i).

  integer, parameter :: reflect_y(nvecs) = &
    (/  2,  1, 4 , 3,  6 , 5 , &
        8,  7, 14, 13, 12, 11, &
        10, 9, 17, 18, 15, 16, &
        19 &
    /)


  integer, parameter :: nnp = 5 !> size of the `negx`,`negy`, etc arrays

  !> \{
  !> \name Arrays used in invasive flow.
  !>
  !> `negx` contains a list of the indices of each vector which
  !> has a negative X componentl `posy` contains a list of the
  !> indices of each vector which has a positive Y component, etc.
  integer, parameter :: negx(nnp) = (/ 1 , 7 , 8 , 9 , 10 /)
  integer, parameter :: negy(nnp) = (/ 3 , 7 , 11 , 15 , 16 /)
  integer, parameter :: negz(nnp) = (/ 5 , 9 , 13 , 15 , 17 /)
  integer, parameter :: posx(nnp) = (/ 2 , 11 , 12 , 13 , 14 /)
  integer, parameter :: posy(nnp) = (/ 4 , 8 , 12 , 17 , 18 /)
  integer, parameter :: posz(nnp) = (/ 6 , 10 , 14 , 16 , 18 /)
  !> \}

  !> Array containing the lattice vectors.
  !>
  !> contains a vector corresponding to the ith lattice vector, but has
  !> to be initialised in code, since you can't initialise an array of
  !> rank greater than 1.
  integer, save :: c(nvecs,3)
  !> \}

  integer, save :: halo_extent = 1
    !> Depth of halo region in each direction. Enlargement can be requested
    !> via [[request_halo_extent]].

  type halo_request
    !> Holds a request for blowing up the halo
    integer :: extent           !> halo width requested
    character(len=FREETEXT) :: name   !> short human-readable description
  end type halo_request

  type(halo_request), save, allocatable, dimension(:) :: halo_requests
    !> List of halo extension requests

  integer, parameter :: n_lp=8
  !> of surrounding lattice nodes for an arbitrary off-lattice position
  integer, save :: lp_sur(3,n_lp) =reshape([0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1],[3,n_lp])
  !> relative coordinates of these nodes
  integer, save :: opp_lp_sur(3,n_lp) = reshape([1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0],[3,n_lp])
  !> relative coordinates of the opposing nodes


  real(kind=FP), save :: tsize(3)
  !> sizes of total system

  real(kind=FP), save :: maxpos(3)
  !> global maximum simulation space boundaries in all directions

  real(kind=FP), save :: minpos(3)
  !> global minimum simulation space boundaries in all directions

  real(kind=FP), save :: chunksize(3)
  !> sizes of my part

  real(kind=FP), save :: border(2,3)
  !> lo/hi boundaries of my box in each dimension

  integer, parameter :: rad_inner = 1, rad_middle = 2, rad_outer = 3
  !> \{
  !> name indices into the radial model array for nskg_init_radial
  !> \}

  integer, save :: timesteps_count = 0 !> Global count of timesteps performed

  logical, save :: use_nskg_force = .false.
  !> name common external forcing
  !> \{
  !> has to be set to  `.true`. if `nskg_force` should be taken into
  !> account as additional forcing during collision

  !! Name indices for timers. Timers are registered in [[nskg_init_timers]] of [[nskg_timer_class]].
  ! LB timers
  integer, save :: ti_total=-1,ti_adv=-1,ti_intf=-1,ti_intc=-1,ti_halo=-1,ti_inv=-1,ti_dump=-1,ti_fields=-1
  ! Mesh timers: general
  integer, save :: ti_mesh_dump=-1
  integer, save :: ti_mesh_reset_forces=-1
  ! Mesh timers: IBM
  integer, save :: ti_ibm_integration_step=-1, ti_ibm_compute_pair_forces=-1, &
    ti_ibm_compute_and_spread_forces=-1, ti_ibm_compute_vertices_velocity=-1, ti_ibm_exchange_halo=-1, ti_ibm_forward_euler=-1,&
    ti_mesh_geometry_internal_forces=-1
  ! Mesh timers: EBF
  integer, save :: ti_ebf_integration_step=-1
  ! MD timers
  integer, save :: ti_md_compute_pair_forces=-1, ti_md_integration_step=-1, ti_md_io=-1

  !! /name for recording how much red fluid is evaporated
  real(kind=FP), save :: init_tot_mass_r = 0.0
  real(kind=FP), save :: eva_tot_mass_r = 0.0
  real(kind=FP), save :: sum_eva_red = 0.0 !> how much red fluid is evaporated, for lbinit%inv_fluid_name == 'INV_NOZZLE_EVAPORATION_SPHERE'
  real(kind=FP), save :: init_tot_mass_b = 0.0
  real(kind=FP), save :: eva_tot_mass_b = 0.0
  real(kind=FP), save :: init_tot_mass_g = 0.0
  real(kind=FP), save :: eva_tot_mass_g = 0.0

  real(kind=FP), private      :: private_kBT  = -1.
  !> we use a simple getter/setter + private variable
  !> to avoid the "accidental" setting of kBT in the code.
  !> Using `set_kBT()` means understanding all the connected
  !> risks, and the consequences, if the code gets merged
  !> and something goes awry.
  !>
  !> This parameter is fixed by thermodynamic consistency
  !> depending on the equation of state of the fluid, e. g. for simple
  !> LB that's \(kBT = m_P \rho cs^2\), where \(m_P\) is the LB particle mass
  !> and \(\rho\) is the number density.
contains
   function kBT()
     real(kind=FP) :: kBT
     kBT = private_kBT
   end function

   subroutine set_kBT(kBT)
     real(kind=FP),intent(in) :: kBT
     private_kBT = kBT
   end subroutine

subroutine makedatestr(datestr)
  !> Write the current date & time, nicely formatted, into `datestr`
  character(len=*)     :: datestr
  integer,dimension(8) :: datevalues
  call date_and_time(values=datevalues)
  write(datestr,"(I0.4,'-',I0.2,'-',I0.2,X,I0.2,':',I0.2,':',I0.2)") &
    datevalues(1), datevalues(2), datevalues(3), datevalues(5), datevalues(6), datevalues(7)
end subroutine makedatestr

end module nskg_globals_module
