add_library(nskg_chkp
   nskg_io_checkpoint.F90
)

target_link_libraries(nskg_chkp nskg_mesh nskg_elec nskg_md  nskg_old ${MPI_Fortran_LIBRARIES})
target_include_directories( nskg_chkp PUBLIC ${CMAKE_CURRENT_BINARY_DIR}
                                       PRIVATE $<TARGET_PROPERTY:nskg_elec,INTERFACE_INCLUDE_DIRECTORIES>
                                       PRIVATE $<TARGET_PROPERTY:nskg_old,INTERFACE_INCLUDE_DIRECTORIES>
                                               ${MPI_Fortran_MODULE_DIR}
                          )
