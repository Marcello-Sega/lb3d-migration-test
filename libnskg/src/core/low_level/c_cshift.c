#include <stdlib.h>
#include <stdio.h>

void c_cshift_4(float*ptr,float * dest, int x,int y,int z, int dx, int dy, int dz) { 
	int i, tot, delta;

	delta = dx + dy*x + dz*x*y;
        tot = x * y * z;
	if (delta<0){
	   for (i=0;i<tot+delta;i++) dest[i] = ptr[i-delta];
	   return;
	}
	if  (delta>0){
	  for (i=tot-1;i>=delta;i--) dest[i] = ptr[i-delta];
	  return;
	}
        if (delta == 0 && ptr == dest) return;
        if (delta == 0) {
		for (i=0;i<tot;i++) dest[i]=ptr[i];
		return;
        }
}
void c_cshift_8(double*ptr,double* dest, int x,int y,int z, int dx, int dy, int dz) { 
	int i, tot, delta;

	delta = dx + dy*x + dz*x*y;
        tot = x * y * z;
	if (delta<0){
	   for (i=0;i<tot+delta;i++) dest[i] = ptr[i-delta];
	   return;
	}
	if  (delta>0){
	  for (i=tot-1;i>=delta;i--) dest[i] = ptr[i-delta];
	  return;
	}
        if (delta == 0 && ptr == dest) return;
        if (delta == 0) {
		for (i=0;i<tot;i++) dest[i]=ptr[i];
		return;
        }
}

void c_cshift_add_4(float*ptr,float * dest, int x,int y,int z, int dx, int dy, int dz) { 
	int i, tot, delta;

	delta = dx + dy*x + dz*x*y;
        tot = x * y * z;
        if (delta == 0){
		for (i=0;i<tot;i++) dest[i]+=ptr[i];
		return;
        }

	if (delta<0){
	   for (i=0;i<tot+delta;i++) dest[i] += ptr[i-delta];
	   return;
	}
	for (i=0;i<tot-delta;i++) dest[tot-1-i] += ptr[tot-1-delta-i];
}

void c_cshift_add_8(double*ptr,double * dest, int x,int y,int z, int dx, int dy, int dz) { 
	int i, tot, delta;

	delta = dx + dy*x + dz*x*y;
        tot = x * y * z;
        if (delta == 0){
		for (i=0;i<tot;i++) dest[i]+=ptr[i];
		return;
        }

	if (delta<0){
	   for (i=0;i<tot+delta;i++) dest[i] += ptr[i-delta];
	   return;
	}
	for (i=0;i<tot-delta;i++) dest[tot-1-i] += ptr[tot-1-delta-i];
}

void c_cshift_madd_4(float*ptr,float * dest, int x,int y,int z, int dx, int dy, int dz, float alpha) {
       int i, tot, delta;

       delta = dx + dy*x + dz*x*y;
       tot = x * y * z;
       if (delta == 0){
              for (i=0;i<tot;i++) dest[i] += alpha * ptr[i];
              return;
       }

       if (delta<0){
          for (i=0;i<tot+delta;i++) dest[i] += alpha * ptr[i-delta];
          return;
       }
       for (i=0;i<tot-delta;i++) dest[tot-1-i] += alpha * ptr[tot-1-delta-i];
}

void c_cshift_madd_8(double*ptr,double * dest, int x,int y,int z, int dx, int dy, int dz, double alpha) {
       int i, tot, delta;

       delta = dx + dy*x + dz*x*y;
       tot = x * y * z;
       if (delta == 0){
              for (i=0;i<tot;i++) dest[i] += alpha * ptr[i];
              return;
       }

       if (delta<0){
          for (i=0;i<tot+delta;i++) dest[i] += alpha * ptr[i-delta];
          return;
       }
       for (i=0;i<tot-delta;i++) dest[tot-1-i] += alpha * ptr[tot-1-delta-i];
}

void c_cshift_vmadd_4(float*ptr,float * dest, int x,int y,int z, int dx, int dy, int dz, float alpha, float *valpha) {
       int i, tot, delta;

       delta = dx + dy*x + dz*x*y;
       tot = x * y * z;
       if (delta == 0){
              for (i=0;i<tot;i++) dest[i] += alpha * valpha[i] * ptr[i];
              return;
       }

       if (delta<0){
          for (i=0;i<tot+delta;i++) dest[i] += alpha * valpha[i] * ptr[i-delta];
          return;
       }
       for (i=0;i<tot-delta;i++) dest[tot-1-i] += alpha * valpha[tot-1-i] * ptr[tot-1-delta-i];
}

void c_cshift_vmadd_8(double*ptr,double * dest, int x,int y,int z, int dx, int dy, int dz, double alpha, double *valpha) {
       int i, tot, delta;

       delta = dx + dy*x + dz*x*y;
       tot = x * y * z;
          if (delta == 0){
                 for (i=0;i<tot;i++) dest[i] += alpha * valpha[i] * ptr[i];
                 return;
          }
          if (delta<0){
             for (i=0;i<tot+delta;i++) dest[i] += alpha * valpha[i] * ptr[i-delta];
             return;
          }
          if(delta>0) {
            for (i=0;i<tot-delta;i++) dest[tot-1-i] += alpha * valpha[tot-1-i] * ptr[tot-1-delta-i];
            return;
          }
}

