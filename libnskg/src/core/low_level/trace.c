#include <stdio.h>
static int cyg_counter=0;
void __cyg_profile_func_enter(void *this_fn, void *call_site) __attribute__((no_instrument_function));
void __cyg_profile_func_enter(void *this_fn, void *call_site) {printf("entering%*c%p  call site %p\n", cyg_counter,':',this_fn,call_site); cyg_counter+=2;} 

void __cyg_profile_func_exit(void *this_fn, void *call_site) __attribute__((no_instrument_function));
void __cyg_profile_func_exit(void *this_fn, void *call_site) { call_site = NULL ; cyg_counter-=2; printf(" exiting%*c%p  call site %p\n", cyg_counter,':',this_fn, call_site); }

