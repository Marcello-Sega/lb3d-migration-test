#include <stdio.h>
#include <immintrin.h>

void * c_mm_malloc (size_t __size, size_t __alignment){
        return _mm_malloc(__size, __alignment);
}

void c_mm_free (void *__ptr) {
        return _mm_free(__ptr);
}

