#include <math.h>

#ifdef SINGLE_PRECISION
typedef float creal;
#define _exp(x) expf((x))
#define _expm1(x) expm1f((x))
#else
typedef double creal;
#define _exp(x) (exp(x))
#define _expm1(x) (expm1(x))
#endif

//> gcc manages to produce faster code (~3x) than MKL's vdExp
//> when compiling with -Ofast.
void c_exp(creal * source, creal * dest,int n) {
	// returns exp(x)
	int i;
	for (i=0;i<n;i++) dest[i] = _exp(source[i]);
}
//> this is not the case as of this commit, for expm1
void c_expm1(creal * source, creal * dest,int n) {
	// returns exp(x)-1
	int i;
	for (i=0;i<n;i++) dest[i] = _expm1(source[i]);
}

//> since expm1 is not that much optimized,  we compute
//> psi through exp.  There should be no big problems of
//> accuracy, but we might want to test this at some stage
void c_psi(creal * source, creal * dest,int n) {
	// returns 1-exp(-x)
	int i;
	for (i=0;i<n;i++) dest[i] = 1.0-_exp(-source[i]);
}
void c_psi_inplace(creal * dest ,int n) {
	// returns 1-exp(-x)
	int i;
	for (i=0;i<n;i++) dest[i] = 1.0-_exp(-dest[i]);
}

