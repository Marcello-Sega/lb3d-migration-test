#include "nskg.h"
module nskg_geometry_class
!> Container to access geometry information of the LB lattice


   use nskg_common_headers_macro
   use nskg_globals_module
   implicit none
   private

   type nskg_geometry
      integer, dimension(3)                      :: size
      integer, dimension(3)                      :: width
      integer                                    :: sites
        !> Count of lattice points
      integer                                    :: he
        !> Halo extent
      integer                                    :: nvecs
      integer                                    :: ncomponents
      type(MPI_Comm),pointer                     :: comm
      integer,dimension(-1:1,-1:1,-1:1)          :: sense
      real(kind=FP), allocatable, dimension(:,:) :: cg
        !> Set of basis vectors for D3Q19 populations
      real(kind=FP), allocatable, dimension(:)   :: w
        !> Set of weights of the D3Q19 populations
   contains
      procedure, pass(this) :: init => init_geometry
      procedure, pass(this) :: destruct => destruct_geometry
      procedure, pass(this) :: indices
      procedure, pass(this) :: offset_ ! TODO MS handle this
   end type
   public nskg_geometry
contains

   subroutine init_geometry(this,size, he,nvecs,ncomponents,comm)
      class(nskg_geometry) :: this
      integer, dimension(3), intent(in) :: size
      integer, intent(in)  :: he, nvecs
      integer, intent(in),optional  :: ncomponents
      type(MPI_Comm),intent(inout), target,optional :: comm
      integer i
      this%size = size
      this%he = he
      this%nvecs = nvecs
      this%width = this%size + 2*he
      this%sites = product(this%width)
      if (present(comm)) this%comm => comm
      if (present(ncomponents)) then
            this%ncomponents  = ncomponents
      end if
      allocate(this%cg(nvecs,3))
      allocate(this%w(nvecs))
      this%w = w
      do i = 1, nvecs
        this%cg(i,:) = (/ cx(i), cy(i), cz(i) /)
      end do
   end subroutine

   subroutine destruct_geometry(this)
      class(nskg_geometry) :: this
      if (allocated(this%cg)) deallocate(this%cg)
      if (allocated(this%w)) deallocate(this%w)
   end subroutine destruct_geometry

   function offset_(this,x,y,z,coords)  result(off)
      class(nskg_geometry) :: this
      integer off
      integer d,nx,ny,nz,xx,yy,zz
      integer, optional :: x,y,z
      integer, dimension(3), optional :: coords
      xx=0
      yy=0
      zz=0
      if(present(coords)) then
         xx = coords(1)
         yy = coords(2)
         zz = coords(3)
      elseif(present(x) .and. present(y) .and. present(z)) then
         xx = x
         yy = y
         zz = z
      else
         error stop 'offset_ called without passing either (integer:: x, integer:: y,integer:: z) or integer(3):: coords'
      end if

      nx = this%size(1)
      ny = this%size(2)
      nz = this%size(3)
      d =  this%he
      off= 1 + (xx-1+d) + (nx + 2*d)*(yy-1+d) + (nx + 2*d)*(ny + 2*d)*(zz-1+d)
   end function

   function indices(this,off) result(coords)
      class(nskg_geometry) :: this
      integer, dimension(3) :: coords
      integer off,doff,x,y,z,he
      he = this%he
      doff = off -1
      x = mod(doff, this%width(1)) +1 - he
      y = mod(int (doff/this%width(1)),this%width(2)) +1 -he
      z = int(doff/( this%width(1) * this%width(2)))  +1 -he
      coords = [x,y,z]
   end function

end module
