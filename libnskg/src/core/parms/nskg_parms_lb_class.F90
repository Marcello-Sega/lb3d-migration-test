#include "nskg.h"
module nskg_parms_lb_class
!> Configuration of Lattice-Boltzmann fluids (collision model, snapshot intervals). This
!> interface is inherited by more complex fluid models like Shan-Chen or Colour Gradient.

   use nskg_common_headers_macro
   use nskg_parms_class
!   use nskg_core_helper
   use nskg_features_class
   use nskg_log_module
   use nskg_globals_module
   use nskg_io_interface, only: nskg_try_open_namelist
   !use nskg_parms_module !,only: fr,fb,fg, MRT_id, collisiontype_id
   implicit none
   private
   type,extends(nskg_parms)  :: nskg_parms_lb
      !> Parameters for the setup of a Lattice-Boltzmann lattice
      integer               :: ncomponents    = 0
      character(len=STRCONST) :: forcing_model = 'guo'
        !> Type of forcing to use in the NSKG.
      real(kind=FP),dimension(MAX_COMPONENTS) :: amass = 1.0
        !> LB particle mass. By default -1: will be set to be consistent with the
        !> temperature parameter in `/system/` and the equation of state of the LB
        !> model. E. g. in single component LB, `kBT = amass * rho * cs^2`
      logical               :: fluctuating = .false.
        !> Whether to switch on fluctuation in the LB model. The temperature
        !> is chosen by `SYST%kBT` and is (thermodynamically) consistent
        !> both with the LB method (through its equation of state) and all
        !> particle coupling methods.
      logical               :: dump_single_precision = .false.
        !> If set to `.true.`, writes HDF5 data at single precision, regardless
        !> of the internal float precision.
      integer               :: dump_n_start = 0
        !> Timestep before which no lb-related quantity is dumped to disk
      integer               :: dump_n_temp  = 0
        !> Dumping frequency of the temperature
      integer               :: dump_n_density(MAX_COMPONENTS) = 0
        !> Dumping frequency of the density
      integer,dimension(3)  :: dump_n_velcomp   = 0
        !> Dumping frequency of the velocity components
      integer               :: dump_n_velocity= 0
        !> Dumping frequency of the velocity field
      integer               :: dump_n_forces(MAX_COMPONENTS)  = 0
        !> Dumping frequency of the force field acting on each component
      integer               :: dump_n_rock  = 0
        !> Dumping frequency of the rock field (colloid and walls)
      integer               :: dump_n_pressure = 0
        !> Dumping frequency of the pressure
      integer               :: dump_n_stress_xz_profile  = 0 
        !> Dumping frequency of the stress
      integer               :: dump_n_stress  = 0
        !> Dumping frequency of the stress
      integer               :: dump_n_colour_clusters  = 0
        !> Interval between dumping the Hoshen-Kopelman cluster sizes.
      integer               :: dump_n_colour_clusters_index  = 0
        !> Interval between dumping the Hoshen-Kopelman cluster index field.
      integer               :: dump_n_start_colour_clusters  = 0
        !> Time step from which to start writing Hoshen-Kopelman data (for
        !> random mixtures this can be unstable for low timesteps).
      integer               :: dump_stress_plane_normal  = 1
        !> TODO to describe
      integer               :: dump_stress_momentum_direction = 3
        !> TODO to describe
      integer               :: dump_stress_plane_min = 1
        !> When set to a positive value, set a cutoff in \(x\)-direction for stress calculation.
      integer               :: dump_stress_plane_max = -1 
        !> When set to a positive value, set a cutoff in \(x\)-direction for stress calculation.

   contains
      procedure, nopass     :: read_namelist => read_namelist_lb
      procedure, nopass     :: log_parms=> log_parms_lb
   end type

   type(nskg_parms_lb),target :: lb = nskg_parms_lb(&
      shortname='lb',longname             ='Lattice Boltzmann')


   namelist /LATTICE_BOLTZMANN/ lb
   public nskg_parms_lb, lb

contains

   subroutine read_namelist_lb()
     if (nskg_try_open_namelist(unit=42, file=features%lattice_boltzmann%inp_file, status = 'UNKNOWN')) then
       read_catch_namelist_error_macro(42,lattice_boltzmann,'lattice_boltzmann')
       close(unit=42)
     endif
   end subroutine

   subroutine log_parms_lb()
     log_parms_template(lb,lattice_boltzmann)
   end subroutine

end module   
