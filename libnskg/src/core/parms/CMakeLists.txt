add_library(nskg_parms nskg_parms_class.F90)
add_library(nskg_parms_lb nskg_parms_lb_class.F90)
add_library(nskg_parms_system nskg_parms_system_class.F90)

target_link_libraries(nskg_parms nskg_globals ${MPI_Fortran_LIBRARIES})
target_link_libraries(nskg_parms_lb nskg_features nskg_globals nskg_parms nskg_io)
target_link_libraries(nskg_parms_system nskg_features nskg_globals nskg_parms)

target_include_directories( nskg_parms PUBLIC ${CMAKE_CURRENT_BINARY_DIR} ${MPI_Fortran_MODULE_DIR})
target_include_directories( nskg_parms_system PUBLIC ${CMAKE_CURRENT_BINARY_DIR} ${MPI_Fortran_MODULE_DIR})
if(LIKWID_WORKS)
        target_include_directories(nskg_parms PRIVATE ${LIKWID_INCLUDE_DIRS})
        target_include_directories(nskg_parms_system PRIVATE ${LIKWID_INCLUDE_DIRS})
endif()
