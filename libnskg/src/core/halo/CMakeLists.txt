add_library(nskg_halo
   nskg_halo_class.F90
)
target_link_libraries(nskg_halo nskg_geometry nskg_globals ${MPI_Fortran_LIBRARIES})

add_definitions(${HDF5_Fortran_DEFINITIONS})

target_include_directories(nskg_halo PUBLIC ${CMAKE_CURRENT_BINARY_DIR}
    PRIVATE $<TARGET_PROPERTY:nskg_features,INTERFACE_INCLUDE_DIRECTORIES>
            ${HDF5_INCLUDE_DIRS}
            ${MPI_Fortran_MODULE_DIR}
)

