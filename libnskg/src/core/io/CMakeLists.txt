add_library(nskg_io
   nskg_io_interface.F90
   nskg_io_helper.F90
   nskg_io_hdf5.F90
   nskg_io_xdrf.F90
)
target_link_libraries(nskg_io nskg_core_helper nskg_features nskg_parms_system)
target_link_libraries(nskg_io ${HDF5_Fortran_LIBRARIES} xdrf)
target_link_libraries(nskg_io ${MPI_Fortran_LIBRARIES})

add_definitions(${HDF5_Fortran_DEFINITIONS})

target_include_directories(nskg_io PUBLIC ${CMAKE_CURRENT_BINARY_DIR}
    PRIVATE $<TARGET_PROPERTY:nskg_features,INTERFACE_INCLUDE_DIRECTORIES>
            ${HDF5_INCLUDE_DIRS}
            ${MPI_Fortran_MODULE_DIR}
)

