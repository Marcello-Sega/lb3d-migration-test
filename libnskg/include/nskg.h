!> Global static definitions used throughout the source code.

!> Floating-point precision (translation from CMake flags to usable Fortran)
#ifdef SINGLE_PRECISION
#define NSKG_REAL MPI_REAL4
#define c_real c_float
#define FP 4
#define FPD 4
#else
#define NSKG_REAL MPI_REAL8
#define c_real c_double
#define FP 8
#define FPD 8
#endif


!> Various debug flags
#ifdef DEBUG_MPI
#define DEBUG_MPI_MSG(msg) call log_mpi(msg)
#define DEBUG_CHECKMPI(ierror,msg) call checkmpi(ierror,msg)
#else
#define DEBUG_MPI_MSG(msg)
#define DEBUG_CHECKMPI(ierror,msg)
#endif

#ifdef DEBUG
#define DEBUG_MSG(msg) call log_msg(msg)
#define DEBUG_MSG_ALL(msg) call log_msg(msg,.true.)
#define DEBUG_BARRIER(msg) call log_msg(msg,.true.) ; call MPI_Barrier(comm_cart) ; call log_msg("POST BARRIER", .true.)
#else
#define DEBUG_MSG(msg)
#define DEBUG_MSG_ALL(msg)
#define DEBUG_BARRIER(msg)
#endif


!> ABnormal ENDing (wrapper macro to enrich fatal ending with some clues of where things broke)
#ifndef Abend
#define Abend() Abend_fun(__FILE__,__LINE__)
#endif


!> File I/O status codes, toolchain-agnostic
#define READ_OK 0
#if __GFORTRAN__
#define READ_EOF -1
#elif __INTEL_COMPILER
#define READ_EOF 613
#endif


!> Lattice iteration chunk size
#define _unroll_ 128


!> Convenient helpers to compactify F03 OOP boilerplate
#define select_type(o,cl) ; select type(o); type is(cl)
#define select_class(o,cl) ; select type(o); class is(cl)
#define select_end end select

#define select_multiple_type(o1,o2,cl)   select type(o1); type is(cl)  ; select type(o2) ; type is (cl)
#define select_multiple_class(o1,o2,cl)  select type(o1); class is(cl) ; select type(o2) ; class is (cl)
#define select_multiple_types(o1,o2,cl1,cl2)   select type(o1); type is(cl1)  ; select type(o2) ; type is (cl2)
#define select_multiple_classes(o1,o2,cl1,cl2)  select type(o1); class is(cl1) ; select type(o2) ; class is (cl2)
#define select_multiple_end  end select; end select

#define log_parms_template(classname,namelist)  if(myrankc.eq.0) then ; tmpstr= "" ; write(tmpstr,nml=namelist) ; call classname%log_format_strings(tmpstr)  ; endif

#define read_catch_namelist_error_macro(unitid,namelist,namelistname) read (unit=unitid,nml=namelist,iostat=iostat,iomsg=iomsg) ; select case(iostat) ; case(READ_OK) ; case(READ_EOF) ; return ; case default ; call log_msg("problem reading "//namelistname//" : "//trim(iomsg)) ; error stop "error reading namelist" ; end select


!> One-liner usage of nskg_timer_module
#define TIMEIT(TIMER,COMMANDS) call start_timer(TIMER) ; COMMANDS ; call stop_timer(TIMER)

!> Maximum number of mesh objects to load.
#define IBM_MAX_MESHES 32
#define EBF_MAX_MESHES 32

!> Maximum number of interactions that each offlattice object can simultaneously handle. For example, an MD particle can interact via LJ with other particles and via Morse with walls.
#define MAX_TYPES 6

!> DEBUG: transitional preprocessor variable definition. (The compiler is sad when we leave USEXDRF undefined)
#define USEXDRF 1

!> String lengths
#define FILEPART    64
                           ! Parts of file names (prefixes, suffixes, tags etc.)
#define SUFFIX       3
                           ! File format/extension
#define FILENAME  1024
                           ! A file name
#define STRCONST    64
                           ! String constants (choice options etc.)
#define MESSAGE    256
                           ! Message (log file, CLI)
#define CLIARG     256
                           ! Command line argument
#define FREETEXT  1024
                           ! What doesn't fit anywhere else
#define RESTORE     32
                           ! Format for checkpoint identifiers
#define HDF5MSG     80
                           ! Line length for HDF5 Metadata

!> Highest velocity for offlattice objects which we feel comfortable with
#define MAX_VELOCITY 0.5
!> Highest acceleration for offlattice objects which we feel comfortable with
#define MAX_ACCELERATION 0.1


!> `use` statements common to all source files. (Note that the swallowing of the first `use` is an abuse of the preprocessor macro semantics, for the sake of source aesthetics and syntax highlighting.)
#ifdef USE_LIKWID
#define nskg_common_headers_macro likwid ; use mpi_f08
#else
#define nskg_common_headers_macro              mpi_f08
#endif
