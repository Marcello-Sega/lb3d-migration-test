# Decide how we call the local version of 3NSKOG (currently from Git; TODO What to do in case of a shallow clone or a tarball distribution?), and generate nskg_version.h to let regular source code use this information.

if(NOT NSKG_PLATFORM)
  set(NSKG_PLATFORM ${CMAKE_SYSTEM})
endif(NOT NSKG_PLATFORM)
message(STATUS "3nskog platform descriptor: \"${NSKG_PLATFORM}\"")

set(GIT_DESC "0-0-g000000000")               # Unique identifier of the current revision (LastTag-CommitsSinceLastTag-HexHash[|-dirty])
set(GIT_LOCALCHANGES " (git unavailable)")   # Bad words in case of uncommited changes
set(GIT_BRANCH "nobranch")                   # Current branch
set(GIT_DIFFCODE "NaN")                      # Exit status of `git diff --quiet`

find_package(Git)
if(Git_FOUND)
  #> Version descriptor
  execute_process(COMMAND ${GIT_EXECUTABLE} describe --dirty --tags --always
                  OUTPUT_VARIABLE TMP
                  OUTPUT_STRIP_TRAILING_WHITESPACE
		  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} RESULT_VARIABLE STATUS)
  if(NOT STATUS OR STATUS EQUAL 0)
	set(GIT_DESC "${TMP}")
  else()
        message(WARNING "Retrieval of version identifier: Version descriptor exited with non-zero status ${STATUS}. Result: \"${GIT_DESC}\" from command \"${GIT_EXECUTABLE} describe --dirty --tags --always\"")
  endif()

  #> Branch descriptor
  execute_process(COMMAND ${GIT_EXECUTABLE} rev-parse --abbrev-ref HEAD
                  OUTPUT_VARIABLE TMP
                  OUTPUT_STRIP_TRAILING_WHITESPACE
                  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} RESULT_VARIABLE STATUS)
  if(NOT STATUS OR STATUS EQUAL 0)
	set(GIT_BRANCH "${TMP}")
  else()
        message(WARNING "Retrieval of version identifier: Branch descriptor exited with non-zero status ${STATUS}. Result: \"${GIT_BRANCH}\" from command \"${GIT_EXECUTABLE} rev-parse --abbrev-ref HEAD\"")
  endif()

  #> Test for uncommitted changes
  execute_process(COMMAND ${GIT_EXECUTABLE} diff --quiet
                  OUTPUT_STRIP_TRAILING_WHITESPACE
                  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} RESULT_VARIABLE STATUS)
  set(GIT_DIFFCODE "${STATUS}")
  if(NOT GIT_DIFFCODE STREQUAL "0")
        message(WARNING "Retrieval of version identifier: Do you have un-commited changes? Working directory tidiness test exited with non-zero status \"${GIT_DIFFCODE}\" from command \"${GIT_EXECUTABLE} diff --quiet\"")
  endif()

  if(GIT_DIFFCODE STREQUAL "1")
    set(GIT_LOCALCHANGES " (with local changes)")
  elseif(GIT_DIFFCODE STREQUAL "0")
    set(GIT_LOCALCHANGES " (clean working directory)")
  else()
    set(GIT_LOCALCHANGES " (unknown diff return code ${GIT_DIFFCODE})")
  endif()
else()
  message(WARNING "Git unavailable. Falling back to stupid defaults for version identification.")
endif()

message(STATUS "version descriptor: \"${GIT_DESC}\" on branch \"${GIT_BRANCH}${GIT_LOCALCHANGES}\"")

# Some passthrough variable boilerplate is needed here, since CMake variables appear to not be accessible within configurable files directly.
set(NSKG_FC "${CMAKE_Fortran_COMPILER}")
set(NSKG_CC "${CMAKE_C_COMPILER}")

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/cmake/nskg_version.h.in
               ${CMAKE_BINARY_DIR}/include/nskg_version.h)
